package noman.handyprotaskerapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import Dialog.PkDialog;
import Map.GPSTracker;
import noman.handyprotaskerapp.Pojo.acceptimage;
import at.grabner.circleprogress.CircleProgressView;
import at.grabner.circleprogress.TextMode;
import de.hdodenhof.circleimageview.CircleImageView;
import noman.handyprotaskerapp.Adapter.AcceptImageAdapter;
import noman.handyprotaskerapp.activity.MyJobs_OnGoingDetailActivity;
import service.ServiceConstant;
import volley.ServiceRequest;

public class newjobrequesttimer extends AppCompatActivity {
    //LinearLayout recyclerView;
    String value;
    ArrayList<String> jobids = new ArrayList<String>();
    // ArrayList<RowData> arrayList;
    //RecyclerViewAdapter adapter;
    LinearLayout layout_back_ongoingback, nonewjob;
    public int count;
    private RecyclerView recyclerView;
    public CircleProgressView circularProgressBar;
    LinearLayout accepttext, reject;
    public TextView categorytext, addresstext, usernametext, bookingdatetext, tips, whatsimportant;
    private LinearLayout Ll_ride_Requst_layout;
    CircleImageView imag;
    public String bookingdata, category, address, username, userimage, Job_id, tipsv, descriptionva;
    // private ContinuousRequestAdapter continuousRequestAdapter;
    private Handler mHandler = new Handler();
    public CircularHandler ch;
    String storejobid;
    ArrayList<CircularHandler> arrobj = new ArrayList<CircularHandler>();
    private ProgressDialog1 myDialog;
    int loadertime = 100;

    String differaccept="0";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newjobrequesttimer);


        value = getIntent().getExtras().getString("valuefortimer");

        layout_back_ongoingback = (LinearLayout) findViewById(R.id.layout_back_ongoingback);
        nonewjob = (LinearLayout) findViewById(R.id.nonewjob);
        //arrayList = new ArrayList<>();

        myDialog = new ProgressDialog1(newjobrequesttimer.this);
        myDialog.setCancelable(false);
        myDialog.setCanceledOnTouchOutside(false);

        circularProgressBar = (CircleProgressView) findViewById(R.id.timer_circleView);
        Ll_ride_Requst_layout = (LinearLayout) findViewById(R.id.Ll_ride_request_layout);


        /*circularProgressBar.setEnabled(false);
        circularProgressBar.setFocusable(false);
        circularProgressBar.setMaxValue(100);
        circularProgressBar.setValueAnimated(0);
        circularProgressBar.setAutoTextSize(true);
        circularProgressBar.setTextScale(0.6f);*/

        circularProgressBar.setFocusable(false);

        circularProgressBar.setValueAnimated(0);
        circularProgressBar.setTextSize(30);
        circularProgressBar.setAutoTextSize(true);
        circularProgressBar.setTextScale(2.1f);
        circularProgressBar.setTextColor(getResources().getColor(R.color.white));


        categorytext = (TextView) findViewById(R.id.category);
        addresstext = (TextView) findViewById(R.id.address);
        usernametext = (TextView) findViewById(R.id.username);
        bookingdatetext = (TextView) findViewById(R.id.bookingdate);
        accepttext = (LinearLayout) findViewById(R.id.accept);
        reject = (LinearLayout) findViewById(R.id.reject);
        imag = (CircleImageView) findViewById(R.id.imag);
        tips = (TextView) findViewById(R.id.tips);
        whatsimportant = (TextView) findViewById(R.id.whatsimportant);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);


        accepttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if(differaccept.equals("1"))
                {

                    SharedPreferences prefjobid = newjobrequesttimer.this.getSharedPreferences("ongoingpage", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", storejobid);
                    prefeditor.apply();
                    prefeditor.commit();

                    Intent intent = new Intent(newjobrequesttimer.this, MyJobs_OnGoingDetailActivity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
                else
                {
                    LocationManager lm = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
                    boolean gps_enabled = false;
                    boolean network_enabled = false;
                    try {
                        gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    } catch (Exception ex) {
                    }

                    try {
                        network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                    } catch (Exception ex) {
                    }

                    if (!gps_enabled && !network_enabled) {
                        // notify user
                        final PkDialog mdialog = new PkDialog(newjobrequesttimer.this);
                        mdialog.setDialogTitle(getResources().getString(R.string.home_location_header_label));
                        mdialog.setDialogMessage(getResources().getString(R.string.turn_on_location));
                        mdialog.setCancelOnTouchOutside(false);
                        mdialog.setPositiveButton(
                                getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mdialog.dismiss();
                                    }
                                }
                        );
                        mdialog.show();
                    } else {


                        GPSTracker gps = new GPSTracker(getApplicationContext());
                        if (gps != null && gps.canGetLocation()) {
                            double Dlatitude = gps.getLatitude();
                            double Dlongitude = gps.getLongitude();

                            if (!myDialog.isShowing()) {
                                myDialog.show();
                            }

                            buttonsClickActions(newjobrequesttimer.this, ServiceConstant.ACCEPT_JOB_URL, storejobid, Dlatitude, Dlongitude);
                        } else {
                            final PkDialog mdialog = new PkDialog(getApplicationContext());
                            mdialog.setDialogTitle(getResources().getString(R.string.home_location_header_label));
                            mdialog.setDialogMessage(getResources().getString(R.string.turn_on_location));
                            mdialog.setCancelOnTouchOutside(false);
                            mdialog.setPositiveButton(
                                    getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mdialog.dismiss();
                                        }
                                    }
                            );
                            mdialog.show();
                        }


                    }
                }



            }
        });

        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //myDialog.show();

                Ll_ride_Requst_layout.setVisibility(View.GONE);
                Intent intent = new Intent(getApplicationContext(), Navigationdrawer.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        });


        layout_back_ongoingback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Navigationdrawer.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        //recycler view
        //  recyclerView = (LinearLayout) findViewById(R.id.button);
      /*  recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());*/




        //  continuousRequestAdapter = new ContinuousRequestAdapter(newjobrequesttimer.this, recyclerView);


        String action = "";
        try {
            JSONObject json1 = new JSONObject(value);
            JSONObject object1 = json1.getJSONObject("data");


            System.out.println("testdata--->" + value);

            if (object1.has("key0")) {
                action = object1.getString("action");
                if (action.contains("job_request_now")) {

                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    String date = df.format(Calendar.getInstance().getTime());
                    System.out.println("date--->" + date);

                    SimpleDateFormat dftime = new SimpleDateFormat("HH:mm:ss");
                    String time = dftime.format(Calendar.getInstance().getTime());


                    if (object1.has("key2")) {
                        if (object1.getString("key2").equals("") || object1.getString("key2").equals(" ") || object1.getString("key2") == null) {

                        } else {
                            int converttosecond = Integer.parseInt(object1.getString("key2"));
                            loadertime = converttosecond * 60;
                            circularProgressBar.setMaxValue(loadertime);
                        }
                    } else {

                    }

                    if (object1.has("key13"))
                    {
                        if(object1.getString("key13").equalsIgnoreCase("no"))
                        {
                            differaccept="1";
                        }
                        else
                        {

                        }

                    }



                    if (date.equalsIgnoreCase(object1.getString("key3"))) {
                        nonewjob.setVisibility(View.GONE);
                        Ll_ride_Requst_layout.setVisibility(View.VISIBLE);
                        if (jobids.size() == 0) {
                            System.out.println("Sara response full--->" + value);
                            String Job_id;
                            JSONArray imagearray = null;
                            String bookingdate, category = "", address, username, userimage;
                            try {
                                if (object1.has("key0")) {

                                    if (object1.has("key10")) {
                                        imagearray = object1.getJSONArray("key10");
                                    } else {
                                        imagearray = new JSONArray("[]");
                                    }

                                    Job_id = object1.getString("key0");
                                    bookingdate = object1.getString("key4");


                                    if (object1.has("key8")) {
                                        address = object1.getString("key8");
                                    } else {
                                        address = getResources().getString(R.string.no_address_available_in_loc);
                                    }

                                    username = object1.getString("key6");
                                    userimage = object1.getString("key7");

                                    String instuction = object1.getString("key11");
                                    String description = object1.getString("key12");

                                    JSONArray service_type = object1.getJSONArray("key9");
                                    for (int b = 0; b < service_type.length(); b++) {
                                        String valsue = "" + service_type.getString(b);
                                        category += valsue + ",";
                                    }
                                    if (category.endsWith(",")) {
                                        category = category.substring(0, category.length() - 1);
                                    }

                                    if (userimage.contains("http:")) {
                                    } else {
                                        userimage = ServiceConstant.BASE_URLIMAGE + userimage;
                                    }
                                    if (jobids.contains(Job_id)) {
                                    } else {
                                        storejobid = Job_id;
                                        jobids.add(Job_id);
                                        //recyclerView.addView(continuousRequestAdapter.getView(1, "JOB ID:"+Job_id+"\n"+bookingdate,category,address,username,userimage,Job_id,imagearray,instuction,description));

                                        List<acceptimage> movieList = new ArrayList<>();
                                        movieList.clear();


                                        for (int b = 0; b < imagearray.length(); b++) {
                                            try {
                                                String imagenames = "" + imagearray.getString(b);
                                                if (imagenames.contains("https://") || imagenames.contains("http://")) {

                                                    acceptimage movie = new acceptimage(imagenames);
                                                    movieList.add(movie);
                                                } else {
                                                    acceptimage movie = new acceptimage(ServiceConstant.BASE_URLIMAGE + imagenames);
                                                    movieList.add(movie);

                                                }

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }


                                        if (imagearray.length() > 0) {

                                            LinearLayoutManager layoutManager
                                                    = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                                            AcceptImageAdapter mAdapter = new AcceptImageAdapter(movieList, getApplicationContext());
                                            recyclerView.setLayoutManager(layoutManager);
                                            recyclerView.setAdapter(mAdapter);
                                            recyclerView.setVisibility(View.VISIBLE);
                                        } else {
                                            recyclerView.setVisibility(View.GONE);
                                        }


                                        categorytext.setText(category);
                                        addresstext.setText(address);
                                        usernametext.setText(username);
                                        bookingdatetext.setText("JOB ID:" + Job_id + "\n" + bookingdate);

                                        tips.setText(instuction);
                                        whatsimportant.setText(description);

                                        Picasso.with(getApplicationContext()).load(userimage)
                                                .placeholder(R.drawable.ic_no_user)
                                                .into(imag);


                                        ch = new CircularHandler(loadertime, 0);
                                        mHandler.post(ch);
                                        arrobj.add(ch);


                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }


                    } else {
                        Ll_ride_Requst_layout.setVisibility(View.GONE);
                        nonewjob.setVisibility(View.VISIBLE);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {

        super.onPause();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }


    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
            Intent intent = new Intent(getApplicationContext(), Navigationdrawer.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("EXIT", true);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }


    private class CircularHandler implements Runnable {

        Integer value;
        int pos;
        boolean isRunning;

        public CircularHandler(Integer val, int i) {

            isRunning = true;
            value = val;
            pos = i;
        }

        @Override
        public void run() {
            if (isRunning) {
                value = value - 1;
                circularProgressBar.setText(String.valueOf(Math.abs(value)));
                circularProgressBar.setTextMode(TextMode.TEXT);
                circularProgressBar.setValueAnimated(value, 500);
                mHandler.postDelayed(this, 1000);
                System.out.println("counter-----jai--------------" + value);
                if (value == 0) {
                    Ll_ride_Requst_layout.setVisibility(View.GONE);
                }
            }
        }
    }


    private void buttonsClickActions(final Context mContext, String url, final String Jobid, Double dlat, Double dlong) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();

        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", 0);
        String provider_id = pref.getString("provider_id", "");

        jsonParams.put("provider_id", provider_id);
        jsonParams.put("job_id", Jobid);
        jsonParams.put("provider_lat", "" + dlat);
        jsonParams.put("provider_lon", "" + dlong);


        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("clickresponse------", response);

                String Str_status = "", Str_message = "";
                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");
                    Str_message = jobject.getString("response");


                    if (Str_status.equalsIgnoreCase("1")) {

                        try {
                            Ll_ride_Requst_layout.setVisibility(View.GONE);

                        } catch (Exception e) {
                        }


                        final PkDialog mdialog = new PkDialog(newjobrequesttimer.this);
                        mdialog.setDialogTitle(getResources().getString(R.string.success_label));
                        mdialog.setDialogMessage(getResources().getString(R.string.action_job_accepted_successfully));
                        mdialog.setPositiveButton(getResources().getString(R.string.ok_label), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mdialog.dismiss();

                                        SharedPreferences prefjobid = newjobrequesttimer.this.getSharedPreferences("ongoingpage", 0);
                                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                                        prefeditor.putString("jobid", Jobid);
                                        prefeditor.apply();
                                        prefeditor.commit();

                                        Intent intent = new Intent(newjobrequesttimer.this, MyJobs_OnGoingDetailActivity.class);
                                        startActivity(intent);
                                        finish();
                                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                                    }
                                }
                        );
                        mdialog.show();

                    } else {

                        try {
                            Ll_ride_Requst_layout.setVisibility(View.GONE);
                            /*Intent intent = new Intent(getApplicationContext(), Navigationdrawer.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("EXIT", true);
                            startActivity(intent);
                            finish();*/
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                        } catch (Exception e) {
                        }


                        final PkDialog mdialog = new PkDialog(newjobrequesttimer.this);
                        mdialog.setDialogTitle(getResources().getString(R.string.failed_label));
                        mdialog.setDialogMessage(Str_message);
                        mdialog.setPositiveButton(getResources().getString(R.string.ok_label), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mdialog.dismiss();
                                    }
                                }
                        );
                        mdialog.show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }


            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }


            }
        });
    }


}
