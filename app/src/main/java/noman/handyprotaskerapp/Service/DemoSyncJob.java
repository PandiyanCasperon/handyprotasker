package noman.handyprotaskerapp.Service;

import android.content.Intent;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import android.util.Log;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;

import fcm.MyFirebaseMessagingService;
import socket.ChatMessageService;
import socket.SocketHandler;

/**
 * Created by user145 on 6/23/2018.
 */
public class DemoSyncJob extends Job {

    public static final String TAG = ">>>> job_demo_tag";

    @Override
    @NonNull
    protected Result onRunJob(@NonNull Params params) {
        // run your job here

        SharedPreferences pref = this.getContext().getSharedPreferences("logindetails", 0);
        String provider_id = pref.getString("provider_id", "0");
        Log.d(TAG, "onRunJob: " + provider_id);

        if (provider_id.equals("0")) {
            Intent myService = new Intent(this.getContext(), ChatMessageService.class);
            this.getContext().stopService(myService);
        } else {

            try {
                SocketHandler.getInstance(this.getContext()).getSocketManager().connect();
            } catch (Exception e) {
                e.printStackTrace();
            }


            try {
                Intent intent1 = new Intent(this.getContext(), ChatMessageService.class);
                this.getContext().startService(intent1);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                Intent intent1 = new Intent(this.getContext(), MyFirebaseMessagingService.class);
                this.getContext().startService(intent1);
            } catch (Exception e) {
                e.printStackTrace();
            }

            scheduleJob();
        }


        return Job.Result.SUCCESS;
    }

    public static void scheduleJob() {
        new JobRequest.Builder(DemoSyncJob.TAG)
                .setExecutionWindow(4_000L, 4_000L)
                .setUpdateCurrent(true)
                .build()
                .schedule();
    }
}
