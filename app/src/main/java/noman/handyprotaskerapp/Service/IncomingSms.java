package noman.handyprotaskerapp.Service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import noman.handyprotaskerapp.newjobrequesttimer;


/**
 * Created by user127 on 21-02-2018.
 */

public class IncomingSms extends BroadcastReceiver {


    public void onReceive(Context context, Intent intent) {


        String actionnew = "";

        SharedPreferences pref2 = context.getApplicationContext().getSharedPreferences("notificationdata", 0);
        String value = pref2.getString("value", "");

        System.out.println("Sara response--->" + value);

        try {

            JSONObject json = new JSONObject(value);
            JSONObject object1 = json.getJSONObject("data");
            if (object1.has("key0")) {

                actionnew = object1.getString("action");


                if (actionnew.equalsIgnoreCase("job_request_now")) {


                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    String date = df.format(Calendar.getInstance().getTime());
                    if (date.equalsIgnoreCase(object1.getString("key3"))) {
                        Intent i = new Intent(context, newjobrequesttimer.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                     /*   SimpleDateFormat dftime = new SimpleDateFormat("HH:mm:ss");
                        String time = dftime.format(Calendar.getInstance().getTime());

                        String pattern = "HH:mm:ss";
                        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

                        try {
                            Date date1 = sdf.parse(time);
                            Date date2 = sdf.parse(object1.getString("key5"));

                            if(date1.before(date2))
                            {
                                Intent i = new Intent(context, newjobrequesttimer.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(i);
                            }
                        }
                        catch (ParseException e)
                        {
                            e.printStackTrace();
                        }*/


                    } else {

                    }

                }


                //  Toast.makeText(context,""+downtext,Toast.LENGTH_LONG).show();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }




        /*SharedPreferences pref1 = context.getApplicationContext().getSharedPreferences("openclosed", 0);
        String status=pref1.getString("status","");

        if(status.equals("0"))
        {



        }*/
        //   else
        {/*




            try {
                SharedPreferences pref2 = context.getApplicationContext().getSharedPreferences("notificationdata", 0);
                String value=pref2.getString("value","");

                JSONObject json =new JSONObject(value);
                JSONObject object1=json.getJSONObject("data");
                if(object1.has("key0"))
                {

                String  Job_status_message=object1.getString("title");



                    if(Job_status_message.contains("request for a new job"))
                    {

                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        String date = df.format(Calendar.getInstance().getTime());
                        if(date.equalsIgnoreCase(object1.getString("key3")))
                        {

                            SimpleDateFormat dftime = new SimpleDateFormat("HH:mm:ss");
                            String time = dftime.format(Calendar.getInstance().getTime());

                            String pattern = "HH:mm:ss";
                            SimpleDateFormat sdf = new SimpleDateFormat(pattern);

                            try {
                                Date date1 = sdf.parse(time);
                                Date date2 = sdf.parse(object1.getString("key5"));

                                if(date1.before(date2))
                                {
                                    Intent i = new Intent(context, newjobrequesttimer.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.startActivity(i);
                                }
                            }
                            catch (ParseException e)
                            {
                                e.printStackTrace();
                            }


                        }
                        else
                        {

                        }



                    }
                  else
                    {
                        if(value.contains("key0"))
                        {

                            Intent i = new Intent(context, Xmpp_PushNotificationPage.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(i);
                        }
                        else
                        {
                            RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                                    R.layout.notificationlay);

                            Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);

// Open Notification View Class on Notification Click
                            Intent intent5 = new Intent(context, Navigationdrawer.class);
                            PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent5,
                                    Pendimobile/provider/update-availabilityngIntent.FLAG_UPDATE_CURRENT);


                            NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                                    // Set Icon
                                    .setLargeIcon(bm)
                                    .setSmallIcon(R.drawable.ic_no_user)
                                    // Dismiss Notification
                                    .setAutoCancel(true)
                                    // Set PendingIntent into Notification
                                    .setContentIntent(pIntent)
// Set RemoteViews into Notification
                                    .setContent(remoteViews);


                            int color = Color.BLACK;

// Set text on a TextView in the RemoteViews programmatically.
                            String notificationTitle="Title";
                            remoteViews.setTextViewText(R.id.tvNotificationTitle,notificationTitle);

//Generate bitmap from text
                            Bitmap bitmap = textAsBitmap(context, "Message", 30, color);

//Show generated bitmap in Imageview
                            remoteViews.setImageViewBitmap(R.id.ivNotificationMessage, bitmap);


// Create Notification Manager
                            NotificationManager notificationmanager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

// Build Notification with Notification Manager
                            int notificationId = new Random().nextInt(100);

                            Notification notification = builder.build();
                            notificationmanager.notify(notificationId, notification);
                            Toast.makeText(context,""+value,Toast.LENGTH_LONG).show();
                        }


                    }
                    //  Toast.makeText(context,""+downtext,Toast.LENGTH_LONG).show();

                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

*/


        }


    }


    public static Bitmap textAsBitmap(Context context, String messageText, float textSize, int textColor) {

        Typeface font = Typeface.createFromAsset(context.getAssets(), "Poppins-Medium.ttf");

        Paint paint = new Paint();
        paint.setTextSize(textSize);
        paint.setTypeface(font);
        paint.setColor(textColor);
        paint.setTextAlign(Paint.Align.LEFT);
        float baseline = -paint.ascent(); // ascent() is negative
        int width = (int) (paint.measureText(messageText) + 0.5f); // round
        int height = (int) (baseline + paint.descent() + 0.5f);
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(image);
        canvas.drawText("TEXT", 0, baseline, paint);
        return image;
    }

}