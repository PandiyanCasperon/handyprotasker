package noman.handyprotaskerapp.Service;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.ListView;

import Dialog.PkDialog;
import me.drakeet.materialdialog.MaterialDialog;
import noman.handyprotaskerapp.R;


/**
 * Created by CAS61 on 2/8/2018.
 */

public class HNDHelper {

    /**
     * Display the alert dialog
     *
     * @param aContext
     * @param aErrorMsg
     */
    public static void showErrorAlert(Context aContext, String aErrorMsg) {
        try {


            final PkDialog mDialog = new PkDialog(aContext);
            mDialog.setDialogTitle(aContext.getResources().getString(R.string.lbel_notification));
            mDialog.setDialogMessage(aErrorMsg);
            mDialog.setPositiveButton(
                    aContext.getResources().getString(R.string.action_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    }
            );
            mDialog.show();
//            Typeface tf = Typeface.createFromAsset(aContext.getAssets(), "Poppins-Medium.ttf");
//            new MaterialDialog.Builder(aContext)
//                    .content(aErrorMsg)
//                    .positiveText(R.string.ok_label)
//                    .typeface(tf, tf)
//                    .positiveColor(aContext.getResources().getColor(R.color.colorAccent))
//                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Show the response error alert
     *
     * @param aContext
     * @param aErrorMsg
     */
    public static void showResponseErrorAlert(Context aContext, String aErrorMsg) {
        try {


            final PkDialog mDialog = new PkDialog(aContext);
            mDialog.setDialogTitle(aContext.getResources().getString(R.string.action_sorry));
            mDialog.setDialogMessage(aErrorMsg);
            mDialog.setPositiveButton(
                    aContext.getResources().getString(R.string.action_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    }
            );
            mDialog.show();

//            Typeface tf = Typeface.createFromAsset(aContext.getAssets(), "Poppins-Medium.ttf");
//            new MaterialDialog.Builder(aContext)
//                    .content(aErrorMsg)
//                    .positiveText(aContext.getResources().getString(R.string.action_ok))
//                    .title(aContext.getResources().getString(R.string.action_sorry))
//                    .typeface(tf, tf)
//                    .positiveColor(aContext.getResources().getColor(R.color.colorAccent))
//                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void showResponseTitle(String title, Context aContext, String aErrorMsg) {
        try {


            final PkDialog mDialog = new PkDialog(aContext);
            mDialog.setDialogTitle(aContext.getResources().getString(R.string.lbel_notification));
            mDialog.setDialogMessage(aErrorMsg);
            mDialog.setPositiveButton(
                    aContext.getResources().getString(R.string.action_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    }
            );
            mDialog.show();


//            Typeface tf = Typeface.createFromAsset(aContext.getAssets(), "Poppins-Medium.ttf");
//            new MaterialDialog.Builder(aContext)
//                    .content(aErrorMsg)
//                    .positiveText(aContext.getResources().getString(R.string.ok_label))
//                    .title(title)
//                    .typeface(tf, tf)
//                    .positiveColor(aContext.getResources().getColor(R.color.colorAccent))
//                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Adjust listview height and scrolling
     *
     * @param listView
     */
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    /**
     * Adjust listview height and scrolling
     *
     * @param gridView
     * @param columns
     */
    public static void setGridViewHeightBasedOnChildren(GridView gridView, int columns) {
        ListAdapter listAdapter = gridView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int items = listAdapter.getCount();
        int rows = 0;

        View listItem = listAdapter.getView(0, null, gridView);
        listItem.measure(0, 0);
        totalHeight = listItem.getMeasuredHeight();

        float x = 1;
        if (items > columns) {
            x = items / columns;
            rows = (int) (x + 1);
            totalHeight *= rows;
        }

        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight;
        gridView.setLayoutParams(params);

    }
}
