package noman.handyprotaskerapp.Service;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import noman.handyprotaskerapp.R;
import noman.handyprotaskerapp.newjobrequesttimer;

/**
 * Created by user127 on 09-04-2018.
 */

public class DetectedService extends Service {
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        System.out.println("I am here sara 2");


        String Job_id = "";
        String Job_status_message = "";

        SharedPreferences pref2 = getApplicationContext().getSharedPreferences("notificationdata", 0);
        String value = pref2.getString("value", "");

        System.out.println("Sara response--->" + value);

        try {

            JSONObject json = new JSONObject(value);
            JSONObject object1 = json.getJSONObject("data");
            if (object1.has("key0")) {
                Job_id = object1.getString("key0");
                Job_status_message = object1.getString("title");

                String heading = Job_status_message;
                String downtext = "";

                if (Job_status_message.contains(getResources().getString(R.string.detected_service_req_for_new_job))) {


                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    String date = df.format(Calendar.getInstance().getTime());
                    if (date.equalsIgnoreCase(object1.getString("key3"))) {

                        SimpleDateFormat dftime = new SimpleDateFormat("HH:mm:ss");
                        String time = dftime.format(Calendar.getInstance().getTime());

                        String pattern = "HH:mm:ss";
                        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

                        try {
                            Date date1 = sdf.parse(time);
                            Date date2 = sdf.parse(object1.getString("key5"));

                            if (date1.before(date2)) {
                                Intent i = new Intent(getApplicationContext(), newjobrequesttimer.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                    } else {

                    }

                } else if (Job_status_message.equalsIgnoreCase(getResources().getString(R.string.detected_service_job_rejected_user))) {

                    downtext = "Job Rejected";
                } else if (Job_status_message.contains("Pending task")) {

                    downtext = "Pending Task";
                } else if (Job_status_message.contains("Expired")) {

                    downtext = "Job Expired";
                }

                //  Toast.makeText(context,""+downtext,Toast.LENGTH_LONG).show();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        // Do whatever you want to do here
    }
}