package noman.handyprotaskerapp.Service;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;

/**
 * Created by user145 on 6/23/2018.
 */
public class DemoJobCreator implements JobCreator {

    @Override
    @Nullable
    public Job create(@NonNull String tag) {
        if (DemoSyncJob.TAG.equals(tag)) {
            return new DemoSyncJob();
        }
        return null;
    }
}
