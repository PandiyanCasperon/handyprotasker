package noman.handyprotaskerapp.ApiClient;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import service.ServiceConstant;

public class ApiClient {

    private static Retrofit retrofit=null;
    private static final String BASE_URL = ServiceConstant.BASE_URL;
    public static Retrofit getdata(){
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;

    }
}
