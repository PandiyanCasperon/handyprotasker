package noman.handyprotaskerapp.WidgetSupport;

/**
 * Created by user127 on 13-02-2018.
 */

import android.content.Context;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.AttributeSet;

public class CustomBold extends AppCompatTextView {

    public CustomBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomBold(Context context) {
        super(context);
        init();
    }


    public void init() {
//	    Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "Raleway-Regular.ttf");
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "Poppins-Bold.ttf");
        setTypeface(tf);
    }
}
