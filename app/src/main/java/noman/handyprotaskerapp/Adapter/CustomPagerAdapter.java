package noman.handyprotaskerapp.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import noman.handyprotaskerapp.R;

/**
 * Created by CAS61 on 1/11/2017.
 */
public class CustomPagerAdapter extends PagerAdapter {
    private Context myContext;
    private int[] myImages;
    private LayoutInflater myLayoutInflater;
    private String[] myText;
    Typeface tf;

    public CustomPagerAdapter(Context aContext, int[] aImageInt, String[] aText) {
        this.myContext = aContext;
        this.myImages = aImageInt;
        this.myText = aText;
        myLayoutInflater = (LayoutInflater) myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        tf = Typeface.createFromAsset(myContext.getAssets(), "AvenirLTStd-Roman.otf");
    }

    @Override
    public int getCount() {
        return myImages.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View itemView = myLayoutInflater.inflate(R.layout.layout_inflate_pager_list_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.layout_inflate_pager_list_item_IMG);
        TextView aTxtVw = (TextView) itemView.findViewById(R.id.layout_inflate_pager_list_item_TXT);
        aTxtVw.setTypeface(tf);
        imageView.setImageResource(myImages[position]);
        aTxtVw.setText(myText[position]);
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
