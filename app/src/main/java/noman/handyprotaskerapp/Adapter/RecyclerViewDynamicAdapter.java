package noman.handyprotaskerapp.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import noman.handyprotaskerapp.Pojo.MultiSubItemPojo;
import noman.handyprotaskerapp.R;

public class RecyclerViewDynamicAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context context;
    private String FromTheActivity;
    private int PositionForOperation;
    private List<MultiSubItemPojo> MainMultSubItemsList;
    private RecyclerViewDynamicAdapterInterface recyclerViewDynamicAdapterInterface;
    public RecyclerViewDynamicAdapter(Context activity, List<MultiSubItemPojo> MainMultSubItemsList,
                                      int PositionForOperation, String FromTheActivity,
                                      RecyclerViewDynamicAdapterInterface recyclerViewDynamicAdapterInterface) {
        this.context = activity;
        this.MainMultSubItemsList = MainMultSubItemsList;
        this.PositionForOperation = PositionForOperation;
        this.FromTheActivity = FromTheActivity;
        this.recyclerViewDynamicAdapterInterface = recyclerViewDynamicAdapterInterface;
    }

    public interface RecyclerViewDynamicAdapterInterface{
        void Delete(int InnerPosition);
        void Edit(int InnerPosition);
    }


    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView TextViewSubItemTitle, TextViewSubItemHours, TextViewSubItemAmount, TextViewSubItemsDescription, TextViewInnerPosition;
        ImageView ImageViewDelete, ImageViewEdit;
        private ViewHolder(View itemView) {
            super(itemView);
            TextViewSubItemTitle = itemView.findViewById(R.id.TextViewSubItemTitle);
            TextViewSubItemHours = itemView.findViewById(R.id.TextViewSubItemHours);
            TextViewSubItemAmount = itemView.findViewById(R.id.TextViewSubItemAmount);
            TextViewSubItemsDescription = itemView.findViewById(R.id.TextViewSubItemsDescription);
            TextViewInnerPosition = itemView.findViewById(R.id.TextViewInnerPosition);
            ImageViewDelete = itemView.findViewById(R.id.ImageViewDelete);
            ImageViewEdit = itemView.findViewById(R.id.ImageViewEdit);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View menuItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.custom_add_sub_items, parent, false);
        viewHolder = new ViewHolder(menuItemLayoutView);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;

        if (FromTheActivity.equals("EstimationBuilderActivity")){
            viewHolder.ImageViewEdit.setVisibility(View.GONE);
            switch (MainMultSubItemsList.get(holder.getAdapterPosition()).getParentEstimationStatus()) {
                case 2:
                    viewHolder.ImageViewDelete.setVisibility(View.GONE);
                    break;
                default:
                    switch (MainMultSubItemsList.get(holder.getAdapterPosition()).getStatus()) {
                        case "User Accepted":
                            viewHolder.ImageViewDelete.setImageResource(R.drawable.ticked);
                            break;
                        case "User Rejected":
                            viewHolder.ImageViewDelete.setImageResource(R.drawable.cancelmark);
                            break;
                        default:
                            if (MainMultSubItemsList.size() > 1) {
                                viewHolder.ImageViewDelete.setVisibility(View.VISIBLE);
                                viewHolder.ImageViewDelete.setImageResource(R.drawable.ic_delete);
                            } else {
                                viewHolder.ImageViewDelete.setVisibility(View.GONE);
                            }
                            break;
                    }
            }
        }else if (FromTheActivity.equals("AddItemClassActivity")){
            viewHolder.ImageViewDelete.setVisibility(View.VISIBLE);
            viewHolder.ImageViewDelete.setImageResource(R.drawable.ic_delete);
        }
        if (MainMultSubItemsList.get(holder.getAdapterPosition()).getName().length() > 20){
            viewHolder.TextViewSubItemTitle.setText(MainMultSubItemsList.get(holder.getAdapterPosition()).getName().substring(0, 20) + "... ");
            viewHolder.TextViewSubItemHours.setText( MainMultSubItemsList.get(holder.getAdapterPosition()).getHours() + " Hours");
        }else {
            viewHolder.TextViewSubItemTitle.setText(MainMultSubItemsList.get(holder.getAdapterPosition()).getName());
            viewHolder.TextViewSubItemHours.setText( MainMultSubItemsList.get(holder.getAdapterPosition()).getHours() + " Hours");
        }

        viewHolder.TextViewSubItemAmount.setText("- $" + MainMultSubItemsList.get(holder.getAdapterPosition()).getAmount());
        viewHolder.TextViewSubItemsDescription.setText(MainMultSubItemsList.get(holder.getAdapterPosition()).getDescription());

        viewHolder.ImageViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewHolder.ImageViewDelete.getDrawable().getConstantState() == context.getResources().getDrawable( R.drawable.ic_delete).getConstantState()){
                    recyclerViewDynamicAdapterInterface.Delete(MainMultSubItemsList.get(holder.getAdapterPosition()).getInnerPosition());
                }
            }
        });

        viewHolder.ImageViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerViewDynamicAdapterInterface.Edit(MainMultSubItemsList.get(holder.getAdapterPosition()).getInnerPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return MainMultSubItemsList.size();
    }

}
