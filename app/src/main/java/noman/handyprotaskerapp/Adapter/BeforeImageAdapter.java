package noman.handyprotaskerapp.Adapter;

/**
 * Created by user127 on 18-04-2018.
 */

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import noman.handyprotaskerapp.Pojo.BeforeImagePojo;
import noman.handyprotaskerapp.R;

public class BeforeImageAdapter extends RecyclerView.Adapter<BeforeImageAdapter.MyViewHolder> {

    private List<BeforeImagePojo> moviesList;
    Context ctxx;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;

        MyViewHolder(View view) {
            super(view);
            image = (ImageView) view.findViewById(R.id.image);

        }
    }


    public BeforeImageAdapter(List<BeforeImagePojo> moviesList, Context ctx) {
        this.moviesList = moviesList;
        this.ctxx = ctx;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.acceptimagelay, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        BeforeImagePojo movie = moviesList.get(position);

        if (movie.getTitle().isEmpty()) {
            holder.image.setImageResource(R.drawable.noimageavailable);
        } else {
            Picasso.with(ctxx)
                    .load(movie.getTitle())
                    .placeholder(R.drawable.noimageavailable)   // optional
                    .error(R.drawable.noimageavailable)      // optional
                    // optional
                    .into(holder.image);
        }


    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
