package noman.handyprotaskerapp.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import noman.handyprotaskerapp.R;

public class ServiceTypeArrayListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context context;
    private ArrayList<String> ServiceTypeArrayList;
    public ServiceTypeArrayListAdapter(Context activity, ArrayList<String> ServiceTypeArrayList) {
        this.context = activity;
        this.ServiceTypeArrayList = ServiceTypeArrayList;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView TextViewJobServiceType;
        private ViewHolder(View itemView) {
            super(itemView);
            TextViewJobServiceType = (TextView) itemView.findViewById(R.id.TextViewJobServiceType);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View menuItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.custom_multiple_service_type_row, parent, false);
        viewHolder = new ViewHolder(menuItemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.TextViewJobServiceType.setText(ServiceTypeArrayList.get(holder.getAdapterPosition()));
    }

    @Override
    public int getItemCount() {
        return ServiceTypeArrayList.size();
    }
}