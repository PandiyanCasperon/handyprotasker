package noman.handyprotaskerapp.Adapter;

/**
 * Created by user127 on 19-02-2018.
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import noman.handyprotaskerapp.Pojo.EstimationPojo;
import noman.handyprotaskerapp.R;

public class EstimateAdapter extends ArrayAdapter<EstimationPojo> {
    protected static final String LOG_TAG = EstimateAdapter.class.getSimpleName();
    Typeface tf;
    SharedPreferences pref;
    String currency;
    private ArrayList<EstimationPojo> items;
    private int layoutResourceId;
    private Context context;

    public EstimateAdapter(Context context, int layoutResourceId, ArrayList<EstimationPojo> items) {
        super(context, layoutResourceId, items);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.items = items;
        tf = Typeface.createFromAsset(getContext().getAssets(), "Poppins-Regular.ttf");
        pref = context.getSharedPreferences("logindetails", 0);
        currency = pref.getString("currency", "");
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        Holder holder = null;

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        row = inflater.inflate(layoutResourceId, parent, false);

        holder = new Holder();
        holder.bean = items.get(position);


        holder.etVal1 = (EditText) row.findViewById(R.id.edVal1);
        holder.etVal2 = (EditText) row.findViewById(R.id.edValue2);
        holder.tvTotal = (TextView) row.findViewById(R.id.txtTotal);
        holder.deletecraftman = (ImageView) row.findViewById(R.id.deletecraftman);
        holder.deletecraftman.setTag(holder.bean);

        holder.etVal1.setTypeface(tf);
        holder.etVal2.setTypeface(tf);
        holder.tvTotal.setTypeface(tf);


        holder.jobtytpeheading = (TextView) row.findViewById(R.id.jobtytpeheading);
        holder.jobdescriptionheading = (TextView) row.findViewById(R.id.jobdescriptionheading);
        holder.amountheading = (TextView) row.findViewById(R.id.amountheading);
        holder.hourheadining = (TextView) row.findViewById(R.id.hourheadining);
        holder.materialheading = (TextView) row.findViewById(R.id.materialheading);
        holder.materialdescheading = (TextView) row.findViewById(R.id.materialdescheading);


        holder.jobtytpeheading.setTypeface(tf);
        holder.jobdescriptionheading.setTypeface(tf);
        holder.amountheading.setTypeface(tf);
        holder.hourheadining.setTypeface(tf);
        holder.materialheading.setTypeface(tf);
        holder.materialdescheading.setTypeface(tf);


        holder.materialdesc = (EditText) row.findViewById(R.id.materialdesc);
        holder.materialamount = (EditText) row.findViewById(R.id.materialamount);
        holder.jobdescription = (EditText) row.findViewById(R.id.jobdescription);
        holder.jobtype = (EditText) row.findViewById(R.id.jobtype);

        holder.jobtype.setTypeface(tf);
        holder.materialdesc.setTypeface(tf);
        holder.jobdescription.setTypeface(tf);
        holder.materialdescheading.setTypeface(tf);
        holder.materialamount.setTypeface(tf);

        if (position == 0) {
            holder.deletecraftman.setVisibility(View.INVISIBLE);
            holder.jobdescriptionheading.setText(context.getResources().getString(R.string.estimate_title_job_desc));
            holder.jobtytpeheading.setText(context.getResources().getString(R.string.estimate_title_job_type));

        } else {
            holder.deletecraftman.setVisibility(View.VISIBLE);
            holder.jobdescriptionheading.setText(context.getResources().getString(R.string.estimate_title_description));
            holder.jobtytpeheading.setText(context.getResources().getString(R.string.estimate_craftmen) + position);
        }

        //setVal1TextChangeListener(holder);
        setVal2TextListeners(holder);

        row.setTag(holder);

        setupItem(holder);

        return row;
    }

    private void setupItem(Holder holder) {


        holder.jobtype.setText(holder.bean.getJobtype());
        holder.jobdescription.setText(holder.bean.getJobdescription());


        holder.etVal1.setText(String.valueOf(holder.bean.getVal1()));
        holder.etVal2.setText(String.valueOf(holder.bean.getVal2()));
        holder.tvTotal.setText("$" + String.valueOf(holder.bean.getTotal()));
    }

    private void setVal2TextListeners(final Holder holder) {
        holder.etVal2.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (s.toString().length() > 0) {
                        holder.bean.setVal2(Integer.parseInt(s.toString()));
                    }
                } catch (NumberFormatException e) {
                    Log.e(LOG_TAG, "error reading double value: " + s.toString());
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 0) {
                    holder.bean.setVal2(Integer.parseInt(s.toString()));

                    double val1 = Double.parseDouble(holder.etVal1.getText().toString());
                    double val2 = Double.parseDouble(holder.etVal2.getText().toString());
                    holder.bean.setTotal(String.valueOf(val1 * val2));

                    holder.tvTotal.setText("$" + String.valueOf(val1 * val2));
                }

            }
        });
    }

    public static class Holder {
        ImageView deletecraftman;
        EstimationPojo bean;
        EditText etVal1;
        EditText etVal2, jobtype, jobdescription, materialamount, materialdesc;
        TextView tvTotal, jobtytpeheading, jobdescriptionheading, amountheading, hourheadining, materialheading, materialdescheading;

    }
}