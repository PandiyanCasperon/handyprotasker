package noman.handyprotaskerapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;

import java.util.ArrayList;

import Dialog.PkDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import noman.handyprotaskerapp.Pojo.FranchiseDescPojo;
import noman.handyprotaskerapp.R;
import noman.handyprotaskerapp.WidgetSupport.CustomTextView;
import noman.handyprotaskerapp.activity.MyJobs_OnGoingDetailActivity;

public class FranchiseListAdapter extends BaseAdapter {

    Context myContext;
    ArrayList<FranchiseDescPojo> myFranchiseInfoList;
    LayoutInflater mInflater;

    public FranchiseListAdapter(Context aContext, ArrayList<FranchiseDescPojo> aFranchiseInfoList) {
        this.myContext = aContext;
        this.myFranchiseInfoList = aFranchiseInfoList;
        mInflater = LayoutInflater.from(myContext);
    }

    @Override
    public int getCount() {
        return myFranchiseInfoList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    class ViewHolder {
        private CustomTextView aUserName;
        private CustomTextView myViewTXT;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.layout_inflate_franchise_list_item, parent, false);
            holder = new ViewHolder();
            holder.aUserName = (CustomTextView) convertView.findViewById(R.id.layout_inflate_franchise_list_item_TXT_name);
            holder.myViewTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflate_franchise_list_item_TXT_view_notes);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.aUserName.setText(myFranchiseInfoList.get(position).getFranchiseDate() + " " +
                myFranchiseInfoList.get(position).getFranchiseStartTime() + " - " + myFranchiseInfoList.get(position).getFranchiseEndTime());
        if (!myFranchiseInfoList.get(position).getFranchiseDate().equalsIgnoreCase("")) {
            holder.myViewTXT.setVisibility(View.VISIBLE);
            holder.myViewTXT.setOnClickListener(new viewLAYclickListener(position));
        } else {
            holder.myViewTXT.setVisibility(View.GONE);
        }
        return convertView;
    }

    private class viewLAYclickListener implements View.OnClickListener {

        int myPosition;

        public viewLAYclickListener(int aPosition) {
            myPosition = aPosition;
        }

        @Override
        public void onClick(View view) {
            final PkDialog mDialog = new PkDialog(myContext);
            mDialog.setDialogTitle(myContext.getResources().getString(R.string.description));
            mDialog.setDialogMessage(myFranchiseInfoList.get(myPosition).getFranchiseDescription());
            mDialog.setPositiveButton(
                    myContext.getResources().getString(R.string.action_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    }
            );
            mDialog.show();
        }
    }
}
