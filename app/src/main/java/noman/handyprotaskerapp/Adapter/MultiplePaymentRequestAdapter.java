package noman.handyprotaskerapp.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import noman.handyprotaskerapp.R;
import noman.handyprotaskerapp.Pojo.User_pre_amount_list;

public class MultiplePaymentRequestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context context;
    private User_pre_amount_list[] user_pre_amount_list_main;
    private ProgressDialog progressBar;
    private String currency;
    MultiplePaymentRequestAdapterInterface multiplePaymentRequestAdapterInterface;
    public MultiplePaymentRequestAdapter(Context activity, User_pre_amount_list[] user_pre_amount_list_main, String currency, MultiplePaymentRequestAdapterInterface multiplePaymentRequestAdapterInterface) {
        this.context = activity;
        this.user_pre_amount_list_main = user_pre_amount_list_main;
        this.currency = currency;
        this.multiplePaymentRequestAdapterInterface = multiplePaymentRequestAdapterInterface;
    }

    public interface MultiplePaymentRequestAdapterInterface{
         public void Refresh(String PrePaymentID);
    }


    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView TextViewAmount, TextViewDateTime;
        ImageView ImageViewDelete;
        private ViewHolder(View itemView) {
            super(itemView);
            TextViewAmount = (TextView) itemView.findViewById(R.id.TextViewAmount);
            TextViewDateTime = (TextView) itemView.findViewById(R.id.TextViewDateTime);
            ImageViewDelete = (ImageView) itemView.findViewById(R.id.ImageViewDelete);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View menuItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.custom_multiple_payment_request_row, parent, false);
        viewHolder = new ViewHolder(menuItemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.TextViewAmount.setText(currency+user_pre_amount_list_main[position].getPre_amount() + " - " + user_pre_amount_list_main[position].getPayment_status());
        String temp = user_pre_amount_list_main[position].getDate().split("T")[0];
        DateToString(StringToDate(temp, "yyyy-MM-dd"), "MM-dd-yyyy");
        viewHolder.TextViewDateTime.setText((DateToString(StringToDate(temp, "yyyy-MM-dd"), "MM-dd-yyyy")+ " "+user_pre_amount_list_main[position].getTime()).toUpperCase());
        switch (user_pre_amount_list_main[position].getPayment_status()){
            case "Pending":
                viewHolder.TextViewAmount.setTextColor(Color.parseColor("#FFA500"));
                viewHolder.ImageViewDelete.setVisibility(View.VISIBLE);
                break;
            case "Accepted":
                viewHolder.TextViewAmount.setTextColor(Color.parseColor("#00FF00"));
                viewHolder.ImageViewDelete.setVisibility(View.GONE);
                break;
            case "Rejected":
                viewHolder.TextViewAmount.setTextColor(Color.parseColor("#FF0000"));
                viewHolder.ImageViewDelete.setVisibility(View.GONE);
                break;
        }
        viewHolder.ImageViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                multiplePaymentRequestAdapterInterface.Refresh(user_pre_amount_list_main[position].get_id());
            }
        });
    }

    @Override
    public int getItemCount() {
        return user_pre_amount_list_main.length;
    }

    private String DateToString(Date date, String Format){
        return new SimpleDateFormat(Format, Locale.getDefault()).format(date);
    }

    private Date StringToDate(String date, String Format){
        Date OutPutDate = null;
        try {
            OutPutDate =  new SimpleDateFormat(Format, Locale.getDefault()).parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return OutPutDate;
    }
}
