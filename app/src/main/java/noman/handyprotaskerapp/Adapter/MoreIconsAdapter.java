package noman.handyprotaskerapp.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import noman.handyprotaskerapp.Pojo.MoreIconsPojo;
import noman.handyprotaskerapp.R;

public class MoreIconsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<MoreIconsPojo> moreIconsArrayList;
    private ClickListen clickListen;

    public MoreIconsAdapter(Context activity, ArrayList<MoreIconsPojo> moreIconsArrayList, ClickListen clickListen) {
        this.context = activity;
        this.moreIconsArrayList = moreIconsArrayList;
        this.clickListen = clickListen;
    }

    public interface ClickListen {
        void OnClickMainHolder(int SelectedIcon);
//        void OnClickMainHolder1(String SelectedIcon);
    }


    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView TextViewName;
        ImageView ImageViewIcon;
        LinearLayout LinearLayoutMainHolder;

        private ViewHolder(View itemView) {
            super(itemView);
            TextViewName = itemView.findViewById(R.id.TextViewName);
            ImageViewIcon = itemView.findViewById(R.id.ImageViewIcon);
            LinearLayoutMainHolder = itemView.findViewById(R.id.LinearLayoutMainHolder);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View menuItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.custom_more_icons_row, parent, false);
        viewHolder = new ViewHolder(menuItemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.TextViewName.setText(moreIconsArrayList.get(holder.getAdapterPosition()).getName());
        viewHolder.ImageViewIcon.setImageResource(moreIconsArrayList.get(holder.getAdapterPosition()).getResourceIcon());
        viewHolder.LinearLayoutMainHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListen.OnClickMainHolder(moreIconsArrayList.get(holder.getAdapterPosition()).getResourceIcon());
//                clickListen.OnClickMainHolder1(moreIconsArrayList.get(holder.getAdapterPosition()).getName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return moreIconsArrayList.size();
    }

}
