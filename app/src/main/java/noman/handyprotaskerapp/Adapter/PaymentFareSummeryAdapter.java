package noman.handyprotaskerapp.Adapter;

/**
 * Created by user127 on 02-04-2018.
 */

import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import noman.handyprotaskerapp.Pojo.PaymentFareSummeryPojo;
import noman.handyprotaskerapp.R;

/**
 * Created by user88 on 12/30/2015.
 */
public class PaymentFareSummeryAdapter extends BaseAdapter {

    private ArrayList<PaymentFareSummeryPojo> data;
    private LayoutInflater mInflater;
    private AppCompatActivity context;
    private String check;

    public PaymentFareSummeryAdapter(AppCompatActivity c, ArrayList<PaymentFareSummeryPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder {
        private TextView Tv_payment_title,Tv_payment_amount,paydesc;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;

        if (convertView==null){
            view = mInflater.inflate(R.layout.payment_fare_summery_single, parent, false);
            holder = new ViewHolder();

            holder.Tv_payment_title = view.findViewById(R.id.payment_fare_title_textView);
            holder.Tv_payment_amount = view.findViewById(R.id.payment_fare_cost_textView);
            holder.paydesc = view.findViewById(R.id.paydesc);

            view.setTag(holder);

        }else{
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        holder.Tv_payment_title.setText(data.get(position).getPayment_title());
        holder.Tv_payment_amount.setText(data.get(position).getPayment_amount());
        if (holder.Tv_payment_title.getText().toString().equals(context.getResources().getString(R.string.balance_due))){
            holder.Tv_payment_amount.setTextColor(context.getResources().getColor(R.color.red));
        }else {
            holder.Tv_payment_amount.setTextColor(context.getResources().getColor(R.color.black));
        }
        if(data.get(position).getPayment_desc()== null||data.get(position).getPayment_desc().equals("")||data.get(position).getPayment_desc().equals(" "))
        {
            holder.paydesc.setVisibility(View.GONE);
        }
        else
        {
            holder.paydesc.setText(data.get(position).getPayment_desc());
            holder.paydesc.setVisibility(View.VISIBLE);
        }


        return view;
    }
}
