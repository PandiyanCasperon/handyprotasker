package noman.handyprotaskerapp.Adapter;

/**
 * Created by user127 on 05-04-2018.
 */

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Dialog.PkDialog;
import Map.GPSTracker;
import at.grabner.circleprogress.CircleProgressView;
import at.grabner.circleprogress.TextMode;
import de.hdodenhof.circleimageview.CircleImageView;
import noman.handyprotaskerapp.activity.MyJobs_OnGoingDetailActivity;
import noman.handyprotaskerapp.Pojo.acceptimage;
import noman.handyprotaskerapp.ProgressDialog1;
import noman.handyprotaskerapp.R;
import service.ServiceConstant;
import volley.ServiceRequest;

/**
 * Created by Administrator on 11/23/2015.
 */
public class ContinuousRequestAdapter {
    public String currentVersion = "";

    private LayoutInflater mInflater;
    private AppCompatActivity context;
    private LinearLayout listview;
    public int req_count;
    private CircleProgressView mCircleView;
    private CountDownTimer timer;
    private String KEY1 = "key1";
    private String KEY2 = "key2";
    private String KEY3 = "key3";
    private String rider_id = "";
    private Dialog dialog;
    private int seconds = 0;
    private MediaPlayer mediaPlayer;
    private Location myLocation;
    private ProgressDialog1 myDialog;
    private Handler mHandler = new Handler();
    public static String userID;

    public CircularHandler ch;
    public int cur_count;

    private float totalDistanceTravelled;
    ViewHolder holder1;
    Boolean clicked = false;

    ArrayList<CircularHandler> arrobj = new ArrayList<CircularHandler>();

    public class ViewHolder {

        public int count;
        private RecyclerView recyclerView;
        public CircleProgressView circularProgressBar;
        LinearLayout accepttext, reject;
        public TextView categorytext, addresstext, usernametext, bookingdatetext, tips, whatsimportant;
        private LinearLayout Ll_ride_Requst_layout;
        CircleImageView imag;
        public String bookingdata, category, address, username, userimage, Job_id, tipsv, descriptionva;


    }

    public ContinuousRequestAdapter(AppCompatActivity context, LinearLayout listview) {
        this.context = context;

        mInflater = LayoutInflater.from(context);
        this.listview = listview;
        this.myDialog = new ProgressDialog1(context);
        this.myDialog.setCancelable(false);
        this.myDialog.setCanceledOnTouchOutside(false);
    }


    public View getView(int i, String bookingdate, String category, String address, String username, String userimage, String Job_id, JSONArray imagearray, String instuction, String description) {
        View view;
        ViewHolder holder;
        String data1 = " ";
        // JSONObject data = (JSONObject) getItem(i);
        view = mInflater.inflate(R.layout.jobrequestvalid, null, false);
        holder = new ViewHolder();
        holder.bookingdata = bookingdate;


        holder.bookingdata = bookingdate;
        holder.category = category;
        holder.address = address;
        holder.username = username;
        holder.userimage = userimage;
        holder.Job_id = Job_id;

        holder.tipsv = instuction;
        holder.descriptionva = description;

        holder.count = i;

        holder.categorytext = (TextView) view.findViewById(R.id.category);
        holder.addresstext = (TextView) view.findViewById(R.id.address);
        holder.usernametext = (TextView) view.findViewById(R.id.username);
        holder.bookingdatetext = (TextView) view.findViewById(R.id.bookingdate);
        holder.accepttext = (LinearLayout) view.findViewById(R.id.accept);
        holder.reject = (LinearLayout) view.findViewById(R.id.reject);
        holder.imag = (CircleImageView) view.findViewById(R.id.imag);

        holder.tips = (TextView) view.findViewById(R.id.tips);
        holder.whatsimportant = (TextView) view.findViewById(R.id.whatsimportant);


        List<acceptimage> movieList = new ArrayList<>();
        movieList.clear();


        for (int b = 0; b < imagearray.length(); b++) {
            try {
                String imagenames = "" + imagearray.getString(b);
                if (imagenames.contains("https://") || imagenames.contains("http://")) {

                    acceptimage movie = new acceptimage(imagenames);
                    movieList.add(movie);
                } else {
                    acceptimage movie = new acceptimage(ServiceConstant.BASE_URLIMAGE + imagenames);
                    movieList.add(movie);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        holder.recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        if (imagearray.length() > 0) {

            LinearLayoutManager layoutManager
                    = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            AcceptImageAdapter mAdapter = new AcceptImageAdapter(movieList, context);
            holder.recyclerView.setLayoutManager(layoutManager);
            holder.recyclerView.setAdapter(mAdapter);
            holder.recyclerView.setVisibility(View.VISIBLE);
        } else {
            holder.recyclerView.setVisibility(View.GONE);
        }


        holder.accepttext.setTag(holder);
        holder.accepttext.setOnClickListener(acceptBtnlistener);

        holder.reject.setTag(holder);
        holder.reject.setOnClickListener(new DeclineBtnListener(i));

        holder.categorytext.setText(holder.category);
        holder.addresstext.setText(holder.address);
        holder.usernametext.setText(holder.username);
        holder.bookingdatetext.setText(holder.bookingdata);

        holder.tips.setText(holder.tipsv);
        holder.whatsimportant.setText(holder.descriptionva);

        Picasso.with(context).load(holder.userimage)
                .placeholder(R.drawable.ic_no_user)
                .into(holder.imag);


        holder.circularProgressBar = (CircleProgressView) view.findViewById(R.id.timer_circleView);
        holder.Ll_ride_Requst_layout = (LinearLayout) view.findViewById(R.id.Ll_ride_request_layout);


        view.setTag(holder);


        // holder.decline.setTag(holder);

        holder.circularProgressBar.setEnabled(false);
        holder.circularProgressBar.setFocusable(false);
        holder.circularProgressBar.setMaxValue(100);
        holder.circularProgressBar.setValueAnimated(0);
        holder.circularProgressBar.setAutoTextSize(true);
        holder.circularProgressBar.setTextScale(0.6f);

        // holder.circularProgressBar.setTextSize(250); // text size set, auto text size off
        //  holder.circularProgressBar.setUnitSize(80); // if i set the text size i also have to set the unit size
        //  holder.circularProgressBar.setAutoTextSize(true); // enable auto text size, previous values are overwritten
        //if you want the calculated text sizes to be bigger/smaller you can do so via
        //  holder.circularProgressBar.setUnitScale(0.9f);
        // holder.circularProgressBar.setTextScale(0.9f);

        ch = new CircularHandler(holder, Integer.parseInt("100"), i);
        mHandler.post(ch);
        arrobj.add(ch);

        return view;
    }

    private class CircularHandler implements Runnable {
        ViewHolder holder;
        Integer value;
        int pos;
        boolean isRunning;

        public CircularHandler(ViewHolder holder, Integer val, int i) {
            this.holder = holder;
            isRunning = true;
            value = val;
            pos = i;
        }

        @Override
        public void run() {
            if (isRunning) {
                value = value - 1;

                holder.circularProgressBar.setText(String.valueOf(Math.abs(value)));
                holder.circularProgressBar.setTextMode(TextMode.TEXT);
                holder.circularProgressBar.setValueAnimated(value, 500);
                mHandler.postDelayed(this, 1000);
                System.out.println("counter-----jai--------------" + value);


                if (value == 0) {
                    holder.Ll_ride_Requst_layout.setVisibility(View.GONE);

                }


            }
        }
    }


    private View.OnClickListener acceptBtnlistener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {


            LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            boolean gps_enabled = false;
            boolean network_enabled = false;
            try {
                gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            } catch (Exception ex) {
            }

            try {
                network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            } catch (Exception ex) {
            }

            if (!gps_enabled && !network_enabled) {
                // notify user
                final PkDialog mdialog = new PkDialog(context);
                mdialog.setDialogTitle(context.getResources().getString(R.string.home_location_header_label));
                mdialog.setDialogMessage(context.getResources().getString(R.string.turn_on_location));
                mdialog.setCancelOnTouchOutside(false);
                mdialog.setPositiveButton(
                        context.getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mdialog.dismiss();
                            }
                        }
                );
                mdialog.show();
            } else {


                GPSTracker gps = new GPSTracker(context);
                if (gps != null && gps.canGetLocation()) {
                    double Dlatitude = gps.getLatitude();
                    double Dlongitude = gps.getLongitude();

                    ViewHolder holder = (ViewHolder) view.getTag();
                    String Jobid = holder.Job_id;

                   /* try {
                        holder.count = holder.count-1;
                        holder.Ll_ride_Requst_layout.setVisibility(View.GONE);

                    } catch (Exception e) {
                    }
*/
                    buttonsClickActions(context, ServiceConstant.ACCEPT_JOB_URL, Jobid, Dlatitude, Dlongitude, holder);
                } else {
                    final PkDialog mdialog = new PkDialog(context);
                    mdialog.setDialogTitle(context.getResources().getString(R.string.home_location_header_label));
                    mdialog.setDialogMessage(context.getResources().getString(R.string.turn_on_location));
                    mdialog.setCancelOnTouchOutside(false);
                    mdialog.setPositiveButton(
                            context.getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mdialog.dismiss();
                                }
                            }
                    );
                    mdialog.show();
                }


            }


        }
    };


    private void buttonsClickActions(final Context mContext, String url, final String Jobid, Double dlat, Double dlong, final ViewHolder holder) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();

        SharedPreferences pref = context.getSharedPreferences("logindetails", 0);
        String provider_id = pref.getString("provider_id", "");

        jsonParams.put("provider_id", provider_id);
        jsonParams.put("job_id", Jobid);
        jsonParams.put("provider_lat", "" + dlat);
        jsonParams.put("provider_lon", "" + dlong);

        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("clickresponse------", response);

                String Str_status = "", Str_message = "";
                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");
                    Str_message = jobject.getString("response");


                    if (Str_status.equalsIgnoreCase("1")) {

                        try {
                            holder.count = holder.count - 1;
                            holder.Ll_ride_Requst_layout.setVisibility(View.GONE);

                        } catch (Exception e) {
                        }


                        final PkDialog mdialog = new PkDialog(context);
                        mdialog.setDialogTitle(context.getResources().getString(R.string.success_label));
                        mdialog.setDialogMessage(context.getResources().getString(R.string.action_job_accepted_successfully));
                        mdialog.setPositiveButton(context.getResources().getString(R.string.ok_label), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mdialog.dismiss();

                                        SharedPreferences prefjobid = mContext.getSharedPreferences("ongoingpage", 0);
                                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                                        prefeditor.putString("jobid", Jobid);
                                        prefeditor.apply();
                                        prefeditor.commit();

                                        Intent intent = new Intent(mContext, MyJobs_OnGoingDetailActivity.class);
                                        mContext.startActivity(intent);


                                    }
                                }
                        );
                        mdialog.show();

                    } else {
                        final PkDialog mdialog = new PkDialog(context);
                        mdialog.setDialogTitle(context.getResources().getString(R.string.failed_label));
                        mdialog.setDialogMessage(Str_message);
                        mdialog.setPositiveButton(context.getResources().getString(R.string.ok_label), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mdialog.dismiss();
                                    }
                                }
                        );
                        mdialog.show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }


            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }


            }
        });
    }


    public class DeclineBtnListener implements View.OnClickListener {
        int mPosition;

        DeclineBtnListener(int position) {
            mPosition = position;
        }

        @Override
        public void onClick(View v) {

            ViewHolder holder = (ViewHolder) v.getTag();

            try {

                holder.count = holder.count - 1;
                holder.Ll_ride_Requst_layout.setVisibility(View.GONE);

            } catch (Exception e) {
            }
        }
    }


}
