package noman.handyprotaskerapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import noman.handyprotaskerapp.ApiInterface.OnclickListener;
import noman.handyprotaskerapp.Pojo.DocumentListsItem;
import noman.handyprotaskerapp.Pojo.DocumentPojo;
import noman.handyprotaskerapp.R;

public class DocumentAdapter extends RecyclerView.Adapter<DocumentAdapter.ViewHolder> {

    Context context;
    ArrayList<DocumentListsItem> documentlist;
    OnclickListener onclickListener;
    public DocumentAdapter(Context context,ArrayList<DocumentListsItem> documentlist,OnclickListener onclickListener){
        this.context = context;
        this.documentlist = documentlist;
        this.onclickListener = onclickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.document_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {


        holder.licenceTitleNameTV.setText(documentlist.get(position).getFilename());
        if(!documentlist.get(position).getUrl().isEmpty()){
        if(documentlist.get(position).getUrl().endsWith(".pdf")){

            Glide.with(context).load(R.drawable.pdf_files_icons).into(holder.choosedImageIV);
        }
        else if(documentlist.get(position).getUrl().endsWith(".xps")){

            Glide.with(context).load(R.drawable.pdf_files_icons).into(holder.choosedImageIV);
        }

        else if(documentlist.get(position).getUrl().endsWith(".doc")||documentlist.get(position).getUrl().endsWith(".docx") ){

            Glide.with(context).load(R.drawable.document_files_icons).into(holder.choosedImageIV);
        }

        else{
            Glide.with(context).load(documentlist.get(position).getUrl()).into(holder.choosedImageIV);
        }}

        else{
            holder.choosedImageIV.setVisibility(View.GONE);
        }

        holder.document_view_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onclickListener.onclick(position,"document_view");
            }
        });

        holder.downloadbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onclickListener.onclick(position,"download_document");
            }
        });


    }

    @Override
    public int getItemCount() {
        return documentlist.size();

}

class ViewHolder extends RecyclerView.ViewHolder {

        ImageView choosedImageIV;
        TextView licenceTitleNameTV;
        Button document_view_tv,downloadbutton;

    public ViewHolder(@NonNull View itemView) {
        super(itemView);

        choosedImageIV = itemView.findViewById(R.id.choosedImageIV);
        licenceTitleNameTV = itemView.findViewById(R.id.licenceTitleNameTV);
        document_view_tv = itemView.findViewById(R.id.document_view_tv);
        downloadbutton  = itemView.findViewById(R.id.downloadbutton);
    }
}}
