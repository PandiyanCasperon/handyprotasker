package noman.handyprotaskerapp.Adapter;

/**
 * Created by user127 on 18-04-2018.
 */

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import noman.handyprotaskerapp.Pojo.AfterImagePojo;
import noman.handyprotaskerapp.R;

public class AfterImageAdapter extends RecyclerView.Adapter<AfterImageAdapter.MyViewHolder> {

    private List<AfterImagePojo> moviesList;
    Context ctxx;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;

        public MyViewHolder(View view) {
            super(view);
            image = (ImageView) view.findViewById(R.id.image);

        }
    }


    public AfterImageAdapter(List<AfterImagePojo> moviesList, Context ctx) {
        this.moviesList = moviesList;
        this.ctxx = ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.acceptimagelay, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        AfterImagePojo movie = moviesList.get(position);

        if (movie.getTitle().isEmpty()) {
            holder.image.setImageResource(R.drawable.noimageavailable);
        } else {
            Picasso.with(ctxx)
                    .load(movie.getTitle())
                    .placeholder(R.drawable.noimageavailable)   // optional
                    .error(R.drawable.noimageavailable)      // optional
                    // optional
                    .into(holder.image);
        }


    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
