/*
 * Copyright 2015 Rudson Lima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package noman.handyprotaskerapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.ContextCompat;
import android.view.Menu;
import android.view.View;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import Utils.ConnectionDetector;
import br.liveo.interfaces.OnItemClickListener;
import br.liveo.interfaces.OnPrepareOptionsMenuLiveo;
import br.liveo.navigationliveo.NavigationLiveo;
import noman.handyprotaskerapp.Fragment.DashboardFragment;
import noman.handyprotaskerapp.Service.HNDHelper;
import noman.handyprotaskerapp.activity.AboutUsActivity;
import noman.handyprotaskerapp.activity.BankDetailsActivity;
import noman.handyprotaskerapp.activity.ChangePasswordActivity;
import noman.handyprotaskerapp.activity.GetMessageChatActivity;
import noman.handyprotaskerapp.activity.LoginActivity;
import noman.handyprotaskerapp.activity.MyJobsActivity;
import noman.handyprotaskerapp.activity.TransactionMenuActivity;
import service.ServiceConstant;
import socket.ChatMessageServicech;
import volley.ServiceRequest;

public class Navigationdrawer extends NavigationLiveo implements OnItemClickListener {

    br.liveo.model.HelpLiveo mHelpLiveo;
    private Boolean isInternetPresent = false;
    ConnectionDetector cd;
    private ProgressDialog1 myDialog;

    @Override
    public void onInt(Bundle savedInstanceState) {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);

        String username = pref.getString("provider_name", "");
        String useremail = pref.getString("email", "");


        // User Information
        this.userName.setText(username);
        this.userEmail.setText(useremail);
        this.userPhoto.setImageResource(R.drawable.ic_no_user);

        this.userBackground.setBackgroundColor(ContextCompat.getColor(Navigationdrawer.this, R.color.commoncolourback));


        // Creating items navigation
        mHelpLiveo = new br.liveo.model.HelpLiveo();
        mHelpLiveo.add("Home", R.drawable.home, 7);
        mHelpLiveo.add("My Jobs", R.drawable.myjobs);

        mHelpLiveo.add("Chat", R.drawable.chatic);
        mHelpLiveo.add("Notifications", R.drawable.notifications_button);
        // mHelpLiveo.addSeparator(); //Item separator
        mHelpLiveo.add("Transactions", R.drawable.payment_icon);
        mHelpLiveo.add("Account Details", R.drawable.account_icon, 120);

        mHelpLiveo.add("Reviews", R.drawable.star_icon, 120);

        mHelpLiveo.add("Statistics", R.drawable.staticsicon, 120);


        mHelpLiveo.add("Change Password", R.drawable.lock, 120);
        mHelpLiveo.add("About Us", R.drawable.abut_icon, 120);
        mHelpLiveo.add("Privacy Policy", R.drawable.aggre, 120);
        mHelpLiveo.add("Logout", R.drawable.logout_icon, 120);


        //{optional} - Header Customization - method customHeader
        //View mCustomHeader = getLayoutInflater().inflate(R.layout.custom_header_user, this.getListView(), false);
        //ImageView imageView = (ImageView) mCustomHeader.findViewById(R.id.imageView);

        with(this).startingPosition(0) //Starting position in the list
                .addAllHelpItem(mHelpLiveo.getHelp())
                .colorItemSelected(R.color.nliveo_blue_colorPrimary) //State the name of the color, icon and meter when it is selected


                .setOnClickUser(onClickPhoto)
                .setOnPrepareOptionsMenu(onPrepare)


                .build();

        int position = this.getCurrentPosition();
        this.setElevationToolBar(position != 2 ? 15 : 0);
    }

    @Override
    public void onItemClick(int position) {
        Fragment mFragment = null;
        FragmentManager mFragmentManager = getSupportFragmentManager();
        Intent intent;
        switch (position) {


            case 11:

                cd = new ConnectionDetector(Navigationdrawer.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    postRequest_ModeChange("gcm");
                } else {
                    HNDHelper.showErrorAlert(Navigationdrawer.this, getResources().getString(R.string.action_no_internet_message));
                }


                break;

            case 1:

                intent = new Intent(Navigationdrawer.this, MyJobsActivity.class);
                intent.putExtra("status", "0");
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;


            case 2:

                intent = new Intent(Navigationdrawer.this, GetMessageChatActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;


            case 3:

                intent = new Intent(Navigationdrawer.this, NotificationMenuActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

            case 4:

                intent = new Intent(Navigationdrawer.this, TransactionMenuActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

            case 5:

                intent = new Intent(Navigationdrawer.this, BankDetailsActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;


            case 6:

                intent = new Intent(Navigationdrawer.this, ReviewMenuActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

            case 7:

                intent = new Intent(Navigationdrawer.this, StatisticsPage.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;


            case 8:

                intent = new Intent(Navigationdrawer.this, ChangePasswordActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

            case 9:

                intent = new Intent(Navigationdrawer.this, AboutUsActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

            case 10:

                intent = new Intent(Navigationdrawer.this, termsprivacynewone.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;


            default:
                mFragment = DashboardFragment.newInstance(mHelpLiveo.get(position).getName());
                break;
        }

        if (mFragment != null) {
            mFragmentManager.beginTransaction().replace(R.id.container, mFragment).commit();
        }

        setElevationToolBar(position != 2 ? 15 : 0);
    }

    private OnPrepareOptionsMenuLiveo onPrepare = new OnPrepareOptionsMenuLiveo() {
        @Override
        public void onPrepareOptionsMenu(Menu menu, int position, boolean visible) {
        }
    };

    private View.OnClickListener onClickPhoto = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Navigationdrawer.this, ProfilePage.class);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            // Toast.makeText(getApplicationContext(), "onClickPhoto :D", Toast.LENGTH_SHORT).show();
            closeDrawer();
        }
    };

    private View.OnClickListener onClickFooter = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            closeDrawer();
        }
    };


    @Override
    protected void onResume() {
        super.onResume();


        Intent intent = new Intent(Navigationdrawer.this, ChatMessageServicech.class);
        startService(intent);


        SharedPreferences pref1 = getApplicationContext().getSharedPreferences("openclosed", 0);
        SharedPreferences.Editor editor1 = pref1.edit();
        editor1.putString("status", "1");
        editor1.apply();
        editor1.commit();

        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);


        String provider_image = pref.getString("provider_image", "");

        System.out.println("uaerimE" + provider_image);

        // User Information

        this.userPhoto.setImageResource(R.drawable.ic_no_user);


        if (provider_image.isEmpty()) {
            this.userPhoto.setImageResource(R.drawable.ic_no_user);
        } else {
            Picasso.with(Navigationdrawer.this)
                    .load(provider_image)
                    .placeholder(R.drawable.ic_no_user)   // optional
                    .error(R.drawable.ic_no_user)      // optional
                    // optional
                    .into(this.userPhoto);
        }


    }


/*    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences pref1 = getApplicationContext().getSharedPreferences("openclosed", 0);
        SharedPreferences.Editor editor1 = pref1.edit();
        editor1.putString("status", "0");
        editor1.apply();
        editor1.commit();


    }*/


    private void postRequest_ModeChange(final String aModeType) {


        myDialog = new ProgressDialog1(Navigationdrawer.this);
        myDialog.setCancelable(false);
        myDialog.setCanceledOnTouchOutside(false);

        if (!myDialog.isShowing()) {
            myDialog.show();
        }


        HashMap<String, String> jsonParams = new HashMap<String, String>();

        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", 0);
        String provider_id = pref.getString("provider_id", "");
        jsonParams.put("provider_id", provider_id);
        jsonParams.put("device_type", "android");


        ServiceRequest mRequest = new ServiceRequest(Navigationdrawer.this);
        mRequest.makeServiceRequest(ServiceConstant.logout_url, Request.Method.POST,
                jsonParams, new ServiceRequest.ServiceListener() {
                    @Override
                    public void onCompleteListener(String response) {

                        if (myDialog.isShowing()) {
                            myDialog.dismiss();
                        }

                        System.out.println("---------------Mode Response-----------------" + response);
                        String sStatus = "", sResponse = "";
                        try {

                            JSONObject object = new JSONObject(response);
                            sStatus = object.getString("status");
                        } catch (JSONException e) {

                            if (myDialog.isShowing()) {
                                myDialog.dismiss();
                            }

                            e.printStackTrace();
                        }

                        SharedPreferences pref1ss = getApplicationContext().getSharedPreferences("available", MODE_PRIVATE);
                        SharedPreferences.Editor editor1ss = pref1ss.edit();
                        editor1ss.putString("status", "out");
                        editor1ss.apply();
                        editor1ss.commit();


                        if (sStatus.equalsIgnoreCase("1")) {

                            SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();

                            editor.remove("provider_id");
                            editor.clear();
                            editor.apply();

                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("EXIT", true);
                            startActivity(intent);
                            finish();
                        } else {
                            SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();

                            editor.remove("provider_id");
                            editor.clear();
                            editor.apply();

                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("EXIT", true);
                            startActivity(intent);
                            finish();
                        }
                    }

                    @Override
                    public void onErrorListener() {

                        if (myDialog.isShowing()) {
                            myDialog.dismiss();
                        }
                    }
                });
    }


    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


}

