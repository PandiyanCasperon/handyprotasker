package noman.handyprotaskerapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.ArrayList;

import Utils.AutoStartHelper;
import Utils.SessionManager;
import noman.handyprotaskerapp.activity.BaseActivity;
import noman.handyprotaskerapp.activity.EstimateBuilderActivity;
import noman.handyprotaskerapp.activity.GetMessageChatActivity;
import noman.handyprotaskerapp.activity.HelperOngoingPageActivity;
import noman.handyprotaskerapp.activity.LoginActivity;
import noman.handyprotaskerapp.activity.MyJobs_OnGoingDetailActivity;
import noman.handyprotaskerapp.activity.WeeklyCalendarActivity;

public class SplashActivity extends BaseActivity {

    private final String BRAND_XIAOMI = "xiaomi";
    private final String BRAND_LETV = "letv";
    private final String BRAND_ASUS = "asus";
    private final String BRAND_HONOR = "honor";
    private final String BRAND_OPPO = "oppo";
    private final String BRAND_VIVO = "vivo";
    private final String BRAND_NOKIA = "nokia";

    private SessionManager mySession;
    private static int SPLASH_TIME = 2500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mySession = new SessionManager(this);
        notificationdb mHelper = new notificationdb(this);
        int count = mHelper.getProfilesCount();
        if (count > 100) {
            this.deleteDatabase(mHelper.DATABASE_NAME);
            System.out.println("Sara response--->fcm-deleted");
        }

//        CreateURLDialog();


    }

    public void forceUpdate() {
        PackageManager packageManager = this.getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String currentVersion = packageInfo.versionName;
//        new ForceUpdateAsync(currentVersion,getApplicationContext()).execute();


        if (mySession.getAutoStart()) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    CheckBundle();
                }
            }, SPLASH_TIME);
        } else {
            String device_company = Build.BRAND.toLowerCase();
            if (device_company.equals(BRAND_ASUS) || device_company.equals(BRAND_HONOR) || device_company.equals(BRAND_LETV) || device_company.equals(BRAND_XIAOMI) || device_company.equals(BRAND_OPPO) || device_company.equals(BRAND_VIVO) || device_company.equals(BRAND_NOKIA))
                AutoStartHelper.getInstance(SplashActivity.this).getAutoStartPermission(SplashActivity.this);
            else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        CheckBundle();
                        /*Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                        SplashActivity.this.startActivity(mainIntent);
                        SplashActivity.this.finish();*/
                    }
                }, SPLASH_TIME);
            }
        }
    }

    public class ForceUpdateAsync extends AsyncTask<String, String, JSONObject> {

        private String latestVersion;
        private String currentVersion;
        private Context context;

        public ForceUpdateAsync(String currentVersion, Context context) {
            this.currentVersion = currentVersion;
            this.context = context;
        }

        @Override
        protected JSONObject doInBackground(String... params) {

            try {
                latestVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=noman.handyprotaskerapp&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                        .first()
                        .ownText();

            } catch (IOException e) {
                e.printStackTrace();
            }
            return new JSONObject();
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            if (latestVersion != null) {
                if (!currentVersion.equalsIgnoreCase(latestVersion)) {
                    // Toast.makeText(context,"update is available.", Toast.LENGTH_LONG).show();
                    // showForceUpdateDialog();
                    AlertDialog.Builder builder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        builder = new AlertDialog.Builder(SplashActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                    } else {
                        builder = new AlertDialog.Builder(SplashActivity.this);
                    }
                    builder.setCancelable(false);

                    builder.setTitle("Update Pending...")
                            .setMessage("New version of app available please update for more option?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    try {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=noman.handyprotaskerapp")));
                                    } catch (android.content.ActivityNotFoundException anfe) {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=noman.handyprotaskerapp")));
                                    }
                                    finish();

                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                    Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                                    SplashActivity.this.startActivity(mainIntent);
                                    SplashActivity.this.finish();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                } else {
                    Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                    SplashActivity.this.startActivity(mainIntent);
                    SplashActivity.this.finish();
                }
            }
            super.onPostExecute(jsonObject);
        }
    }

    void CreateURLDialog() {
        SharedPreferences shared = getSharedPreferences("BaseURL", MODE_PRIVATE);
        String channel = shared.getString("URL", "");
        if (channel.equals("")) {
            final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(SplashActivity.this);
            // Get the layout inflater
            android.view.LayoutInflater inflater = SplashActivity.this.getLayoutInflater();
            // Inflate the layout for the dialog
            // Pass null as the parent view because its going in the dialog layout
            android.view.View RequestURLView = inflater.inflate(R.layout.dialog_custom_url, null);
            // Get your views by using view.findViewById() here and do your listeners.
            final EditText EditTextURL = RequestURLView.findViewById(R.id.EditTextURL);
            Button ButtonSave = RequestURLView.findViewById(R.id.ButtonSave);

            // Set the dialog layout
            builder.setView(RequestURLView);
            builder.create();
            final androidx.appcompat.app.AlertDialog alertDialog = builder.show();

            ButtonSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (EditTextURL.getText().toString().length() > 0) {
                        SharedPreferences.Editor editor = getSharedPreferences("BaseURL", MODE_PRIVATE).edit();
                        editor.putString("URL", EditTextURL.getText().toString());
                        editor.apply();
                        Toast.makeText(SplashActivity.this, EditTextURL.getText().toString(), Toast.LENGTH_SHORT).show();
                        alertDialog.dismiss();
                    }

                }
            });

        } else {
            forceUpdate();
        }
    }

    void CheckBundle(){
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("data") && bundle.getString("data") != null) {
            JSONObject object1;
            try {
                String usernamee, Title = "";
                ArrayList<String> PrepaymentList = new ArrayList<>();;
                object1 = new JSONObject(bundle.getString("data"));
                if (object1.has("action")) {
                    String actionnew = object1.getString("action");
                    String Job_id = object1.getString("key0");
                    String jobid = object1.getString("key0");
                    String Message = object1.getString("message");

                    if (actionnew.equalsIgnoreCase("job_request") || actionnew.equalsIgnoreCase("change_order_notaccepted") || actionnew.equalsIgnoreCase("change_order_accepted") || actionnew.equalsIgnoreCase("estimation_accepted") || actionnew.equalsIgnoreCase("estimation_notaccepted")) {
                        if (object1.has("key3")) {
                            usernamee = object1.getString("key3");
                        }

                    } else if (actionnew.equalsIgnoreCase("job_accepted")) {
                        if (object1.has("key2")) {
                            usernamee = object1.getString("key2");
                        }
                    } else if (actionnew.equalsIgnoreCase("job_rescheduled")) {
                        if (object1.has("key3")) {
                            usernamee = object1.getString("key3");
                        }
                    } else if (actionnew.equalsIgnoreCase("job_photo_status")) {
                        if (object1.has("key2")) {
                            usernamee = object1.getString("key2");
                        }
                    } else if (actionnew.equalsIgnoreCase("job_request_now")) {
                        if (object1.has("key6")) {
                            usernamee = object1.getString("key6");
                        }
                    } else if (actionnew.equalsIgnoreCase("cash_initiated")) {
                        if (object1.has("key2")) {
                            usernamee = object1.getString("key2");
                        }
                    } else if (actionnew.equalsIgnoreCase("pre_payment_cash_initiated")) {
                        if (object1.has("key2")) {
                            usernamee = object1.getString("key2");
                        }

                        if (object1.has("action") && object1.has("key3")) {
                            if (object1.getString("action").equals("pre_payment_cash_initiated")) {
                                PrepaymentList = new ArrayList<>();
                                JSONArray jsonArray = object1.getJSONArray("key3");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    PrepaymentList.add(jsonArray.get(i).toString());
                                }
                            }
                        }
                    } else if (actionnew.equalsIgnoreCase("payment_paid")) {
                        if (object1.has("key2")) {
                            usernamee = object1.getString("key2");
                        }
                    } else if (actionnew.equalsIgnoreCase("prepayment_rejected")) {
                        if (object1.has("key2")) {
                            usernamee = object1.getString("key2");
                        }
                    } else {
                        usernamee = "";
                    }


                    if (actionnew.equalsIgnoreCase("admin_notification")) {
                        Title = "ADMIN NOTIFICATION";
                    } else {
                        if (actionnew.equalsIgnoreCase("job_request") && object1.has("key4")) {
                            if (object1.getString("key4").equalsIgnoreCase("Job")) {
                                Title = "JOB ID:" + Job_id;
                            } else if (object1.getString("key4").equalsIgnoreCase("Estimate")) {
                                Title = "ESTIMATION ID:" + Job_id;
                            }

                        } else if (actionnew.equalsIgnoreCase("job_accepted") && object1.has("key3")) {
                            if (object1.getString("key3").equalsIgnoreCase("Job")) {
                                Title = "JOB ID:" + Job_id;
                            } else if (object1.getString("key3").equalsIgnoreCase("Estimate")) {
                                Title = "ESTIMATION ID:" + Job_id;
                            }

                        } else {
                            Title = "JOB ID:" + Job_id;
                        }

                    }


                    SharedPreferences Userpref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
                    String username = Userpref.getString("provider_name", "");

                    if (actionnew.equalsIgnoreCase("job_request")) {
                        if (Message.contains("You have been assigned for a job")) {
                            Message = getResources().getString(R.string.jobre);
                        }
                    } else if (actionnew.equalsIgnoreCase("job_accepted")) {
                        if (Message.contains("You are assigned to the Job")) {
                            Message = getResources().getString(R.string.jobre);
                        }
                    } else if (actionnew.equalsIgnoreCase("estimation_accepted")) {

                        Message = getResources().getString(R.string.estaccepted);

                    } else if (actionnew.equalsIgnoreCase("job_rescheduled")) {
                        Message = getResources().getString(R.string.ree);
                    } else if (actionnew.equalsIgnoreCase("estimation_notaccepted")) {

                        Message = getResources().getString(R.string.estrejected);

                    } else if (actionnew.equalsIgnoreCase("job_photo_status")) {
                        if (Message.contains("User approved the job")) {
                            Message = getResources().getString(R.string.jobapprove);
                        }


                    } else if (actionnew.equalsIgnoreCase("cash_initiated")) {

                        if (Message.contains("User")) {
                            Message = Message.replace("User", "client");
                        }

                    } else if (actionnew.equalsIgnoreCase("pre_payment_cash_initiated")) {
                        if (Message.contains("User")) {
                            Message = Message.replace("User", "client");
                        }
                    } else if (actionnew.equalsIgnoreCase("prepayment_rejected")) {
                        if (Message.contains("User")) {
                            Message = Message.replace("User", "client");
                        }
                    }

                    Message = "Hi " + username + "!. " + Message;
//                Textview_alert_header.setText(getResources().getString(R.string.notif_from_jobid) + "\n" + usernamee);


                    if (actionnew.equalsIgnoreCase("payment_paid")) {


                        Title = "Well Done " + username + "!";

                        SpannableStringBuilder builders = new SpannableStringBuilder();
                        String red = "Thank You for continuing to build trust in the";
                        SpannableString redSpannable = new SpannableString(red);
                        redSpannable.setSpan(new ForegroundColorSpan(Color.BLACK), 0, red.length(), 0);
                        builders.append(redSpannable);

                        String white = " HANDY";
                        SpannableString whiteSpannable = new SpannableString(white);
                        whiteSpannable.setSpan(new ForegroundColorSpan(Color.BLUE), 0, white.length(), 0);
                        builders.append(whiteSpannable);

                        String blue = "PRO ";
                        SpannableString blueSpannable = new SpannableString(blue);
                        blueSpannable.setSpan(new ForegroundColorSpan(Color.RED), 0, blue.length(), 0);
                        builders.append(blueSpannable);

                        String Craftsman = "Brand.";
                        SpannableString blueSpannableCraftsman = new SpannableString(Craftsman);
                        blueSpannableCraftsman.setSpan(new ForegroundColorSpan(Color.BLACK), 0, blue.length(), 0);
                        builders.append(blueSpannableCraftsman);
                        Message = builders.toString();
                    }
                    /*commonTrayNotification(Job_id, Title, Message);*/
                    String value = Title;

                    if (actionnew.equalsIgnoreCase("job_request")) {

                        SharedPreferences presf1 = getApplicationContext().getSharedPreferences("needrefresh", 0);
                        SharedPreferences.Editor editosr1 = presf1.edit();
                        editosr1.putString("yesno", "2");
                        editosr1.apply();


                        SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("page", "1");
                        editor.apply();


                        System.out.println("dispaly jobid-------->" + jobid);
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("jobid", Job_id);
                        prefeditor.apply();

                        Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (actionnew.equalsIgnoreCase("job_request_now")) {

                        Intent intent = new Intent(getApplicationContext(), newjobrequesttimer.class);
                        intent.putExtra("valuefortimer", value);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    } else if (actionnew.equalsIgnoreCase("admin_notification")) {
                        finish();
                    } else if (actionnew.equalsIgnoreCase("helper_job_actions")) {

                        SharedPreferences presf1 = getApplicationContext().getSharedPreferences("needrefresh", 0);
                        SharedPreferences.Editor editosr1 = presf1.edit();
                        editosr1.putString("yesno", "2");
                        editosr1.apply();


                        SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("page", "1");
                        editor.apply();


                        System.out.println("dispaly jobid-------->" + jobid);
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("jobid", Job_id);
                        prefeditor.apply();

                        Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (actionnew.equalsIgnoreCase("assign_helper")) {

                        SharedPreferences presf1 = getApplicationContext().getSharedPreferences("needrefresh", 0);
                        SharedPreferences.Editor editosr1 = presf1.edit();
                        editosr1.putString("yesno", "2");
                        editosr1.apply();


                        SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("page", "1");
                        editor.apply();


                        System.out.println("dispaly jobid-------->" + jobid);
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("jobid", Job_id);
                        prefeditor.apply();

                        Intent intent = new Intent(getApplicationContext(), HelperOngoingPageActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (actionnew.equalsIgnoreCase("estimation_pending")) {


                        SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("page", "1");
                        editor.apply();


                        System.out.println("dispaly jobid-------->" + jobid);
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("jobid", Job_id);
                        prefeditor.apply();


                        Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                        startActivity(intent);
                        finish();


                    } else if (actionnew.equalsIgnoreCase("estimation_notaccepted")) {
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("jobid", Job_id);
                        prefeditor.apply();

                        SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("page", "1");
                        editor.apply();


//                    Intent intent = new Intent(getApplicationContext(), EstimateBuilderActivity.class);
                        Intent intent = new Intent(getApplicationContext(), WeeklyCalendarActivity.class);
                        startActivity(intent);
                        finish();


                    } else if (actionnew.equalsIgnoreCase("change_order_notaccepted")) {

                        SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("page", "1");
                        editor.apply();


                        System.out.println("dispaly jobid-------->" + jobid);
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("jobid", Job_id);
                        prefeditor.apply();


                        Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                        startActivity(intent);
                        finish();

                    } else if (actionnew.equalsIgnoreCase("cash_initiated") || actionnew.equalsIgnoreCase("pre_payment_cash_initiated")) {
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("jobid", Job_id);
                        prefeditor.apply();


                        Intent intent = new Intent(getApplicationContext(), OtpPage.class);
                        intent.putExtra("jobId", Job_id);
                        intent.putExtra("Type", actionnew);
                        if (actionnew.equalsIgnoreCase("pre_payment_cash_initiated")) {
                            if (PrepaymentList != null && PrepaymentList.size() > 0)
                            intent.putStringArrayListExtra("PrepaymentList", PrepaymentList);
                        }
                        startActivity(intent);
                        finish();


                    } else if (actionnew.equalsIgnoreCase("helper_assigned")) {
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("page", "1");
                        editor.apply();


                        System.out.println("dispaly jobid-------->" + jobid);
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("jobid", Job_id);
                        prefeditor.apply();


                        Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                        startActivity(intent);
                        finish();


                    } else if (actionnew.equalsIgnoreCase("change_order_accepted")) {
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("page", "1");
                        editor.apply();


                        System.out.println("dispaly jobid-------->" + jobid);
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("jobid", Job_id);
                        prefeditor.apply();

                        Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                        startActivity(intent);
                        finish();


                    } else if (actionnew.equalsIgnoreCase("job_cancelled")) {


                        SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("page", "1");
                        editor.apply();


                        System.out.println("dispaly jobid-------->" + jobid);
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("jobid", Job_id);
                        prefeditor.apply();

                        Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                        startActivity(intent);
                        finish();


                    } else if (actionnew.equalsIgnoreCase("payment_paid")) {
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("page", "1");
                        editor.apply();


                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("jobid", Job_id);
                        prefeditor.apply();


                        Intent intent = new Intent(SplashActivity.this, ReviwesPage.class);
                        intent.putExtra("jobId", Job_id);
                        startActivity(intent);
                        finish();

                    } else if (actionnew.equalsIgnoreCase("pre_payment_paid")) {
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("page", "1");
                        editor.apply();

                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("jobid", Job_id);
                        prefeditor.apply();

                        Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (actionnew.equalsIgnoreCase("estimation_accepted")) {
                        System.out.println("dispaly jobid-------->" + jobid);
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("jobid", Job_id);
                        prefeditor.apply();

                        SharedPreferences pref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("jobid", Job_id);
                        editor.apply();

                        Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("EXIT", true);
                        startActivity(intent);
                        finish();

                    } else if (actionnew.equalsIgnoreCase("job_photo_status")) {

                        SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("page", "1");
                        editor.apply();

                        System.out.println("dispaly jobid-------->" + jobid);
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("jobid", Job_id);
                        prefeditor.apply();

                        Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                        startActivity(intent);
                        finish();


                    } else if (actionnew.equalsIgnoreCase("job_accepted")) {


                        SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("page", "1");
                        editor.apply();


                        System.out.println("dispaly jobid-------->" + jobid);
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("jobid", Job_id);
                        prefeditor.apply();


                        Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (actionnew.equalsIgnoreCase("start_off")) {

                        SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("page", "1");
                        editor.apply();

                        System.out.println("dispaly jobid-------->" + jobid);
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("jobid", Job_id);
                        prefeditor.apply();


                        Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (actionnew.equalsIgnoreCase("provider_reached")) {

                        SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("page", "1");
                        editor.apply();

                        System.out.println("dispaly jobid-------->" + jobid);
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("jobid", Job_id);
                        prefeditor.apply();

                        Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (actionnew.equalsIgnoreCase("job_started")) {


                        System.out.println("dispaly jobid-------->" + jobid);
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("jobid", Job_id);
                        prefeditor.apply();

                        SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("page", "1");
                        editor.apply();

                        Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (actionnew.equalsIgnoreCase("job_rescheduled")) {


                        SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("page", "1");
                        editor.apply();


                        System.out.println("dispaly jobid-------->" + jobid);
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("jobid", Job_id);
                        prefeditor.apply();

                        Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (actionnew.equalsIgnoreCase("job_completed")) {


                        SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("page", "1");
                        editor.apply();


                        System.out.println("dispaly jobid-------->" + jobid);
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("jobid", Job_id);
                        prefeditor.apply();

                        Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (actionnew.equalsIgnoreCase("change_order_request")) {

                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("jobid", Job_id);
                        prefeditor.apply();

                        Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (actionnew.equalsIgnoreCase("estimation_received")) {

                        SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("page", "1");
                        editor.apply();

                        System.out.println("dispaly jobid-------->" + jobid);
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("jobid", Job_id);
                        prefeditor.apply();

                        Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (actionnew.equalsIgnoreCase("chat")) {
                        Intent intent = new Intent(getApplicationContext(), GetMessageChatActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (actionnew.equalsIgnoreCase("prepayment_rejected")) {
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("page", "1");
                        editor.apply();

                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("jobid", Job_id);
                        prefeditor.apply();

                        Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (actionnew.equalsIgnoreCase("subitem_rejected")) {
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("jobid", Job_id);
                        editor.apply();

                        Intent intent = new Intent(getApplicationContext(), EstimateBuilderActivity.class);
                        startActivity(intent);
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }else {
            Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
            SplashActivity.this.startActivity(mainIntent);
            SplashActivity.this.finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        forceUpdate();
    }
}







