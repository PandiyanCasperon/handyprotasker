package noman.handyprotaskerapp.Pojo;

public class WeekViewPojo {

    private String job_id;

    private String user_name;

    private String booking_time_std;

    private String color;

    private String role;

    private String FromTime;

    private String ToTime;

    /*public WeekViewPojo(String job_id, String user_name, String booking_time_std, String role, String color, String FromTime, String ToTime) {
        this.job_id = job_id;
        this.user_name = user_name;
        this.booking_time_std = booking_time_std;
        this.role = role;
        this.color = color;
        this.FromTime = FromTime;
        this.ToTime = ToTime;
    }*/

    public WeekViewPojo() {
    }

    public WeekViewPojo(String job_id, String user_name, String booking_time_std, String color, String role, String fromTime, String toTime, Not_working_hours[] not_working_hours) {
        this.job_id = job_id;
        this.user_name = user_name;
        this.booking_time_std = booking_time_std;
        this.color = color;
        this.role = role;
        FromTime = fromTime;
        ToTime = toTime;
        this.not_working_hours = not_working_hours;
    }

    public String getFromTime() {
        return FromTime;
    }

    public void setFromTime(String fromTime) {
        FromTime = fromTime;
    }

    public String getToTime() {
        return ToTime;
    }

    public void setToTime(String toTime) {
        ToTime = toTime;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getBooking_time_std() {
        return booking_time_std;
    }

    public void setBooking_time_std(String booking_time_std) {
        this.booking_time_std = booking_time_std;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getJob_id() {
        return job_id;
    }

    public void setJob_id(String job_id) {
        this.job_id = job_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    private Not_working_hours[] not_working_hours;

    public Not_working_hours[] getNot_working_hours ()
    {
        return not_working_hours;
    }

    public void setNot_working_hours (Not_working_hours[] not_working_hours)
    {
        this.not_working_hours = not_working_hours;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [not_working_hours = "+not_working_hours+"]";
    }

}
