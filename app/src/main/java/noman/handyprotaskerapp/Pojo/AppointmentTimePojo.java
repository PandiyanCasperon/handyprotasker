package noman.handyprotaskerapp.Pojo;

/**
 * Created by CAS63 on 2/23/2018.
 */

public class AppointmentTimePojo {
    private String AppointmentTime = "";
    private String Selected = "";
    private String Enabled = "";

    public String getEnabled() {
        return Enabled;
    }

    public void setEnabled(String enabled) {
        Enabled = enabled;
    }

    public String getAppointmentTime() {
        return AppointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        AppointmentTime = appointmentTime;
    }

    public String getSelected() {
        return Selected;
    }

    public void setSelected(String selected) {
        Selected = selected;
    }
}
