package noman.handyprotaskerapp.Pojo;

public class Not_working_hours {
    private String date;

    private String ToTime;

    private String navid;

    private String kind;

    private String from;

    private String _id;

    private String to;

    private String memo_description;

    private String day;

    private String FromTime;

    public String getDate ()
    {
        return date;
    }

    public void setDate (String date)
    {
        this.date = date;
    }

    public String getToTime ()
    {
        return ToTime;
    }

    public void setToTime (String ToTime)
    {
        this.ToTime = ToTime;
    }

    public String getNavid ()
    {
        return navid;
    }

    public void setNavid (String navid)
    {
        this.navid = navid;
    }

    public String getKind ()
    {
        return kind;
    }

    public void setKind (String kind)
    {
        this.kind = kind;
    }

    public String getFrom ()
    {
        return from;
    }

    public void setFrom (String from)
    {
        this.from = from;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public String getTo ()
    {
        return to;
    }

    public void setTo (String to)
    {
        this.to = to;
    }

    public String getMemo_description ()
    {
        return memo_description;
    }

    public void setMemo_description (String memo_description)
    {
        this.memo_description = memo_description;
    }

    public String getDay ()
    {
        return day;
    }

    public void setDay (String day)
    {
        this.day = day;
    }

    public String getFromTime ()
    {
        return FromTime;
    }

    public void setFromTime (String FromTime)
    {
        this.FromTime = FromTime;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [date = "+date+", ToTime = "+ToTime+", navid = "+navid+", kind = "+kind+", from = "+from+", _id = "+_id+", to = "+to+", memo_description = "+memo_description+", day = "+day+", FromTime = "+FromTime+"]";
    }
}
