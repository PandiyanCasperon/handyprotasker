package noman.handyprotaskerapp.Pojo;

/**
 * Created by user127 on 04-05-2018.
 */

public class DataPojo {

    String name;
    String type;
    String version_number;
    String feature;

    public DataPojo(String name, String type, String version_number, String feature ) {
        this.name=name;
        this.type=type;
        this.version_number=version_number;
        this.feature=feature;

    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getVersion_number() {
        return version_number;
    }

    public String getFeature() {
        return feature;
    }

}
