package noman.handyprotaskerapp.Pojo;

import android.text.Spannable;
import android.widget.EditText;

public class SubItemUpdatePojo {
    private String Total;
    private float Hours;
    private EditText editText;

    public String getTotal() {
        return Total;
    }

    public void setTotal(String total) {
        Total = total;
    }

    public float getHours() {
        return Hours;
    }

    public void setHours(float hours) {
        Hours = hours;
    }

    public String getEditText() {
        return editText.getText().toString();
    }

    public void setEditText(String editTextValue) {
        this.editText.setText(editTextValue);
    }
}
