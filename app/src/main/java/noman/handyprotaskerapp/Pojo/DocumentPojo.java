package noman.handyprotaskerapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class DocumentPojo {


    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("documentLists")
    @Expose
    private ArrayList<DocumentListsItem> documentLists = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public ArrayList<DocumentListsItem> getDocumentLists() {
        return documentLists;
    }

    public void setDocumentLists(ArrayList<DocumentListsItem> documentLists) {
        this.documentLists = documentLists;
    }
}
