package noman.handyprotaskerapp.Pojo;

/**
 * Created by user127 on 13-02-2018.
 */



/**
 * Created by user127 on 13-02-2018.
 */

public class Movie1Pojo {
    private String title, genre, year;

    public Movie1Pojo() {
    }

    public Movie1Pojo(String title, String genre, String year) {
        this.title = title;
        this.genre = genre;
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
