package noman.handyprotaskerapp.Pojo;


/**
 * Created by CAS63 on 3/12/2018.
 */

public class joblistpojo {
    private String OrderId = "";
    private String OrderTime = "";
    private String OrderServiceType = "";
    private String OrderServiceIcon = "";
    private String OrderBookingDate = "";
    private String OrderDate = "";
    private String OrderStatus = "";


    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }




    public String getOrderTime() {
        return OrderTime;
    }

    public void setOrderTime(String orderTime) {
        OrderTime = orderTime;
    }

    public String getOrderServiceType() {
        return OrderServiceType;
    }

    public void setOrderServiceType(String orderServiceType) {
        OrderServiceType = orderServiceType;
    }

    public String getOrderServiceIcon() {
        return OrderServiceIcon;
    }

    public void setOrderServiceIcon(String orderServiceIcon) {
        OrderServiceIcon = orderServiceIcon;
    }

    public String getOrderBookingDate() {
        return OrderBookingDate;
    }

    public void setOrderBookingDate(String orderBookingDate) {
        OrderBookingDate = orderBookingDate;
    }

    public String getOrderDate() {
        return OrderDate;
    }

    public void setOrderDate(String orderDate) {
        OrderDate = orderDate;
    }

    public String getOrderStatus() {
        return OrderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        OrderStatus = orderStatus;
    }


}

