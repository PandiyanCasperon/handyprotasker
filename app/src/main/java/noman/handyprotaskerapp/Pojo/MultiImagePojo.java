package noman.handyprotaskerapp.Pojo;

public class MultiImagePojo {
    private String ImagePath;
    private int PositionForOperation;
    private String ImageName;
    private String FileType;

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }

    public int getPositionForOperation() {
        return PositionForOperation;
    }

    public void setPositionForOperation(int positionForOperation) {
        PositionForOperation = positionForOperation;
    }

    public String getImageName() {
        return ImageName;
    }

    public void setImageName(String imageName) {
        ImageName = imageName;
    }

    public String getFileType() {
        return FileType;
    }

    public void setFileType(String fileType) {
        FileType = fileType;
    }
}
