package noman.handyprotaskerapp.Pojo;

/**
 * Created by user127 on 18-04-2018.
 */

public class BeforeImagePojo {
    private String title;
    private String textname;

    public BeforeImagePojo() {
    }

    public BeforeImagePojo(String title, String textname) {
        this.title = title;
        this.textname = textname;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }



    public String getTextname() {
        return textname;
    }

    public void setTextname(String textname) {
        this.textname = textname;
    }


}