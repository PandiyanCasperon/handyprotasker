package noman.handyprotaskerapp.Pojo;

public class FranchiseDescPojo {

    String FranchiseDescription ="";
    String FranchiseId ="";
    String FranchiseStartTime ="";
    String FranchiseEndTime ="";
    String FranchiseStartIndex = "";
    String FranchiseEndIdex ="";
    String FranchsiseHours ="";
    String FranchiseDate ="";
    String FranchiseTasker ="";
    String FranchiseTaskerName ="";

    public String getFranchiseDescription() {
        return FranchiseDescription;
    }

    public void setFranchiseDescription(String franchiseDescription) {
        FranchiseDescription = franchiseDescription;
    }

    public String getFranchiseId() {
        return FranchiseId;
    }

    public void setFranchiseId(String franchiseId) {
        FranchiseId = franchiseId;
    }

    public String getFranchiseStartTime() {
        return FranchiseStartTime;
    }

    public void setFranchiseStartTime(String franchiseStartTime) {
        FranchiseStartTime = franchiseStartTime;
    }

    public String getFranchiseEndTime() {
        return FranchiseEndTime;
    }

    public void setFranchiseEndTime(String franchiseEndTime) {
        FranchiseEndTime = franchiseEndTime;
    }

    public String getFranchiseStartIndex() {
        return FranchiseStartIndex;
    }

    public void setFranchiseStartIndex(String franchiseStartIndex) {
        FranchiseStartIndex = franchiseStartIndex;
    }

    public String getFranchiseEndIdex() {
        return FranchiseEndIdex;
    }

    public void setFranchiseEndIdex(String franchiseEndIdex) {
        FranchiseEndIdex = franchiseEndIdex;
    }

    public String getFranchsiseHours() {
        return FranchsiseHours;
    }

    public void setFranchsiseHours(String franchsiseHours) {
        FranchsiseHours = franchsiseHours;
    }

    public String getFranchiseDate() {
        return FranchiseDate;
    }

    public void setFranchiseDate(String franchiseDate) {
        FranchiseDate = franchiseDate;
    }

    public String getFranchiseTasker() {
        return FranchiseTasker;
    }

    public void setFranchiseTasker(String franchiseTasker) {
        FranchiseTasker = franchiseTasker;
    }

    public String getFranchiseTaskerName() {
        return FranchiseTaskerName;
    }

    public void setFranchiseTaskerName(String franchiseTaskerName) {
        FranchiseTaskerName = franchiseTaskerName;
    }

    public String getFranchiseScheduleStatus() {
        return FranchiseScheduleStatus;
    }

    public void setFranchiseScheduleStatus(String franchiseScheduleStatus) {
        FranchiseScheduleStatus = franchiseScheduleStatus;
    }

    String FranchiseScheduleStatus ="";


}
