package noman.handyprotaskerapp.Pojo;

/**
 * Created by CAS63 on 3/12/2018.
 */

public class MyOrdersPojo {
    private String OrderId = "";
    private String OrderTime = "";
    private String OrderServiceType = "";
    private String OrderServiceIcon = "";
    private String OrderBookingDate = "";
    private String OrderDate = "";
    private String OrderStatus = "";
    private String ContactNumber = "";
    private String doCall = "";
    private String isSupport = "";
    private String doMsg = "";
    private String doCancel = "";
    private String countrycode="";
    private String userphoto="";

    private String taskerid="";

    private String taskid="";


    private String taskname="";
    private String taskimage="";

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }


    public String getTaskname() {
        return taskname;
    }

    public void setTaskname(String Taskname) {
        taskname = Taskname;
    }



    public String getTaskimage() {
        return taskimage;
    }

    public void setTaskimage(String Taskimage) {
        taskimage = Taskimage;
    }

    public String getTaskid() {
        return taskid;
    }

    public void setTaskid(String Taskid) {
        taskid = Taskid;
    }


    public String getTaskerid() {
        return taskerid;
    }

    public void setTaskerid(String taskeridd) {
        taskerid = taskeridd;
    }


    public String getUserphoto() {
        return userphoto;
    }

    public void setUserphoto(String Userphoto) {
        userphoto = Userphoto;
    }

    public String getOrderTime() {
        return OrderTime;
    }

    public void setOrderTime(String orderTime) {
        OrderTime = orderTime;
    }

    public String getOrderServiceType() {
        return OrderServiceType;
    }

    public void setOrderServiceType(String orderServiceType) {
        OrderServiceType = orderServiceType;
    }

    public String getOrderServiceIcon() {
        return OrderServiceIcon;
    }

    public void setOrderServiceIcon(String orderServiceIcon) {
        OrderServiceIcon = orderServiceIcon;
    }

    public String getOrderBookingDate() {
        return OrderBookingDate;
    }

    public void setOrderBookingDate(String orderBookingDate) {
        OrderBookingDate = orderBookingDate;
    }

    public String getOrderDate() {
        return OrderDate;
    }

    public void setOrderDate(String orderDate) {
        OrderDate = orderDate;
    }

    public String getOrderStatus() {
        return OrderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        OrderStatus = orderStatus;
    }

    public String getContactNumber() {
        return ContactNumber;
    }

    public void setContactNumber(String contactNumber) {
        ContactNumber = contactNumber;
    }

    public String getDoCall() {
        return doCall;
    }

    public void setDoCall(String doCall) {
        this.doCall = doCall;
    }

    public String getIsSupport() {
        return isSupport;
    }

    public void setIsSupport(String isSupport) {
        this.isSupport = isSupport;
    }

    public String getDoMsg() {
        return doMsg;
    }

    public void setDoMsg(String doMsg) {
        this.doMsg = doMsg;
    }

    public String getDoCancel() {
        return doCancel;
    }

    public void setDoCancel(String doCancel) {
        this.doCancel = doCancel;
    }

    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }
}
