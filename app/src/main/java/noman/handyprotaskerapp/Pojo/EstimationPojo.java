package noman.handyprotaskerapp.Pojo;


import java.io.Serializable;
import java.util.List;

public class EstimationPojo implements Serializable
{

    private List<MultiSubItemPojo> multiSubItemPojo;

    private List<MultiImagePojo> multiImagePojos;

    private boolean CanDelete;

    private static final long serialVersionUID = -5435670920302756945L;
    private String val1 ="";

    private String helpertype ="";

    private String helperhour ="";


    private float val2 =0;

    private int etval2 =0;

    private int Checkedhelper =0;

    private String whatsincluded;

    private String Total = "0";
    private double helperTotal = 0;
    private String jobtype,Jobdescription,Materialdescription,Material,depositamount,deletetype ;

    private double fixTotal = 0;

    public String getVal1() {
        return val1;
    }

    public void setVal1(String val1) {
        this.val1 = val1;
    }


    public String getHelperhr() {
        return helperhour;
    }

    public void setHelperhr(String Helperhr) {
        this.helperhour = Helperhr;
    }



    public String getHelpertype() {
        return helpertype;
    }

    public void setHelpertype(String Helpertype) {
        this.helpertype = Helpertype;
    }

    public String getWhatsincluded() {
        return whatsincluded;
    }

    public void setWhatsincluded(String whatsincluded) {
        this.whatsincluded = whatsincluded;
    }

    public String getDepositamount() {
        return depositamount;
    }

    public void setDepositamount(String Depositamount) {
        this.depositamount = Depositamount;
    }

    public int getetval2() {
        return etval2;
    }

    public void setetval2(int Etval2) {
        this.etval2 = Etval2;
    }

    public String getTotal() {
        return Total;
    }

    public void setTotal(String total) {
        Total = total;
    }

    public double getfixTotal() {
        return fixTotal;
    }

    public void setfixTotal(Double fixtotal) {
        fixTotal = fixtotal;
    }



    public float getVal2() {
        return val2;
    }

    public void setVal2(float val2) {
        this.val2 = val2;
    }

    public int getCheckedhelper() {
        return Checkedhelper;
    }

    public void setCheckedhelper(int checkedhelper) {
        this.Checkedhelper = checkedhelper;
    }



    public double gethelperTotal() {
        return helperTotal;
    }

    public void sethelperTotal(Double helpertotal) {
        helperTotal = helpertotal;
    }



    public void setJobtype(String jobtype) {
        this.jobtype = jobtype;
    }

    public String getJobtype() {
        return jobtype;
    }




    public void setDeletetype(String deletetype) {
        this.deletetype = deletetype;
    }

    public String getDeletetype() {
        return deletetype;
    }


    public void setJobdescription(String Jobdescription)
    {
        this.Jobdescription = Jobdescription;
    }

    public String getJobdescription() {
        return Jobdescription;
    }



    public void setMaterialdescription(String Materialdescription)
    {
        this.Materialdescription = Materialdescription;
    }

    public String getMaterialdescription() {
        return Materialdescription;
    }

    public void setMaterial(String Material)
    {
        this.Material = Material;
    }

    public String getMaterial() {
        return Material;
    }

    public List<MultiSubItemPojo> getMultiSubItemPojo() {
        return multiSubItemPojo;
    }

    public void setMultiSubItemPojo(List<MultiSubItemPojo> multiSubItemPojo) {
        this.multiSubItemPojo = multiSubItemPojo;
    }

    public List<MultiImagePojo> getMultiImagePojos() {
        return multiImagePojos;
    }

    public void setMultiImagePojos(List<MultiImagePojo> multiImagePojos) {
        this.multiImagePojos = multiImagePojos;
    }

    public boolean isCanDelete() {
        return CanDelete;
    }

    public void setCanDelete(boolean canDelete) {
        CanDelete = canDelete;
    }



}

