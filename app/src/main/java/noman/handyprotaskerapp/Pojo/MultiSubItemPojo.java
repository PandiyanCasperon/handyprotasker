package noman.handyprotaskerapp.Pojo;

public class MultiSubItemPojo {
    private String Name;
    private String Hours;
    private String Description;
    private String ID;
    private String Status;
    private int InnerPosition;
    private int ParentEstimationStatus;
    private String Amount;

    public int getParentEstimationStatus() {
        return ParentEstimationStatus;
    }

    public void setParentEstimationStatus(int parentEstimationStatus) {
        ParentEstimationStatus = parentEstimationStatus;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public int getInnerPosition() {
        return InnerPosition;
    }

    public void setInnerPosition(int innerPosition) {
        InnerPosition = innerPosition;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getHours() {
        return Hours;
    }

    public void setHours(String hours) {
        Hours = hours;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }
}
