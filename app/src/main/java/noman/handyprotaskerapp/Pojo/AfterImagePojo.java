package noman.handyprotaskerapp.Pojo;

/**
 * Created by user127 on 18-04-2018.
 */

public class AfterImagePojo {
    private String title;

    public AfterImagePojo() {
    }

    public AfterImagePojo(String title) {
        this.title = title;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }


}