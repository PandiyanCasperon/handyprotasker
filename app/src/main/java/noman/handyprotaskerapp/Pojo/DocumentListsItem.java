package noman.handyprotaskerapp.Pojo;

import com.google.gson.annotations.SerializedName;

public class DocumentListsItem{

    @SerializedName("filename")
    private String filename;

    @SerializedName("url")
    private String url;

    public String getFilename(){
        return filename;
    }

    public String getUrl(){
        return url;
    }
}