package noman.handyprotaskerapp.Pojo;

public class MoreIconsPojo {
    private String DisplayStatus, Name;
    private int ResourceIcon;

    public MoreIconsPojo(){

    }

    public MoreIconsPojo(String displayStatus, String name, int resourceIcon) {
        DisplayStatus = displayStatus;
        Name = name;
        ResourceIcon = resourceIcon;
    }

    public String getDisplayStatus() {
        return DisplayStatus;
    }

    public void setDisplayStatus(String displayStatus) {
        DisplayStatus = displayStatus;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getResourceIcon() {
        return ResourceIcon;
    }

    public void setResourceIcon(int resourceIcon) {
        ResourceIcon = resourceIcon;
    }
}
