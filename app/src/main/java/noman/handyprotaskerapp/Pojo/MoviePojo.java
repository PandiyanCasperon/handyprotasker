package noman.handyprotaskerapp.Pojo;

/**
 * Created by user127 on 13-02-2018.
 */

public class MoviePojo {
    private String title, genre, year,user_image,user_name,bookingmode,Colourchangevalue,
            ColourChangeConditionValue, Craftmanhelper;



    public MoviePojo() {
    }

    public MoviePojo(String title, String genre, String year, String user_image, String user_name,
                     String bookingmode, String colourchangevalue, String ColourChangeConditionValue, String craftmanhelper) {
        this.title = title;
        this.genre = genre;
        this.year = year;
        this.user_image=user_image;
        this.user_name=user_name;
        this.bookingmode=bookingmode;
        this.Colourchangevalue=colourchangevalue;
        this.ColourChangeConditionValue = ColourChangeConditionValue;
        this.Craftmanhelper=craftmanhelper;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }



    public String getCraftmanhelper() {
        return Craftmanhelper;
    }

    public void setCraftmanhelper(String craftmanhelper) {
        this.Craftmanhelper = craftmanhelper;
    }



    public String getColourchangevalue() {
        return Colourchangevalue;
    }

    public void setColourchangevalue(String colourchangevalue) {
        this.Colourchangevalue = colourchangevalue;
    }




    public String getBookingmode() {
        return bookingmode;
    }

    public void setBookingmode(String Bookingmode) {
        this.bookingmode = Bookingmode;
    }


    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }


    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }




    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getColourChangeConditionValue() {
        return ColourChangeConditionValue;
    }

    public void setColourChangeConditionValue(String colourChangeConditionValue) {
        ColourChangeConditionValue = colourChangeConditionValue;
    }
}
