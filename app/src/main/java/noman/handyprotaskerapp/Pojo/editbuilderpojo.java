package noman.handyprotaskerapp.Pojo;

/**
 * Created by user127 on 19-02-2018.
 */

import java.io.Serializable;
import java.util.List;

public class editbuilderpojo implements Serializable
{

    private List<MultiSubItemPojo> multiSubItemPojo;

    private static final long serialVersionUID = -5435670920302756945L;
    private int val1 =0;
    private float val2 =0;
    private double Total = 0;
    private String jobtype,Jobdescription,Materialdescription,Material,depositamount ,
            Approvestatus,Singleamount,Helperamount,whatsincluded, service_id;

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public int getVal1() {
        return val1;
    }

    public void setVal1(int val1) {
        this.val1 = val1;
    }

    public float getVal2() {
        return val2;
    }

    public void setVal2(float val2) {
        this.val2 = val2;
    }

    public double getTotal() {
        return Total;
    }

    public void setTotal(Double total) {
        Total = total;
    }

    public void setJobtype(String jobtype) {
        this.jobtype = jobtype;
    }

    public String getJobtype() {
        return jobtype;
    }


    public void setWhats(String Whats) {
        this.whatsincluded = Whats;
    }

    public String getWhats() {
        return whatsincluded;
    }








    public void setHelperamount(String helperamount) {
        this.Helperamount = helperamount;
    }

    public String getHelperamount() {
        return Helperamount;
    }





    public void setSingleamount(String singleamount) {
        this.Singleamount = singleamount;
    }

    public String getSingleamount() {
        return Singleamount;
    }











    public void setApprovestatus(String approvestatus) {
        this.Approvestatus = approvestatus;
    }

    public String getApprovestatus() {
        return Approvestatus;
    }


    public void setJobdescription(String Jobdescription) {
        this.Jobdescription = Jobdescription;
    }

    public String getJobdescription() {
        return Jobdescription;
    }

    public void setMaterialdescription(String Materialdescription)
    {
        this.Materialdescription = Materialdescription;
    }

    public String getMaterialdescription() {
        return Materialdescription;
    }








    public void setMaterial(String Material)
    {
        this.Material = Material;
    }

    public String getMaterial() {
        return Material;
    }





    public void setDepositamount(String Depositamount)
    {
        this.depositamount = Depositamount;
    }

    public String getDepositamount() {
        return depositamount;
    }

    private EstimatedDetails estimatedDetails;

    public EstimatedDetails getEstimatedDetails() {
        return estimatedDetails;
    }

    public void setEstimatedDetails(EstimatedDetails estimatedDetails) {
        this.estimatedDetails = estimatedDetails;
    }

    public List<MultiSubItemPojo> getMultiSubItemPojo() {
        return multiSubItemPojo;
    }

    public void setMultiSubItemPojo(List<MultiSubItemPojo> multiSubItemPojo) {
        this.multiSubItemPojo = multiSubItemPojo;
    }
}

