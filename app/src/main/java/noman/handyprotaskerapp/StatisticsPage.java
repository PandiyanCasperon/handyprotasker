package noman.handyprotaskerapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import Utils.ConnectionDetector;
import Utils.SessionManager;
import noman.handyprotaskerapp.Fragment.JobStateFragment;
import socket.SocketHandler;


/**
 * Created by user88 on 12/31/2015.
 */
public class StatisticsPage extends AppCompatActivity {

    SessionManager session;
    ConnectionDetector cd;
    private Boolean isInternetPresent = false;


    private TabLayout tabLayout;
    private ViewPager viewPager;
    private RelativeLayout layout_statistics_back;
    private SocketHandler socketHandler;

    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent.getAction().equalsIgnoreCase("com.avail.finish")){
                finish();
            }

        }
    }

   // private RefreshReceiver finishReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.staticspage);
        initialize();
    }

    private void initialize() {
        session = new SessionManager(StatisticsPage.this);
        cd = new ConnectionDetector(StatisticsPage.this);
        socketHandler = SocketHandler.getInstance(this);

        viewPager = (ViewPager) findViewById(R.id.statistics_viewpager);
        layout_statistics_back = (RelativeLayout) findViewById(R.id.layout_back_statistics);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.statistics_tabs);
        tabLayout.setupWithViewPager(viewPager);

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();

        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);

            int tabChildsCount = vgTab.getChildCount();

            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    //Put your font in assests folder
                    //assign name of the font here (Must be case sensitive)
                    ((TextView) tabViewChild).setTypeface(Typeface.createFromAsset(getAssets(), "AvenirLTStd-Roman.otf"));
                }
            }
        }

        layout_statistics_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

   /*     finishReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.avail.finish");
        registerReceiver(finishReceiver, intentFilter);*/
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Statistics_EarningsStats_Fragment(), getResources().getString(R.string.statistes_page_header_earnings_stats));
        adapter.addFragment(new JobStateFragment(), getResources().getString(R.string.statistes_page_header_job_stats));
        viewPager.setAdapter(adapter);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    protected void onResume() {
        super.onResume();
        SharedPreferences pref1 = getApplicationContext().getSharedPreferences("openclosed", 0);
        SharedPreferences.Editor editor1 = pref1.edit();
        editor1.putString("status", "1");
        editor1.apply();
        editor1.commit();
    }


    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences pref1 = getApplicationContext().getSharedPreferences("openclosed", 0);
        SharedPreferences.Editor editor1 = pref1.edit();
        editor1.putString("status", "0");
        editor1.apply();
        editor1.commit();
    }


}


