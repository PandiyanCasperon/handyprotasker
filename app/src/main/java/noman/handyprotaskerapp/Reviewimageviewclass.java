package noman.handyprotaskerapp;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

public class Reviewimageviewclass extends AppCompatActivity {

    ImageView reviewimage;
    String image="";
    RelativeLayout bacarrow;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reviewimageviewclass);

        intialize();

        bacarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
    }

    public void intialize(){
        bacarrow=(RelativeLayout)findViewById(R.id.layout_back_image);

       reviewimage=(ImageView)findViewById(R.id.reviewimage);
        Intent i=getIntent();
        image=i.getExtras().getString("reviewimage");

        Picasso.with(getApplicationContext()).load(image).placeholder(R.drawable.ic_no_user).memoryPolicy(MemoryPolicy.NO_CACHE).into(reviewimage);

    }
}
