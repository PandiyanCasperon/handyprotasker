package noman.handyprotaskerapp.Fragment;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

import java.util.HashMap;
import java.util.List;

import Utils.SessionManager;
import socket.SocketHandler;
import volley.ServiceRequest;


/**
 * Casperon Technology on 11/12/2015.
 */
public class FragmentHockeyApp extends Fragment {
//    private static String APP_ID = "a0edd6450fc641bead62a9bed17c39cc";
    private static String APP_ID = "80fed715a76d420c941bf3e2354a1f9d";
    private SessionManager session;
    private String UserID = "";
    private ServiceRequest mRequest;
    private SocketHandler socketHandler;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        socketHandler = SocketHandler.getInstance(getActivity());
        checkForUpdates();
        session = new SessionManager(getActivity());
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_PROVIDERID);
        return null;
    }

    @Override
    public void onResume() {
        super.onResume();
        checkForCrashes();
        session = new SessionManager(getActivity());
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_PROVIDERID);
        socketHandler = SocketHandler.getInstance(getActivity());

      if(session.isLoggedIn()){
          if (!socketHandler.getSocketManager().isConnected) {
//              Log.e("connect", "Socket");
              socketHandler.getSocketManager().connect();
          }
         // postRequest_ModeChange("socket");


      }

    }

    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }


    @Override
    public void onPause() {
        super.onPause();
        unregisterManagers();
        if (isAppIsInBackground(getActivity())) {

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterManagers();
        Intent intent = new Intent("restartApps");
        getActivity().sendBroadcast(intent);
    }

    private void checkForCrashes() {
        CrashManager.register(getActivity(), APP_ID);
    }

    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(getActivity(), APP_ID);
    }

    private void unregisterManagers() {
        UpdateManager.unregister();
        // unregister other managers if necessary...
    }

}

