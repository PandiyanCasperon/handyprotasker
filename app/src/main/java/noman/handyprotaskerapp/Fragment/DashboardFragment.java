/*
 * Copyright 2015 Rudson Lima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package noman.handyprotaskerapp.Fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.Request;
//import com.suke.widget.SwitchButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import Dialog.LoadingDialog;
import Dialog.PkDialog;
import noman.handyprotaskerapp.Service.DemoSyncJob;
import noman.handyprotaskerapp.Service.HNDHelper;
import noman.handyprotaskerapp.activity.MyJobsActivity;
import noman.handyprotaskerapp.R;
import noman.handyprotaskerapp.StatisticsPage;
import noman.handyprotaskerapp.activity.WeeklyCalendarActivity;
import service.ServiceConstant;
import socket.ChatMessageServicech;
import socket.SocketHandler;
import volley.ServiceRequest;

import static android.content.Context.POWER_SERVICE;

public class DashboardFragment extends Fragment {

    private ServiceRequest mRequest;
    private boolean mSearchCheck;
    private static final String TEXT_FRAGMENT = "TEXT_FRAGMENT";
    LinearLayout mycalendar, pexlay, statics, support;
    LoadingDialog myDialog;
    private String Appinfoemail = "";
    public static final long INTERVAL = 5000;//variable to execute services every 10 second
    private Timer mTimer = null; // timer handling
    private Handler mHandler = new Handler(); // run on another Thread to avoid crash


    public static DashboardFragment newInstance(String text) {
        DashboardFragment mFragment = new DashboardFragment();
        Bundle mBundle = new Bundle();
        mBundle.putString(TEXT_FRAGMENT, text);
        mFragment.setArguments(mBundle);
        return mFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        if (mTimer != null)
            mTimer.cancel();
        else
            mTimer = new Timer(); // recreate new timer
        mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(), 0, INTERVAL);//


        mycalendar = (LinearLayout) rootView.findViewById(R.id.mycalendar);
        statics = (LinearLayout) rootView.findViewById(R.id.statics);
        pexlay = (LinearLayout) rootView.findViewById(R.id.pexlay);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent();
            String packageName = getActivity().getPackageName();
            PowerManager pm = (PowerManager) getActivity().getSystemService(POWER_SERVICE);
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + packageName));
                startActivity(intent);
            }
        }


        SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("AppInfo", 0);
        Appinfoemail = pref.getString("email", "");


        postRequest_ModeChange();

        support = (LinearLayout) rootView.findViewById(R.id.support);

        mycalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
				/*Intent in=new Intent(getActivity(),MainActivity.class);
				startActivity(in);
				getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);*/
                Intent in = new Intent(getActivity(), WeeklyCalendarActivity.class);
                startActivity(in);
                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        support.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //shareToGMail("quickrabbit@gmail.com", "", "");
                Intent in = new Intent(getActivity(), StatisticsPage.class);
                startActivity(in);
                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);


            }
        });


        statics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                HNDHelper.showErrorAlert(getActivity(), getResources().getString(R.string.hyperlink_content_coming_soon));


            }
        });

        pexlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MyJobsActivity.class);
                intent.putExtra("status", "0");
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
        rootView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Auto-generated method stub
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu, menu);

        /*SwitchButton mSwitchShowSecure;
        mSwitchShowSecure = (SwitchButton) menu.findItem(R.id.menu_add).getActionView().findViewById(R.id.switch_show_protected);*/


        ImageView supportimage;
        supportimage = (ImageView) menu.findItem(R.id.menu_add).getActionView().findViewById(R.id.supportimage);
        supportimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final PkDialog mDialog = new PkDialog(getContext());
                mDialog.setDialogTitle(getResources().getString(R.string.message_label));
                mDialog.setCancelOnTouchOutside(false);
                mDialog.setDialogMessage("To reschedule your appointment, please email us");
                mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                        Intent emailIntent = new Intent(Intent.ACTION_SEND);
                        //  emailIntent.putExtra(Intent.EXTRA_EMAIL, email);
                        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"hpsupport@handypro.com"});
                        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Handy Pro");
                        emailIntent.setType("text/plain");
                        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
                        final PackageManager pm = getActivity().getPackageManager();
                        final List<ResolveInfo> matches = pm.queryIntentActivities(emailIntent, 0);
                        ResolveInfo best = null;
                        for (final ResolveInfo info : matches)
                            if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail"))
                                best = info;
                        if (best != null)
                            emailIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
                        getActivity().startActivity(emailIntent);
                    }
                });
                mDialog.setNegativeButton("CANCEL", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mDialog.dismiss();
                    }
                });
                mDialog.show();
            }
        });

        SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("available", 0);
        String statusavailable = pref.getString("status", "");
        if (statusavailable.equalsIgnoreCase("1")) {
//            mSwitchShowSecure.setChecked(true);
        } else {
//            mSwitchShowSecure.setChecked(false);
        }


    }


    private SearchView.OnQueryTextListener onQuerySearchView = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String s) {
            if (mSearchCheck) {
                // implement your search here
            }
            return false;
        }
    };


    public void onResume() {
        super.onResume();
        //postRequest_ModeChange("gcm");


        try {

            DemoSyncJob.scheduleJob();

        } catch (Exception e) {
            e.printStackTrace();
        }


		/*try {
			Intent intent = new Intent(getActivity(), ChatMessageService.class);
			getActivity().startService(intent);
		} catch (Exception e) {
			e.printStackTrace();
		}*/


        try {
            Intent intent = new Intent(getActivity(), ChatMessageServicech.class);
            getActivity().startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private class TimeDisplayTimerTask extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
                @Override
                public void run() {

//					Log.d("SOCKET MANAGER", "Running inside...");
                    SocketHandler.getInstance(getActivity()).getSocketManager().connect();
                }
            });
        }
    }


    private void postRequest_ModeChange() {
        HashMap<String, String> jsonParams = new HashMap<String, String>();

        SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("logindetails", 0);
        String provider_id = pref.getString("provider_id", "");
        jsonParams.put("user", provider_id);
        jsonParams.put("user_type", "tasker");
        jsonParams.put("mode", "gcm");
        jsonParams.put("type", "android");


        mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(ServiceConstant.MODEUPDATE_URL, Request.Method.POST,
                jsonParams, new ServiceRequest.ServiceListener() {
                    @Override
                    public void onCompleteListener(String response) {

                        System.out.println("---------------Mode Response-----------------" + response);
                        String sStatus = "";
                        try {

                            JSONObject object = new JSONObject(response);
                            sStatus = object.getString("status");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (sStatus.equalsIgnoreCase("1")) {
                            //    session.logoutUser();
                            if ("gcm".equalsIgnoreCase("socket")) {
                                Log.e("MODE UPDATED", "SOCKET");
                                System.out.println("---------------Mode Response-----------------SOCKET");
                            } else {
                                Log.e("MODE UPDATED", "GCM");
                                System.out.println("---------------Mode Response-----------------GCM");
                            }
                        }
                    }

                    @Override
                    public void onErrorListener() {
                    }
                });
    }

}
