package noman.handyprotaskerapp;

/**
 * Created by user127 on 07-05-2018.
 */

public class viewflip {
    private String title, genre, year;

    public viewflip() {
    }

    public viewflip(String title, String genre, String year) {
        this.title = title;
        this.genre = genre;
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
