package noman.handyprotaskerapp.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.Request;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import Dialog.LoadingDialog;
import Dialog.PkDialog;
import Utils.MyCustomCompressClass;
import me.echodev.resizer.Resizer;
import noman.handyprotaskerapp.Adapter.EstimateAdapter;
import noman.handyprotaskerapp.Pojo.changeorderpojo;
import noman.handyprotaskerapp.ProgressDialog1;
import noman.handyprotaskerapp.R;
import service.ServiceConstant;
import volley.ServiceRequest;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

public class BillAttachmentActivity extends AppCompatActivity {

    String[] listContent = {};
    TextView materhead;
    CheckBox checkedhelp;
    ProgressBar loadme;

    private File compressedImage;
    LinearLayout layout_back_myjob;
    int pojoupdatepostion;
    LoadingDialog dialog;
    private Uri fileUri; // file url to store image/video
    private static String IMAGE_DIRECTORY_NAME = "";
    private static final int PICK_CAMERA_IMAGE = 1;
    private static final int PICK_GALLERY_IMAGE = 2;
    SharedPreferences pref;
    private BillAdapter adapter;
    ArrayList<changeorderpojo> arrayList;
   int   recipt_count=0;
    int  return_count=0;
    Double save_amount=0.0;
    ListView listView;
    Button save;
    String add_receipt="",return_receipt="";

    private Dialog dialogxx;
    LinearLayout addva;
    ImageView returnreceipt;
    File dir;
    LinearLayout overallscreen;
    TextView hintshow,receiptype;
    String zipcode = "", storeoverallresponse = "", aftersending = "0", Str_job_group;
    BroadcastReceiver receiver;
    String lastvalue = "";
    SharedPreferences pref1;
    String provider_id = "", clientname = "", saveresforward = "", storewhatsincluded = "", description = "", jobinstruction = "", set = "0";
    RelativeLayout returnlayout;
    changeorderpojo atomPayment;
    ArrayList<changeorderpojo> recipt_array;
    ArrayList<changeorderpojo> retrun_array;

    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.billlayout);
        IMAGE_DIRECTORY_NAME = getResources().getString(R.string.directory_name);
        atomPayment = new changeorderpojo();
        recipt_array=new ArrayList<>();
        retrun_array=new ArrayList<>();

        arrayList = new ArrayList<changeorderpojo>();

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        dir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + BillAttachmentActivity.this.getApplicationContext().getPackageName()
                + "/Files/Compressed");

        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (String child : children) {
                new File(dir, child).delete();
            }
        }

        checkedhelp = findViewById(R.id.checkedhelp);
        materhead = findViewById(R.id.materhead);
        hintshow = findViewById(R.id.hintshow);
        addva = findViewById(R.id.addva);
        returnreceipt = (ImageView) findViewById(R.id.returnreceipt);
        save = findViewById(R.id.save);
        loadme = findViewById(R.id.loadme);
        listView = findViewById(R.id.listview);
        layout_back_myjob = findViewById(R.id.layout_back_myjob);
        overallscreen = findViewById(R.id.overallscreen);
        returnlayout = findViewById(R.id.returnlayout);
        layout_back_myjob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

        pref1 = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
        jobinstruction = pref1.getString("jobinstruction", "");
        description = pref1.getString("description", "");
        storewhatsincluded = pref1.getString("storewhatsincluded", "");
        saveresforward = pref1.getString("saveresforward", "");
        clientname = pref1.getString("clinetname", "");
        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        provider_id = pref.getString("provider_id", "");

        addva.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                System.out.println("------rcount-----"+recipt_count+"=>"+return_count);

                if (arrayList.size()==0){

                }else {
                    for (int i=0;i<arrayList.size();i++){

                        if (arrayList.get(i).getStatuss().equals("0")){
                            recipt_count++;

                        }else {

                            return_count++;
                        }


                    }



                }


                listView.setVisibility(View.VISIBLE);
                hintshow.setVisibility(View.GONE);
                atomPayment = new changeorderpojo();
                atomPayment.setJobtype("");
                atomPayment.setMaterial("");
                atomPayment.setMaterialdescription("");
                atomPayment.setJobdescription("");
//                    if (atomPayment.getStatuss().equals("1")){
//                        atomPayment.setStatuss("0");
//                    }else {
//                        atomPayment.setStatuss("0");
//
//                    }
                atomPayment.setStatuss("0");
                arrayList.add(atomPayment);




//                if (arrayList.size() == 1) {
//                    adapter = new BillAdapter(BillAttachmentActivity.this, R.layout.billadapter, arrayList, "receipt");
//                    listView.setAdapter(adapter);
//                } else {
//                    adapter.notifyDataSetChanged();
//                }
                adapter = new BillAdapter(BillAttachmentActivity.this, R.layout.billadapter, arrayList, "receipt");
                listView.setAdapter(adapter);
            }
        });

        returnreceipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                System.out.println("------rcount-----"+recipt_count+"=>"+return_count);


                recipt_count=0;
                return_count=0;


                if (arrayList.size()==0)
                {
                    Toast.makeText(BillAttachmentActivity.this,"Please add receipt",Toast.LENGTH_SHORT).show();

                }
                else
                {
//                    for (int i=0;i<arrayList.size();i++){
//
//                        if (arrayList.get(i).getStatuss().equals("0"))
//                        {
//                            recipt_count++;
//
//                        }
//                        else
//                        {
//                            return_count++;
//                        }
//
//
//                    }
//
//                    if (recipt_count>return_count)
//                    {
                        listView.setVisibility(View.VISIBLE);
                        hintshow.setVisibility(View.GONE);
                        atomPayment = new changeorderpojo();
                        atomPayment.setJobtype("");
                        atomPayment.setMaterial("");
                        atomPayment.setMaterialdescription("");
                        atomPayment.setJobdescription("");


//                if (atomPayment.getStatuss().equals("0")){
//                    atomPayment.setStatuss("1");
//                }else {
//                    atomPayment.setStatuss("1");
//
//                }

                        atomPayment.setStatuss("1");

                        arrayList.add(atomPayment);

                        int size = arrayList.size();


//                if (arrayList.size() == 1) {
//                    adapter = new BillAdapter(BillAttachmentActivity.this, R.layout.billadapter, arrayList, "return");
//                    listView.setAdapter(adapter);
//                } else {
//                    adapter.notifyDataSetChanged();
//
//                }

                        adapter = new BillAdapter(BillAttachmentActivity.this, R.layout.billadapter, arrayList, "return");
                        listView.setAdapter(adapter);

//                    }else {
//
//                        Toast.makeText(BillAttachmentActivity.this,"Please add receipt",Toast.LENGTH_SHORT).show();
//
//
//                    }



                }





            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String value ="";
                int exit =0;

                for (int i=0;i<arrayList.size();i++){
                   value =  arrayList.get(i).getJobdescription();

                   if(value.equals("")){
                       exit =1;
                   }
                }

                if(exit == 1){
                    Toast.makeText(BillAttachmentActivity.this, "Please check the receipt details ", Toast.LENGTH_SHORT).show();
                }else {

                    save_amount = 0.0;
                    Double return_amount = 0.0;
                    for (int i = 0; i < arrayList.size(); i++) {

                        if (arrayList.get(i).getStatuss().equals("0")) {
                            save_amount = save_amount + Double.parseDouble(arrayList.get(i).getMaterial());

                        } else {
                            return_amount = return_amount + Double.parseDouble(arrayList.get(i).getMaterial());
                        }


                    }


                    if (save_amount > return_amount) {
                        if (checkedhelp.isChecked()) {
                            if (aftersending.equals("0")) {
                                Intent intent = new Intent(BillAttachmentActivity.this, MyJobs_OnGoingDetailActivity.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                finish();
                            } else {
//                        Intent intent = new Intent(BillAttachmentActivity.this, BeforeAndAfterPhotoActivity.class);
//                        startActivity(intent);
//                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                        finish();

                                SharedPreferences pickup;
                                pickup = getApplicationContext().getSharedPreferences("arrivedjobid", 0);

                                Intent intent = new Intent(BillAttachmentActivity.this, MoreInfoPageActivity.class);
                                SharedPreferences pref1 = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
                                SharedPreferences.Editor editor1 = pref1.edit();
                                editor1.putString("jobid", pickup.getString("jobid", ""));
                                editor1.putString("photostatus", "1");
                                editor1.putString("completed", "0");
                                editor1.putString("jobinstruction", jobinstruction);
                                editor1.putString("description", description);
                                editor1.putString("user_id", provider_id);
                                editor1.putString("storewhatsincluded", storewhatsincluded);
                                editor1.putString("saveresforward", saveresforward);
                                editor1.putString("clinetname", clientname);
                                editor1.apply();
                                startActivity(intent);
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                finish();
                            }
                        } else {
                            if (arrayList.size() == 0) {
                                final PkDialog mdialog = new PkDialog(BillAttachmentActivity.this);
                                mdialog.setDialogTitle(getResources().getString(R.string.action_sorry));
                                mdialog.setDialogMessage(getResources().getString(R.string.bill_attachment_attch_cost_detail));
                                mdialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                mdialog.dismiss();
                                            }
                                        }
                                );
                                mdialog.show();
                            } else {
                                int nozero = 0;
                                for (int jk = 0; jk < arrayList.size(); jk++) {
                                    String amount = "" + arrayList.get(jk).getMaterial();
                                    String name = "" + arrayList.get(jk).getJobtype();
                                    String cost = "" + arrayList.get(jk).getMaterial();
                                    String others = "" + arrayList.get(jk).getMaterialdescription();

                                    if (amount.equalsIgnoreCase("")  || name.equals("") || cost.equals("") || others.equals("")) {
                                        nozero = 1;
                                    }else {
                                        nozero = 0;

                                    }

                                }

                                if (nozero == 0) {
                                    loadme.setVisibility(View.VISIBLE);
                                    dialog = new LoadingDialog(BillAttachmentActivity.this);
                                    dialog.setLoadingTitle(getResources().getString(R.string.loading_in));
                                    dialog.show();
                                    postRequestConfirmBooking(BillAttachmentActivity.this, ServiceConstant.BILSEND);
                                } else {
                                    final PkDialog mdialog = new PkDialog(BillAttachmentActivity.this);
                                    mdialog.setDialogTitle(getResources().getString(R.string.action_sorry));
                                    mdialog.setDialogMessage(getResources().getString(R.string.bill_attachment_attach_image));
                                    mdialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    mdialog.dismiss();
                                                }
                                            }
                                    );
                                    mdialog.show();
                                }
                            }
                        }
                    } else {
                        if (checkedhelp.isChecked()) {

//                                            Intent intent = new Intent(BillAttachmentActivity.this, BeforeAndAfterPhotoActivity.class);
//                                            startActivity(intent);
//                                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                                            finish();

                            SharedPreferences pickup;
                            pickup = getApplicationContext().getSharedPreferences("arrivedjobid", 0);

                            Intent intent = new Intent(BillAttachmentActivity.this, MoreInfoPageActivity.class);
                            SharedPreferences pref1 = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
                            SharedPreferences.Editor editor1 = pref1.edit();
                            editor1.putString("jobid", pickup.getString("jobid", ""));
                            editor1.putString("photostatus", "1");
                            editor1.putString("completed", "0");
                            editor1.putString("jobinstruction", jobinstruction);
                            editor1.putString("description", description);
                            editor1.putString("user_id", provider_id);
                            editor1.putString("storewhatsincluded", storewhatsincluded);
                            editor1.putString("saveresforward", saveresforward);
                            editor1.putString("clinetname", clientname);
                            editor1.apply();
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            finish();

                        } else{
                            Toast.makeText(BillAttachmentActivity.this, "Receipt amount is to larger then return amount", Toast.LENGTH_SHORT).show();

                        }


                    }

                }







            }
        });

        SharedPreferences pickup;
        pickup = getApplicationContext().getSharedPreferences("arrivedjobid", 0); // 0 - for private mode
        storeoverallresponse = pickup.getString("storeresponse", "");
        aftersending = pickup.getString("afterr", "");

        getData(BillAttachmentActivity.this, pickup.getString("zipcode", ""));


        if (storeoverallresponse.length() > 6) {
            try {
                JSONObject jobject = new JSONObject(storeoverallresponse);

                JSONObject object = jobject.getJSONObject("response");
                JSONObject object2 = object.getJSONObject("job");
                Str_job_group = object2.getString("btn_group");

                if (Str_job_group.equals("1") || Str_job_group.equals("2") || Str_job_group.equals("3") || Str_job_group.equals("4") || Str_job_group.equals("5")) {
                    overallscreen.setVisibility(View.VISIBLE);
                    save.setVisibility(View.VISIBLE);
                    addva.setVisibility(View.VISIBLE);
                    returnlayout.setVisibility(View.VISIBLE);
                    materhead.setText(getResources().getString(R.string.billlayout_header_TXT));
                } else {
                    materhead.setText(getResources().getString(R.string.headingchange));
                    overallscreen.setVisibility(View.GONE);
                    save.setVisibility(View.GONE);
                    addva.setVisibility(View.INVISIBLE);
                    returnreceipt.setVisibility(View.GONE);
                }


                JSONArray jobservice_type1 = object2.getJSONArray("bills");

                if (jobservice_type1.length() == 0) {


                    listView.setVisibility(View.INVISIBLE);
                    hintshow.setVisibility(View.VISIBLE);

                    /*arrayList.clear();
                    changeorderpojo atomPayment = new changeorderpojo();
                    atomPayment.setJobtype("");
                    atomPayment.setMaterial("");
                    atomPayment.setMaterialdescription("");
                    atomPayment.setJobdescription("");
                    arrayList.add(atomPayment);
                    adapter = new billadapter(BillAttachmentActivity.this, R.layout.billadapter, arrayList);
                    listView.setAdapter(adapter);*/


                } else {

                    listView.setVisibility(View.VISIBLE);
                    hintshow.setVisibility(View.GONE);
                    returnlayout.setVisibility(View.VISIBLE);
                    arrayList.clear();

                    for (int b1 = 0; b1 < jobservice_type1.length(); b1++) {
                        String  Str_add_receipt = "", Str_return_receipt = "";
                        JSONObject getarrayvalue = jobservice_type1.getJSONObject(b1);
                        String name = "" + getarrayvalue.getString("name");
                        String kind = "" + getarrayvalue.getString("kind");
                        String cost = "" + getarrayvalue.getString("cost");

                        if(getarrayvalue.has("add_receipt")){
                            Str_add_receipt = getarrayvalue.getString("add_receipt");
                        }
                        if(getarrayvalue.has("return_receipt")){
                            Str_return_receipt = getarrayvalue.getString("return_receipt");
                            if(return_receipt.equalsIgnoreCase("true"))
                            {
//                                receiptype.setText("Return");
//                                receiptype.setTextColor(getResources().getColor(R.color.app_color));
//                                receiptype.setVisibility(View.VISIBLE);
                            }
                        }


                        if(add_receipt.equalsIgnoreCase("true")){
//                              receiptype.setText("Receipt");
//                          receiptype.setVisibility(View.VISIBLE);
                        }

                        String bill_image;

                        if (getarrayvalue.getString("bill_image").startsWith("https") || getarrayvalue.getString("bill_image").startsWith("http")) {
                            bill_image = getarrayvalue.getString("bill_image");
                        } else {
                            bill_image = ServiceConstant.Review_image + getarrayvalue.getString("bill_image");
                        }

                        changeorderpojo atomPayment = new changeorderpojo();
                        atomPayment.setJobtype(name);
                        atomPayment.setMaterial(cost);
                        atomPayment.setMaterialdescription(kind);
                        atomPayment.setJobdescription(bill_image);
                        if (Str_add_receipt.equalsIgnoreCase("true")){
                            atomPayment.setStatuss("0");
                        }else {
                            atomPayment.setStatuss("1");
                        }
                        arrayList.add(atomPayment);
                    }

                    adapter = new BillAdapter(BillAttachmentActivity.this, R.layout.billadapter, arrayList, "");
                    listView.setAdapter(adapter);


                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //      arrayList.get(pojoupdatepostion).setJobdescription(dir.toString() + "/" + lastvalue);
                adapter.notifyDataSetChanged();
            }
        };

    }


    public class BillAdapter extends ArrayAdapter<changeorderpojo> {
        protected final String LOG_TAG = EstimateAdapter.class.getSimpleName();

        private ArrayList<changeorderpojo> items;
        private int layoutResourceId;
        private Context context;
        String receipttype1 = "", returntype = "";
        Typeface tf;
        changeorderpojo changeorderpojo1 = new changeorderpojo();
        SharedPreferences pref;
        String currency;

        BillAdapter(Context context, int layoutResourceId, ArrayList<changeorderpojo> items, String receipttype) {
            super(context, layoutResourceId, items);
            this.layoutResourceId = layoutResourceId;
            this.receipttype1 = receipttype;
            this.context = context;
            this.items = items;
            tf = Typeface.createFromAsset(getContext().getAssets(), "Poppins-Regular.ttf");
            pref = context.getSharedPreferences("logindetails", 0);
            currency = pref.getString("currency", "");
        }

        @SuppressLint("ViewHolder")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            BillAdapter.Holder holder = null;

            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new BillAdapter.Holder();
            holder.bean = items.get(position);
            holder.deletecraftman = row.findViewById(R.id.deletecraftman);
            holder.receiptype = row.findViewById(R.id.receiptype);
            holder.deletecraftman.setTag(holder.bean);

            holder.billpage = row.findViewById(R.id.billpage);

            holder.materialdesc = row.findViewById(R.id.materialdesc);
            holder.materialamount = row.findViewById(R.id.materialamount);
            holder.jobtype = row.findViewById(R.id.jobtype);
            holder.jobtype.setTypeface(tf);
            holder.materialdesc.setTypeface(tf);
            holder.materialamount.setTypeface(tf);

            holder.deletecraftman.setVisibility(View.VISIBLE);

            if (Str_job_group.equals("1") || Str_job_group.equals("2") || Str_job_group.equals("3") || Str_job_group.equals("4") || Str_job_group.equals("5")) {


                holder.deletecraftman.setVisibility(View.VISIBLE);

                if (items.get(position).getStatuss().equals("0")) {
                    holder.receiptype.setText("Receipt");
                    holder.receiptype.setVisibility(View.VISIBLE);
                } else {
                    holder.receiptype.setText("Return");
                    holder.receiptype.setTextColor(getResources().getColor(R.color.app_color));
                    holder.receiptype.setVisibility(View.VISIBLE);
                }

            } else {
                holder.deletecraftman.setVisibility(View.GONE);
                if (items.get(position).getStatuss().equals("0")) {
                    holder.receiptype.setText("Receipt");
                    holder.receiptype.setVisibility(View.VISIBLE);
                } else {
                    holder.receiptype.setText("Return");
                    holder.receiptype.setTextColor(getResources().getColor(R.color.app_color));
                    holder.receiptype.setVisibility(View.VISIBLE);
                }
            }

            holder.jobtype.setText("");

            setVal1TextChangeListener(holder);

            setVal1TextChangeListener2(holder);

            row.setTag(holder);
            setupItem(holder, position);

            return row;
        }

        private void setupItem(final BillAdapter.Holder holder, final int postion) {
            holder.jobtype.setText(holder.bean.getJobtype());
            holder.materialamount.setText(holder.bean.getMaterial());


            holder.materialdesc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogxx = new Dialog(BillAttachmentActivity.this);
                    dialogxx.setContentView(R.layout.dialoglayout);
                    dialogxx.setTitle(getResources().getString(R.string.custom_dialog));

                    dialogxx.setCancelable(true);
                    dialogxx.setCanceledOnTouchOutside(true);


                    ListView dialog_ListView = dialogxx.findViewById(R.id.dialoglist);
                    ArrayAdapter<String> adapter
                            = new ArrayAdapter<String>(BillAttachmentActivity.this,
                            android.R.layout.simple_list_item_1, listContent);
                    dialog_ListView.setAdapter(adapter);
                    dialog_ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {
                            holder.bean.setMaterialdescription(parent.getItemAtPosition(position).toString());
                            holder.materialdesc.setText(parent.getItemAtPosition(position).toString());
                            dialogxx.dismiss();
                        }
                    });

                    dialogxx.show();
                }
            });


            holder.materialdesc.setText(holder.bean.getMaterialdescription());


            if (holder.bean.getJobdescription().startsWith("http") || holder.bean.getJobdescription().startsWith("https")) {

                Picasso.with(context).load(holder.bean.getJobdescription())
                        .resize(500, 500)
                        .onlyScaleDown()
                        .rotate(90)
                        .placeholder(R.drawable.fileupload)
                        .into(holder.billpage);
            } else {
                Picasso.with(context).load("file://" + holder.bean.getJobdescription())
                        .resize(500, 500)
                        .onlyScaleDown()
                        .rotate(90)
                        .placeholder(R.drawable.fileupload)
                        .into(holder.billpage);
            }



            holder.billpage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    pojoupdatepostion = postion;

                    final PkDialog mDialog = new PkDialog(BillAttachmentActivity.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.select_label));
                    mDialog.setDialogMessage(getResources().getString(R.string.activity_beforephoto_choose_to_add_photos));
                    mDialog.setPositiveButton(
                            getResources().getString(R.string.cameraword), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();

                                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                                    fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

                                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

                                    // start the image capture Intent
                                    startActivityForResult(intent, PICK_CAMERA_IMAGE);

                                }
                            }
                    );
                    mDialog.setNegativeButton(
                            getResources().getString(R.string.galleryword), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();
                                    Intent i = new Intent(
                                            Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    startActivityForResult(i, PICK_GALLERY_IMAGE);
                                }
                            }
                    );

                    mDialog.show();
                }
            });

        }


        private void setVal1TextChangeListener(final BillAdapter.Holder holder) {
            holder.jobtype.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.toString().length() > 0)
                        holder.bean.setJobtype(s.toString());
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }


        private void setVal1TextChangeListener2(final BillAdapter.Holder holder) {
            holder.materialamount.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.toString().length() > 0)
                        holder.bean.setMaterial(s.toString());
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }


        public class Holder {
            ImageView deletecraftman, billpage;
            changeorderpojo bean;
            EditText jobtype, materialamount;
            TextView materialdesc,receiptype;
            changeorderpojo atomPayment = new changeorderpojo();
        }


    }


    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("file_uri", fileUri);
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        fileUri = savedInstanceState.getParcelable("file_uri");
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    //Remove this section if you don't want camera code (Start)
    //Got new image taken from camera intent
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 1) {
                String picturePath = fileUri.getPath();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
                String currentDateandTime = sdf.format(new Date());

                File folder = new File(Environment.getExternalStorageDirectory() +
                        File.separator + "handdy");
                boolean success = true;
                if (!folder.exists()) {
                    success = folder.mkdirs();
                }
                try {
                    File resizedImage = new Resizer(this)
                            .setTargetLength(1080)
                            .setQuality(80)
                            .setOutputFormat("JPEG")
                            .setOutputFilename(currentDateandTime)
                            .setOutputDirPath(folder.toString())
                            .setSourceImage(new File(picturePath))
                            .getResizedFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    File actualImage = new File(picturePath);
                    MyCustomCompressClass myCustomCompressClass = new MyCustomCompressClass();
                    myCustomCompressClass.constructor(this, actualImage);


                } catch (Exception e) {
                    e.printStackTrace();
                }

                arrayList.get(pojoupdatepostion).setJobdescription(folder.toString() + "/" + currentDateandTime + ".jpg");
                adapter.notifyDataSetChanged();
                assert picturePath != null;
                String[] hj = picturePath.split("\\/");
                lastvalue = "";

                for (String s : hj) {

                    lastvalue = s;
                }


            } else if (requestCode == 2) {

                //gallery part
                String picturePath;
                Uri selectedImageUri = data.getData();
                picturePath = getPath(getApplicationContext().getApplicationContext(), selectedImageUri);
                File folder = new File(Environment.getExternalStorageDirectory() +
                        File.separator + "handdy");
                boolean success = true;
                if (!folder.exists()) {
                    success = folder.mkdirs();
                }
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
                String currentDateandTime = sdf.format(new Date());
                try {
                    File resizedImage = new Resizer(this)
                            .setTargetLength(1080)
                            .setQuality(80)
                            .setOutputFormat("JPEG")
                            .setOutputFilename(currentDateandTime)
                            .setOutputDirPath(folder.toString())
                            .setSourceImage(new File(picturePath))
                            .getResizedFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                arrayList.get(pojoupdatepostion).setJobdescription(folder.toString() + "/" + currentDateandTime + ".jpg");
                adapter.notifyDataSetChanged();
                try {
                    String[] hj = picturePath.split("\\/");

                    lastvalue = "";

                    for (String s : hj) {

                        lastvalue = s;
                    }

                    File actualImage = new File(picturePath);
                    MyCustomCompressClass myCustomCompressClass = new MyCustomCompressClass();
                    myCustomCompressClass.constructor(this, actualImage);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public static String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }


    private void postRequestConfirmBooking(final Context aContext, String url) {

        HashMap<String, String> params = new HashMap<>();
        SharedPreferences pickup;
        pickup = getApplicationContext().getSharedPreferences("arrivedjobid", 0); // 0 - for private mode
        params.put("job_id", pickup.getString("jobid", ""));
        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        final String provider_id = pref.getString("provider_id", "");
        params.put("provider_id", provider_id);


        for (int k = 0; k < arrayList.size(); k++) {

            String imgString = "";
            if (arrayList.get(k).getJobdescription().startsWith("http://") || arrayList.get(k).getJobdescription().startsWith("https://")) {
                imgString = arrayList.get(k).getJobdescription();
            } else {
                Bitmap src = BitmapFactory.decodeFile(arrayList.get(k).getJobdescription());
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                src.compress(Bitmap.CompressFormat.PNG, 50, baos);
                imgString = Base64.encodeToString(getBytesFromBitmap(src),
                        Base64.NO_WRAP);
            }


            params.put("bills[" + k + "][bill_image]", imgString);
            params.put("bills[" + k + "][name]", arrayList.get(k).getJobtype());
            params.put("bills[" + k + "][cost]", arrayList.get(k).getMaterial());
            params.put("bills[" + k + "][kind]", arrayList.get(k).getMaterialdescription());

            if(arrayList.get(k).getStatuss().equals("0")){
                params.put("bills[" + k + "][add_receipt]",  "true");
            }
            else{
                params.put("bills[" + k + "][return_receipt]",  "true");

            }


        }


        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                loadme.setVisibility(View.GONE);

                System.out.println("-------------get Confirm Category list Response----------------" + response);
                dialog.dismiss();
                String booklaterornow = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {

                        final PkDialog mDialog = new PkDialog(BillAttachmentActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.success_label));
                        mDialog.setDialogMessage(object.getString("response"));
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mDialog.dismiss();
                                        if (aftersending.equals("0")) {
                                            Intent intent = new Intent(BillAttachmentActivity.this, MyJobs_OnGoingDetailActivity.class);
                                            startActivity(intent);
                                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                            finish();
                                        } else {
//                                            Intent intent = new Intent(BillAttachmentActivity.this, BeforeAndAfterPhotoActivity.class);
//                                            startActivity(intent);
//                                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                                            finish();

                                            SharedPreferences pickup;
                                            pickup = getApplicationContext().getSharedPreferences("arrivedjobid", 0);

                                            Intent intent = new Intent(BillAttachmentActivity.this, MoreInfoPageActivity.class);
                                            SharedPreferences pref1 = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
                                            SharedPreferences.Editor editor1 = pref1.edit();
                                            editor1.putString("jobid", pickup.getString("jobid", ""));
                                            editor1.putString("photostatus", "1");
                                            editor1.putString("completed", "0");
                                            editor1.putString("jobinstruction", jobinstruction);
                                            editor1.putString("description", description);
                                            editor1.putString("user_id", provider_id);
                                            editor1.putString("storewhatsincluded", storewhatsincluded);
                                            editor1.putString("saveresforward", saveresforward);
                                            editor1.putString("clinetname", clientname);
                                            editor1.apply();
                                            startActivity(intent);
                                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                            finish();
                                        }
                                    }
                                }
                        );
                        mDialog.show();
                    } else {
                        final PkDialog mDialog = new PkDialog(BillAttachmentActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.failed_label));
                        mDialog.setDialogMessage(object.getString("response"));
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        mDialog.dismiss();
                                    }
                                }
                        );
                        mDialog.show();
                        dialog.dismiss();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    dialog.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
                loadme.setVisibility(View.GONE);
            }
        });
    }

    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }

    public void itemDeleteListner1(View v) {
        changeorderpojo itemToRemove = (changeorderpojo) v.getTag();
        adapter.remove(itemToRemove);
        adapter.notifyDataSetChanged();
        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.overalllayouit), getResources().getString(R.string.bill_attachment_deleted_successfuly), Snackbar.LENGTH_LONG);
        TextView tv = (snackbar.getView()).findViewById(com.google.android.material.R.id.snackbar_text);
        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Poppins-Bold.ttf");
        tv.setTypeface(font);
        tv.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white_color));
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.green));
        snackbar.show();
    }


    private void getData(Context mContext, String zipcode) {
        final ProgressDialog1 myDialog = new ProgressDialog1(BillAttachmentActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("zipcode", zipcode);
        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(ServiceConstant.billtypee, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("app/billsource", response);
                String Str_status;
                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {

                        JSONObject info_Object = jobject.getJSONObject("response");

                        JSONArray sources = info_Object.getJSONArray("material");
                        listContent = new String[sources.length()];
                        for (int b = 0; b < sources.length(); b++) {
                            listContent[b] = sources.getString(b);

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                myDialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                myDialog.dismiss();
            }
        });
    }


    private String getByteArrayFromImageURL(String url) {
        try {
            URL imageUrl = new URL(url);
            URLConnection ucon = imageUrl.openConnection();
            InputStream is = ucon.getInputStream();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int read = 0;
            while ((read = is.read(buffer, 0, buffer.length)) != -1) {
                baos.write(buffer, 0, read);
            }
            baos.flush();
            return Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
        } catch (Exception e) {
            Log.d("Error", e.toString());
        }
        return null;
    }


    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.handypro.files.selected");
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
