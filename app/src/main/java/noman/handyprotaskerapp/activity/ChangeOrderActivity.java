package noman.handyprotaskerapp.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.MediaStore;

import androidx.annotation.NonNull;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;

import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import Dialog.LoadingDialog;
import Dialog.PkDialog;
import Utils.ConnectionDetector;
import noman.handyprotaskerapp.OtpPage;
import noman.handyprotaskerapp.Service.HNDHelper;
import noman.handyprotaskerapp.WidgetSupport.CustomTextRegular;
import noman.handyprotaskerapp.WidgetSupport.MyGridView;
import noman.handyprotaskerapp.Pojo.changeorderpojo;
import noman.handyprotaskerapp.R;
import noman.handyprotaskerapp.DB.ChangeOrderSQlite;
import service.ServiceConstant;
import volley.ServiceRequest;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static android.view.View.VISIBLE;

/**
 * Created by user127 on 31-03-2018.
 */

public class ChangeOrderActivity extends AppCompatActivity {


    ScrollView scrollView;
    EditText amountde;
    Double stratvalue = 0.0;
    TextView changeorder;
    String storemybase64 = "";
    LoadingDialog dialog;
    public static final int SIGNATURE_ACTIVITY = 1;
    SharedPreferences pref;
    TextView jobidnumber, categoryname, locationname, datename, timename, currentamtname,
            curentamt, updateamt, clinetnamevalue, purchasetytpeheading, PurchaseOrderNumber;
    String jobid, category, location, date, currentamt;
    private changeorderadapter adapter;
    ArrayList<changeorderpojo> arrayList = new ArrayList<changeorderpojo>();
    MyGridView listView;
    Button save;
    String deductedamountfromserver = "0";
    RelativeLayout layout_cancel_back;
    private SQLiteDatabase dataBase;
    ChangeOrderSQlite mHelper;
    RelativeLayout addva;
    private Boolean isInternetPresent = false;
    ConnectionDetector cd;
    int amount = 0;
    SharedPreferences prefs;
    String currencysymbol;
    Double currentamtzz = 0.0;
    Double myCalculatedTax = 0.0;
    TextView grandam, TextViewSignatureTitle;
    ArrayList<String> changearray = new ArrayList<String>();
    ArrayList<String> assignchangearray = new ArrayList<String>();
    RelativeLayout acceptclick;
    String user_id;
    CheckBox checkedhelp;
    Double updatestoreall;
    EditText descriptionf;
    public static String tempDir;
    LinearLayout mContent;
    signature mSignature;
    Button mClear, mGetSign, mCancel;

    public int count = 1;
    public String current = null;
    private Bitmap mBitmap;
    View mView;
    File mypath;

    private String uniqueId;
    private EditText yourName;
    ImageView setsignimage;

    SharedPreferences saveim;
    private CustomTextRegular myCalculatedTaxAmtTXT;
    private String is_selected_page="signature";
    private String OTP="";

    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.changeorderla);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        scrollView = (ScrollView) findViewById(R.id.scrollView);

        saveim = getApplicationContext().getSharedPreferences("changordersign", 0);
        SharedPreferences.Editor prefeditor = saveim.edit();
        prefeditor.clear();
        prefeditor.commit();

        cd = new ConnectionDetector(ChangeOrderActivity.this);
        isInternetPresent = cd.isConnectingToInternet();
        mHelper = new ChangeOrderSQlite(ChangeOrderActivity.this);
        prefs = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        currencysymbol = prefs.getString("myCurrencySymbol", "");
        pref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
        jobid = pref.getString("jobid", "");

        category = pref.getString("category", "");
        location = pref.getString("location", "");
        date = pref.getString("date", "");
        currentamt = pref.getString("currentamt", "");

        mHelper = new ChangeOrderSQlite(ChangeOrderActivity.this);
        deleteDatabase(mHelper.DATABASE_NAME);

        descriptionf = (EditText) findViewById(R.id.descriptionf);
        amountde = (EditText) findViewById(R.id.amountde);
        checkedhelp = (CheckBox) findViewById(R.id.checkedhelp);
        acceptclick = (RelativeLayout) findViewById(R.id.acceptclick);
        layout_cancel_back = (RelativeLayout) findViewById(R.id.layout_cancel_back);
        changeorder = (TextView) findViewById(R.id.changeorder);
        grandam = (TextView) findViewById(R.id.grandam);
        addva = (RelativeLayout) findViewById(R.id.addva);
        save = (Button) findViewById(R.id.save);
        listView = (MyGridView) findViewById(R.id.listview);
        jobidnumber = (TextView) findViewById(R.id.jobidnumber);
        clinetnamevalue = (TextView) findViewById(R.id.clinetnamevalue);
        categoryname = (TextView) findViewById(R.id.categoryname);
        locationname = (TextView) findViewById(R.id.locationname);
        setsignimage = (ImageView) findViewById(R.id.setsignimage);
        datename = (TextView) findViewById(R.id.datename);
        currentamtname = (TextView) findViewById(R.id.currentamtname);
        curentamt = (TextView) findViewById(R.id.curentamt);
        updateamt = (TextView) findViewById(R.id.updateamt);
        purchasetytpeheading = (TextView) findViewById(R.id.purchasetytpeheading);
        PurchaseOrderNumber = (TextView) findViewById(R.id.PurchaseOrderNumber);
        TextViewSignatureTitle = (TextView) findViewById(R.id.TextViewSignatureTitle);

        myCalculatedTaxAmtTXT = (CustomTextRegular) findViewById(R.id.changeorderla_TXT_amount);


        jobidnumber.setText(jobid);
        categoryname.setText(category);
        locationname.setText(location);
        datename.setText(date);
        currentamtname.setText(currentamt);
        String grandtotal = "" + amount;
        if (grandtotal.endsWith(".0") || grandtotal.endsWith(".5") || grandtotal.endsWith(".1") || grandtotal.endsWith(".2") || grandtotal.endsWith(".3") || grandtotal.endsWith(".4") || grandtotal.endsWith(".6") || grandtotal.endsWith(".7") || grandtotal.endsWith(".8") || grandtotal.endsWith(".9")) {
            curentamt.setText(currencysymbol + amount + "0");
            updateamt.setText(currencysymbol + amount + "0");
            updatestoreall = Double.parseDouble(amount + "0");
        } else {
            curentamt.setText(currencysymbol + amount);
            updateamt.setText(currencysymbol + amount);
            updatestoreall = Double.parseDouble("" + amount);
        }


        checkedhelp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (checkedhelp.isChecked()) {

                    ChangeOrderOptionDialgShow();
                    System.out.println("Checked");


                } else {
                    storemybase64 = "";
                    setsignimage.setVisibility(View.GONE);
                    TextViewSignatureTitle.setVisibility(View.GONE);
                    saveim = getApplicationContext().getSharedPreferences("changordersign", 0);
                    SharedPreferences.Editor prefeditor = saveim.edit();
                    prefeditor.putString("sign", "");
                    prefeditor.apply();
                    save.setText(getResources().getString(R.string.activity_addbeforephoto_send_client_approval));
                    System.out.println("Un-Checked");
                }
            }
        });


        amountde.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.toString().length() > 0) {
                    if (s.toString().endsWith(".")) {

                    } else {
                        Double typing = Double.parseDouble(s.toString());
                        if (typing <= currentamtzz) {
                            Double minus = updatestoreall - typing;
                            String grandtotal = "" + minus + myCalculatedTax;
                            if (grandtotal.endsWith(".0") || grandtotal.endsWith(".5") || grandtotal.endsWith(".1") || grandtotal.endsWith(".2") || grandtotal.endsWith(".3") || grandtotal.endsWith(".4") || grandtotal.endsWith(".6") || grandtotal.endsWith(".7") || grandtotal.endsWith(".8") || grandtotal.endsWith(".9")) {
                                grandam.setText(currencysymbol + String.format("%.2f", minus) + "0");
                            } else {
                                grandam.setText(currencysymbol + String.format("%.2f", minus));
                            }

                        } else {
                            amountde.setText("0");
                            Double minus = updatestoreall - 0.0;
                            String grandtotal = "" + minus + myCalculatedTax;
                            if (grandtotal.endsWith(".0") || grandtotal.endsWith(".5") || grandtotal.endsWith(".1") || grandtotal.endsWith(".2") || grandtotal.endsWith(".3") || grandtotal.endsWith(".4") || grandtotal.endsWith(".6") || grandtotal.endsWith(".7") || grandtotal.endsWith(".8") || grandtotal.endsWith(".9")) {
                                grandam.setText(currencysymbol + String.format("%.2f", minus) + "0");
                            } else {
                                grandam.setText(currencysymbol + String.format("%.2f", minus));
                            }
                            HNDHelper.showErrorAlert(ChangeOrderActivity.this, getResources().getString(R.string.crossam));
                        }

                    }
                } else {

                    Double minus = updatestoreall - 0.0 + myCalculatedTax;
                    String grandtotal = "" + minus;
                    if (grandtotal.endsWith(".0") || grandtotal.endsWith(".5") || grandtotal.endsWith(".1") || grandtotal.endsWith(".2") || grandtotal.endsWith(".3") || grandtotal.endsWith(".4") || grandtotal.endsWith(".6") || grandtotal.endsWith(".7") || grandtotal.endsWith(".8") || grandtotal.endsWith(".9")) {
                        grandam.setText(currencysymbol + String.format("%.2f", minus) + "0");
                    } else {
                        grandam.setText(currencysymbol + String.format("%.2f", minus));
                    }
                }
            }
        });


        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            myjobOngoingDetail(ChangeOrderActivity.this, ServiceConstant.MYJOB_DETAIL_INFORMATION_URL);
            System.out.println("--------------detail-------------------" + ServiceConstant.MYJOB_DETAIL_INFORMATION_URL);
        } else {
            final PkDialog mDialog = new PkDialog(ChangeOrderActivity.this);
            mDialog.setDialogTitle(getResources().getString(R.string.internet_label));
            mDialog.setCancelOnTouchOutside(false);
            mDialog.setDialogMessage(getResources().getString(R.string.action_no_internet_message));
            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                    finish();
                }
            });
            mDialog.show();
        }

        layout_cancel_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

        addva.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Snackbar snackbar = Snackbar
                        .make(updateamt, getResources().getString(R.string.changeorder_row_added), Snackbar.LENGTH_LONG);
                TextView tv = (TextView) (snackbar.getView()).findViewById(com.google.android.material.R.id.snackbar_text);
                Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Poppins-Medium.ttf");
                tv.setTypeface(font);
                tv.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white_color));
                snackbar.getView().setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.green));
                snackbar.show();

                changeorderpojo atomPayment = new changeorderpojo();
                atomPayment.setJobtype("");
                atomPayment.setMaterial(changearray.get(0));
                atomPayment.setMaterialdescription("");
                atomPayment.setJobdescription("1");
                atomPayment.setStatus("0");
                arrayList.add(atomPayment);
                if (arrayList.size() > 1) {
                    adapter.notifyDataSetChanged();
                } else {
                    adapter = new changeorderadapter(ChangeOrderActivity.this, R.layout.changeorderitem, arrayList);
                    listView.setAdapter(adapter);
                }

            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkedhelp.isChecked()) {

                    if(is_selected_page.equals("signature")){


                        if (storemybase64.equals("") || storemybase64.equals(" ")) {
                            HNDHelper.showErrorAlert(ChangeOrderActivity.this, getResources().getString(R.string.pleasecap));
                        } else if (storemybase64.equals("cap")) {
                            HNDHelper.showErrorAlert(ChangeOrderActivity.this, getResources().getString(R.string.capsignag));
                        } else {
                            mHelper = new ChangeOrderSQlite(ChangeOrderActivity.this);
                            deleteDatabase(mHelper.DATABASE_NAME);


                            for (int k = 0; k < arrayList.size(); k++) {
                                mHelper = new ChangeOrderSQlite(ChangeOrderActivity.this);
                                dataBase = mHelper.getWritableDatabase();
                                ContentValues values = new ContentValues();
                                values.put(ChangeOrderSQlite.KEY_AMOUNT, arrayList.get(k).getJobtype());
                                values.put(ChangeOrderSQlite.KEY_TYPE, arrayList.get(k).getMaterial());
                                values.put(ChangeOrderSQlite.KEY_DESCRIPTION, arrayList.get(k).getMaterialdescription());
                                dataBase.insert(ChangeOrderSQlite.TABLE_NAME, null, values);

                            }

                            int nozero = 0;
                            for (int jk = 0; jk < arrayList.size(); jk++) {
                                String amount = "" + arrayList.get(jk).getMaterial();
                                if (amount.equalsIgnoreCase("")) {
                                    nozero = 1;
                                } else {

                                }
                            }
                            if (nozero == 0) {
                                if (isInternetPresent) {
                                    if (arrayList.size() == 0) {

                                        postRequestConfirmBooking(ChangeOrderActivity.this, ServiceConstant.SENDCLIENTAPPROVAL);
                                        // HNDHelper.showErrorAlert(ChangeOrderActivity.this, getResources().getString(R.string.changeorder_no_change_in_amt));
                                    } else {

                                        int nozeroforall = 0;
                                        for (int jk = 0; jk < arrayList.size(); jk++) {
                                            String amount = "" + arrayList.get(jk).getJobtype();
                                            if (amount.equalsIgnoreCase("0") || amount.equalsIgnoreCase("")) {
                                                nozeroforall = 1;
                                            } else {

                                            }
                                        }

                                        if (nozeroforall == 0) {

                                            postRequestConfirmBooking(ChangeOrderActivity.this, ServiceConstant.SENDCLIENTAPPROVAL);

                                        } else {
                                            HNDHelper.showErrorAlert(ChangeOrderActivity.this, getResources().getString(R.string.changeorder_enter_change_amount));
                                        }


                                    }


                                } else {
                                    HNDHelper.showErrorAlert(ChangeOrderActivity.this, getResources().getString(R.string.action_no_internet_message));
                                }
                            } else {
                                HNDHelper.showErrorAlert(ChangeOrderActivity.this, getResources().getString(R.string.changeorder_choose_chng_type));
                            }

                        }

                    }  else{

                            mHelper = new ChangeOrderSQlite(ChangeOrderActivity.this);
                            deleteDatabase(mHelper.DATABASE_NAME);


                            for (int k = 0; k < arrayList.size(); k++) {
                                mHelper = new ChangeOrderSQlite(ChangeOrderActivity.this);
                                dataBase = mHelper.getWritableDatabase();
                                ContentValues values = new ContentValues();
                                values.put(ChangeOrderSQlite.KEY_AMOUNT, arrayList.get(k).getJobtype());
                                values.put(ChangeOrderSQlite.KEY_TYPE, arrayList.get(k).getMaterial());
                                values.put(ChangeOrderSQlite.KEY_DESCRIPTION, arrayList.get(k).getMaterialdescription());
                                dataBase.insert(ChangeOrderSQlite.TABLE_NAME, null, values);

                            }

                            int nozero = 0;
                            for (int jk = 0; jk < arrayList.size(); jk++) {
                                String amount = "" + arrayList.get(jk).getMaterial();
                                if (amount.equalsIgnoreCase("")) {
                                    nozero = 1;
                                } else {

                                }
                            }
                            if (nozero == 0) {
                                if (isInternetPresent) {
                                    if (arrayList.size() == 0) {

                                        postRequestConfirmBooking(ChangeOrderActivity.this, ServiceConstant.SENDCLIENTAPPROVAL);
                                        // HNDHelper.showErrorAlert(ChangeOrderActivity.this, getResources().getString(R.string.changeorder_no_change_in_amt));
                                    } else {

                                        int nozeroforall = 0;
                                        for (int jk = 0; jk < arrayList.size(); jk++) {
                                            String amount = "" + arrayList.get(jk).getJobtype();
                                            if (amount.equalsIgnoreCase("0") || amount.equalsIgnoreCase("")) {
                                                nozeroforall = 1;
                                            } else {

                                            }
                                        }

                                        if (nozeroforall == 0) {

                                            postRequestConfirmBooking(ChangeOrderActivity.this, ServiceConstant.SENDCLIENTAPPROVAL);

                                        } else {
                                            HNDHelper.showErrorAlert(ChangeOrderActivity.this, getResources().getString(R.string.changeorder_enter_change_amount));
                                        }


                                    }


                                } else {
                                    HNDHelper.showErrorAlert(ChangeOrderActivity.this, getResources().getString(R.string.action_no_internet_message));
                                }
                            } else {
                                HNDHelper.showErrorAlert(ChangeOrderActivity.this, getResources().getString(R.string.changeorder_choose_chng_type));
                            }


                    }




                } else {
                    mHelper = new ChangeOrderSQlite(ChangeOrderActivity.this);
                    deleteDatabase(mHelper.DATABASE_NAME);


                    for (int k = 0; k < arrayList.size(); k++) {
                        mHelper = new ChangeOrderSQlite(ChangeOrderActivity.this);
                        dataBase = mHelper.getWritableDatabase();
                        ContentValues values = new ContentValues();
                        values.put(ChangeOrderSQlite.KEY_AMOUNT, arrayList.get(k).getJobtype());
                        values.put(ChangeOrderSQlite.KEY_TYPE, arrayList.get(k).getMaterial());
                        values.put(ChangeOrderSQlite.KEY_DESCRIPTION, arrayList.get(k).getMaterialdescription());
                        dataBase.insert(ChangeOrderSQlite.TABLE_NAME, null, values);

                    }

                    int nozero = 0;
                    for (int jk = 0; jk < arrayList.size(); jk++) {
                        String amount = "" + arrayList.get(jk).getMaterial();
                        if (amount.equalsIgnoreCase("")) {
                            nozero = 1;
                        } else {

                        }
                    }
                    if (nozero == 0) {
                        if (isInternetPresent) {
                            if (arrayList.size() == 0) {

                                postRequestConfirmBooking(ChangeOrderActivity.this, ServiceConstant.SENDCLIENTAPPROVAL);
                                // HNDHelper.showErrorAlert(ChangeOrderActivity.this, getResources().getString(R.string.changeorder_no_change_in_amt));
                            } else {

                                int nozeroforall = 0;
                                for (int jk = 0; jk < arrayList.size(); jk++) {
                                    String amount = "" + arrayList.get(jk).getJobtype();
                                    if (amount.equalsIgnoreCase("0") || amount.equalsIgnoreCase("")) {
                                        nozeroforall = 1;
                                    } else {

                                    }
                                }

                                if (nozeroforall == 0) {

                                    postRequestConfirmBooking(ChangeOrderActivity.this, ServiceConstant.SENDCLIENTAPPROVAL);

                                } else {
                                    HNDHelper.showErrorAlert(ChangeOrderActivity.this, getResources().getString(R.string.changeorder_enter_change_amount));
                                }


                            }


                        } else {
                            HNDHelper.showErrorAlert(ChangeOrderActivity.this, getResources().getString(R.string.action_no_internet_message));
                        }
                    } else {
                        HNDHelper.showErrorAlert(ChangeOrderActivity.this, getResources().getString(R.string.changeorder_choose_chng_type));
                    }

                }


            }
        });


    }


    void ChangeOrderOptionDialgShow() {
        final PkDialog mDialog = new PkDialog(ChangeOrderActivity.this);
        mDialog.setDialogTitle(getResources().getString(R.string.select_label));
        mDialog.setDialogMessage(getResources().getString(R.string.change_order_through));
        mDialog.setPositiveButton(
                getResources().getString(R.string.TitleSignature), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        SharedPreferences pref1 = getApplicationContext().getSharedPreferences("jjjooo", 0);
                        SharedPreferences.Editor editor1 = pref1.edit();
                        editor1.putString("jobbidd", "" + jobid);
                        editor1.apply();
                        editor1.commit();

                        is_selected_page="signature";

                        Intent intent = new Intent(getApplicationContext(), CaptureSignActivity.class);
                        intent.putExtra("page","change_order");
                        intent.putExtra("amount",updateamt.getText().toString());
                        startActivityForResult(intent, ServiceConstant.GetSignatureRequestCode);

                        mDialog.dismiss();




                    }
                }
        );
        mDialog.setNegativeButton(
                getResources().getString(R.string.otp_txt), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        is_selected_page="otp";

                        Intent intent = new Intent(getApplicationContext(), OtpPage.class);
                        intent.putExtra("page", "change_order");
                        intent.putExtra("jobId", jobid);
                        intent.putExtra("Type", "");
                        ArrayList<String> PrepaymentList = new ArrayList<>();
                        PrepaymentList.add("");
                        intent.putStringArrayListExtra("PrepaymentList", PrepaymentList);
                        startActivityForResult(intent, ServiceConstant.Change_order_OTP_RequestCode);

                        mDialog.dismiss();

                    }
                }
        );

        mDialog.show();
    }

    private void postRequestConfirmBooking(final Context aContext, String url) {

        loadingDialog();
        HashMap<String, String> params = new HashMap<>();
        params.put("job_id", jobid);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        String provider_id = pref.getString("provider_id", "");
        params.put("provider_id", provider_id);

//        mHelper = new ChangeOrderSQlite(ChangeOrderActivity.this);
//        dataBase = mHelper.getWritableDatabase();
//        Cursor mCursor = dataBase.rawQuery("SELECT * FROM " + ChangeOrderSQlite.TABLE_NAME, null);
//        int jk = 0;
//        if (mCursor.moveToFirst()) {
//            do {
//                params.put("miscellaneous[" + jk + "][name]", mCursor.getString(mCursor.getColumnIndex(ChangeOrderSQlite.KEY_TYPE)));
//                params.put("miscellaneous[" + jk + "][price]", mCursor.getString(mCursor.getColumnIndex(ChangeOrderSQlite.KEY_AMOUNT)));
//                params.put("miscellaneous[" + jk + "][description]", mCursor.getString(mCursor.getColumnIndex(ChangeOrderSQlite.KEY_DESCRIPTION)));
//
//                jk++;
//            } while (mCursor.moveToNext());
//        }

        if (arrayList.size() > 0) {
            for (int p = 0; p < arrayList.size(); p++) {
                params.put("miscellaneous[" + p + "][name]", arrayList.get(p).getMaterial());
                params.put("miscellaneous[" + p + "][price]", arrayList.get(p).getJobtype());
                params.put("miscellaneous[" + p + "][description]", arrayList.get(p).getMaterialdescription());
                if (arrayList.get(p).getStatus().equalsIgnoreCase("1")) {
                    params.put("miscellaneous[" + p + "][status]", "1");
                } else {
                    params.put("miscellaneous[" + p + "][status]", "0");
                }
            }
        }

        if (checkedhelp.isChecked()) {

            if(is_selected_page.equals("signature")){

                params.put("user_approval", "1");
                params.put("signature", storemybase64);

            } else {

                params.put("user_approval", "1");
                params.put("signature", "");
                params.put("changeorderOTP", OTP);
            }

        } else {

            params.put("user_approval", "0");
            params.put("signature", "");
            params.put("changeorderOTP", "");
        }

        if (amountde.getText().toString().equals("")) {
            params.put("amount_to_reduce[price]", "0");
        } else {
            params.put("amount_to_reduce[price]", amountde.getText().toString());
        }
        params.put("amount_to_reduce[source]", provider_id);
        params.put("amount_to_reduce[status]", "0");

        params.put("amount_to_reduce[description]", descriptionf.getText().toString());


        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {


                System.out.println("-------------get Confirm Category list Response----------------" + response);


                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {


                        try {
                            String PopUpMessage = "";
                            if (storemybase64.trim().length() > 0) {
                                PopUpMessage = getResources().getString(R.string.ChangeOrderAccept);
                            } else {
                                PopUpMessage = object.getString("response");
                            }

                            final PkDialog mDialog = new PkDialog(ChangeOrderActivity.this);
                            mDialog.setDialogTitle(getResources().getString(R.string.success_label));
                            mDialog.setDialogMessage(PopUpMessage);
                            mDialog.setPositiveButton(
                                    getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            finish();
                                            mDialog.dismiss();
                                        }
                                    }
                            );
                            mDialog.show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {

                        final PkDialog mDialog = new PkDialog(ChangeOrderActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.failed_label));
                        mDialog.setDialogMessage(object.getString("response"));
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        mDialog.dismiss();
                                    }
                                }
                        );


                        mDialog.show();

                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block


                    e.printStackTrace();
                }

                dismissDialog();


            }

            @Override
            public void onErrorListener() {
                dismissDialog();

            }
        });
    }


    public class changeorderadapter extends ArrayAdapter<changeorderpojo> {


        private ArrayList<changeorderpojo> items;
        private int layoutResourceId;
        private Context context;
        Typeface tf;
        SharedPreferences pref;
        String currency;
        SharedPreferences providerid;

        changeorderadapter(Context context, int layoutResourceId, ArrayList<changeorderpojo> items) {
            super(context, layoutResourceId, items);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.items = items;
            tf = Typeface.createFromAsset(getContext().getAssets(), "Poppins-Regular.ttf");
            pref = context.getSharedPreferences("logindetails", 0);
            currency = pref.getString("currency", "");
            providerid = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        }

        @NonNull
        @SuppressLint("ViewHolder")
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            View row = convertView;
            changeorderadapter.Holder holder = null;

            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new changeorderadapter.Holder();
            holder.bean = items.get(position);
            holder.deletecraftman = (ImageView) row.findViewById(R.id.deletecraftman);
            holder.deletecraftman.setTag(holder.bean);
            holder.maincolourchange = (LinearLayout) row.findViewById(R.id.maincolourchange);


            holder.materialdesc = (EditText) row.findViewById(R.id.materialdesc);
            holder.materialamount = (MaterialSpinner) row.findViewById(R.id.materialamount);


            holder.jobtype = (EditText) row.findViewById(R.id.jobtype);
            holder.jobtype.setTypeface(tf);
            holder.materialdesc.setTypeface(tf);
            holder.materialamount.setTypeface(tf);


            holder.jobtype.setText("");


            setVal1TextChangeListener(holder);


            setVal1TextChangeListener3(holder);


            row.setTag(holder);
            setupItem(holder);

            return row;
        }

        private void setupItem(final changeorderadapter.Holder holder) {
            holder.jobtype.setText(holder.bean.getJobtype());
            // holder.materialamount.setText(holder.bean.getMaterial());
            holder.materialdesc.setText(holder.bean.getMaterialdescription());

         /*   holder.jobtype.setOnTouchListener(new View.OnTouchListener()
            {
                public boolean onTouch(View arg0, MotionEvent arg1)
                {
                    Toast.makeText(getApplicationContext(),"fg",Toast.LENGTH_LONG).show();
                    return false;
                }
            });*/

            holder.jobtype.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View arg0, boolean hasfocus) {
                    if (hasfocus) {
                        // Toast.makeText(getApplicationContext(),"focused",Toast.LENGTH_LONG).show();
                        holder.bean.setStatus("0");


                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(holder.jobtype, InputMethodManager.SHOW_IMPLICIT);
                        // adapter.notifyDataSetChanged();

                    } else {


                    }
                }
            });


            assignchangearray.clear();
            assignchangearray.add(holder.bean.getMaterial());

            for (int jk = 0; jk < changearray.size(); jk++) {
                if (assignchangearray.contains("" + changearray.get(jk))) {

                } else {
                    assignchangearray.add(changearray.get(jk));
                }
            }


            holder.materialamount.setItems(assignchangearray.toArray());
            holder.materialamount.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

                @Override
                public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                    holder.bean.setMaterial(item);
                }
            });


            if (holder.bean.getJobdescription().equalsIgnoreCase("1")) {
                holder.deletecraftman.setVisibility(VISIBLE);
                holder.jobtype.setFocusable(true);
                holder.jobtype.setEnabled(true);

                holder.materialamount.setEnabled(true);
                holder.materialamount.setFocusable(true);


                holder.materialdesc.setEnabled(true);
                holder.materialdesc.setFocusable(true);
            } else {
                holder.deletecraftman.setVisibility(View.GONE);
                holder.jobtype.setEnabled(false);
                holder.jobtype.setFocusable(false);

                holder.materialamount.setEnabled(false);
                holder.materialamount.setFocusable(false);


                holder.materialdesc.setEnabled(false);
                holder.materialdesc.setFocusable(false);
            }
            if (holder.bean.getStatus().equals("1")) {
                holder.materialamount.setClickable(false);
                holder.materialamount.setEnabled(false);
                holder.materialamount.setFocusable(false);

                holder.jobtype.setEnabled(false);
                holder.jobtype.setFocusable(false);

                holder.materialdesc.setEnabled(false);
                holder.materialdesc.setFocusable(false);

                holder.deletecraftman.setVisibility(View.GONE);

                holder.maincolourchange.setBackgroundResource(R.drawable.straightgreen);
            } else if (holder.bean.getStatus().equals("2")) {
                holder.materialamount.setClickable(true);
                holder.materialamount.setEnabled(true);
                holder.materialamount.setFocusable(true);

                holder.materialdesc.setClickable(true);
                holder.materialdesc.setEnabled(true);
                holder.materialdesc.setFocusable(true);

                holder.jobtype.setEnabled(true);
                holder.jobtype.setFocusable(true);

                holder.deletecraftman.setVisibility(VISIBLE);

                holder.maincolourchange.setBackgroundResource(R.drawable.straightred);
            } else {
                holder.materialamount.setClickable(true);
                holder.materialamount.setEnabled(true);
                holder.materialamount.setFocusable(true);

                holder.materialdesc.setClickable(true);
                holder.materialdesc.setEnabled(true);
                holder.materialdesc.setFocusable(true);

                holder.jobtype.setEnabled(true);
                holder.jobtype.setFocusable(true);

                holder.deletecraftman.setVisibility(VISIBLE);

                holder.maincolourchange.setBackgroundResource(R.drawable.straightblack);
            }

        }


        private void setVal1TextChangeListener(final changeorderadapter.Holder holder) {
            holder.jobtype.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.toString().length() >= 0) {

                        holder.bean.setJobtype(s.toString());

                        Double addvalue = currentamtzz;
                        for (int i = 0; i < arrayList.size(); i++) {
                            String amount = arrayList.get(i).getJobtype();
                            String statt = arrayList.get(i).getStatus();
                            if (amount.equalsIgnoreCase("") || amount.equalsIgnoreCase(null) | amount.equalsIgnoreCase("null")) {
                            } else {
                                System.out.println("amount---->" + amount);
                                if (statt.equals("2")) {

                                } else {
                                    addvalue = (addvalue + Double.parseDouble(amount));
                                }

                            }
                        }

                        DecimalFormat twoDForm = new DecimalFormat("#.##");
                        Double rt = Double.valueOf(twoDForm.format(addvalue));
                        String grandtotal = "" + rt;

                        if (grandtotal.endsWith(".0") || grandtotal.endsWith(".5") || grandtotal.endsWith(".1") || grandtotal.endsWith(".2") || grandtotal.endsWith(".3") || grandtotal.endsWith(".4") || grandtotal.endsWith(".6") || grandtotal.endsWith(".7") || grandtotal.endsWith(".8") || grandtotal.endsWith(".9")) {
                            updateamt.setText(currencysymbol + rt + "0");
                            updatestoreall = Double.parseDouble(rt + "0");

                            if (amountde.getText().toString().endsWith(".")) {
                            } else {
                                if (amountde.getText().toString().equals("")) {


                                    Double minus = updatestoreall - 0.0 + myCalculatedTax;
                                    String grandtotalrt = "" + minus;
                                    if (grandtotalrt.endsWith(".0") || grandtotalrt.endsWith(".5") || grandtotalrt.endsWith(".1") || grandtotalrt.endsWith(".2") || grandtotalrt.endsWith(".3") || grandtotalrt.endsWith(".4") || grandtotalrt.endsWith(".6") || grandtotalrt.endsWith(".7") || grandtotalrt.endsWith(".8") || grandtotalrt.endsWith(".9")) {
                                        grandam.setText(currencysymbol + minus + "0");
                                    } else {
                                        grandam.setText(currencysymbol + minus);
                                    }

                                } else {
                                    Double typing = Double.parseDouble(amountde.getText().toString());
                                    Double minus = updatestoreall - typing + myCalculatedTax;
                                    String grandtotalrt = "" + minus;
                                    if (grandtotalrt.endsWith(".0") || grandtotalrt.endsWith(".5") || grandtotalrt.endsWith(".1") || grandtotalrt.endsWith(".2") || grandtotalrt.endsWith(".3") || grandtotalrt.endsWith(".4") || grandtotalrt.endsWith(".6") || grandtotalrt.endsWith(".7") || grandtotalrt.endsWith(".8") || grandtotalrt.endsWith(".9")) {
                                        grandam.setText(currencysymbol + minus + "0");
                                    } else {
                                        grandam.setText(currencysymbol + minus);
                                    }
                                }
                            }


                        } else {
                            updateamt.setText(currencysymbol + rt);
                            updatestoreall = Double.parseDouble("" + rt);

                            if (amountde.getText().toString().endsWith(".")) {


                            } else {
                                if (amountde.getText().toString().equals("")) {

                                    Double minus = updatestoreall - 0.0 + myCalculatedTax;
                                    String grandtotalrt = "" + minus;
                                    if (grandtotalrt.endsWith(".0") || grandtotalrt.endsWith(".5") || grandtotalrt.endsWith(".1") || grandtotalrt.endsWith(".2") || grandtotalrt.endsWith(".3") || grandtotalrt.endsWith(".4") || grandtotalrt.endsWith(".6") || grandtotalrt.endsWith(".7") || grandtotalrt.endsWith(".8") || grandtotalrt.endsWith(".9")) {
                                        grandam.setText(currencysymbol + minus + "0");
                                    } else {
                                        grandam.setText(currencysymbol + minus);
                                    }

                                } else {
                                    Double typing = Double.parseDouble(amountde.getText().toString());
                                    Double minus = updatestoreall - typing + myCalculatedTax;
                                    String grandtotalrt = "" + minus;
                                    if (grandtotalrt.endsWith(".0") || grandtotalrt.endsWith(".5") || grandtotalrt.endsWith(".1") || grandtotalrt.endsWith(".2") || grandtotalrt.endsWith(".3") || grandtotalrt.endsWith(".4") || grandtotalrt.endsWith(".6") || grandtotalrt.endsWith(".7") || grandtotalrt.endsWith(".8") || grandtotalrt.endsWith(".9")) {
                                        grandam.setText(currencysymbol + minus + "0");
                                    } else {
                                        grandam.setText(currencysymbol + minus);
                                    }
                                }
                            }
                        }


                    }


                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }


        private void setVal1TextChangeListener3(final changeorderadapter.Holder holder) {
            holder.materialdesc.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.toString().length() > 0)
                        holder.bean.setMaterialdescription(s.toString());
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }

        public class Holder {
            ImageView deletecraftman;
            changeorderpojo bean;
            EditText jobtype, materialdesc;
            MaterialSpinner materialamount;
            LinearLayout maincolourchange;
        }


    }


    private void myjobOngoingDetail(Context mContext, String url) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        String provider_id = pref.getString("provider_id", "");
        jsonParams.put("provider_id", provider_id);
        jsonParams.put("job_id", jobid);


        loadingDialog();

        ServiceRequest mservicerequest = new ServiceRequest(mContext);

        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------reponsedetail-------------------" + response);
                Log.e("detail", response);
                String Str_status = "", Str_Response = "";

                try {

                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        addva.setVisibility(VISIBLE);

                        updateamt.setVisibility(VISIBLE);

                        JSONObject object = jobject.getJSONObject("response");

                        JSONObject object2 = object.getJSONObject("job");
                        if (object2.has("purchase_order")) {
                            purchasetytpeheading.setVisibility(View.VISIBLE);
                            PurchaseOrderNumber.setVisibility(View.VISIBLE);
                            PurchaseOrderNumber.setText(object2.getString("purchase_order"));
                        } else {
                            purchasetytpeheading.setVisibility(View.GONE);
                            PurchaseOrderNumber.setVisibility(View.GONE);
                        }
                        String totalcost = "0";
                        if (object2.has("task_cost")) {
                            totalcost = object2.getString("task_cost");

                        }

                        if (object2.has("ctax")) {
                           myCalculatedTax = Double.parseDouble(object2.getString("ctax"));
                         //   myCalculatedTax = 100.0;
                        }

                        if (object2.has("signatures")) {

                            if (object2.isNull("signatures")) {

                            } else {
                                String signaturesjj = object2.getString("signatures");

                                JSONObject signaturesjjobj = new JSONObject(signaturesjj);
                                if (signaturesjjobj.has("ChangeOrderActivity")) {
                                    String changeorder = signaturesjjobj.getString("ChangeOrderActivity");

                                    if (changeorder.equals("") || changeorder.equals(" ")) {
                                        setsignimage.setVisibility(View.GONE);
                                        TextViewSignatureTitle.setVisibility(View.GONE);
                                    } else {
                                        Picasso.with(ChangeOrderActivity.this).load(/*ServiceConstant.BASE_URLIMAGE+*/changeorder)
                                                .resize(500, 500)
                                                .onlyScaleDown()
                                                .placeholder(R.drawable.fileupload)
                                                .into(setsignimage);

                                        setsignimage.setVisibility(VISIBLE);
                                        TextViewSignatureTitle.setVisibility(View.VISIBLE);
                                        storemybase64 = "cap";
                                        /*checkedhelp.setChecked(true);*/
                                    }


                                } else {
                                    setsignimage.setVisibility(View.GONE);
                                    TextViewSignatureTitle.setVisibility(View.GONE);
                                }


                            }
                        }

                        if (object2.has("amount_to_reduce")) {

                            if (object2.isNull("amount_to_reduce")) {

                            } else {
                                String amount_to_reduce = object2.getString("amount_to_reduce");
                                JSONObject amount_to_reduceobj = new JSONObject(amount_to_reduce);
                                if (amount_to_reduceobj.has("price")) {
                                    deductedamountfromserver = amount_to_reduceobj.getString("price");
                                } else {

                                }


                                if (amount_to_reduceobj.has("description")) {
                                    descriptionf.setText(amount_to_reduceobj.getString("description"));
                                } else {

                                }

                            }


                        } else {

                        }


                        user_id = object2.getString("user_id");

                        if (object2.has("user_name")) {
                            clinetnamevalue.setText(object2.getString("user_name"));
                        }


                        JSONArray service_type1 = object.getJSONArray("misc_type");

                        if (service_type1.length() > 0) {
                            changearray.clear();
                            for (int b1 = 0; b1 < service_type1.length(); b1++) {
                                String type = "" + service_type1.getString(b1);
                                if (changearray.contains(type)) {

                                } else {
                                    changearray.add(type);
                                    System.out.println("typee========" + type);
                                }

                            }
                            addva.setVisibility(VISIBLE);
                        } else {
                            addva.setVisibility(View.GONE);
                        }


                        String misc_approval_status = object2.getString("misc_approval_status");


                        currentamtzz = Double.parseDouble(totalcost);
                        String grandtotal = "" + currentamtzz;
                        if (grandtotal.endsWith(".0") || grandtotal.endsWith(".5") || grandtotal.endsWith(".1") || grandtotal.endsWith(".2")
                                || grandtotal.endsWith(".3") || grandtotal.endsWith(".4") || grandtotal.endsWith(".6") || grandtotal.endsWith(".7") ||
                                grandtotal.endsWith(".8") || grandtotal.endsWith(".9")) {
                            curentamt.setText("$" + currentamtzz + "0");
                        } else {
                            curentamt.setText("$" + currentamtzz);

                        }
                        String calculatedTax = "" + myCalculatedTax;
                        if (calculatedTax.endsWith(".0") || calculatedTax.endsWith(".5") || calculatedTax.endsWith(".1") || calculatedTax.endsWith(".2")
                                || calculatedTax.endsWith(".3") || calculatedTax.endsWith(".4") || calculatedTax.endsWith(".6") || calculatedTax.endsWith(".7") ||
                                calculatedTax.endsWith(".8") || calculatedTax.endsWith(".9")) {
                            myCalculatedTaxAmtTXT.setText("$" + myCalculatedTax + "0");
                        } else {
                            myCalculatedTaxAmtTXT.setText("$" + myCalculatedTax);
                        }


                        stratvalue = currentamtzz;
                        JSONArray service_type = object2.getJSONArray("miscellaneous");


                        if (service_type.length() > 0) {
                            changeorder.setVisibility(VISIBLE);
                            if (misc_approval_status.equalsIgnoreCase("0")) {
                                //   acceptclick.setVisibility(View.VISIBLE);
                                changeorder.setText(getResources().getString(R.string.changeorder_waiting_for_approval));
                            } else if (misc_approval_status.equalsIgnoreCase("1")) {
                                // acceptclick.setVisibility(View.GONE);
                                changeorder.setText(getResources().getString(R.string.changeorder_accepted));
                            } else if (misc_approval_status.equalsIgnoreCase("2")) {
                                // acceptclick.setVisibility(View.GONE);
                                changeorder.setText(getResources().getString(R.string.changeorder_rejected));
                            }
                        } else {
                            changeorder.setVisibility(VISIBLE);
                            // acceptclick.setVisibility(View.GONE);
                            if (misc_approval_status.equalsIgnoreCase("0")) {
                                //   acceptclick.setVisibility(View.VISIBLE);
                                changeorder.setText(getResources().getString(R.string.changeorder_waiting_for_approval));
                            } else if (misc_approval_status.equalsIgnoreCase("1")) {
                                // acceptclick.setVisibility(View.GONE);
                                changeorder.setText(getResources().getString(R.string.changeorder_accepted));
                            } else if (misc_approval_status.equalsIgnoreCase("2")) {
                                // acceptclick.setVisibility(View.GONE);
                                changeorder.setText(getResources().getString(R.string.changeorder_rejected));
                            }
                        }

                        arrayList.clear();
                        ;
                        for (int b = 0; b < service_type.length(); b++) {
                            JSONObject miscellaneousobj = service_type.getJSONObject(b);
                            String price = miscellaneousobj.getString("price");
                            String name = miscellaneousobj.getString("name");
                            String description = miscellaneousobj.getString("description");
                            String source = miscellaneousobj.getString("edit");


                            String status = "0";
                            if (miscellaneousobj.has("status")) {
                                status = miscellaneousobj.getString("status");
                            } else {

                            }

                            if (source.equals("1")) {
                                if (price.equalsIgnoreCase("null") || price.equalsIgnoreCase(null)) {
                                    changeorderpojo atomPayment = new changeorderpojo();
                                    atomPayment.setJobtype("0");
                                    atomPayment.setMaterial("" + name);
                                    atomPayment.setMaterialdescription("" + description);
                                    atomPayment.setJobdescription("" + source);
                                    atomPayment.setStatus("" + status);
                                    arrayList.add(atomPayment);
                                } else {
                                    changeorderpojo atomPayment = new changeorderpojo();
                                    atomPayment.setJobtype("" + price);
                                    atomPayment.setMaterial("" + name);
                                    atomPayment.setMaterialdescription("" + description);
                                    atomPayment.setJobdescription(source);
                                    atomPayment.setStatus("" + status);
                                    arrayList.add(atomPayment);
                                }
                            } else {

                            }


                        }

                        if (arrayList.size() > 0) {
                            adapter = new changeorderadapter(ChangeOrderActivity.this, R.layout.changeorderitem, arrayList);
                            listView.setAdapter(adapter);
                        }
                        scrollView.scrollTo(0, 0);
                        scrollView.pageScroll(View.FOCUS_UP);
                        scrollView.smoothScrollTo(0, 0);
                        String grandtotasl = "" + currentamtzz;

                        if (grandtotasl.endsWith(".0") || grandtotasl.endsWith(".5") || grandtotasl.endsWith(".1") || grandtotasl.endsWith(".2") || grandtotasl.endsWith(".3") || grandtotasl.endsWith(".4") || grandtotasl.endsWith(".6") || grandtotasl.endsWith(".7") || grandtotasl.endsWith(".8") || grandtotasl.endsWith(".9")) {
                            updateamt.setText("$" + currentamtzz + "0");
                            updatestoreall = Double.parseDouble(currentamtzz + "0");

                            if (amountde.getText().toString().endsWith(".")) {


                            } else {
                                if (amountde.getText().toString().equals("")) {

                                    Double minus = updatestoreall - 0.0 + myCalculatedTax;
                                    String grandtotalrt = "" + minus;
                                    if (grandtotalrt.endsWith(".0") || grandtotalrt.endsWith(".5") || grandtotalrt.endsWith(".1") || grandtotalrt.endsWith(".2") || grandtotalrt.endsWith(".3") || grandtotalrt.endsWith(".4") || grandtotalrt.endsWith(".6") || grandtotalrt.endsWith(".7") || grandtotalrt.endsWith(".8") || grandtotalrt.endsWith(".9")) {
                                        grandam.setText(currencysymbol + minus + "0");
                                    } else {
                                        grandam.setText(currencysymbol + minus);
                                    }

                                } else {
                                    Double typing = Double.parseDouble(amountde.getText().toString());
                                    Double minus = updatestoreall - typing + myCalculatedTax;
                                    String grandtotalrt = "" + minus;
                                    if (grandtotalrt.endsWith(".0") || grandtotalrt.endsWith(".5") || grandtotalrt.endsWith(".1") || grandtotalrt.endsWith(".2") || grandtotalrt.endsWith(".3") || grandtotalrt.endsWith(".4") || grandtotalrt.endsWith(".6") || grandtotalrt.endsWith(".7") || grandtotalrt.endsWith(".8") || grandtotalrt.endsWith(".9")) {
                                        grandam.setText(currencysymbol + minus + "0");
                                    } else {
                                        grandam.setText(currencysymbol + minus);
                                    }
                                }
                            }


                        } else {
                            updateamt.setText("$" + currentamtzz);
                            updatestoreall = Double.parseDouble("" + currentamtzz);


                            if (amountde.getText().toString().endsWith(".")) {


                            } else {
                                if (amountde.getText().toString().equals("")) {

                                    Double minus = updatestoreall - 0.0 + myCalculatedTax;
                                    String grandtotalrt = "" + minus;
                                    if (grandtotalrt.endsWith(".0") || grandtotalrt.endsWith(".5") || grandtotalrt.endsWith(".1") || grandtotalrt.endsWith(".2") || grandtotalrt.endsWith(".3") || grandtotalrt.endsWith(".4") || grandtotalrt.endsWith(".6") || grandtotalrt.endsWith(".7") || grandtotalrt.endsWith(".8") || grandtotalrt.endsWith(".9")) {
                                        grandam.setText(currencysymbol + minus + "0");
                                    } else {
                                        grandam.setText(currencysymbol + minus);
                                    }

                                } else {
                                    Double typing = Double.parseDouble(amountde.getText().toString());
                                    Double minus = updatestoreall - typing + myCalculatedTax;
                                    String grandtotalrt = "" + minus;
                                    if (grandtotalrt.endsWith(".0") || grandtotalrt.endsWith(".5") || grandtotalrt.endsWith(".1") || grandtotalrt.endsWith(".2") || grandtotalrt.endsWith(".3") || grandtotalrt.endsWith(".4") || grandtotalrt.endsWith(".6") || grandtotalrt.endsWith(".7") || grandtotalrt.endsWith(".8") || grandtotalrt.endsWith(".9")) {
                                        grandam.setText(currencysymbol + minus + "0");
                                    } else {
                                        grandam.setText(currencysymbol + minus);
                                    }
                                }
                            }
                        }


                        amountde.setText(deductedamountfromserver);

                    } else {
                        final PkDialog mdialog = new PkDialog(ChangeOrderActivity.this);
                        mdialog.setDialogTitle(getResources().getString(R.string.server_lable_header));
                        mdialog.setDialogMessage(Str_Response);
                        mdialog.setCancelOnTouchOutside(false);
                        mdialog.setPositiveButton(getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        mdialog.dismiss();
                                        finish();

                                    }
                                }
                        );
                        mdialog.show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                dismissDialog();


            }

            @Override
            public void onErrorListener() {

                dismissDialog();


            }
        });

    }


    public void itemDeleteListner(View v) {

        changeorderpojo itemToRemove = (changeorderpojo) v.getTag();
        adapter.remove(itemToRemove);
        adapter.notifyDataSetChanged();
        Snackbar snackbar = Snackbar
                .make(updateamt, getResources().getString(R.string.bill_attachment_deleted_successfuly), Snackbar.LENGTH_LONG);
        TextView tv = (TextView) (snackbar.getView()).findViewById(com.google.android.material.R.id.snackbar_text);
        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Poppins-Medium.ttf");
        tv.setTypeface(font);
        tv.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white_color));
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.green));
        snackbar.show();

        if (arrayList.size() == 0) {
            String grandtotasl = "" + stratvalue;

            if (grandtotasl.endsWith(".0") || grandtotasl.endsWith(".5") || grandtotasl.endsWith(".1") || grandtotasl.endsWith(".2") || grandtotasl.endsWith(".3") || grandtotasl.endsWith(".4") || grandtotasl.endsWith(".6") || grandtotasl.endsWith(".7") || grandtotasl.endsWith(".8") || grandtotasl.endsWith(".9")) {
                updateamt.setText("$" + stratvalue + "0");
                updatestoreall = Double.parseDouble(stratvalue + "0");


                if (amountde.getText().toString().endsWith(".")) {


                } else {
                    if (amountde.getText().toString().equals("")) {

                        Double minus = updatestoreall - 0.0 + myCalculatedTax;
                        String grandtotalrt = "" + minus;
                        if (grandtotalrt.endsWith(".0") || grandtotalrt.endsWith(".5") || grandtotalrt.endsWith(".1") || grandtotalrt.endsWith(".2") || grandtotalrt.endsWith(".3") || grandtotalrt.endsWith(".4") || grandtotalrt.endsWith(".6") || grandtotalrt.endsWith(".7") || grandtotalrt.endsWith(".8") || grandtotalrt.endsWith(".9")) {
                            grandam.setText(currencysymbol + minus + "0");
                        } else {
                            grandam.setText(currencysymbol + minus);
                        }

                    } else {
                        Double typing = Double.parseDouble(amountde.getText().toString());
                        Double minus = updatestoreall - typing + myCalculatedTax;
                        String grandtotalrt = "" + minus;
                        if (grandtotalrt.endsWith(".0") || grandtotalrt.endsWith(".5") || grandtotalrt.endsWith(".1") || grandtotalrt.endsWith(".2") || grandtotalrt.endsWith(".3") || grandtotalrt.endsWith(".4") || grandtotalrt.endsWith(".6") || grandtotalrt.endsWith(".7") || grandtotalrt.endsWith(".8") || grandtotalrt.endsWith(".9")) {
                            grandam.setText(currencysymbol + minus + "0");
                        } else {
                            grandam.setText(currencysymbol + minus);
                        }
                    }
                }


            } else {
                updateamt.setText("$" + stratvalue);
                updatestoreall = Double.parseDouble("" + stratvalue);


                if (amountde.getText().toString().endsWith(".")) {

                } else {
                    if (amountde.getText().toString().equals("")) {

                        Double minus = updatestoreall - 0.0 + myCalculatedTax;
                        String grandtotalrt = "" + minus;
                        if (grandtotalrt.endsWith(".0") || grandtotalrt.endsWith(".5") || grandtotalrt.endsWith(".1") || grandtotalrt.endsWith(".2") || grandtotalrt.endsWith(".3") || grandtotalrt.endsWith(".4") || grandtotalrt.endsWith(".6") || grandtotalrt.endsWith(".7") || grandtotalrt.endsWith(".8") || grandtotalrt.endsWith(".9")) {
                            grandam.setText(currencysymbol + minus + "0");
                        } else {
                            grandam.setText(currencysymbol + minus);
                        }


                    } else {
                        Double typing = Double.parseDouble(amountde.getText().toString());
                        Double minus = updatestoreall - typing + myCalculatedTax;
                        String grandtotalrt = "" + minus;
                        if (grandtotalrt.endsWith(".0") || grandtotalrt.endsWith(".5") || grandtotalrt.endsWith(".1") || grandtotalrt.endsWith(".2") || grandtotalrt.endsWith(".3") || grandtotalrt.endsWith(".4") || grandtotalrt.endsWith(".6") || grandtotalrt.endsWith(".7") || grandtotalrt.endsWith(".8") || grandtotalrt.endsWith(".9")) {
                            grandam.setText(currencysymbol + minus + "0");
                        } else {
                            grandam.setText(currencysymbol + minus);
                        }
                    }
                }
            }


        }


    }


    public void init_modal_bottomsheet() {
        View modalbottomsheet = getLayoutInflater().inflate(R.layout.acceptcall, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(modalbottomsheet);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        TextView accept = (TextView) modalbottomsheet.findViewById(R.id.acceptcall);
        TextView rejectcall = (TextView) modalbottomsheet.findViewById(R.id.rejectcall);


        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    dialog.dismiss();
                    accepthit(ChangeOrderActivity.this, ServiceConstant.CLIENTAPPROVE, 1);
                } else {
                    final PkDialog mDialog = new PkDialog(ChangeOrderActivity.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.internet_label));
                    mDialog.setCancelOnTouchOutside(false);
                    mDialog.setDialogMessage(getResources().getString(R.string.action_no_internet_message));
                    mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                            finish();
                        }
                    });
                    mDialog.show();
                }
            }
        });


        rejectcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    dialog.dismiss();
                    accepthit(ChangeOrderActivity.this, ServiceConstant.CLIENTAPPROVE, 2);
                } else {
                    final PkDialog mDialog = new PkDialog(ChangeOrderActivity.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.internet_label));
                    mDialog.setCancelOnTouchOutside(false);
                    mDialog.setDialogMessage(getResources().getString(R.string.action_no_internet_message));
                    mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                            finish();
                        }
                    });
                    mDialog.show();
                }
            }
        });

        dialog.show();

    }


    private void accepthit(Context mContext, String url, final int sttaus) {


        loadingDialog();


        HashMap<String, String> jsonParams = new HashMap<String, String>();


        jsonParams.put("user_id", user_id);
        jsonParams.put("job_id", jobid);
        jsonParams.put("approval_status", "" + sttaus);


        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("payment", response);

                String Str_status = "", Str_response = "", Str_jobDescription = "", Str_NeedPayment = "", Str_Currency = "", Str_BtnGroup = "";
                dismissDialog();


                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        // acceptclick.setVisibility(View.GONE);
                        if (sttaus == 1) {
                            changeorder.setText(getResources().getString(R.string.changeorder_request_accepted));
                        } else {
                            changeorder.setText(getResources().getString(R.string.changeorder_request_rejected));
                        }
                        Str_response = jobject.getString("response");
                        // HNDHelper.showResponseTitle("Success",ChangeOrderActivity.this, Str_response);

                    } else {
                        //  acceptclick.setVisibility(View.VISIBLE);
                        Str_response = jobject.getString("response");
                        HNDHelper.showResponseTitle(getResources().getString(R.string.action_sorry), ChangeOrderActivity.this, Str_response);
                    }


                    System.out.println("payment1---------------------------");

                } catch (Exception e) {
                    e.printStackTrace();
                }


                dismissDialog();


            }

            @Override
            public void onErrorListener() {
                dismissDialog();


            }
        });
    }


    private void loadingDialog() {

        dialog = new LoadingDialog(ChangeOrderActivity.this);
        dialog.setLoadingTitle(getResources().getString(R.string.loading_in));
        dialog.show();
    }

    private void dismissDialog() {

        dialog.dismiss();
    }

    public class signature extends View {
        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();

        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();

        public signature(Context context, AttributeSet attrs) {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        @SuppressLint("WrongThread")
        public void save(View v) {
            Log.v("log_tag", "Width: " + v.getWidth());
            Log.v("log_tag", "Height: " + v.getHeight());
            if (mBitmap == null) {
                mBitmap = Bitmap.createBitmap(mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);
                ;
            }
            Canvas canvas = new Canvas(mBitmap);
            try {
                FileOutputStream mFileOutStream = new FileOutputStream(mypath);

                v.draw(canvas);
                mBitmap.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream);
                mFileOutStream.flush();
                mFileOutStream.close();
                String url = MediaStore.Images.Media.insertImage(getContentResolver(), mBitmap, "title", null);
                Log.v("log_tag", "url: " + mypath.toString());

                Bitmap src = BitmapFactory.decodeFile(mypath.toString());
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                src.compress(Bitmap.CompressFormat.PNG, 100, baos);
                //  byte[] data = baos.toByteArray();
                String imgString = Base64.encodeToString(getBytesFromBitmap(src),
                        Base64.NO_WRAP);
                Log.v("log_tag", "Base64: " + imgString);

                setsignimage.setVisibility(VISIBLE);
                TextViewSignatureTitle.setVisibility(View.VISIBLE);

                byte[] decodedString = Base64.decode(imgString, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                setsignimage.setImageBitmap(decodedByte);

                setsignimage.setRotation(90);

                //In case you want to delete the file
                //boolean deleted = mypath.delete();
                //Log.v("log_tag","deleted: " + mypath.toString() + deleted);
                //If you want to convert the image to string use base64 converter

            } catch (Exception e) {
                Log.v("log_tag", e.toString());
            }
        }

        public void clear() {
            path.reset();
            invalidate();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawPath(path, paint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float eventX = event.getX();
            float eventY = event.getY();
            mGetSign.setEnabled(true);
            mGetSign.setVisibility(VISIBLE);

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:

                case MotionEvent.ACTION_UP:

                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++) {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;

                default:
                    debug("Ignored touch event: " + event.toString());
                    return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void debug(String string) {
        }

        private void expandDirtyRect(float historicalX, float historicalY) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX;
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX;
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY;
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY) {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }

    private boolean captureSignature() {

        boolean error = false;
        String errorMessage = "";


        if (yourName.getText().toString().equalsIgnoreCase("")) {
            errorMessage = errorMessage + "Please enter your Name\n";
            error = true;
        }

        if (error) {
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP, 105, 50);
            toast.show();
        }

        return error;
    }

    private String getTodaysDate() {

        final Calendar c = Calendar.getInstance();
        int todaysDate = (c.get(Calendar.YEAR) * 10000) +
                ((c.get(Calendar.MONTH) + 1) * 100) +
                (c.get(Calendar.DAY_OF_MONTH));
        Log.w("DATE:", String.valueOf(todaysDate));
        return (String.valueOf(todaysDate));

    }

    private String getCurrentTime() {

        final Calendar c = Calendar.getInstance();
        int currentTime = (c.get(Calendar.HOUR_OF_DAY) * 10000) +
                (c.get(Calendar.MINUTE) * 100) +
                (c.get(Calendar.SECOND));
        Log.w("TIME:", String.valueOf(currentTime));
        return (String.valueOf(currentTime));

    }


    private boolean prepareDirectory() {
        try {
            if (makedirs()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }
    }

    private boolean makedirs() {
        File tempdir = new File(tempDir);
        if (!tempdir.exists())
            tempdir.mkdirs();

        if (tempdir.isDirectory()) {
            File[] files = tempdir.listFiles();
            for (File file : files) {
                if (!file.delete()) {
                    System.out.println("Failed to delete " + file);
                }
            }
        }
        return (tempdir.isDirectory());
    }

    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ServiceConstant.GetSignatureRequestCode && resultCode == RESULT_OK) {
            String bitmaa = saveim.getString("sign", "");
            if (bitmaa.equals("") || bitmaa.equals(" ")) {
                storemybase64 = "";
                setsignimage.setVisibility(View.GONE);
                TextViewSignatureTitle.setVisibility(View.GONE);
            } else {
                Bitmap src = BitmapFactory.decodeFile(bitmaa);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                src.compress(Bitmap.CompressFormat.PNG, 100, baos);
                //  byte[] data = baos.toByteArray();
                storemybase64 = Base64.encodeToString(getBytesFromBitmap(src),
                        Base64.NO_WRAP);
                Log.v("log_tag", "Base64: " + storemybase64);


                setsignimage.setVisibility(VISIBLE);
                TextViewSignatureTitle.setVisibility(View.VISIBLE);

                byte[] decodedString = Base64.decode(storemybase64, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                setsignimage.setImageBitmap(decodedByte);
                save.setText(getResources().getString(R.string.send_sig_to_server));
                // setsignimage.setRotation(90);
            }
        } else if(requestCode == ServiceConstant.Change_order_OTP_RequestCode && resultCode == RESULT_OK){

               if(data!=null){

                   OTP=data.getStringExtra("otp");

               }
        } else{

            checkedhelp.setChecked(false);
        }
    }
}
