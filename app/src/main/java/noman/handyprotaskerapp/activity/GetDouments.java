package noman.handyprotaskerapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.socket.client.Url;
import noman.handyprotaskerapp.Adapter.DocumentAdapter;
import noman.handyprotaskerapp.ApiClient.ApiClient;
import noman.handyprotaskerapp.ApiInterface.ApiInterface;
import noman.handyprotaskerapp.ApiInterface.OnclickListener;
import noman.handyprotaskerapp.Pojo.DocumentListsItem;
import noman.handyprotaskerapp.Pojo.DocumentPojo;
import noman.handyprotaskerapp.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class GetDouments extends AppCompatActivity {


    RecyclerView recyclerView;
    DocumentAdapter adapter;
    File path,dirfile,finalpath,path1,path2;
    LinearLayout layout_back_ongoingback;
    TextView nodocumentfound;
//    DocumentListsItem heroList;
    String jobid = "";
    long downloadID=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_douments);
        registerReceiver(onDownloadComplete,new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        initialize();


    }

    public void initialize(){

        final Intent intent = getIntent();

        jobid = intent.getStringExtra("Job_ID");
        layout_back_ongoingback = findViewById(R.id.layout_back_ongoingback);
        nodocumentfound = findViewById(R.id.nodocumentfound);
        recyclerView = findViewById(R.id.documentlist);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        layout_back_ongoingback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

//        heroList = new DocumentListsItem();

        ApiInterface apiInterface = ApiClient.getdata().create(ApiInterface.class);

        HashMap<String,String> map = new HashMap<>();
        map.put("job_id",jobid);

        Call<DocumentPojo> call = apiInterface.geImgData(map);


        call.enqueue(new Callback<DocumentPojo>() {
            @Override
            public void onResponse(Call<DocumentPojo> call, Response<DocumentPojo> response) {
                if (response.body() != null) {

//                  String  heroList = response.body();
                        final ArrayList<DocumentListsItem> heroList = response.body().getDocumentLists();

                        if(heroList.size()==0){
                            nodocumentfound.setVisibility(View.VISIBLE);
                        }


                        adapter = new DocumentAdapter(GetDouments.this, heroList, new OnclickListener() {
                            @Override
                            public void onclick(int position, String name) {

                                if (name.equalsIgnoreCase("document_view")) {

                                    Intent intent1 = new Intent(GetDouments.this, DocumentWebVIew.class);
                                    intent1.putExtra("url", heroList.get(position).getUrl());
                                    startActivity(intent1);
                                }
                                if (name.equalsIgnoreCase("download_document")) {
                                    path = new File("/HandyproCraftman/" + new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date()) + heroList.get(position).getUrl());
                                    dirfile = new File(path, Environment.DIRECTORY_DOWNLOADS);
                                    finalpath = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), String.valueOf(path));
                                    DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                                    DownloadManager.Request request = new DownloadManager.Request(Uri.parse(heroList.get(position).getUrl()));
                                    request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE);
                                    request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
                                    request.setTitle("Downloading");
                                    request.setDescription("Please wait...");
                                    request.setVisibleInDownloadsUi(true);
                                    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, String.valueOf(path));
                                    // downloadManager.enqueue(request);
                                    downloadID = downloadManager.enqueue(request);


//                                 DownloadManager downloadmanager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
//                                 Uri uri = Uri.parse(heroList.get(position).getUrl());
//
//                                 DownloadManager.Request request = new DownloadManager.Request(uri);
//                                 request.setTitle("HandyProCraftMan");
//                                 request.setDescription("Downloading");
//                                 request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//                                 request.setVisibleInDownloadsUi(false);
//                                 request.setDestinationUri(Uri.parse("file://" + folderName + "/myfile.mp3"));
//
//                                 downloadmanager.enqueue(request);

                                }
                            }
                        });
                        recyclerView.setAdapter(adapter);
                    }


            }
            @Override
            public void onFailure(Call<DocumentPojo> call, Throwable t) {
                System.out.println("Error message"+t);
            }
        });



    }

    private BroadcastReceiver onDownloadComplete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //Fetching the download id received with the broadcast
            long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            //Checking if the received broadcast is for our enqueued download by matching download id
            if (downloadID == id) {
                Toast.makeText(GetDouments.this, "Download Completed", Toast.LENGTH_SHORT).show();
//                Intent shareIntent = new Intent();
//                shareIntent.setAction(Intent.ACTION_SEND);
//                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION );
//                shareIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
//                shareIntent.putExtra(Intent.EXTRA_STREAM,Uri.fromFile(finalpath));
//                shareIntent.setType("*/*");
//                // Launch sharing dialog for image
//                startActivity(Intent.createChooser(shareIntent, "Share Image"));
            }
        }
    };
    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(onDownloadComplete);
    }

}