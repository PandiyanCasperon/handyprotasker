package noman.handyprotaskerapp.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import Dialog.LoadingDialog;
import Dialog.PkDialog;
import Utils.ConnectionDetector;
import Utils.CurrencySymbolConverter;
import Utils.SessionManager;
import noman.handyprotaskerapp.OtpPage;
import noman.handyprotaskerapp.Adapter.PaymentFareSummeryAdapter;
import noman.handyprotaskerapp.Pojo.BeforeImagePojo;
import noman.handyprotaskerapp.Pojo.PaymentFareSummeryPojo;
import noman.handyprotaskerapp.ProgressDialog1;
import noman.handyprotaskerapp.R;
import noman.handyprotaskerapp.ReviwesPage;
import noman.handyprotaskerapp.WidgetSupport.listhei;
import service.ServiceConstant;
import socket.SocketHandler;
import volley.ServiceRequest;

import static android.view.View.VISIBLE;

public class MoreInfoPageActivity extends AppCompatActivity {
    listhei fare_listview;
    LinearLayout Rl_layout_main;
    Double balanceamo = 0.0;
    PaymentFareSummeryAdapter adapter;
    LinearLayout signlinear;
    RecyclerView beforerecycler_view;
    List<BeforeImagePojo> beforemovieList = new ArrayList<>();
    TextView bjobphotos;
    TextView custom_text_view, jobid, whatsimportant, tellabout, jobphoto, faredetails, beforejobphoto;
    CheckBox checkedhelp;
    String jobinstruction, description, service, storewhatsincluded, saveresforward, jobcompleted, clientname, currencyCode;
    SharedPreferences pref1;
    String userid;
    private ConnectionDetector cd;
    private Context context;
    private SessionManager session;
    private Boolean isInternetPresent = false;
    private boolean show_progress_status = false;
    private Handler mHandler;
    private ProgressDialog1 myDialog;
    private String provider_id = "", user_type = "";
    private TextView Tv_JobId, jobinvoice, TextViewTitle;
    private String asyntask_name = "normal";
    private ArrayList<PaymentFareSummeryPojo> farelist, TempFareList;
    private LoadingDialog dialog;
    private ImageView setsignimage;
    //----------------Loading Method-----------
    Runnable dialogRunnable = new Runnable() {
        @Override
        public void run() {
            dialog = new LoadingDialog(MoreInfoPageActivity.this);
            dialog.setLoadingTitle(getResources().getString(R.string.loading_in));
            dialog.show();
        }
    };
    private String Job_id = "", storemybase64 = "", jobstate = "";
    private boolean isPaymetFare = false;
    private LinearLayout Rl_layout_back;
    private Button Bt_Request_payment, ButtonRequestPayment, Bt_Receivecash, Bt_completepay;
    private SocketHandler socketHandler;
    private int PreAmount = 0;
    SharedPreferences saveim;
    private String Str_BtnGroup = "";

    //method to convert currency code to currency symbol
    private static Locale getLocale(String strCode) {

        for (Locale locale : NumberFormat.getAvailableLocales()) {
            String code = NumberFormat.getCurrencyInstance(locale).getCurrency().getCurrencyCode();
            if (strCode.equals(code)) {
                return locale;
            }
        }
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.more_info);
        this.myDialog = new ProgressDialog1(getApplicationContext());
        this.myDialog.setCancelable(true);
        this.myDialog.setCanceledOnTouchOutside(true);

        setsignimage = (ImageView) findViewById(R.id.setsignimage);

        pref1 = getApplicationContext().getSharedPreferences("arrivedjobid", 0);

        saveim = getApplicationContext().getSharedPreferences("changordersign", 0);
        SharedPreferences.Editor prefeditor = saveim.edit();
        prefeditor.clear();
        prefeditor.commit();


        jobcompleted = pref1.getString("jobinstruction", "");
        clientname = pref1.getString("clinetname", "");
        userid = pref1.getString("user_id", "");


        jobinstruction = pref1.getString("jobinstruction", "");
        description = pref1.getString("description", "");
        storewhatsincluded = pref1.getString("storewhatsincluded", "");
        saveresforward = pref1.getString("saveresforward", "");

        try {
            JSONObject jobject = new JSONObject(saveresforward);
            String Str_status = jobject.getString("status");

            if (Str_status.equalsIgnoreCase("1")) {


                JSONObject object = jobject.getJSONObject("response");
                JSONObject object2 = object.getJSONObject("job");


                service = "";
                JSONArray service_type = object2.getJSONArray("job_type");
                for (int b = 0; b < service_type.length(); b++) {
                    String value = "" + service_type.getString(b);
                    service += value + " ,";
                }
                if (service.endsWith(",")) {
                    service = service.substring(0, service.length() - 1);
                }


                TextView servicetypp = (TextView) findViewById(R.id.servicetypp);
                servicetypp.setText(service);


                TextView servicetype = (TextView) findViewById(R.id.servicetype);
                servicetype.setPaintFlags(servicetype.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


            }
        } catch (Exception e) {

        }

        Bt_completepay = (Button) findViewById(R.id.Bt_completepay);
        Bt_completepay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    if (!myDialog.isShowing()) {
                        myDialog.show();
                    }
                    balanceclose(MoreInfoPageActivity.this, ServiceConstant.BALANCECLOSE);
                    System.out.println("--------------payment-------------------" + ServiceConstant.BALANCECLOSE);
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                }

            }
        });

        TextView clientnamevalue = (TextView) findViewById(R.id.clientnamevalue);
        clientnamevalue.setText(clientname);

        TextView clientnameheading = (TextView) findViewById(R.id.clientnameheading);
        clientnameheading.setPaintFlags(clientnameheading.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        TextView jonspecialin = (TextView) findViewById(R.id.jonspecialin);
        jonspecialin.setPaintFlags(jonspecialin.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        TextView Whatsincludedtext = (TextView) findViewById(R.id.Whatsincludedtext);
        Whatsincludedtext.setPaintFlags(Whatsincludedtext.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        TextView whatsincluded = (TextView) findViewById(R.id.whats);
        whatsincluded.setText(storewhatsincluded);

        if (storewhatsincluded.length() > 0) {
            whatsincluded.setVisibility(View.VISIBLE);
            Whatsincludedtext.setVisibility(View.VISIBLE);
        } else {
            whatsincluded.setVisibility(View.GONE);
            Whatsincludedtext.setVisibility(View.GONE);
        }


        whatsimportant = (TextView) findViewById(R.id.whatsimportant);
        tellabout = (TextView) findViewById(R.id.tellabout);

        whatsimportant.setText("Instruction: " + jobinstruction);
        tellabout.setText("Description: " + description);


        initialize();

        Rl_layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });


    }

    private void initialize() {
        session = new SessionManager(MoreInfoPageActivity.this);
        cd = new ConnectionDetector(MoreInfoPageActivity.this);
        mHandler = new Handler();
        socketHandler = SocketHandler.getInstance(this);

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        provider_id = user.get(SessionManager.KEY_PROVIDERID);
        farelist = new ArrayList<PaymentFareSummeryPojo>();
        TempFareList = new ArrayList<PaymentFareSummeryPojo>();

        SharedPreferences pickup;
        pickup = getApplicationContext().getSharedPreferences("arrivedjobid", 0); // 0 - for private mode
        Job_id = pickup.getString("jobid", "");
        jobstate = pref1.getString("photostatus", "");

        checkedhelp = (CheckBox) findViewById(R.id.checkedhelp);
        fare_listview = (listhei) findViewById(R.id.cancelreason_listView);
        Tv_JobId = (TextView) findViewById(R.id.paymentfare_jobId_);
        TextViewTitle = (TextView) findViewById(R.id.TextViewTitle);


        jobinvoice = (TextView) findViewById(R.id.jobinvoice);
        jobinvoice.setPaintFlags(jobinvoice.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        bjobphotos = (TextView) findViewById(R.id.bjobphotos);
        signlinear = (LinearLayout) findViewById(R.id.signlinear);
        beforerecycler_view = (RecyclerView) findViewById(R.id.beforerecycler_view);

        bjobphotos.setPaintFlags(bjobphotos.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        Bt_Request_payment = (Button) findViewById(R.id.Bt_faresummery_requestpaymet);
        ButtonRequestPayment = (Button) findViewById(R.id.ButtonRequestPayment);
        /*Bt_Receivecash = (Button) findViewById(R.id.Bt_faresummery_receivecash);*/
        Rl_layout_back = (LinearLayout) findViewById(R.id.layout_jobfare_back);

        /*Bt_Receivecash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MoreInfoPageActivity.this, OtpPage.class);
                intent.putExtra("jobId", Job_id);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });*/

        Bt_Request_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Bt_Request_payment.getText().toString().equals(getResources().getString(R.string.next))) {
//                    Intent intent = new Intent(MoreInfoPageActivity.this, OtpPage.class);
//                    intent.putExtra("jobId", Job_id);
//                    startActivity(intent);
//                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    if (Str_BtnGroup.equalsIgnoreCase("5")) {
                        buttonsClickActions(MoreInfoPageActivity.this, ServiceConstant.JOBCOMPLETE_URL, "completejob", "");
                    } else {
                        requestpayment(MoreInfoPageActivity.this, ServiceConstant.REQUEST_PAYMENT_URL);
                    }

                } else if (Bt_Request_payment.getText().toString().equals(getResources().getString(R.string.sign_and_request_payment))) {
                    isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        if (!myDialog.isShowing()) {
                            myDialog.show();
                        }
                        Intent intent = new Intent(MoreInfoPageActivity.this, CaptureSignActivity.class);
                        intent.putExtra("From", "MoreInfoPageActivity");
                        startActivityForResult(intent, ServiceConstant.GetSignatureRequestCode);
                    } else {
                        Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                    }
                }
            }
        });

        ButtonRequestPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    if (!myDialog.isShowing()) {
                        myDialog.show();
                    }
                    requestpayment(MoreInfoPageActivity.this, ServiceConstant.REQUEST_PAYMENT_URL);
                    System.out.println("--------------provider/request-payment-------------------" + ServiceConstant.PAYMENT_URL);
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                }
            }
        });


        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            paymentPost(MoreInfoPageActivity.this, ServiceConstant.PAYMENT_URL);
            System.out.println("--------------provider/job-more-info-------------------" + ServiceConstant.PAYMENT_URL);
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
        }



    }


    //--------------Alert Method-----------
    private void Alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(MoreInfoPageActivity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    private void buttonsClickActions(Context mContext, String url, String key, final String Message) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        final SharedPreferences pickup;
        pickup = getApplicationContext().getSharedPreferences("arrivedjobid", 0); // 0 - for private mode
        jsonParams.put("job_id", pickup.getString("jobid", ""));
        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        final String provider_id = pref.getString("provider_id", "");
        jsonParams.put("provider_id", provider_id);

        if (key.equalsIgnoreCase("completejob")) {
            jsonParams.put("signature", storemybase64);
        }


        if (key.equalsIgnoreCase("startjob")) {
            jsonParams.put("jobstatus", "0");
        }

        jsonParams.put("eta", "few min");


        dialog = new LoadingDialog(MoreInfoPageActivity.this);
        dialog.setLoadingTitle(getResources().getString(R.string.loading_in));
        dialog.show();

        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {
                Log.e("clickresponse------", response);
                dialog.dismiss();


                String Str_status = "", Str_message = "";

                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");

                    if (Str_status.equalsIgnoreCase("1")) {

                        requestpayment(MoreInfoPageActivity.this, ServiceConstant.REQUEST_PAYMENT_URL);
                        JSONObject object = jobject.getJSONObject("response");
                        Str_message = object.getString("message");


                        if (Str_message.contains("On My Way")) {
                            if (object.has("DisplayMessage")) {
                                Str_message = object.getString("DisplayMessage");
                            } else {
                                Str_message = getResources().getString(R.string.startoff);
                            }
                        } else if (Str_message.contains("Arrived")) {
                            Str_message = getResources().getString(R.string.arrived);
                        } else if (Str_message.contains("Job Started")) {
                            Str_message = getResources().getString(R.string.startjob);
                        } else if (Str_message.contains("Job has been completed")) {
                            Str_message = getResources().getString(R.string.completejob);
                        }

//                        final PkDialog mdialog = new PkDialog(MoreInfoPageActivity.this);
//                        mdialog.setDialogTitle(getResources().getString(R.string.action_loading_sucess));
//                        mdialog.setDialogMessage(Message + " and " + Str_message);
//                        mdialog.setPositiveButton(getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//                                        mdialog.dismiss();
//                                        SharedPreferences pickup;
//                                        pickup = getApplicationContext().getSharedPreferences("arrivedjobid", 0); // 0 - for private mode
//
//                                        if (jobstate.equalsIgnoreCase("0")) {
//                                            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
//                                            SharedPreferences.Editor prefeditor = prefjobid.edit();
//                                            prefeditor.putString("jobid", pickup.getString("jobid", ""));
//                                            prefeditor.apply();
//
//                                            Intent intent = new Intent(MoreInfoPageActivity.this, MyJobs_OnGoingDetailActivity.class);
//                                            intent.putExtra("JobId", pickup.getString("jobid", ""));
//                                            startActivity(intent);
//                                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//
//                                            finish();
//                                        } else if (jobstate.equalsIgnoreCase("1")) {
//                                            Intent intent = new Intent(MoreInfoPageActivity.this, BeforeAndAfterPhotoActivity.class);
//                                            startActivity(intent);
//                                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                                            finish();
//                                        }
//                                    }
//                                }
//                        );
//                        mdialog.show();

                    } else {
                        Str_message = jobject.getString("response");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    Runnable dialogRunnable1 = new Runnable() {
        @Override
        public void run() {
            dialog = new LoadingDialog(MoreInfoPageActivity.this);
            dialog.setLoadingTitle(getResources().getString(R.string.loading_in));
            dialog.show();
        }
    };


    //----------------------Post method for Payment Fare------------
    private void paymentPost(Context mContext, String url) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        String provider_ids = pref.getString("provider_id", "");
        jsonParams.put("provider_id", provider_ids);
        jsonParams.put("job_id", Job_id);

        mHandler.post(dialogRunnable);

        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("provider/job-more-info", response);

                String Str_status = "", Str_response = "", user_approval_status = "", Str_jobDescription = "", Str_NeedPayment = "", Str_Currency = "";


                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject object = jobject.getJSONObject("response");
                        JSONObject object2 = object.getJSONObject("job");
                        Str_jobDescription = object2.getString("job_summary");
                        Str_NeedPayment = object2.getString("need_payment");
                        Str_Currency = object2.getString("currency");

                        if (object2.has("user_approval_status")) {
                            user_approval_status = object2.getString("user_approval_status");
                        }

                        if (object2.has("user_type")) {
                            //This user_type --> Key is used to check where the user is commercial user or not.
                            //If he going to be a commercial user, then the payment flow is going to different.
                            user_type = object2.getString("user_type");
                        }

                        beforemovieList.clear();
                        farelist.clear();
                        TempFareList.clear();
                        beforemovieList = new ArrayList<>();
                        farelist = new ArrayList<>();
                        TempFareList = new ArrayList<>();


                        if (object2.has("signatures")) {

                            if (object2.isNull("signatures")) {

                            } else {
                                String signaturesjj = object2.getString("signatures");

                                JSONObject signaturesjjobj = new JSONObject(signaturesjj);
                                if (signaturesjjobj.has("ChangeOrderActivity")) {
                                    String changeorder = signaturesjjobj.getString("ChangeOrderActivity");
                                    if (changeorder.equals("") || changeorder.equals(" ")) {
                                    } else {
                                        BeforeImagePojo beforemovie = new BeforeImagePojo(/*ServiceConstant.BASE_URLIMAGE +*/ changeorder, "Change Order");
                                        beforemovieList.add(beforemovie);
                                    }
                                }

                                if (signaturesjjobj.has("startjob")) {
                                    String startjob = signaturesjjobj.getString("startjob");
                                    if (startjob.equals("") || startjob.equals(" ")) {
                                    } else {
                                        BeforeImagePojo beforemovie = new BeforeImagePojo(/*ServiceConstant.BASE_URLIMAGE +*/ startjob, "Start Job");
                                        beforemovieList.add(beforemovie);
                                    }
                                }

                                if (signaturesjjobj.has("completejob")) {
                                    String completejob = signaturesjjobj.getString("completejob");
                                    if (completejob.equals("") || completejob.equals(" ")) {
                                    } else {
                                        BeforeImagePojo beforemovie = new BeforeImagePojo(/*ServiceConstant.BASE_URLIMAGE +*/ completejob, "Complete Job");
                                        beforemovieList.add(beforemovie);
                                    }
                                }

                                if (signaturesjjobj.has("estimateApproval")) {
                                    String estimateApproval = signaturesjjobj.getString("estimateApproval");
                                    if (estimateApproval.equals("") || estimateApproval.equals(" ")) {
                                    } else {
                                        BeforeImagePojo beforemovie = new BeforeImagePojo(/*ServiceConstant.BASE_URLIMAGE +*/ estimateApproval, "Estimate Approval");
                                        beforemovieList.add(beforemovie);
                                    }
                                }

                                if (signaturesjjobj.has("requestpayment")) {
                                    String estimateApproval = signaturesjjobj.getString("requestpayment");
                                    if (estimateApproval.equals("") || estimateApproval.equals(" ")) {
                                    } else {
                                        BeforeImagePojo beforemovie = new BeforeImagePojo(/*ServiceConstant.BASE_URLIMAGE +*/ estimateApproval, "Job Complete");
                                        beforemovieList.add(beforemovie);
                                        storemybase64 = estimateApproval;
                                        TextViewTitle.setText(getResources().getString(R.string.invoice_completed));
                                        Bt_Request_payment.setText(getResources().getString(R.string.next));
                                        if (user_type.equals("1")) {
                                            ButtonRequestPayment.setVisibility(View.GONE);
                                            ButtonRequestPayment.setText(getResources().getString(R.string.request_payment));
                                        } else if (user_type.equals("0")) {
                                            ButtonRequestPayment.setVisibility(View.VISIBLE);
                                            ButtonRequestPayment.setText(getResources().getString(R.string.request_payment));
                                        }
                                    }
                                } else {
                                    TextViewTitle.setText(getResources().getString(R.string.invoice_review));
                                    Bt_Request_payment.setText(getResources().getString(R.string.sign_and_request_payment));
                                    ButtonRequestPayment.setVisibility(View.GONE);
                                }


                                if (beforemovieList.size() > 0) {
                                    signlinear.setVisibility(View.VISIBLE);
                                    LinearLayoutManager layoutManager
                                            = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                                    beforeimageadapter mAdapter = new beforeimageadapter(beforemovieList, getApplicationContext());
                                    beforerecycler_view.setLayoutManager(layoutManager);
                                    beforerecycler_view.setAdapter(mAdapter);
                                } else {
                                    signlinear.setVisibility(View.GONE);
                                    beforerecycler_view.setVisibility(View.GONE);
                                }


                            }
                        }


                        if (object2.has("user_pre_amount")) {
                            JSONArray user_pre_amount_obj = object2.getJSONArray("user_pre_amount");
                            for (int i = 0; i < user_pre_amount_obj.length(); i++) {
                                if (user_pre_amount_obj.getJSONObject(i).has("payment_status")
                                        && user_pre_amount_obj.getJSONObject(i).has("pre_amount")
                                        && user_pre_amount_obj.getJSONObject(i).getString("payment_status").equals("Accepted")) {
                                    PreAmount = PreAmount + Integer.parseInt(user_pre_amount_obj.getJSONObject(i).getString("pre_amount"));
                                }

                            }
                        }


                        // Currency currencycode = Currency.getInstance(getLocale(Str_Currency));

                        currencyCode = CurrencySymbolConverter.getCurrencySymbol(Str_Currency);

                        Str_BtnGroup = object2.getString("btn_group");


                        if (Str_BtnGroup.equalsIgnoreCase("5")) {
                            if (session.getInvoiceAlert()) {
                                Alert("Info", "A pending change order has not been accepted. If the invoice is correct then proceed, if not then go back and make corrections.");
                            }
                        }

                        JSONArray jarry = object2.getJSONArray("billing");

                        if (jarry.length() > 0) {

                            for (int i = 0; i < jarry.length(); i++) {
                                JSONObject jobjects_amount = jarry.getJSONObject(i);
                                PaymentFareSummeryPojo pojo = new PaymentFareSummeryPojo();

                                String title = jobjects_amount.getString("title");

                                String desc = "";
                                if (jobjects_amount.has("description")) {
                                    desc = jobjects_amount.getString("description");
                                } else {
                                    desc = "";
                                }


                                pojo.setPayment_desc(desc);
                                if (user_approval_status.equalsIgnoreCase("1")) {

                                    checkedhelp.setChecked(true);

                                    if (jobjects_amount.getString("title").equalsIgnoreCase("Balance amount")) {
                                        if (jobjects_amount.getString("amount").matches("[0-9.]*")) {
                                            balanceamo = Double.parseDouble(jobjects_amount.getString("amount"));

                                        }

                                    }


                                    if (jobjects_amount.getString("title").equalsIgnoreCase("Total amount")) {


                                        pojo.setPayment_title(jobjects_amount.getString("title"));

                                        if (title.contains("Hours") || title.contains("Payment mode") || title.contains("Task type") || title.contains("Start Date") || title.contains("Time") || title.contains("Month") || title.contains("End Date")) {
                                            pojo.setPayment_amount("-");
                                        } else {
                                            pojo.setPayment_amount("-");
                                        }
                                    } else {


                                        if (jobjects_amount.has("description")) {

                                            pojo.setPayment_title(jobjects_amount.getString("title"));

                                            if (title.contains("Hours") || title.contains("Payment mode") || title.contains("Task type") || title.contains("Start Date") || title.contains("Time") || title.contains("Month") || title.contains("End Date")) {
                                                pojo.setPayment_amount("-");
                                            } else {
                                                pojo.setPayment_amount("-");
                                            }

                                        } else {
                                            pojo.setPayment_title(jobjects_amount.getString("title"));

                                            if (title.contains("Hours") || title.contains("Payment mode") || title.contains("Task type") || title.contains("Start Date") || title.contains("Time") || title.contains("Month") || title.contains("End Date")) {
                                                pojo.setPayment_amount(jobjects_amount.getString("amount"));
                                            } else {
                                                pojo.setPayment_amount(currencyCode + jobjects_amount.getString("amount"));
                                            }
                                        }

                                    }

                                } else {

                                    if (jobjects_amount.getString("title").equalsIgnoreCase("Balance amount")) {
                                        if (jobjects_amount.getString("amount").matches("[0-9.]*")) {
                                            balanceamo = Double.parseDouble(jobjects_amount.getString("amount"));

                                        }

                                    }


                                    checkedhelp.setChecked(false);

                                    if (jobjects_amount.getString("title").equalsIgnoreCase("Balance amount") &&
                                            TextViewTitle.getText().toString().equals(getResources().getString(R.string.invoice_completed))) {
                                        pojo.setPayment_title(getResources().getString(R.string.balance_due));
                                    } else {
                                        pojo.setPayment_title(jobjects_amount.getString("title"));
                                    }


                                    if (title.contains("Hours") || title.contains("Payment mode") || title.contains("Task type") || title.contains("Start Date") || title.contains("Time") || title.contains("Month") || title.contains("End Date")) {
                                        pojo.setPayment_amount(jobjects_amount.getString("amount"));
                                    } else {
                                        pojo.setPayment_amount(currencyCode + jobjects_amount.getString("amount"));
                                    }
                                }


                                if (balanceamo > 0) {
                                    Bt_completepay.setVisibility(View.GONE);
                                    Bt_Request_payment.setVisibility(View.VISIBLE);
                                } else {
                                    Bt_completepay.setVisibility(View.VISIBLE);
                                    Bt_Request_payment.setVisibility(View.GONE);
                                    ButtonRequestPayment.setVisibility(View.GONE);
                                }

                                System.out.println("payment1---------------------------" + jobjects_amount.getString("title"));
                                farelist.add(pojo);
                                TempFareList.add(pojo);
                            }
                            show_progress_status = true;
                        } else {
                            show_progress_status = false;
                        }

                    } else {
                        Str_response = jobject.getString("response");
                    }

                    System.out.println("payment1---------------------------");

                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (Str_status.equalsIgnoreCase("1")) {

                    System.out.println();


                    Tv_JobId.setText(Job_id);

                    System.out.println();
                    fare_listview.setEnabled(false);

                    if (PreAmount != 0 && PreAmount > 0) {
                        PaymentFareSummeryPojo pojo = new PaymentFareSummeryPojo();
                        pojo.setPayment_title("Prepayment");
                        pojo.setPayment_amount(currencyCode + String.valueOf(PreAmount));
                        TempFareList.set((TempFareList.size() - 1), pojo);
                        pojo = new PaymentFareSummeryPojo();
                        pojo.setPayment_title(farelist.get(farelist.size() - 1).getPayment_title());
                        pojo.setPayment_amount(farelist.get(farelist.size() - 1).getPayment_amount());
                        TempFareList.add(pojo);
                        adapter = new PaymentFareSummeryAdapter(MoreInfoPageActivity.this, TempFareList);
                    } else if (PreAmount == 0) {
                        adapter = new PaymentFareSummeryAdapter(MoreInfoPageActivity.this, farelist);
                    }

                    fare_listview.setAdapter(adapter);

//                    if (Str_NeedPayment.equalsIgnoreCase("1")) {
//                        Rl_layout_farepayment_methods.setVisibility(View.VISIBLE);
//                    } else {
//                        Rl_layout_farepayment_methods.setVisibility(View.GONE);
//                    }


                } else {
                    Alert(getResources().getString(R.string.server_lable_header), Str_response);
                }

                dialog.dismiss();

            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void requestpayment(Context mContext, String url) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        String provider_ids = pref.getString("provider_id", "");
        jsonParams.put("provider_id", provider_ids);
        jsonParams.put("job_id", Job_id);
        jsonParams.put("signature", storemybase64);

        System.out.println("provider_id------------" + provider_id);
        System.out.println("job_id------------" + Job_id);
        if (checkedhelp.isChecked()) {
            jsonParams.put("user_approval", "1");
        } else {
            jsonParams.put("user_approval", "0");
        }


        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("payment", response);

                String Str_status = "", Str_response = "";


                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {

                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            paymentPost(MoreInfoPageActivity.this, ServiceConstant.PAYMENT_URL);
                            System.out.println("--------------provider/job-more-info-------------------" + ServiceConstant.PAYMENT_URL);
                        } else {
                            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                        }


                        Str_response = jobject.getString("response");

                        if (storemybase64.length() > 0 && !storemybase64.contains("https:")) {
                            Str_response = getResources().getString(R.string.job_complete_and_request_payment);
                            saveim = getApplicationContext().getSharedPreferences("changordersign", 0);
                            SharedPreferences.Editor prefeditor = saveim.edit();
                            prefeditor.putString("sign", "");
                            prefeditor.apply();
                            storemybase64 = "";
                            setsignimage.setVisibility(View.GONE);
                        } else {
                            Str_response = getResources().getString(R.string.PaymentRequestSent);
                        }
                        final PkDialog mDialog = new PkDialog(MoreInfoPageActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.success_label));
                        mDialog.setDialogMessage(Str_response);
                        mDialog.setCancelOnTouchOutside(false);
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mDialog.dismiss();

                                        SharedPreferences pickup;
                                        pickup = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
                                        if (jobstate.equalsIgnoreCase("0")) {
                                            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                                            SharedPreferences.Editor prefeditor = prefjobid.edit();
                                            prefeditor.putString("jobid", pickup.getString("jobid", ""));
                                            prefeditor.apply();

                                            Intent intent = new Intent(MoreInfoPageActivity.this, MyJobs_OnGoingDetailActivity.class);
                                            intent.putExtra("JobId", pickup.getString("jobid", ""));
                                            startActivity(intent);
                                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                                            finish();
                                        } else if (jobstate.equalsIgnoreCase("1")) {

                                            String Balance_Amount="";

                                            if(farelist.size()>0){

                                                for(int i=0;i<farelist.size();i++){

                                                    if(farelist.get(i).getPayment_title().equals("Balance amount") || farelist.get(i).getPayment_title().equals("Balance Due")){

                                                        Balance_Amount=farelist.get(i).getPayment_amount();
                                                        break;
                                                    }
                                                }

                                            } else if(TempFareList.size()>0){

                                                for(int i=0;i<TempFareList.size();i++){

                                                    if(TempFareList.get(i).getPayment_title().equals("Balance amount")){

                                                        Balance_Amount=TempFareList.get(i).getPayment_amount();

                                                        break;
                                                    }
                                                }
                                            }

                                            Intent intent = new Intent(MoreInfoPageActivity.this, BeforeAndAfterPhotoActivity.class);
                                            intent.putExtra("balance_amount",Balance_Amount);
                                            intent.putExtra("user_type",user_type);
                                            startActivity(intent);
                                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                            finish();
                                        }

                                        /*Intent intent = new Intent(getApplicationContext(), MyJobsActivity.class);
                                        intent.putExtra("status", "complete");
                                        startActivity(intent);
                                        finish();
                                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                        mDialog.dismiss();*/
                                    }
                                }
                        );


                        mDialog.show();

                        if (Str_response.contains("Request sent")) {

                        }

                    } else {
                        Str_response = jobject.getString("response");
                        Alert(getResources().getString(R.string.server_lable_header), Str_response);
                    }

                    System.out.println("payment1---------------------------");

                } catch (Exception e) {
                    e.printStackTrace();
                }


                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    private void balanceclose(Context mContext, String url) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", userid);
        jsonParams.put("booking_id", Job_id);


        System.out.println("user_id------------" + provider_id);
        System.out.println("job_id------------" + Job_id);


        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("payment", response);

                String Str_status = "", Str_response = "";


                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {

                        Str_response = jobject.getString("response");

                        final PkDialog mDialog = new PkDialog(MoreInfoPageActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.success_label));
                        mDialog.setDialogMessage(Str_response);
                        mDialog.setCancelOnTouchOutside(false);
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(MoreInfoPageActivity.this, ReviwesPage.class);
                                        intent.putExtra("jobId", Job_id);
                                        startActivity(intent);
                                        finish();
                                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                        mDialog.dismiss();
                                    }
                                }
                        );


                        mDialog.show();

                    } else {
                        Str_response = jobject.getString("response");
                        Alert(getResources().getString(R.string.server_lable_header), Str_response);
                    }

                    System.out.println("payment1---------------------------");

                } catch (Exception e) {
                    e.printStackTrace();
                }


                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        //starting XMPP service
        /*if (!socketHandler.getSocketManager().isConnected){
            socketHandler.getSocketManager().connect();
        }*/


    }

    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }


    public class beforeimageadapter extends RecyclerView.Adapter<beforeimageadapter.MyViewHolder> {

        Context ctxx;
        private List<BeforeImagePojo> moviesList;

        public beforeimageadapter(List<BeforeImagePojo> moviesList, Context ctx) {
            this.moviesList = moviesList;
            this.ctxx = ctx;
        }

        @Override
        public beforeimageadapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.imte, parent, false);

            return new beforeimageadapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(beforeimageadapter.MyViewHolder holder, int position) {
            BeforeImagePojo movie = moviesList.get(position);

            holder.textme.setText(movie.getTextname());
            if (movie.getTitle().isEmpty()) {
                holder.image.setImageResource(R.drawable.noimageavailable);
            } else {
                Picasso.with(ctxx)
                        .load(movie.getTitle())
                        .placeholder(R.drawable.noimageavailable)   // optional
                        .error(R.drawable.noimageavailable)      // optional
                        // optional
                        .into(holder.image);
                System.out.println("Image signature" + movie.getTitle());
            }

        }

        @Override
        public int getItemCount() {
            return moviesList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public ImageView image;
            TextView textme;

            public MyViewHolder(View view) {
                super(view);
                image = (ImageView) view.findViewById(R.id.image);
                textme = (TextView) view.findViewById(R.id.textme);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ServiceConstant.GetSignatureRequestCode && resultCode == RESULT_OK) {

            String bitmaa = saveim.getString("sign", "");
            if (bitmaa.equals("") || bitmaa.equals(" ")) {
                storemybase64 = "";
                setsignimage.setVisibility(View.GONE);
            } else if (bitmaa.length() > 0) {
                Bitmap src = BitmapFactory.decodeFile(bitmaa);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                src.compress(Bitmap.CompressFormat.PNG, 100, baos);
                //  byte[] data = baos.toByteArray();
                storemybase64 = Base64.encodeToString(getBytesFromBitmap(src),
                        Base64.NO_WRAP);
                Log.v("log_tag", "Base64: " + storemybase64);

                setsignimage.setVisibility(VISIBLE);

                byte[] decodedString = Base64.decode(storemybase64, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                setsignimage.setImageBitmap(decodedByte);
                if (Str_BtnGroup.equalsIgnoreCase("5")) {
                    buttonsClickActions(MoreInfoPageActivity.this, ServiceConstant.JOBCOMPLETE_URL, "completejob", "");
                } else {
                    requestpayment(MoreInfoPageActivity.this, ServiceConstant.REQUEST_PAYMENT_URL);
                }

                System.out.println("--------------provider/request-payment-------------------" + ServiceConstant.REQUEST_PAYMENT_URL);

                // setsignimage.setRotation(90);
            }
        }
    }
}
