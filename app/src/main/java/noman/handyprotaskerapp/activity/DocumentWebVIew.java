package noman.handyprotaskerapp.activity;

import Dialog.LoadingDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import noman.handyprotaskerapp.R;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class DocumentWebVIew extends AppCompatActivity {

    private WebView webView;
    String Url = "";
    private LoadingDialog dialog;
    ImageView documentimage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document_web_v_iew);

        Intent intent = getIntent();
        Url = intent.getStringExtra("url");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setTitle(getResources().getString(R.string.document_preview));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        webView = findViewById(R.id.WebView);
        documentimage = findViewById(R.id.documentimage);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);

        if(Url.endsWith(".jpg") || Url.endsWith(".png")|| Url.endsWith(".jpeg")){

            Glide.with(this).load(Url).into(documentimage);
            documentimage.setVisibility(View.VISIBLE);
            webView.setVisibility(View.GONE);
        }
        else{
            String url = "https://docs.google.com/viewer?embedded=true&url=" + Url;
            webView.loadUrl(url);
        }






        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }
        });

    }

    private void loadingDialog() {

        dialog = new LoadingDialog(DocumentWebVIew.this);
        dialog.setLoadingTitle(getResources().getString(R.string.loading_in));
        dialog.show();
    }

    private void dismissDialog() {

        dialog.dismiss();
    }
}