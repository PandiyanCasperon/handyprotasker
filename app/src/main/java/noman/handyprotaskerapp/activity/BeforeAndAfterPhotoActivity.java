package noman.handyprotaskerapp.activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import Dialog.LoadingDialog;
import Dialog.PkDialog;
import Utils.ConnectionDetector;
import Utils.CurrencySymbolConverter;
import Utils.MyCustomCompressClass;
import noman.handyprotaskerapp.Adapter.AcceptImageAdapter;
import noman.handyprotaskerapp.Adapter.PaymentFareSummeryAdapter;
import noman.handyprotaskerapp.DB.ChangeOrderSQlite;
import noman.handyprotaskerapp.OtpPage;
import noman.handyprotaskerapp.Pojo.PaymentFareSummeryPojo;
import noman.handyprotaskerapp.Pojo.acceptimage;
import noman.handyprotaskerapp.R;
import noman.handyprotaskerapp.ReceiveCashPage;
import noman.handyprotaskerapp.Service.HNDHelper;
import noman.handyprotaskerapp.WidgetSupport.MyGridView;
import noman.handyprotaskerapp.WidgetSupport.listhei;
import service.ServiceConstant;
import volley.ServiceRequest;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class BeforeAndAfterPhotoActivity extends AppCompatActivity {

    ImageView imageView;
    LinearLayout addphoto, LinearLayoutJobDetails, LinearLayoutNoImage, LinearLayoutDisplayList;
    MyGridView gridview;
    listhei listview;
    TextView done, afterphoto;
    LinearLayout beforename;
    String storemybase64 = "";

    private GridViewAdapter gridAdapter;
    private List<String> imageItems;
    private Boolean isInternetPresent = false;
    private static final int PICK_CAMERA_IMAGE = 1;
    private static final int PICK_GALLERY_IMAGE = 2;
    ConnectionDetector cd;
    private Uri fileUri; // file url to store image/video
    private static String IMAGE_DIRECTORY_NAME;
    LoadingDialog dialog;
    SharedPreferences pref1;
    private Handler mHandler;
    String jobstate = "", currencyCode;
    ChangeOrderSQlite mHelper;
    CheckBox checkedhelp;
    String service;
    String jobinstruction, description, storewhatsincluded, saveresforward, clientname;
    TextView custom_text_view, jobid, whatsimportant, tellabout, jobphoto, faredetails, beforejobphoto, clientnamevalue, clientnameheading;
    List<acceptimage> movieList = new ArrayList<>();
    private ArrayList<PaymentFareSummeryPojo> farelist, TempFareList;
    PaymentFareSummeryAdapter adapter;
    ImageView setsignimage;
    private int PreAmount = 0;
    SharedPreferences saveim;
    String[] permissionsRequired = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};
    private static int MY_PERMISSIONS_REQUEST_LOCATION = 123;
    private static int REQUEST_PERMISSION_SETTING = 151;
    BroadcastReceiver receiver;
    private String myAnswerStr = "";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addbeforephoto);
        farelist = new ArrayList<>();
        TempFareList = new ArrayList<>();
        saveim = getApplicationContext().getSharedPreferences("changordersign", 0);
        SharedPreferences.Editor prefeditor = saveim.edit();
        prefeditor.clear();
        prefeditor.apply();

        mHandler = new Handler();


        IMAGE_DIRECTORY_NAME = getResources().getString(R.string.directory_name);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());


        pref1 = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
        jobstate = pref1.getString("photostatus", "");
        mHelper = new ChangeOrderSQlite(BeforeAndAfterPhotoActivity.this);
        jobinstruction = pref1.getString("jobinstruction", "");
        description = pref1.getString("description", "");
        storewhatsincluded = pref1.getString("storewhatsincluded", "");
        saveresforward = pref1.getString("saveresforward", "");
        clientname = pref1.getString("clinetname", "");

        clientnamevalue = findViewById(R.id.clientnamevalue);
        clientnamevalue.setText(clientname);
        clientnameheading = findViewById(R.id.clientnameheading);
        clientnameheading.setPaintFlags(clientnameheading.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        setsignimage = findViewById(R.id.setsignimage);
        LinearLayoutNoImage = findViewById(R.id.LinearLayoutNoImage);
        LinearLayoutDisplayList = findViewById(R.id.LinearLayoutDisplayList);


        cd = new ConnectionDetector(BeforeAndAfterPhotoActivity.this);
        isInternetPresent = cd.isConnectingToInternet();

        File dir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + BeforeAndAfterPhotoActivity.this.getApplicationContext().getPackageName()
                + "/Files/Compressed");

        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (String aChildren : children) {
                new File(dir, aChildren).delete();
            }
        }

        //location where photo saved
        imageItems = new ArrayList<>();


        LinearLayout userphoto = findViewById(R.id.userphoto);
        RecyclerView recyclerView = findViewById(R.id.recycler_view);

        faredetails = findViewById(R.id.faredetails);

        if (jobstate.equalsIgnoreCase("1")) {
            //after
            userphoto.setVisibility(GONE);
            faredetails.setVisibility(GONE);
            try {
                JSONObject jobject = new JSONObject(saveresforward);
                String Str_status = jobject.getString("status");

                if (Str_status.equalsIgnoreCase("1")) {


                    JSONObject object = jobject.getJSONObject("response");
                    JSONObject object2 = object.getJSONObject("job");


                    service = "";
                    JSONArray service_type = object2.getJSONArray("job_type");
                    for (int b = 0; b < service_type.length(); b++) {
                        String value = "" + service_type.getString(b);
                        service += value + ",";
                    }
                    if (service.endsWith(",")) {
                        service = service.substring(0, service.length() - 1);
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            faredetails.setVisibility(View.VISIBLE);
            faredetails.setPaintFlags(faredetails.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

            try {
                JSONObject jobject = new JSONObject(saveresforward);
                String Str_status = jobject.getString("status");

                if (Str_status.equalsIgnoreCase("1")) {

                    movieList.clear();
                    JSONObject object = jobject.getJSONObject("response");
                    JSONObject object2 = object.getJSONObject("job");


                    service = "";
                    JSONArray service_type = object2.getJSONArray("job_type");
                    for (int b = 0; b < service_type.length(); b++) {
                        String value = "" + service_type.getString(b);
                        service += value + ",";
                    }
                    if (service.endsWith(",")) {
                        service = service.substring(0, service.length() - 1);
                    }

                    JSONArray aCategoryArray = object2.getJSONArray("catdata");
                    if (aCategoryArray.length() > 0) {
                        for (int j = 0; j < aCategoryArray.length(); j++) {
                            JSONObject aCategoryObject = aCategoryArray.getJSONObject(j);
                            JSONArray aAnswerArray = aCategoryObject.getJSONArray("answer");
                            if (aAnswerArray.length() > 0) {
                                for (int h = 0; h < aAnswerArray.length(); h++) {
                                    if (myAnswerStr.equalsIgnoreCase("")) {
                                        myAnswerStr = "\n\u2022 " + aAnswerArray.getString(h);
                                    } else {
                                        myAnswerStr = myAnswerStr + "\n\u2022 " + aAnswerArray.getString(h);
                                    }
                                }
                            }
                        }
                    }


                    JSONArray taskimage_type = object2.getJSONArray("user_taskimages");
                    for (int b = 0; b < taskimage_type.length(); b++) {
                        String value = "" + taskimage_type.getString(b);
                        if (value.startsWith("http://") || value.startsWith("https://")) {
                            value = value;
                        } else {
                            value = /*ServiceConstant.BASE_URLIMAGE +*/ value;
                        }
                        acceptimage movie = new acceptimage(value);
                        movieList.add(movie);
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            if (movieList.size() > 0) {
                userphoto.setVisibility(View.VISIBLE);
                LinearLayoutManager layoutManager
                        = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                AcceptImageAdapter mAdapter = new AcceptImageAdapter(movieList, getApplicationContext());
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(mAdapter);
            } else {
                userphoto.setVisibility(GONE);
                recyclerView.setVisibility(GONE);
            }
        }


        LinearLayout whatsincluded = findViewById(R.id.whatsincluded);
        TextView whats = findViewById(R.id.whats);
        whats.setText(storewhatsincluded);

        if (storewhatsincluded.length() > 0) {
            whatsincluded.setVisibility(View.VISIBLE);
        } else {
            whatsincluded.setVisibility(GONE);
        }


        beforejobphoto = findViewById(R.id.beforejobphoto);
        if (imageItems.size() == 0) {
            LinearLayoutDisplayList.setVisibility(GONE);
        } else {
            LinearLayoutDisplayList.setVisibility(VISIBLE);
        }


        beforejobphoto.setPaintFlags(beforejobphoto.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        TextView servicetypp = findViewById(R.id.servicetypp);
        servicetypp.setText(service);


        TextView servicetype = findViewById(R.id.servicetype);
        servicetype.setPaintFlags(servicetype.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        TextView Whatsincludedtext = findViewById(R.id.Whatsincludedtext);
        Whatsincludedtext.setPaintFlags(Whatsincludedtext.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        TextView jonspecialin = findViewById(R.id.jonspecialin);
        jonspecialin.setPaintFlags(jonspecialin.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        jobphoto = findViewById(R.id.jobphoto);
        jobphoto.setPaintFlags(jobphoto.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        whatsimportant = findViewById(R.id.whatsimportant);
        tellabout = findViewById(R.id.tellabout);

        /*whatsimportant.setText("Instruction: " + jobinstruction);
        tellabout.setText("Description: " + description);*/


        SpannableStringBuilder builders = new SpannableStringBuilder();
        String string1 = getResources().getString(R.string.job_des) + " : ";
        SpannableString redSpannable = new SpannableString(string1);
        redSpannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.red)), 0, string1.length(), 0);
        builders.append(redSpannable);

        String string2 = "\n\u2022 " + description + myAnswerStr;
        SpannableString whiteSpannable = new SpannableString(string2);
//                whiteSpannable.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, string2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        whiteSpannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.black)), 0, string2.length(), 0);
        builders.append(whiteSpannable);
        tellabout.setText(builders, TextView.BufferType.SPANNABLE);


        SpannableStringBuilder builders1 = new SpannableStringBuilder();
        String string3 = getResources().getString(R.string.bottomdialog_whats_important) + " : ";
        SpannableString redSpannable2 = new SpannableString(string3);
        redSpannable2.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.red)), 0, string3.length(), 0);
        builders1.append(redSpannable2);

        String string4 = jobinstruction;
        SpannableString whiteSpannable1 = new SpannableString(string4);
//                whiteSpannable.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, string2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        whiteSpannable1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.black)), 0, string4.length(), 0);
        builders1.append(whiteSpannable1);
        whatsimportant.setText(builders1, TextView.BufferType.SPANNABLE);


        jobid = findViewById(R.id.jobid);
        jobid.setText("Job Id:" + pref1.getString("jobid", ""));

        afterphoto = findViewById(R.id.afterphoto);
        LinearLayoutJobDetails = findViewById(R.id.LinearLayoutJobDetails);
        done = findViewById(R.id.done);
        beforename = findViewById(R.id.beforename);
        imageView = findViewById(R.id.imageView);
        gridview = findViewById(R.id.gridview);
        listview = findViewById(R.id.listview);
        addphoto = findViewById(R.id.addphoto);
        checkedhelp = findViewById(R.id.checkedhelp);
        custom_text_view = findViewById(R.id.custom_text_view);
        //  gridview.setEnabled(false);
        gridview.setEnabled(false);
        gridAdapter = new GridViewAdapter(this, R.layout.grid_item, imageItems);
        gridview.setAdapter(gridAdapter);

        if (imageItems.size() == 0) {
            done.setText(getResources().getString(R.string.before_photo_add_text));
        }

        if (!jobstate.equalsIgnoreCase("1")) {
            if (isInternetPresent) {
                paymentPost();
                System.out.println("--------------payment-------------------" + ServiceConstant.PAYMENT_URL);
            } else {
                Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
            }
        }


        if (jobstate.equalsIgnoreCase("1")) {
            custom_text_view.setVisibility(GONE);
            checkedhelp.setVisibility(GONE);
            done.setText(getResources().getString(R.string.next));
            afterphoto.setText(getResources().getString(R.string.activity_beforephoto_after_photo_label));
            LinearLayoutNoImage.setVisibility(VISIBLE);
            LinearLayoutJobDetails.setVisibility(GONE);

        } else {
            LinearLayoutJobDetails.setVisibility(VISIBLE);
            LinearLayoutNoImage.setVisibility(GONE);
        }

        checkedhelp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (checkedhelp.isChecked()) {
                    System.out.println("Checked");

                    SharedPreferences Job_idpref = getApplicationContext().getSharedPreferences("arrivedjobid", MODE_PRIVATE);
                    String Job_id = Job_idpref.getString("jobid", "");

                    SharedPreferences pref1 = getApplicationContext().getSharedPreferences("jjjooo", 0);
                    SharedPreferences.Editor editor1 = pref1.edit();
                    editor1.putString("jobbidd", "" + Job_id);
                    editor1.apply();
                    editor1.commit();


                    Intent intent = new Intent(getApplicationContext(), CaptureSignActivity.class);
                    intent.putExtra("startjob", "st");
                    startActivityForResult(intent, ServiceConstant.GetSignatureRequestCode);

                } else {
                    System.out.println("Un-Checked");
                }
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imageItems.size() > 0) {
                    if (isInternetPresent) {
                        if (jobstate.equalsIgnoreCase("1")) {
                            postRequestConfirmBooking();
                        } else {
                            if (checkedhelp.isChecked()) {
                                if (storemybase64.equals("") || storemybase64.equals(" ")) {
                                    HNDHelper.showErrorAlert(BeforeAndAfterPhotoActivity.this, getResources().getString(R.string.pleasecap));
                                } else {
                                    postRequestConfirmBooking();
                                }
                            } else {
                                postRequestConfirmBooking();
                            }
                        }
                    } else {
                        final PkDialog mdialog = new PkDialog(BeforeAndAfterPhotoActivity.this);
                        mdialog.setDialogTitle(getResources().getString(R.string.internet_label));
                        mdialog.setDialogMessage(getResources().getString(R.string.action_no_internet_message));
                        mdialog.setPositiveButton(getResources().getString(R.string.ok_label), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mdialog.dismiss();
                                    }
                                }
                        );
                        mdialog.show();
                    }
                } else {

                   /* final PkDialog mdialog = new PkDialog(BeforeAndAfterPhotoActivity.this);
                    mdialog.setDialogTitle(getResources().getString(R.string.sorry));
                    if (jobstate.equalsIgnoreCase("1")) {
                        mdialog.setDialogMessage(getResources().getString(R.string.activity_beforephoto_take_photo_after_submiting));
                    } else {
                        mdialog.setDialogMessage(getResources().getString(R.string.activity_beforephoto_take_photo_before_submiting));
                    }
                    mdialog.setPositiveButton(getResources().getString(R.string.ok_label), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mdialog.dismiss();
                                }
                            }
                    );
                    mdialog.show();*/

                    ActivityCompat.requestPermissions(
                            BeforeAndAfterPhotoActivity.this,
                            permissionsRequired,
                            MY_PERMISSIONS_REQUEST_LOCATION
                    );
                }


            }
        });
        beforename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences pickup;
                pickup = getApplicationContext().getSharedPreferences("arrivedjobid", 0);


                SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                SharedPreferences.Editor prefeditor = prefjobid.edit();
                prefeditor.putString("jobid", pickup.getString("jobid", ""));
                prefeditor.apply();
                prefeditor.commit();

                Intent intent = new Intent(BeforeAndAfterPhotoActivity.this, BeforeAndAfterPhotoActivity.class);
                // 0 - for private mode
                intent.putExtra("JobId", pickup.getString("jobid", ""));
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                finish();

            }
        });
        addphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ActivityCompat.requestPermissions(
                        BeforeAndAfterPhotoActivity.this,
                        permissionsRequired,
                        MY_PERMISSIONS_REQUEST_LOCATION
                );
            }
        });
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                getImages("Add");
                gridAdapter.notifyDataSetChanged();
                if (imageItems.size() == 0) {
                    done.setText(getResources().getString(R.string.before_photo_add_text));
                } else {

                    done.setText(getResources().getString(R.string.next));
                }

            }
        };
    }

    private void getImages(String Operation) {


        File fileTarget = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + BeforeAndAfterPhotoActivity.this.getApplicationContext().getPackageName()
                + "/Files/Compressed");

        File[] files = fileTarget.listFiles();

        imageItems.clear();

        if (files != null) {
            for (File file : files) {
                imageItems.add(file.getAbsolutePath());
            }
        }

        if (imageItems.size() == 0) {
            LinearLayoutDisplayList.setVisibility(GONE);
            if (Operation.equals("Add") || jobstate.equals("1"))
                LinearLayoutNoImage.setVisibility(VISIBLE);
        } else {
            LinearLayoutDisplayList.setVisibility(View.VISIBLE);
            LinearLayoutNoImage.setVisibility(GONE);
        }

        if (jobstate.equalsIgnoreCase("1")) {
            beforejobphoto.setText(R.string.bottomdialog_aftr_job_photo);
        } else {
            beforejobphoto.setText(R.string.bottomdialog_befr_job_photoss);
        }
    }


    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.handypro.files.selected");
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on scren orientation
        // changes
        outState.putParcelable("file_uri", fileUri);

    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        fileUri = savedInstanceState.getParcelable("file_uri");
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    //Remove this section if you don't want camera code (Start)
    //Got new image taken from camera intent
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == REQUEST_PERMISSION_SETTING) {
            if (checkPermissions()) {
                PicSelectionPopUp();
            }
        } else if (requestCode == ServiceConstant.GetSignatureRequestCode && resultCode == RESULT_OK) {
            String bitmaa = saveim.getString("sign", "");
            assert bitmaa != null;
            if (bitmaa.equals("") || bitmaa.equals(" ")) {
                storemybase64 = "";
                setsignimage.setVisibility(GONE);
            } else {
                Bitmap src = BitmapFactory.decodeFile(bitmaa);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                src.compress(Bitmap.CompressFormat.PNG, 100, baos);
                //  byte[] data = baos.toByteArray();
                storemybase64 = Base64.encodeToString(getBytesFromBitmap(src),
                        Base64.NO_WRAP);

                setsignimage.setVisibility(VISIBLE);

                byte[] decodedString = Base64.decode(storemybase64, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                setsignimage.setImageBitmap(decodedByte);

                // setsignimage.setRotation(90);
            }
        }

        File compressedImage;
        if (requestCode == 1 && resultCode != 0) {
            String picturePath = fileUri.getPath();

            try {
                File actualImage = new File(picturePath);
                MyCustomCompressClass myCustomCompressClass = new MyCustomCompressClass();
                myCustomCompressClass.constructor(this, actualImage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == 2 && resultCode != 0) {
            String picturePath = "";
            Uri selectedImageUri = data.getData();
            picturePath = getPath(getApplicationContext().getApplicationContext(), selectedImageUri);
            try {
                File actualImage = new File(picturePath);
                MyCustomCompressClass myCustomCompressClass = new MyCustomCompressClass();
                myCustomCompressClass.constructor(this, actualImage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            if (checkPermissions()) {
                PicSelectionPopUp();
            } else {
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setIcon(R.drawable.craftmanappmainlogo);
                alertDialog.setTitle("Alert!!!");
                alertDialog.setMessage("Storage Read, Write and Camera permission is required to read files from your device. Enable it to get basic details\n\nThank you");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Proceed", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        intent.setData(Uri.fromParts("package", getPackageName(), null));
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                    }
                });
            }
        }
    }

    private boolean checkPermissions() {
        boolean Status = false;
        Status = ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED;

        Status = ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED;

        return Status;
    }

    public static String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }


    //Remove this section if you don't want camera code (End)


    public class GridViewAdapter extends ArrayAdapter {
        private Context context;
        private List<String> data;
        private int resourceId;
        private LayoutInflater inflater;

        GridViewAdapter(Context context, int resourceId, List<String> data) {
            super(context, resourceId, data);
            this.resourceId = resourceId;
            this.context = context;
            this.data = data;
            inflater = LayoutInflater.from(context);

        }

        //I prefer to have Holder to keep all controls
        //So that I can recycle easily in getView
        class ViewHolder {
            ImageView image, close;
        }


        @NonNull
        @Override
        public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
            View row = convertView;
            GridViewAdapter.ViewHolder holder = null;
            if (row == null) {
                row = inflater.inflate(resourceId, parent, false);
                holder = new GridViewAdapter.ViewHolder();
                holder.image = row.findViewById(R.id.image);
                holder.close = row.findViewById(R.id.close);
                row.setTag(holder);
            } else {
                holder = (GridViewAdapter.ViewHolder) row.getTag(); //Easy to recycle view
            }

            Picasso.with(context).load("file://" + data.get(position))
                    .resize(160, 160)
                    .placeholder(R.drawable.nouserimg)
                    .into(holder.image, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            final PkDialog mDialog = new PkDialog(BeforeAndAfterPhotoActivity.this);
                            mDialog.setDialogTitle(getResources().getString(R.string.sorry));
                            mDialog.setDialogMessage(getResources().getString(R.string.uploaded_image_error));
                            mDialog.setPositiveButton(getResources().getString(R.string.ok_label), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mDialog.dismiss();
                                        }
                                    }
                            );
                            mDialog.show();
                        }
                    });


            holder.close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // String imagePath =   parent.getAdapter().getItem(position).toString();

                    final PkDialog mDialog = new PkDialog(BeforeAndAfterPhotoActivity.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.delete_label));
                    mDialog.setDialogMessage(getResources().getString(R.string.activity_beforephoto_delete_photo));
                    mDialog.setPositiveButton(
                            getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    File fdelete = new File(imageItems.get(position));
                                    if (fdelete.exists()) {
                                        if (fdelete.delete()) {

                                        } else {

                                        }
                                    }
                                    getImages("Remove");
                                    gridAdapter.notifyDataSetChanged();

                                    if (data.size() == 0) {

                                        if (jobstate.equals("1")) {

                                            done.setText(getResources().getString(R.string.next));
                                        } else {

                                            done.setText(getResources().getString(R.string.before_photo_add_text));
                                        }


                                    }
                                /*gridAdapter = new GridViewAdapter(BeforeAndAfterPhotoActivity.this, R.layout.grid_item, imageItems);
                                gridview.setAdapter(gridAdapter);*/
                                    mDialog.dismiss();
                                }
                            }
                    );
                    mDialog.setNegativeButton(
                            getResources().getString(R.string.ongoing_detail_cancelbtn_label), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();
                                }
                            }
                    );

                    mDialog.show();

                }
            });


            return row;
        }


    }


    private void postRequestConfirmBooking() {
        dialog = new LoadingDialog(BeforeAndAfterPhotoActivity.this);
        dialog.setLoadingTitle(getResources().getString(R.string.loading_in));
        dialog.show();

        HashMap<String, String> params = new HashMap<>();
        SharedPreferences pickup;
        pickup = getApplicationContext().getSharedPreferences("arrivedjobid", 0); // 0 - for private mode
        params.put("job_id", pickup.getString("jobid", ""));
        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        String provider_id = pref.getString("provider_id", "");
        params.put("provider_id", provider_id);

        if (checkedhelp.isChecked()) {
            params.put("user_approval", "1");
            params.put("signature", storemybase64);
        } else {
            params.put("user_approval", "0");
            params.put("signature", "");
        }


        if (jobstate.equalsIgnoreCase("1")) {
            params.put("jobstatus", "1");
        } else {
            params.put("jobstatus", "0");

        }


        JSONArray aJsonArr = null;
        try {
            if (imageItems != null && imageItems.size() > 0) {
                aJsonArr = new JSONArray(imageItems);

                for (int a = 0; a < aJsonArr.length(); a++) {
                    Bitmap src = BitmapFactory.decodeFile(aJsonArr.getString(a));
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    src.compress(Bitmap.CompressFormat.PNG, 100, baos);
                    //  byte[] data = baos.toByteArray();
                    String imgString = Base64.encodeToString(getBytesFromBitmap(src),
                            Base64.NO_WRAP);
                    params.put("taskimages[" + a + "]", imgString);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        ServiceRequest mRequest = new ServiceRequest(BeforeAndAfterPhotoActivity.this);
        mRequest.makeServiceRequest(ServiceConstant.beforephoto, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------get Confirm Category list Response----------------" + response);
                dialog.dismiss();
                String booklaterornow = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {

                        String responsenew = object.getString("response");
                        if (jobstate.equalsIgnoreCase("0")) {
                            responsenew = getResources().getString(R.string.aftersuccesstext);
                        } else {
                            if (responsenew.contains("Sent for user Approval")) {
                                responsenew = getResources().getString(R.string.beforesuccesstext);
                            } else if (responsenew.contains("sent to tasker")) {
                                responsenew = getResources().getString(R.string.aftersuccesstext);
                            }
                        }


                        //Handling the success state,
                        SharedPreferences pickup;
                        pickup = getApplicationContext().getSharedPreferences("arrivedjobid", 0); // 0 - for private mode

                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("jobid", pickup.getString("jobid", ""));
                        prefeditor.apply();
                        prefeditor.commit();
                        if (jobstate.equalsIgnoreCase("0")) {
                            buttonsClickActions(BeforeAndAfterPhotoActivity.this, ServiceConstant.STARTJOB_URL, "startjob", responsenew);
                        } else if (jobstate.equalsIgnoreCase("1")) {

                            String Balance_amount = "", User_type = "";

                            if (getIntent().hasExtra("balance_amount")) {

                                Balance_amount = getIntent().getStringExtra("balance_amount");
                            }

                            if (getIntent().hasExtra("user_type")) {

                                User_type = getIntent().getStringExtra("user_type");
                            }

                            Intent intent = new Intent(BeforeAndAfterPhotoActivity.this, ReceiveCashPage.class);
                            intent.putExtra("jobId", pickup.getString("jobid", ""));
                            intent.putExtra("Amount", Balance_amount);
                            intent.putExtra("otp", "");
                            intent.putExtra("user_type", User_type);
                            intent.putExtra("Type","postpayment");
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            finish();

                           /* Intent intent = new Intent(BeforeAndAfterPhotoActivity.this, OtpPage.class);
                            intent.putExtra("jobId", pickup.getString("jobid", ""));
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            finish();*/
                        }
//                         if (jobstate.equalsIgnoreCase("1")) {
//                            buttonsClickActions(BeforeAndAfterPhotoActivity.this, ServiceConstant.JOBCOMPLETE_URL, "completejob", responsenew);
//                        }

                    } else {

                        final PkDialog mDialog = new PkDialog(BeforeAndAfterPhotoActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.failed_label));
                        mDialog.setDialogMessage(object.getString("response"));
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        mDialog.dismiss();
                                    }
                                }
                        );
                        mDialog.show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            SharedPreferences pickup;
            pickup = getApplicationContext().getSharedPreferences("arrivedjobid", 0); // 0 - for private mode

            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("jobid", pickup.getString("jobid", ""));
            prefeditor.apply();
            prefeditor.commit();

            //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
            Intent intent = new Intent(BeforeAndAfterPhotoActivity.this, MyJobs_OnGoingDetailActivity.class);
                                        /*intent.putExtra("JobId", str_jobId);
                                        ArrayList<EstimationPojo> arrayList=new ArrayList<EstimationPojo>();
                                        intent.putExtra("mylist", arrayList);*/


            intent.putExtra("JobId", pickup.getString("jobid", ""));
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            finish();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }


    private void Alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(BeforeAndAfterPhotoActivity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    //----------------------Post method for Payment Fare------------
    private void paymentPost() {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        SharedPreferences pickup;
        pickup = getApplicationContext().getSharedPreferences("arrivedjobid", 0); // 0 - for private mode
        jsonParams.put("job_id", pickup.getString("jobid", ""));

        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        String provider_id = pref.getString("provider_id", "");
        jsonParams.put("provider_id", provider_id);

        System.out.println("provider_id------------" + provider_id);
        System.out.println("job_id------------" + pickup.getString("jobid", ""));

        mHandler.post(dialogRunnable);

        ServiceRequest mservicerequest = new ServiceRequest(BeforeAndAfterPhotoActivity.this);
        mservicerequest.makeServiceRequest(ServiceConstant.PAYMENT_URL, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("payment", response);

                String Str_status = "", Str_Currency = "";


                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject object = jobject.getJSONObject("response");
                        JSONObject object2 = object.getJSONObject("job");
                        Str_Currency = object2.getString("currency");
                        currencyCode = CurrencySymbolConverter.getCurrencySymbol(Str_Currency);

                        if (object2.has("user_pre_amount")) {
                            JSONArray user_pre_amount_obj = object2.getJSONArray("user_pre_amount");
                            for (int i = 0; i < user_pre_amount_obj.length(); i++) {
                                if (user_pre_amount_obj.getJSONObject(i).has("payment_status")
                                        && user_pre_amount_obj.getJSONObject(i).has("pre_amount")
                                        && user_pre_amount_obj.getJSONObject(i).getString("payment_status").equals("Accepted")) {
                                    PreAmount = PreAmount + Integer.parseInt(user_pre_amount_obj.getJSONObject(i).getString("pre_amount"));
                                }

                            }
                        }

                        JSONArray jarry = object2.getJSONArray("billing");

                        if (jarry.length() > 0) {

                            for (int i = 0; i < jarry.length(); i++) {
                                JSONObject jobjects_amount = jarry.getJSONObject(i);
                                PaymentFareSummeryPojo pojo = new PaymentFareSummeryPojo();

                                String title = jobjects_amount.getString("title");
                                pojo.setPayment_title(jobjects_amount.getString("title"));

                                if (title.contains("Hours") || title.contains("Payment mode") || title.contains("Task type") || title.contains("Start Date") || title.contains("Time") || title.contains("Month") || title.contains("End Date")) {
                                    pojo.setPayment_amount(jobjects_amount.getString("amount"));
                                } else {
                                    pojo.setPayment_amount(currencyCode + jobjects_amount.getString("amount"));
                                }

                                if (jobjects_amount.has("description")) {
                                    pojo.setPayment_desc(jobjects_amount.getString("description"));
                                } else {
                                    pojo.setPayment_desc("");
                                }

                                farelist.add(pojo);
                                TempFareList.add(pojo);
                            }
                        }

                    } else {
                        Toast.makeText(BeforeAndAfterPhotoActivity.this, jobject.getString("response"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (Str_status.equalsIgnoreCase("1")) {
                    listview.setEnabled(false);

                    if (PreAmount != 0 && PreAmount > 0) {
                        PaymentFareSummeryPojo pojo = new PaymentFareSummeryPojo();
                        pojo.setPayment_title("Prepayment");
                        pojo.setPayment_amount(currencyCode + PreAmount);
                        TempFareList.set((TempFareList.size() - 1), pojo);
                        pojo = new PaymentFareSummeryPojo();
                        pojo.setPayment_title(farelist.get(farelist.size() - 1).getPayment_title());
                        pojo.setPayment_amount(farelist.get(farelist.size() - 1).getPayment_amount());
                        TempFareList.add(pojo);
                        adapter = new PaymentFareSummeryAdapter(BeforeAndAfterPhotoActivity.this, TempFareList);
                    } else if (PreAmount == 0) {
                        adapter = new PaymentFareSummeryAdapter(BeforeAndAfterPhotoActivity.this, farelist);
                    }
                    listview.setAdapter(adapter);

                }

                dialog.dismiss();

            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    private void buttonsClickActions(Context mContext, String url, String key, final String Message) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        final SharedPreferences pickup;
        pickup = getApplicationContext().getSharedPreferences("arrivedjobid", 0); // 0 - for private mode
        jsonParams.put("job_id", pickup.getString("jobid", ""));
        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        final String provider_id = pref.getString("provider_id", "");
        jsonParams.put("provider_id", provider_id);

        if (key.equalsIgnoreCase("completejob")) {
            jsonParams.put("signature", storemybase64);
        }


        if (key.equalsIgnoreCase("startjob")) {
            jsonParams.put("jobstatus", "0");
        }

        jsonParams.put("eta", "few min");


        dialog = new LoadingDialog(BeforeAndAfterPhotoActivity.this);
        dialog.setLoadingTitle(getResources().getString(R.string.loading_in));
        dialog.show();

        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                Log.e("clickresponse------", response);


                dialog.dismiss();


                String Str_status = "", Str_message = "";

                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");

                    if (Str_status.equalsIgnoreCase("1")) {


                        JSONObject object = jobject.getJSONObject("response");
                        Str_message = object.getString("message");


                        if (Str_message.contains("On My Way")) {
                            if (object.has("DisplayMessage")) {
                                Str_message = object.getString("DisplayMessage");
                            } else {
                                Str_message = getResources().getString(R.string.startoff);
                            }
                        } else if (Str_message.contains("Arrived")) {
                            Str_message = getResources().getString(R.string.arrived);
                        } else if (Str_message.contains("Job Started")) {
                            Str_message = getResources().getString(R.string.startjob);
                        } else if (Str_message.contains("Job has been completed")) {
                            Str_message = getResources().getString(R.string.completejob);
                        }


                        final PkDialog mdialog = new PkDialog(BeforeAndAfterPhotoActivity.this);
                        mdialog.setDialogTitle(getResources().getString(R.string.action_loading_sucess));
                        mdialog.setDialogMessage(Message + " and " + Str_message);
                        mdialog.setPositiveButton(getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mdialog.dismiss();
                                        SharedPreferences pickup;
                                        pickup = getApplicationContext().getSharedPreferences("arrivedjobid", 0); // 0 - for private mode

                                        if (jobstate.equalsIgnoreCase("0")) {
                                            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                                            SharedPreferences.Editor prefeditor = prefjobid.edit();
                                            prefeditor.putString("jobid", pickup.getString("jobid", ""));
                                            prefeditor.apply();

                                            Intent intent = new Intent(BeforeAndAfterPhotoActivity.this, MyJobs_OnGoingDetailActivity.class);
                                            intent.putExtra("JobId", pickup.getString("jobid", ""));
                                            startActivity(intent);
                                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                                            finish();
                                        } else if (jobstate.equalsIgnoreCase("1")) {
//                                            Intent intent = new Intent(BeforeAndAfterPhotoActivity.this, MoreInfoPageActivity.class);
//                                            SharedPreferences pref1 = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
//                                            SharedPreferences.Editor editor1 = pref1.edit();
//                                            editor1.putString("jobid", pickup.getString("jobid", ""));
//                                            editor1.putString("photostatus", "1");
//                                            editor1.putString("completed", "0");
//                                            editor1.putString("jobinstruction", jobinstruction);
//                                            editor1.putString("description", description);
//                                            editor1.putString("user_id", provider_id);
//                                            editor1.putString("storewhatsincluded", storewhatsincluded);
//                                            editor1.putString("saveresforward", saveresforward);
//                                            editor1.putString("clinetname", clientname);
//                                            editor1.apply();
//                                            startActivity(intent);
//                                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                                            finish();

                                            Intent intent = new Intent(BeforeAndAfterPhotoActivity.this, OtpPage.class);
                                            intent.putExtra("jobId", pickup.getString("jobid", ""));
                                            startActivity(intent);
                                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                            finish();
                                        }
                                    }
                                }
                        );
                        mdialog.show();

                    } else {
                        Str_message = jobject.getString("response");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    Runnable dialogRunnable = new Runnable() {
        @Override
        public void run() {
            dialog = new LoadingDialog(BeforeAndAfterPhotoActivity.this);
            dialog.setLoadingTitle(getResources().getString(R.string.loading_in));
            dialog.show();
        }
    };

    void PicSelectionPopUp() {
        final PkDialog mDialog = new PkDialog(BeforeAndAfterPhotoActivity.this);
        mDialog.setDialogTitle(getResources().getString(R.string.select_label));
        mDialog.setDialogMessage(getResources().getString(R.string.activity_beforephoto_choose_to_add_photos));
        mDialog.setPositiveButton(
                getResources().getString(R.string.cameraword), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();

                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

                        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

                        // start the image capture Intent
                        startActivityForResult(intent, PICK_CAMERA_IMAGE);


                    }
                }
        );
        mDialog.setNegativeButton(
                getResources().getString(R.string.galleryword), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();

                        Intent i = new Intent(
                                Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, PICK_GALLERY_IMAGE);
                    }
                }
        );

        mDialog.show();
    }


}
