package noman.handyprotaskerapp.activity;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.HashMap;

import Dialog.LoadingDialog;
import Dialog.PkDialog;
import Utils.ConnectionDetector;
import Utils.SessionManager;
import noman.handyprotaskerapp.R;
import noman.handyprotaskerapp.TypefaceSpan;
import service.ServiceConstant;
import socket.SocketHandler;
import volley.ServiceRequest;


/**
 * Created by user88 on 1/27/2016.
 */
public class FotgotPasswordActivity extends AppCompatActivity {

    private RelativeLayout Rl_layout_forgtpwd_done;
    ImageView back_otp;
    TextView fp;
    LinearLayout layout_forgot_pwd_back;
    private EditText Et_Email_Edittext;
    private LoadingDialog dialog;
    private StringRequest postrequest;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    private SocketHandler socketHandler;

    Typeface tf;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password);
        tf = Typeface.createFromAsset(getAssets(), "Poppins-Regular.ttf");
        initilize();

        Rl_layout_forgtpwd_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cd = new ConnectionDetector(FotgotPasswordActivity.this);
                isInternetPresent = cd.isConnectingToInternet();

                if (Et_Email_Edittext.getText().toString().length()==0){
                    erroredit(Et_Email_Edittext, getResources().getString(R.string.mainpage_logineithemail_validate_lable));
                }



                else{
                    if (isInternetPresent) {
                        forgotpasswordPostRequest(FotgotPasswordActivity.this, ServiceConstant.FORGOT_PASSWORD_URL);
                        System.out.println("forgotpwd-----------"+ServiceConstant.FORGOT_PASSWORD_URL);
                    } else {

                        Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                    }
                }

            }
        });


        back_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        fp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        layout_forgot_pwd_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

    }

    private void initilize() {

        cd = new ConnectionDetector(FotgotPasswordActivity.this);
        session = new SessionManager(FotgotPasswordActivity.this);
        socketHandler = SocketHandler.getInstance(this);
        layout_forgot_pwd_back= (LinearLayout) findViewById(R.id.layout_forgot_pwd_back);
        Rl_layout_forgtpwd_done = (RelativeLayout) findViewById(R.id.settings_forgotpwd_button);
        back_otp = (ImageView) findViewById(R.id.back_otp);
        Et_Email_Edittext = (EditText)findViewById(R.id.editText_email_forgotpwd);
        fp= (TextView) findViewById(R.id.fp);
    }


    // --------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(FotgotPasswordActivity.this,
                R.anim.shake);
        editname.startAnimation(shake);



        SpannableString s = new SpannableString(getResources().getString(R.string.forgotpassword));
        s.setSpan(new TypefaceSpan(tf), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        editname.setError(s);


    }


    private void Alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(FotgotPasswordActivity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                //onBackPressed();
            }
        });
        mDialog.show();
    }


    //-----------------------Code Forgotpassword post request-----------------
    private void forgotpasswordPostRequest(Context mContext, String url) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("email",Et_Email_Edittext.getText().toString() );

        dialog = new LoadingDialog(FotgotPasswordActivity.this);
        dialog.setLoadingTitle(getResources().getString(R.string.loading_in));
        dialog.show();

        ServiceRequest mservicerequest = new ServiceRequest(mContext);

        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                Log.e("forgotpwd",response);

                String Str_status = "", Str_response = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    Str_response = object.getString("response");
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (Str_status.equalsIgnoreCase("1")) {
                    Alert(getResources().getString(R.string.label_pushnotification_cashreceived), Str_response);

                } else {
                    Alert(getResources().getString(R.string.alert_servererror), Str_response);
                }

                dialog.dismiss();

            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();

            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();
       /* if (!socketHandler.getSocketManager().isConnected){
            socketHandler.getSocketManager().connect();
        }*/
    }


    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR

            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }


}
