package noman.handyprotaskerapp.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import Dialog.LoadingDialog;
import Dialog.PkDialog;
import Utils.ConnectionDetector;
import Utils.CurrencySymbolConverter;
import Utils.MyCustomCompressClass;
import Utils.DecimalDigitsInputFilter;
import noman.handyprotaskerapp.Adapter.RecyclerViewDynamicAdapter;
import noman.handyprotaskerapp.Pojo.EstimationPojo;
import noman.handyprotaskerapp.Pojo.MultiImagePojo;
import noman.handyprotaskerapp.Pojo.MultiSubItemPojo;
import noman.handyprotaskerapp.Pojo.SubItemUpdatePojo;
import noman.handyprotaskerapp.Pojo.editbuilderpojo;
import noman.handyprotaskerapp.Pojo.joblistpojo;
import noman.handyprotaskerapp.ProgressDialog1;
import noman.handyprotaskerapp.R;
import noman.handyprotaskerapp.Service.HNDHelper;
import noman.handyprotaskerapp.WidgetSupport.MyGridView;
import noman.handyprotaskerapp.WidgetSupport.listhei;
import noman.handyprotaskerapp.viewflip;
import service.ServiceConstant;
import volley.ServiceRequest;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

public class AddEstimateItemActivity extends AppCompatActivity {

    View modalbottomsheet;
    ImageView imageView;
    LinearLayout displaylist;
    ListView listView;
    LinearLayout hidedownn;
    LinearLayout done;
    String dataisthere = "";
    BottomPageNumberAdapter mAdapter;
    Joblistadapter myOrderAdapterss;
    ArrayList<EstimationPojo> arrayList = new ArrayList<EstimationPojo>();
    ArrayList<editbuilderpojo> getarrayList1 = new ArrayList<editbuilderpojo>();
    String postionss;
    SharedPreferences pref;
    Button addbuttonone;
    ArrayList<String> fromrangestore = new ArrayList<String>();
    ArrayList<String> torangestore = new ArrayList<String>();
    ArrayList<String> rangeamount = new ArrayList<String>();
    ArrayList<String> servicestore = new ArrayList<String>();
    ArrayList<String> defaultestimate = new ArrayList<String>();
    ArrayList<String> ServiceIDArray = new ArrayList<>();
    RecyclerView recyclerView;
    int showpage = 0;
    int storeunderline = 0;
    Button donebutton;
    List<viewflip> movieList = new ArrayList<>();
    LinearLayout addbutton;
    BottomSheetDialog dialogs;
    private ConnectionDetector cd;
    private EstimationAdapter adapter;
    private Boolean isInternetPresent = false;
    private ProgressDialog1 myDialog;
    private LoadingDialog dialog;
    private ArrayList<joblistpojo> myjobList;
    private String estimateamount = "0";
    private static final int PICK_CAMERA_IMAGE = 1;
    private static final int PICK_GALLERY_IMAGE = 2;
    private Uri fileUri; // file url to store image/video
    private static String IMAGE_DIRECTORY_NAME;
    MyGridView gridview;
    private GridViewAdapter gridAdapter;
    private List<MultiImagePojo> imageItems;
    private List<MultiSubItemPojo> MainMultSubItemsList;
    private ArrayList<EstimationPojo> TempArrayList = new ArrayList<>();
    private File compressedImage;
    //In order to handle EstimateAdapter from outside of adapter, declaring holder in global.
    EstimationAdapter.Holder holder = null;
    //Need position as global, because need to add multiple photos from outside.
    int AddImagePosition = 0;
    RecyclerViewDynamicAdapter recyclerViewDynamicAdapter;
    BroadcastReceiver receiver;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.additemclasslay);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        myDialog = new ProgressDialog1(getApplicationContext());
        myDialog.setCancelable(true);
        myDialog.setCanceledOnTouchOutside(true);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        IMAGE_DIRECTORY_NAME = getResources().getString(R.string.directory_name);
        cd = new ConnectionDetector(AddEstimateItemActivity.this);
        imageView = findViewById(R.id.imageView);
        recyclerView = findViewById(R.id.recycler_view);
        displaylist = findViewById(R.id.displaylist);
        listView = findViewById(R.id.listview);
        done = findViewById(R.id.done);
        hidedownn = findViewById(R.id.hidedown);
        addbutton = findViewById(R.id.addbutton);
        donebutton = findViewById(R.id.donebutton);
        addbuttonone = findViewById(R.id.addbuttonone);


        myjobList = new ArrayList<joblistpojo>();
        imageItems = new ArrayList<>();


        addbuttonone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commoncategory();
            }
        });
        addbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                commoncategory();
            }
        });


        donebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    if (!myDialog.isShowing()) {
                        myDialog.show();
                    }


                    int nozero = 0;
                    boolean PassHelper = true;
                    for (int jk = 0; jk < arrayList.size(); jk++) {
                        if (arrayList.get(jk).getCheckedhelper() == 1) {
                            if (arrayList.get(jk).gethelperTotal() == 0 || Integer.parseInt(arrayList.get(jk).getHelperhr()) == 0) {
                                PassHelper = false;
                                break;
                            } else {
                                PassHelper = true;
                            }

                        } else {
                            PassHelper = true;
                        }
                        String amount = "" + arrayList.get(jk).getTotal();
                        if (amount.equalsIgnoreCase("0") || amount.equalsIgnoreCase("0.0") || amount.equalsIgnoreCase("0.00")) {
                            nozero = 1;
                        }
                    }

                    if (nozero == 0) {
                        if (PassHelper) {
                            loadingDialog(getResources().getString(R.string.loading_in));
                            postRequestConfirmBooking(AddEstimateItemActivity.this);
                        } else {
                            HNDHelper.showErrorAlert(AddEstimateItemActivity.this, getResources().getString(R.string.error_helper_inputs_for_estimate));
                        }

                    } else {
                        HNDHelper.showErrorAlert(AddEstimateItemActivity.this, getResources().getString(R.string.error_enter_hours_for_estimate));

                    }


                } else {
                    HNDHelper.showErrorAlert(AddEstimateItemActivity.this, getResources().getString(R.string.error_enter_hours_for_estimate));
                }
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    if (!myDialog.isShowing()) {
                        myDialog.show();
                    }


                    int nozero = 0;
                    boolean PassHelper = true;
                    for (int jk = 0; jk < arrayList.size(); jk++) {
                        if (arrayList.get(jk).getCheckedhelper() == 1) {
                            if (arrayList.get(jk).gethelperTotal() == 0 || Integer.parseInt(arrayList.get(jk).getHelperhr()) == 0) {
                                PassHelper = false;
                                break;
                            } else {
                                PassHelper = true;
                            }

                        } else {
                            PassHelper = true;
                        }
                        String amount = "" + arrayList.get(jk).getTotal();
                        if (amount.equalsIgnoreCase("0") || amount.equalsIgnoreCase("0.0") || amount.equalsIgnoreCase("0.00")) {
                            nozero = 1;
                        }
                    }

                    if (nozero == 0) {
                        if (PassHelper) {
                            loadingDialog(getResources().getString(R.string.loading_in));
                            postRequestConfirmBooking(AddEstimateItemActivity.this);
                        } else {
                            HNDHelper.showErrorAlert(AddEstimateItemActivity.this, getResources().getString(R.string.error_helper_inputs_for_estimate));
                        }

                    } else {
                        HNDHelper.showErrorAlert(AddEstimateItemActivity.this, getResources().getString(R.string.error_enter_hours_for_estimate));

                    }


                } else {
                    HNDHelper.showErrorAlert(AddEstimateItemActivity.this, getResources().getString(R.string.error_enter_hours_for_estimate));
                }

            }
        });


        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);*/

                //Before Going back get confirmation from Crafts to delete unsaved items and remove it in backend also.
                //The following function will do it.

                final PkDialog mDialog = new PkDialog(AddEstimateItemActivity.this);
                mDialog.setDialogTitle(getResources().getString(R.string.Alert));
                mDialog.setDialogMessage(getResources().getString(R.string.RemoveWarning));
                mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DeleteTheUnSaveItems();
                        mDialog.dismiss();
                    }
                });
                mDialog.setNegativeButton(getResources().getString(R.string.ongoing_detail_cancelbtn_label), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                    }
                });
                mDialog.show();
            }
        });


        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {

            paymentPost(AddEstimateItemActivity.this);
        } else {
            final PkDialog mDialog = new PkDialog(AddEstimateItemActivity.this);
            mDialog.setDialogTitle(getResources().getString(R.string.internet_label));
            mDialog.setDialogMessage(getResources().getString(R.string.action_no_internet_message));
            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                }
            });
            mDialog.show();
        }

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                /*getImages();
                adapter.notifyDataSetChanged();
                if (gridAdapter != null) {
                    gridAdapter.notifyDataSetChanged();
                }*/

                getImages();
                adapter.MultipleImage(holder, AddImagePosition);
                adapter.notifyDataSetChanged();
                if (gridAdapter != null) {
                    gridAdapter.notifyDataSetChanged();
                }
            }
        };

    }


    public void itemDeleteListner(View v) {
        EstimationPojo itemToRemove = (EstimationPojo) v.getTag();
        adapter.remove(itemToRemove);
    }


    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            /*finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);*/

            //Before Going back get confirmation from Crafts to delete unsaved items and remove it in backend also.
            //The following function will do it.

            final PkDialog mDialog = new PkDialog(AddEstimateItemActivity.this);
            mDialog.setDialogTitle(getResources().getString(R.string.Alert));
            mDialog.setDialogMessage(getResources().getString(R.string.RemoveWarning));
            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DeleteTheUnSaveItems();
                    mDialog.dismiss();
                }
            });
            mDialog.setNegativeButton(getResources().getString(R.string.ongoing_detail_cancelbtn_label), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                }
            });
            mDialog.show();
        }

        return super.onKeyDown(keyCode, event);
    }

    private void additem(Context mContext, String url, String serviceid, final String servicetypename) {

        if (servicetypename.equals("")) {
            for (int i = 0; i < arrayList.size(); i++) {
                if (arrayList.get(i).getVal1().equals(serviceid)) {
                    arrayList.remove(i);
                    ServiceIDArray.remove(serviceid);
                    TempArrayList = arrayList;
                    break;
                }
            }
        }


        loadingDialog(getResources().getString(R.string.loading_in));


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        String provider_ids = pref.getString("provider_id", "");
        jsonParams.put("provider_id", provider_ids);

        SharedPreferences Job_idpref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
        String Job_id = Job_idpref.getString("jobid", "");
        jsonParams.put("booking_id", Job_id);

        jsonParams.put("service_id", serviceid);
        jsonParams.put("service_type", servicetypename);


        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        final ArrayList<EstimationPojo> finalTempArrayList = TempArrayList;
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("payment", response);

                dismissDialog();

                String provide_estimation = "";


                try {
                    JSONObject jobject = new JSONObject(response);
                    provide_estimation = jobject.getString("status");
                    if (provide_estimation.equalsIgnoreCase("1")) {

                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            if (!servicetypename.equals(""))
                                paymentPost(AddEstimateItemActivity.this);
                            else {
                                if (arrayList.size() > 0) {
                                    if (arrayList.size() == 1) {
                                        hidedownn.setVisibility(View.GONE);

                                    } else {
                                        hidedownn.setVisibility(View.VISIBLE);

                                    }

                                    storeunderline = 0;
                                    showpage = 0;
                                    adapter.notifyDataSetChanged();
                                    mAdapter.notifyDataSetChanged();
                                    listView.setSelection(0);

                                    movieList.clear();
                                    for (int k = 0; k < arrayList.size(); k++) {
                                        viewflip movie = new viewflip("" + (k + 1), "", "");
                                        movieList.add(movie);
                                    }

                                    if (arrayList.size() <= 1) {
                                        recyclerView.setVisibility(View.GONE);
                                    } else {
                                        recyclerView.setVisibility(View.VISIBLE);
                                    }

                                    mAdapter = new BottomPageNumberAdapter(movieList);
                                    LinearLayoutManager mLayoutManager
                                            = new LinearLayoutManager(AddEstimateItemActivity.this, LinearLayoutManager.HORIZONTAL, false);

                                    recyclerView.setLayoutManager(mLayoutManager);
                                    recyclerView.setAdapter(mAdapter);
                                    recyclerView.getLayoutManager().scrollToPosition(0);
                                } else {
                                    hidedownn.setVisibility(View.GONE);
                                }
                            }
                        } else {
                            final PkDialog mDialog = new PkDialog(AddEstimateItemActivity.this);
                            mDialog.setDialogTitle(getResources().getString(R.string.internet_label));
                            mDialog.setDialogMessage(getResources().getString(R.string.action_no_internet_message));
                            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();
                                }
                            });
                            mDialog.show();
                        }

                    } else {
                        final PkDialog mDialog = new PkDialog(AddEstimateItemActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.failed_label));
                        mDialog.setDialogMessage(getResources().getString(R.string.sorry_invalid_data));
                        mDialog.setPositiveButton(getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                            }
                        });
                        mDialog.show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {
                dismissDialog();
            }
        });
    }

    private void paymentPost(Context mContext) {
        loadingDialog(getResources().getString(R.string.loading_in));
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        String provider_ids = pref.getString("provider_id", "");
        jsonParams.put("provider_id", provider_ids);

        SharedPreferences Job_idpref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
        String Job_id = Job_idpref.getString("jobid", "");
        jsonParams.put("job_id", Job_id);


        fromrangestore.clear();
        torangestore.clear();
        servicestore.clear();
        rangeamount.clear();
        defaultestimate.clear();

        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(ServiceConstant.ESTIMATE_URL, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("AddItemClass", response);
                String provide_estimation = "", Str_Currency = "";


                try {
                    JSONObject jobject = new JSONObject(response);
                    provide_estimation = jobject.getString("provide_estimation");

                    if (provide_estimation.equalsIgnoreCase("1")) {
                        String currencyCode = CurrencySymbolConverter.getCurrencySymbol(Str_Currency);

                        if (jobject.has("additionalEstimations")) {
                            JSONArray jarryadditionalEstimations = jobject.getJSONArray("additionalEstimations");
                            if (jarryadditionalEstimations.length() > 0) {
                                myjobList.clear();
                                for (int i = 0; i < jarryadditionalEstimations.length(); i++) {
                                    JSONObject jobjects_amount = jarryadditionalEstimations.getJSONObject(i);
                                    String service_type = jobjects_amount.getString("service_type");
                                    String service_id = jobjects_amount.getString("service_id");

                                    joblistpojo aOrderPojo = new joblistpojo();
                                    aOrderPojo.setOrderId(service_id);
                                    aOrderPojo.setOrderServiceType(service_type);
                                    aOrderPojo.setOrderStatus("0");
                                    myjobList.add(aOrderPojo);
                                }

                                addbutton.setVisibility(View.VISIBLE);
                            } else {
                                addbutton.setVisibility(View.GONE);
                            }
                        } else {
                            addbutton.setVisibility(View.GONE);
                        }


                        JSONArray jarry = jobject.getJSONArray("estimation_category");
                        if (jarry.length() > 0) {
                            for (int i = 0; i < jarry.length(); i++) {
                                JSONObject jobjects_amount = jarry.getJSONObject(i);

                                String title = jobjects_amount.getString("service_type");

                                estimateamount = jobjects_amount.getString("estimateamount");


                                String service_id = jobjects_amount.getString("service_id");


                                String type = jobjects_amount.getString("type");


                                JSONArray farearray = jobjects_amount.getJSONArray("fare");
                                for (int k = 0; k < farearray.length(); k++) {
                                    JSONObject fareobj = farearray.getJSONObject(k);
                                    String fromhour = fareobj.getString("fromhour");
                                    String tohour = fareobj.getString("tohour");
                                    String amount = fareobj.getString("amount");
                                    String percentage = fareobj.getString("percentage");


                                    if (amount.equalsIgnoreCase("") || amount.equalsIgnoreCase("null")) {
                                        amount = "0";
                                    }
//                                    estimateamount = amount;

                                    fromrangestore.add(fromhour);
                                    torangestore.add(tohour);
                                    servicestore.add(title);
                                    rangeamount.add(percentage);
                                    defaultestimate.add(estimateamount);
                                }

                                if (estimateamount.equalsIgnoreCase("") || estimateamount.equalsIgnoreCase("null")) {
                                    estimateamount = "0";
                                }


                                if (jobjects_amount.has("estimated_details")) {

                                    JSONObject fareobj = jobjects_amount.getJSONObject("estimated_details");

                                    if (arrayList != null && arrayList.size() > 0) {
                                        /*for (EstimationPojo TempTestEstimationPojo: arrayList) {
                                            if (!TempTestEstimationPojo.getVal1().equals(fareobj.getString("service_id"))){*/

                                        if (!ServiceIDArray.contains(fareobj.getString("service_id"))) {


                                            String hours = "0";

                                            if (fareobj.has("hours")) {
                                                hours = fareobj.getString("hours");
                                            }


                                            String materialamount;
                                            if (fareobj.has("material")) {

                                                materialamount = fareobj.getString("material");
                                            } else {
                                                materialamount = "";
                                            }


                                            String depositamount;
                                            if (fareobj.has("depositamount")) {
                                                depositamount = fareobj.getString("depositamount");
                                                if (depositamount.equals("null") || depositamount == null) {
                                                    depositamount = "0";
                                                }
                                            } else {
                                                depositamount = "0";
                                            }


                                            String description = "";

                                            if (fareobj.has("description")) {
                                                description = fareobj.getString("description");
                                            }

                                            String material_desc;
                                            if (fareobj.has("material_desc")) {
                                                material_desc = fareobj.getString("material_desc");
                                            } else {
                                                material_desc = "";
                                            }

                                            String amount = "0";
                                            if (fareobj.has("amount")) {
                                                amount = fareobj.getString("amount");
                                            }


                                            EstimationPojo atomPayment = new EstimationPojo();
                                            atomPayment.setJobtype(title);
                                            atomPayment.setDeletetype(type);
                                            atomPayment.setJobdescription(description);
                                            atomPayment.setMaterial(materialamount);
                                            atomPayment.setDepositamount(depositamount);
                                            atomPayment.setMaterialdescription(material_desc);
                                            atomPayment.setVal2(Float.parseFloat(hours));
                                            atomPayment.setVal1(service_id);
                                            atomPayment.setTotal((amount));
                                            atomPayment.setWhatsincluded(jobjects_amount.getString("whatsincluded"));

                                            if (fareobj.has("helper_amount")) {
                                                String helper_amount = fareobj.getString("helper_amount");
                                                String helper_description = fareobj.getString("helper_description");
                                                String helper_hr = "0";
                                                if (fareobj.has("helper_hour")) {
                                                    helper_hr = fareobj.getString("helper_hour");
                                                }
                                                atomPayment.setHelpertype(helper_description);
                                                atomPayment.setCheckedhelper(1);
                                                atomPayment.sethelperTotal(Double.parseDouble(helper_amount));
                                                atomPayment.setHelperhr(helper_hr);

                                            } else {
                                                atomPayment.setHelpertype("");
                                                atomPayment.setCheckedhelper(0);
                                                atomPayment.sethelperTotal(0.0);
                                                atomPayment.setHelperhr("0");
                                            }

                                            if (fareobj.has("estimation_images")) {
                                                for (int img = 0; img < fareobj.getJSONArray("estimation_images").length(); img++) {
                                                    MultiImagePojo multiImagePojo = new MultiImagePojo();
                                                    multiImagePojo.setImageName("");
                                                    multiImagePojo.setImagePath(fareobj.getJSONArray("estimation_images").getString(img));
                                                    multiImagePojo.setPositionForOperation(i);
                                                    multiImagePojo.setFileType("URL");
                                                    imageItems.add(multiImagePojo);
                                                }
                                            }

                                            if (jobjects_amount.has("sub_items") && jobjects_amount.getJSONArray("sub_items").length() > 0) {
                                                JSONArray jsonArraySubItems = jobjects_amount.getJSONArray("sub_items");
                                                MainMultSubItemsList = new ArrayList<>();
                                                for (int j = 0; j < jsonArraySubItems.length(); j++) {
                                                    JSONObject jsonObjectSubItems = jsonArraySubItems.getJSONObject(j);
                                                    MultiSubItemPojo multiSubItemPojo = new MultiSubItemPojo();
                                                    multiSubItemPojo.setName(jsonObjectSubItems.getString("name"));
                                                    multiSubItemPojo.setHours(jsonObjectSubItems.getString("hours"));
                                                    multiSubItemPojo.setAmount(jsonObjectSubItems.getString("amount"));
                                                    multiSubItemPojo.setDescription(jsonObjectSubItems.getString("description"));
                                                    multiSubItemPojo.setInnerPosition(j);
                                                    multiSubItemPojo.setID(jsonObjectSubItems.getString("_id"));
                                                    MainMultSubItemsList.add(multiSubItemPojo);
                                                }
                                            }
                                            atomPayment.setMultiImagePojos(imageItems);
                                            atomPayment.setMultiSubItemPojo(MainMultSubItemsList);
                                            atomPayment.setCanDelete(false);
                                            //After assigning value to pojo class, clear the MainMultSubItemsList. If not it'll keep on adding on all estimation.
                                            MainMultSubItemsList = new ArrayList<>();

                                            arrayList.add(atomPayment);
                                            ServiceIDArray.add(service_id);
                                               /* }

                                            }*/
                                        }
                                    } else {

                                        String hours = "0";

                                        if (fareobj.has("hours")) {
                                            hours = fareobj.getString("hours");
                                        }


                                        String materialamount;
                                        if (fareobj.has("material")) {

                                            materialamount = fareobj.getString("material");
                                        } else {
                                            materialamount = "";
                                        }


                                        String depositamount;
                                        if (fareobj.has("depositamount")) {
                                            depositamount = fareobj.getString("depositamount");
                                            if (depositamount.equals("null") || depositamount == null) {
                                                depositamount = "0";
                                            }
                                        } else {
                                            depositamount = "0";
                                        }


                                        String description = "";

                                        if (fareobj.has("description")) {
                                            description = fareobj.getString("description");
                                        }

                                        String material_desc;
                                        if (fareobj.has("material_desc")) {
                                            material_desc = fareobj.getString("material_desc");
                                        } else {
                                            material_desc = "";
                                        }

                                        String amount = "0";
                                        if (fareobj.has("amount")) {
                                            amount = fareobj.getString("amount");
                                        }


                                        EstimationPojo atomPayment = new EstimationPojo();
                                        atomPayment.setJobtype(title);
                                        atomPayment.setDeletetype(type);
                                        atomPayment.setJobdescription(description);
                                        atomPayment.setMaterial(materialamount);
                                        atomPayment.setDepositamount(depositamount);
                                        atomPayment.setMaterialdescription(material_desc);
                                        atomPayment.setVal2(Float.parseFloat(hours));
                                        atomPayment.setVal1(service_id);
                                        atomPayment.setTotal((amount));
                                        atomPayment.setWhatsincluded(jobjects_amount.getString("whatsincluded"));


                                        if (fareobj.has("estimation_images")) {
                                            for (int img = 0; img < fareobj.getJSONArray("estimation_images").length(); img++) {
                                                MultiImagePojo multiImagePojo = new MultiImagePojo();
                                                multiImagePojo.setImageName("");
                                                multiImagePojo.setImagePath(fareobj.getJSONArray("estimation_images").getString(img));
                                                multiImagePojo.setPositionForOperation(i);
                                                multiImagePojo.setFileType("URL");
                                                imageItems.add(multiImagePojo);
                                            }
                                        }

                                        if (jobjects_amount.has("sub_items") && jobjects_amount.getJSONArray("sub_items").length() > 0) {
                                            JSONArray jsonArraySubItems = jobjects_amount.getJSONArray("sub_items");
                                            MainMultSubItemsList = new ArrayList<>();
                                            for (int j = 0; j < jsonArraySubItems.length(); j++) {
                                                JSONObject jsonObjectSubItems = jsonArraySubItems.getJSONObject(j);
                                                MultiSubItemPojo multiSubItemPojo = new MultiSubItemPojo();
                                                multiSubItemPojo.setName(jsonObjectSubItems.getString("name"));
                                                multiSubItemPojo.setHours(jsonObjectSubItems.getString("hours"));
                                                multiSubItemPojo.setAmount(jsonObjectSubItems.getString("amount"));
                                                multiSubItemPojo.setDescription(jsonObjectSubItems.getString("description"));
                                                multiSubItemPojo.setInnerPosition(j);
                                                multiSubItemPojo.setID(jsonObjectSubItems.getString("_id"));
                                                MainMultSubItemsList.add(multiSubItemPojo);
                                            }
                                        }


                                        atomPayment.setMultiImagePojos(imageItems);
                                        atomPayment.setMultiSubItemPojo(MainMultSubItemsList);
                                        //After assigning value to pojo class, clear the MainMultSubItemsList. If not it'll keep on adding on all estimation.
                                        MainMultSubItemsList = new ArrayList<>();


                                        if (fareobj.has("helper_amount")) {
                                            String helper_amount = fareobj.getString("helper_amount");
                                            String helper_description = fareobj.getString("helper_description");
                                            String helper_hr = "0";
                                            if (fareobj.has("helper_hour")) {
                                                helper_hr = fareobj.getString("helper_hour");
                                            }
                                            atomPayment.setHelpertype(helper_description);
                                            atomPayment.setCheckedhelper(1);
                                            atomPayment.sethelperTotal(Double.parseDouble(helper_amount));
                                            atomPayment.setHelperhr(helper_hr);

                                        } else {
                                            atomPayment.setHelpertype("");
                                            atomPayment.setCheckedhelper(0);
                                            atomPayment.sethelperTotal(0.0);
                                            atomPayment.setHelperhr("0");
                                        }
                                        atomPayment.setCanDelete(false);
                                        arrayList.add(atomPayment);
                                        ServiceIDArray.add(service_id);
                                    }
                                } else if (!ServiceIDArray.contains(service_id)) {
                                    EstimationPojo atomPayment = new EstimationPojo();
                                    atomPayment.setJobtype(title);
                                    atomPayment.setJobdescription("");
                                    atomPayment.setDeletetype(type);
                                    atomPayment.setMaterial("");
                                    atomPayment.setMaterialdescription("");
                                    atomPayment.setVal1(service_id);
                                    atomPayment.setVal2(0);
                                    atomPayment.setTotal("0");
                                    atomPayment.setHelperhr("0");
                                    atomPayment.setCheckedhelper(0);
                                    atomPayment.sethelperTotal(0.0);
                                    atomPayment.setHelpertype("");
                                    atomPayment.setWhatsincluded(jobjects_amount.getString("whatsincluded"));
                                    atomPayment.setDepositamount("0");
                                    atomPayment.setCanDelete(true);

                                    arrayList.add(atomPayment);
                                    ServiceIDArray.add(service_id);
                                }
                            }
                        }

                        if (arrayList.size() > 0) {
                            if (arrayList.size() == 1) {
                                hidedownn.setVisibility(View.GONE);

                            } else {
                                hidedownn.setVisibility(View.VISIBLE);

                            }

                            listView.setSelection(0);

                            adapter = new EstimationAdapter(AddEstimateItemActivity.this, R.layout.row_item, arrayList);
                            listView.setAdapter(adapter);

                            movieList.clear();
                            for (int k = 0; k < arrayList.size(); k++) {
                                viewflip movie = new viewflip("" + (k + 1), "", "");
                                movieList.add(movie);
                            }

                            if (arrayList.size() <= 1) {
                                recyclerView.setVisibility(View.GONE);
                            } else {
                                recyclerView.setVisibility(View.VISIBLE);
                            }

                            mAdapter = new BottomPageNumberAdapter(movieList);
                            LinearLayoutManager mLayoutManager
                                    = new LinearLayoutManager(AddEstimateItemActivity.this, LinearLayoutManager.HORIZONTAL, false);

                            recyclerView.setLayoutManager(mLayoutManager);
                            recyclerView.setAdapter(mAdapter);
                        } else {
                            hidedownn.setVisibility(View.GONE);
                        }
                    } else {
                        final PkDialog mDialog = new PkDialog(AddEstimateItemActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.failed_label));
                        mDialog.setDialogMessage(getResources().getString(R.string.sorry_invalid_data));
                        mDialog.setPositiveButton(getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                            }
                        });
                        mDialog.show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }


                dismissDialog();

            }

            @Override
            public void onErrorListener() {
                dismissDialog();
            }
        });
    }

    private void loadingDialog(String LoadingMessage) {

        dialog = new LoadingDialog(AddEstimateItemActivity.this);
        dialog.setLoadingTitle(LoadingMessage);
        dialog.show();
    }

    private void dismissDialog() {

        dialog.dismiss();
    }

    private void commoncategory() {

        listhei newcategory;
        LinearLayout backdialog;
        Button backdialogbutton;

        modalbottomsheet = getLayoutInflater().inflate(R.layout.addestimatenew, null);
        dialogs = new BottomSheetDialog(AddEstimateItemActivity.this);
        dialogs.setContentView(modalbottomsheet);
        dialogs.setCanceledOnTouchOutside(true);
        dialogs.setCancelable(true);

        newcategory = modalbottomsheet.findViewById(R.id.newcategory);
        backdialog = modalbottomsheet.findViewById(R.id.backdialog);


        backdialogbutton = modalbottomsheet.findViewById(R.id.backdialogbutton);


        backdialogbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogs.dismiss();
            }
        });

        backdialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogs.dismiss();
            }
        });


        myOrderAdapterss = new Joblistadapter(AddEstimateItemActivity.this, myjobList);
        newcategory.setEnabled(false);
        newcategory.setAdapter(myOrderAdapterss);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) modalbottomsheet.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();


        View parent = (View) modalbottomsheet.getParent();
        parent.setFitsSystemWindows(true);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(parent);
        modalbottomsheet.measure(0, 0);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenHeight = displaymetrics.heightPixels;
        bottomSheetBehavior.setPeekHeight(screenHeight);


        params.height = screenHeight;
        parent.setLayoutParams(params);


        dialogs.show();
    }

    public class EstimationAdapter extends ArrayAdapter<EstimationPojo> {
        final String LOG_TAG = EstimationAdapter.class.getSimpleName();
        Typeface tf;
        SharedPreferences pref;
        String currency;
        private ArrayList<EstimationPojo> items;
        private int layoutResourceId;
        private Context context;

        EstimationAdapter(Context context, int layoutResourceId,
                          ArrayList<EstimationPojo> items) {
            super(context, layoutResourceId, items);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.items = items;
            tf = Typeface.createFromAsset(getContext().getAssets(), "Poppins-Regular.ttf");
            pref = context.getSharedPreferences("logindetails", 0);
            currency = pref.getString("myCurrencySymbol", "");
        }

        @NonNull
        @SuppressLint("ViewHolder")
        @Override
        public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
            View row = convertView;

            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new EstimationAdapter.Holder();
            holder.bean = items.get(position);
            holder.etVal2 = row.findViewById(R.id.edValue2);
            holder.EditTextTotal = row.findViewById(R.id.EditTextTotal);
            holder.EditTextTotal.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(8, 2)});
            holder.EditTextWhatsIncluded = row.findViewById(R.id.EditTextWhatsIncluded);
            holder.etVal2.setTypeface(tf);
            holder.EditTextTotal.setTypeface(tf);
            holder.jobtytpeheading = row.findViewById(R.id.jobtytpeheading);
            holder.jobdescriptionheading = row.findViewById(R.id.jobdescriptionheading);
            holder.amountheading = row.findViewById(R.id.amountheading);
            holder.hourheadining = row.findViewById(R.id.hourheadining);
            holder.materialheading = row.findViewById(R.id.materialheading);
            holder.materialdescheading = row.findViewById(R.id.materialdescheading);
            holder.helperdesc = row.findViewById(R.id.helperdesc);
            holder.overalllayout = row.findViewById(R.id.overalllayout);

            holder.RecyclerViewDynamic = row.findViewById(R.id.RecyclerViewDynamic);
            LinearLayoutManager llm = new LinearLayoutManager(AddEstimateItemActivity.this);
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            holder.RecyclerViewDynamic.setLayoutManager(llm);
            holder.RecyclerViewDynamic.setHasFixedSize(true);

            holder.jobtytpeheading.setTypeface(tf);
            holder.jobdescriptionheading.setTypeface(tf);
            holder.amountheading.setTypeface(tf);
            holder.hourheadining.setTypeface(tf);
            holder.materialheading.setTypeface(tf);
            holder.materialdescheading.setTypeface(tf);
            holder.helperdesc.setTypeface(tf);
            holder.headingforestimate = row.findViewById(R.id.headingforestimate);

            holder.myjobconverted_cardView_layout = row.findViewById(R.id.myjobconverted_cardView_layout);
            holder.materialdesc = row.findViewById(R.id.materialdesc);
            holder.materialamount = row.findViewById(R.id.materialamount);
            holder.jobdescription = row.findViewById(R.id.jobdescription);
            holder.jobtype = row.findViewById(R.id.jobtype);

            holder.helperhoursupdate = row.findViewById(R.id.helperhoursupdate);

            holder.helperedValue2 = row.findViewById(R.id.helperedValue2);
            holder.depositamount = row.findViewById(R.id.depositamount);

            holder.checkedhelp = row.findViewById(R.id.checkedhelp);
            holder.ImageViewAttachment = row.findViewById(R.id.ImageViewAttachment);
            holder.ImageViewAddSubItem = row.findViewById(R.id.ImageViewAddSubItem);

            holder.gridview = row.findViewById(R.id.gridview);
            holder.EstimationJobPhoto = row.findViewById(R.id.EstimationJobPhoto);

            holder.jobtype.setTypeface(tf);
            holder.materialdesc.setTypeface(tf);
            holder.jobdescription.setTypeface(tf);
            holder.materialdescheading.setTypeface(tf);
            holder.materialamount.setTypeface(tf);
            holder.depositamount.setTypeface(tf);
            holder.EditTextWhatsIncluded.setTypeface(tf);

            holder.helperedValue2.setTypeface(tf);

            holder.jobdescriptionheading.setText(getResources().getString(R.string.activity_rowitem_job_desc));


            setVal2TextListeners(holder, position);

            setjobdescriptionlistener(holder);

            sethelperhr(holder);

            sethelpertyping(holder);

            setValmaterailTextListeners(holder);

            setDeposiamount(holder);

            setValmateraildecTextListeners(holder);
            setHelperadd(holder);
            setWhatsIncluded(holder);

            row.setTag(holder);

            setupItem(holder, position);
            setTotalTextListener(holder);
            MultipleImage(holder, position);
            SetUpSubItems(holder, position, holder.etVal2, holder.EditTextTotal);

            holder.ImageViewAttachment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final PkDialog mDialog = new PkDialog(AddEstimateItemActivity.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.select_label));
                    mDialog.setDialogMessage(getResources().getString(R.string.activity_beforephoto_choose_to_add_photos));
                    mDialog.setPositiveButton(
                            getResources().getString(R.string.cameraword), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();
                                    AddImagePosition = position;
                                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
                                    intent.putExtra("AdapterPosition", position);
                                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                                    startActivityForResult(intent, PICK_CAMERA_IMAGE);
                                }
                            }
                    );
                    mDialog.setNegativeButton(
                            getResources().getString(R.string.galleryword), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();
                                    AddImagePosition = position;
                                    Intent intent = new Intent(
                                            Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    intent.putExtra("AdapterPosition", position);
                                    startActivityForResult(intent, PICK_GALLERY_IMAGE);
                                }
                            }
                    );
                    mDialog.show();
                }
            });


            return row;
        }

        private void setTotalTextListener(final EstimationAdapter.Holder holder) {
            holder.EditTextTotal.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.toString().length() > 0)
                        holder.bean.setTotal(s.toString());
                    Log.e("Total", holder.bean.getTotal());
                }
            });
        }


        private void SetUpSubItems(final Holder holder, final int position, final EditText EditTextValue, final EditText EditTextTotal) {
            if (holder.bean.getMultiSubItemPojo() != null && holder.bean.getMultiSubItemPojo().size() > 0) {
                holder.RecyclerViewDynamic.setVisibility(View.VISIBLE);
                List<MultiSubItemPojo> TempMainMultSubItemsList = new ArrayList<>();
                float TotalHours = 0, TotalAmount = 0;
                for (int i = 0; i < holder.bean.getMultiSubItemPojo().size(); i++) {
                    EditTextValue.setEnabled(false);
                    EditTextValue.setFocusable(false);
                    TotalHours = TotalHours + Float.parseFloat(holder.bean.getMultiSubItemPojo().get(i).getHours());
                    EditTextValue.setText(String.valueOf(TotalHours));
                    EditTextValue.setBackground(null);

                    EditTextTotal.setEnabled(false);
                    EditTextTotal.setFocusable(false);
                    TotalAmount = TotalAmount + Float.parseFloat(holder.bean.getMultiSubItemPojo().get(i).getAmount());
                    EditTextTotal.setText(String.format("%.2f", TotalAmount));
                    EditTextTotal.setBackground(null);


                    MultiSubItemPojo multiSubItemPojo = new MultiSubItemPojo();
                    multiSubItemPojo.setName(holder.bean.getMultiSubItemPojo().get(i).getName());
                    multiSubItemPojo.setHours(holder.bean.getMultiSubItemPojo().get(i).getHours());
                    multiSubItemPojo.setAmount(holder.bean.getMultiSubItemPojo().get(i).getAmount());
                    multiSubItemPojo.setDescription(holder.bean.getMultiSubItemPojo().get(i).getDescription());
                    multiSubItemPojo.setInnerPosition(holder.bean.getMultiSubItemPojo().get(i).getInnerPosition());
                    multiSubItemPojo.setID(holder.bean.getMultiSubItemPojo().get(i).getID());
                    TempMainMultSubItemsList.add(multiSubItemPojo);
                }
                recyclerViewDynamicAdapter = new RecyclerViewDynamicAdapter(
                        AddEstimateItemActivity.this, TempMainMultSubItemsList, position, "AddItemClassActivity", new RecyclerViewDynamicAdapter.RecyclerViewDynamicAdapterInterface() {
                    @Override
                    public void Delete(int InnerPosition) {
                        if (holder.bean.getMultiSubItemPojo() != null && holder.bean.getMultiSubItemPojo().size() > 0) {
                            float TotalHours = Float.parseFloat(EditTextValue.getText().toString());
                            for (int i = 0; i < holder.bean.getMultiSubItemPojo().size(); i++) {
                                if (holder.bean.getMultiSubItemPojo().get(i).getInnerPosition() == InnerPosition) {
                                    //Subtracting the particular hours from holder.bean.getMultiSubItemPojo() and updating it to EditText(EditTextValue)
                                    TotalHours = TotalHours - Float.parseFloat(holder.bean.getMultiSubItemPojo().get(i).getHours());
                                    EditTextValue.setText(String.valueOf(TotalHours));
                                    holder.bean.setVal2(TotalHours);
                                    UpdateCategoryMainAmountBasedOnTime(String.valueOf(TotalHours));
                                    holder.bean.getMultiSubItemPojo().remove(i);
                                }
                            }
                        } else {
                            EditTextValue.setEnabled(true);
                            EditTextValue.setFocusable(true);
                            EditTextValue.setText("0");
                            EditTextTotal.setEnabled(true);
                            EditTextTotal.setFocusable(true);
                            EditTextTotal.setText("0");
                            holder.bean.getMultiSubItemPojo().clear();
                            List<MultiSubItemPojo> multiSubItemPojos = new ArrayList<>();
                            holder.bean.setMultiSubItemPojo(multiSubItemPojos);
                        }

                        recyclerViewDynamicAdapter.notifyDataSetChanged();
                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void Edit(final int InnerPosition) {
                        for (int i = 0; i < holder.bean.getMultiSubItemPojo().size(); i++) {
                            if (holder.bean.getMultiSubItemPojo().get(i).getInnerPosition() == InnerPosition) {

                                final AlertDialog.Builder builder = new AlertDialog.Builder(AddEstimateItemActivity.this);
                                // Get the layout inflater
                                LayoutInflater inflater = AddEstimateItemActivity.this.getLayoutInflater();
                                // Inflate the layout for the dialog
                                // Pass null as the parent view because its going in the dialog layout
                                View RequestSubItemsView = inflater.inflate(R.layout.dialog_custom_add_sub_items, null);
                                final EditText EditTextName = RequestSubItemsView.findViewById(R.id.EditTextName);
                                final EditText EditTextHours = RequestSubItemsView.findViewById(R.id.EditTextHours);
                                EditTextHours.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(3, 1)});
                                final EditText EditTextAmount = RequestSubItemsView.findViewById(R.id.EditTextAmount);
                                EditTextAmount.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(3, 1)});
                                final EditText EditTextDescription = RequestSubItemsView.findViewById(R.id.EditTextDescription);
                                Button ButtonAdd = RequestSubItemsView.findViewById(R.id.ButtonAdd);
                                Button ButtonCancel = RequestSubItemsView.findViewById(R.id.ButtonCancel);

                                EditTextName.setText(holder.bean.getMultiSubItemPojo().get(i).getName());
                                EditTextHours.setText(holder.bean.getMultiSubItemPojo().get(i).getHours());
                                EditTextAmount.setText(holder.bean.getMultiSubItemPojo().get(i).getAmount());
                                EditTextDescription.setText(holder.bean.getMultiSubItemPojo().get(i).getDescription());

                                // Set the dialog layout
                                builder.setView(RequestSubItemsView);
                                builder.create();
                                final AlertDialog alertDialog = builder.show();

                                EditTextHours.addTextChangedListener(new TextWatcher() {

                                    @Override
                                    public void afterTextChanged(Editable s) {
                                    }

                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start,
                                                                  int count, int after) {
                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start,
                                                              int before, int count) {
                                        if (s.length() != 0) {
//                                EditTextHours.setText("");
                                            SubItemUpdatePojo subItemUpdatePojo = UpdateSubCategoryAmountBasedOnTime(s.toString());
//                                EditTextHours.setText(String.valueOf(subItemUpdatePojo.getHours()));
                                            EditTextAmount.setText(subItemUpdatePojo.getTotal());
                                        } else {
                                            EditTextAmount.setText("");
                                        }

                                    }
                                });

                                /*((EditText)EditTextHours).setOnFocusChangeListener(new View.OnFocusChangeListener() {

                                    @Override
                                    public void onFocusChange(View v, boolean hasFocus) {
                                        *//* When focus is lost check that the text field
                                 * has valid values.
                                 *//*
                                        if (!hasFocus) {
                                            EditText EditTextHours = v.findViewById(R.id.EditTextHours);

                                            SubItemUpdatePojo subItemUpdatePojo = UpdateSubCategoryAmountBasedOnTime(EditTextHours.getText().toString());
                                            EditTextHours.setText(String.valueOf(subItemUpdatePojo.getHours()));
                                            EditTextAmount.setText(subItemUpdatePojo.getTotal());
                                        }
                                    }
                                });*/

                                ButtonAdd.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (EditTextName.getText().toString().trim().length() > 0) {
                                            if (EditTextHours.getText().toString().trim().length() > 0 && Float.parseFloat(EditTextHours.getText().toString().trim()) >= 1) {
                                                if (EditTextHours.getText().toString().contains(".")) {
                                                    if (EditTextHours.getText().toString().endsWith(".0") || EditTextHours.getText().toString().endsWith(".5") || EditTextHours.getText().toString().endsWith(".")) {
                                                        if (EditTextAmount.getText().toString().trim().length() > 0) {
                                                            if (EditTextDescription.getText().toString().trim().length() > 0) {
                                                                alertDialog.dismiss();
                                                                if (holder.bean.getMultiSubItemPojo() != null && holder.bean.getMultiSubItemPojo().size() > 0) {

                                                                    float TotalHours = 0, TotalAmount = 0;
                                                                    for (int i = 0; i < holder.bean.getMultiSubItemPojo().size(); i++) {
                                                                        if (holder.bean.getMultiSubItemPojo().get(i).getInnerPosition() == InnerPosition) {

                                                                            MultiSubItemPojo multiSubItemPojo = new MultiSubItemPojo();
                                                                            multiSubItemPojo.setName(EditTextName.getText().toString().trim());
                                                                            multiSubItemPojo.setHours(EditTextHours.getText().toString());
                                                                            multiSubItemPojo.setAmount(EditTextAmount.getText().toString());
                                                                            multiSubItemPojo.setDescription(EditTextDescription.getText().toString());
                                                                            multiSubItemPojo.setInnerPosition(holder.bean.getMultiSubItemPojo().get(i).getInnerPosition());
                                                                            multiSubItemPojo.setID(holder.bean.getMultiSubItemPojo().get(i).getID());
                                                                            holder.bean.getMultiSubItemPojo().set(i, multiSubItemPojo);

                                                                            TotalHours = TotalHours + Float.parseFloat(holder.bean.getMultiSubItemPojo().get(i).getHours());
                                                                            TotalAmount = TotalAmount + Float.parseFloat(holder.bean.getMultiSubItemPojo().get(i).getAmount());
                                                                        } else {
                                                                            TotalHours = TotalHours + Float.parseFloat(holder.bean.getMultiSubItemPojo().get(i).getHours());
                                                                            TotalAmount = TotalAmount + Float.parseFloat(holder.bean.getMultiSubItemPojo().get(i).getAmount());
                                                                        }

                                                                        EditTextValue.setText(String.valueOf(TotalHours));
                                                                        EditTextValue.setEnabled(false);
                                                                        EditTextValue.setFocusable(false);
                                                                        holder.bean.setVal2(TotalHours);
                                                                        EditTextValue.setBackground(null);

                                                                        EditTextTotal.setText(String.format("%.2f", TotalAmount));
                                                                        EditTextTotal.setEnabled(false);
                                                                        EditTextTotal.setFocusable(false);
                                                                        holder.bean.setTotal(String.format("%.2f", TotalAmount));
                                                                        EditTextTotal.setBackground(null);
                                                                        adapter.notifyDataSetChanged();
                                                                        if (recyclerViewDynamicAdapter != null)
                                                                            recyclerViewDynamicAdapter.notifyDataSetChanged();
                                                                    }
                                                                }
                                                                holder.RecyclerViewDynamic.setAdapter(recyclerViewDynamicAdapter);
                                                                recyclerViewDynamicAdapter.notifyDataSetChanged();
                                                                adapter.notifyDataSetChanged();
                                                            } else {
                                                                Alert(getResources().getString(R.string.sorry), getResources().getString(R.string.add_description));
                                                            }
                                                        } else {
                                                            Alert(getResources().getString(R.string.sorry), getResources().getString(R.string.enter_valid_amount));
                                                        }
                                                    } else {
                                                        Alert(getResources().getString(R.string.sorry), getResources().getString(R.string.enter_valid_hours_format));
                                                    }
                                                } else {
                                                    if (EditTextAmount.getText().toString().trim().length() > 0) {
                                                        if (EditTextDescription.getText().toString().trim().length() > 0) {
                                                            alertDialog.dismiss();
                                                            if (holder.bean.getMultiSubItemPojo() != null && holder.bean.getMultiSubItemPojo().size() > 0) {

                                                                float TotalHours = 0, TotalAmount = 0;
                                                                for (int i = 0; i < holder.bean.getMultiSubItemPojo().size(); i++) {
                                                                    if (holder.bean.getMultiSubItemPojo().get(i).getInnerPosition() == InnerPosition) {

                                                                        MultiSubItemPojo multiSubItemPojo = new MultiSubItemPojo();
                                                                        multiSubItemPojo.setName(EditTextName.getText().toString().trim());
                                                                        multiSubItemPojo.setHours(EditTextHours.getText().toString());
                                                                        multiSubItemPojo.setAmount(EditTextAmount.getText().toString());
                                                                        multiSubItemPojo.setDescription(EditTextDescription.getText().toString());
                                                                        multiSubItemPojo.setInnerPosition(holder.bean.getMultiSubItemPojo().get(i).getInnerPosition());
                                                                        multiSubItemPojo.setID(holder.bean.getMultiSubItemPojo().get(i).getID());
                                                                        holder.bean.getMultiSubItemPojo().set(i, multiSubItemPojo);
                                                                    } else {
                                                                        TotalHours = TotalHours + Float.parseFloat(holder.bean.getMultiSubItemPojo().get(i).getHours());
                                                                        TotalAmount = TotalAmount + Float.parseFloat(holder.bean.getMultiSubItemPojo().get(i).getAmount());
                                                                    }

                                                                    EditTextValue.setText(String.valueOf(TotalHours));
                                                                    EditTextValue.setEnabled(false);
                                                                    EditTextValue.setFocusable(false);
                                                                    holder.bean.setVal2(TotalHours);
                                                                    EditTextValue.setBackground(null);

                                                                    EditTextTotal.setText(String.format("%.2f", TotalAmount));
                                                                    EditTextTotal.setEnabled(false);
                                                                    EditTextTotal.setFocusable(false);
                                                                    holder.bean.setTotal(String.format("%.2f", TotalAmount));
                                                                    EditTextTotal.setBackground(null);
                                                                    adapter.notifyDataSetChanged();
                                                                    if (recyclerViewDynamicAdapter != null)
                                                                        recyclerViewDynamicAdapter.notifyDataSetChanged();
                                                                }
                                                            }
                                                            holder.RecyclerViewDynamic.setAdapter(recyclerViewDynamicAdapter);
                                                            recyclerViewDynamicAdapter.notifyDataSetChanged();
                                                            adapter.notifyDataSetChanged();
                                                        } else {
                                                            Alert(getResources().getString(R.string.sorry), getResources().getString(R.string.add_description));
                                                        }
                                                    } else {
                                                        Alert(getResources().getString(R.string.sorry), getResources().getString(R.string.enter_valid_amount));
                                                    }
                                                }
                                            } else {
                                                Alert(getResources().getString(R.string.sorry), getResources().getString(R.string.enter_valid_hours));
                                            }
                                        } else {
                                            Alert(getResources().getString(R.string.sorry), getResources().getString(R.string.sub_item_title_empty));
                                        }
                                    }
                                });

                                ButtonCancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alertDialog.dismiss();
                                    }
                                });
                            }
                        }
                    }
                });
                holder.RecyclerViewDynamic.setAdapter(recyclerViewDynamicAdapter);
                recyclerViewDynamicAdapter.notifyDataSetChanged();
                adapter.notifyDataSetChanged();
            } else {
                holder.RecyclerViewDynamic.setVisibility(View.GONE);
            }
            holder.ImageViewAddSubItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditTextValue.setText("0");
                    EditTextValue.setEnabled(false);
                    EditTextValue.setFocusable(false);

                    EditTextTotal.setText("0");
                    EditTextTotal.setEnabled(false);
                    EditTextTotal.setFocusable(false);

                    if (recyclerViewDynamicAdapter != null)
                        recyclerViewDynamicAdapter.notifyDataSetChanged();
                    adapter.notifyDataSetChanged();
                    final AlertDialog.Builder builder = new AlertDialog.Builder(AddEstimateItemActivity.this);
                    // Get the layout inflater
                    LayoutInflater inflater = AddEstimateItemActivity.this.getLayoutInflater();
                    // Inflate the layout for the dialog
                    // Pass null as the parent view because its going in the dialog layout
                    View RequestSubItemsView = inflater.inflate(R.layout.dialog_custom_add_sub_items, null);
                    final EditText EditTextName = RequestSubItemsView.findViewById(R.id.EditTextName);
                    final EditText EditTextHours = RequestSubItemsView.findViewById(R.id.EditTextHours);
                    EditTextHours.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(3, 1)});
                    final EditText EditTextAmount = RequestSubItemsView.findViewById(R.id.EditTextAmount);
                    EditTextAmount.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(3, 1)});
                    final EditText EditTextDescription = RequestSubItemsView.findViewById(R.id.EditTextDescription);
                    Button ButtonAdd = RequestSubItemsView.findViewById(R.id.ButtonAdd);
                    Button ButtonCancel = RequestSubItemsView.findViewById(R.id.ButtonCancel);

                    // Set the dialog layout
                    builder.setView(RequestSubItemsView);
                    builder.create();
                    final AlertDialog alertDialog = builder.show();

                    EditTextHours.addTextChangedListener(new TextWatcher() {

                        @Override
                        public void afterTextChanged(Editable s) {
                        }

                        @Override
                        public void beforeTextChanged(CharSequence s, int start,
                                                      int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start,
                                                  int before, int count) {
                            if (s.length() != 0) {
//                                EditTextHours.setText("");
                                SubItemUpdatePojo subItemUpdatePojo = UpdateSubCategoryAmountBasedOnTime(s.toString());
//                                EditTextHours.setText(String.valueOf(subItemUpdatePojo.getHours()));
                                EditTextAmount.setText(subItemUpdatePojo.getTotal());
                            } else {
                                EditTextAmount.setText("");
                            }

                        }
                    });


                    /*((EditText)EditTextHours).setOnFocusChangeListener(new View.OnFocusChangeListener() {

                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            *//* When focus is lost check that the text field
                     * has valid values.
                     *//*
                            if (!hasFocus) {
                                EditText EditTextHours = v.findViewById(R.id.EditTextHours);

                                SubItemUpdatePojo subItemUpdatePojo = UpdateSubCategoryAmountBasedOnTime(EditTextHours.getText().toString());
                                EditTextHours.setText(String.valueOf(subItemUpdatePojo.getHours()));
                                EditTextAmount.setText(subItemUpdatePojo.getTotal());
                            }
                        }
                    });*/


                    ButtonAdd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (EditTextName.getText().toString().trim().length() > 0) {
                                if (EditTextHours.getText().toString().trim().length() > 0 && Float.parseFloat(EditTextHours.getText().toString().trim()) >= 1) {
                                    if (EditTextHours.getText().toString().contains(".")) {
                                        if (EditTextHours.getText().toString().endsWith(".0") || EditTextHours.getText().toString().endsWith(".5") || EditTextHours.getText().toString().endsWith(".")) {
                                            if (EditTextAmount.getText().toString().trim().length() > 0) {
                                                if (EditTextDescription.getText().toString().trim().length() > 0) {
                                                    alertDialog.dismiss();

                                                    MultiSubItemPojo multiSubItemPojo = new MultiSubItemPojo();
                                                    multiSubItemPojo.setName(EditTextName.getText().toString().trim().trim());
                                                    multiSubItemPojo.setHours(EditTextHours.getText().toString().trim());
                                                    multiSubItemPojo.setAmount(EditTextAmount.getText().toString().trim());
                                                    multiSubItemPojo.setDescription(EditTextDescription.getText().toString().trim());
                                                    List<MultiSubItemPojo> multiSubItemPojos;
                                                    if (holder.bean.getMultiSubItemPojo() == null) {
                                                        multiSubItemPojo.setInnerPosition(0);
                                                        multiSubItemPojos = new ArrayList<>();
                                                        multiSubItemPojos.add(multiSubItemPojo);
                                                    } else {
                                                        multiSubItemPojo.setInnerPosition(holder.bean.getMultiSubItemPojo().size());
                                                        multiSubItemPojos = holder.bean.getMultiSubItemPojo();
                                                        multiSubItemPojos.add(multiSubItemPojo);
                                                    }
                                                    holder.bean.setMultiSubItemPojo(multiSubItemPojos);

                                                    if (holder.bean.getMultiSubItemPojo() != null && holder.bean.getMultiSubItemPojo().size() > 0) {
                                                        float TotalHours = 0, TotalAmount = 0;
                                                        for (int i = 0; i < holder.bean.getMultiSubItemPojo().size(); i++) {
                                                            TotalHours = TotalHours + Float.parseFloat(holder.bean.getMultiSubItemPojo().get(i).getHours());
                                                            TotalAmount = TotalAmount + Float.parseFloat(holder.bean.getMultiSubItemPojo().get(i).getAmount());
                                                        }

                                                        EditTextValue.setText(String.valueOf(TotalHours));
                                                        EditTextValue.setEnabled(false);
                                                        EditTextValue.setFocusable(false);
                                                        holder.bean.setVal2(TotalHours);
                                                        EditTextValue.setBackground(null);

                                                        EditTextTotal.setText(String.format("%.2f", TotalAmount));
                                                        EditTextTotal.setEnabled(false);
                                                        EditTextTotal.setFocusable(false);
                                                        holder.bean.setTotal(String.format("%.2f", TotalAmount));
                                                        EditTextTotal.setBackground(null);
                                                        adapter.notifyDataSetChanged();
                                                        if (recyclerViewDynamicAdapter != null)
                                                            recyclerViewDynamicAdapter.notifyDataSetChanged();
                                                    }
                                                    holder.RecyclerViewDynamic.setAdapter(recyclerViewDynamicAdapter);
                                                    recyclerViewDynamicAdapter.notifyDataSetChanged();
                                                    adapter.notifyDataSetChanged();
                                                } else {
                                                    Alert(getResources().getString(R.string.sorry), getResources().getString(R.string.add_description));
                                                }
                                            } else {
                                                Alert(getResources().getString(R.string.sorry), getResources().getString(R.string.enter_valid_amount));
                                            }

                                        } else {
                                            Alert(getResources().getString(R.string.sorry), getResources().getString(R.string.enter_valid_hours_format));
                                        }
                                    } else {
                                        if (EditTextAmount.getText().toString().trim().length() > 0) {
                                            if (EditTextDescription.getText().toString().trim().length() > 0) {
                                                alertDialog.dismiss();

                                                MultiSubItemPojo multiSubItemPojo = new MultiSubItemPojo();
                                                multiSubItemPojo.setName(EditTextName.getText().toString().trim());
                                                multiSubItemPojo.setHours(EditTextHours.getText().toString().trim());
                                                multiSubItemPojo.setAmount(EditTextAmount.getText().toString().trim());
                                                multiSubItemPojo.setDescription(EditTextDescription.getText().toString().trim());
                                                List<MultiSubItemPojo> multiSubItemPojos;
                                                if (holder.bean.getMultiSubItemPojo() == null) {
                                                    multiSubItemPojo.setInnerPosition(0);
                                                    multiSubItemPojos = new ArrayList<>();
                                                    multiSubItemPojos.add(multiSubItemPojo);
                                                } else {
                                                    multiSubItemPojo.setInnerPosition(holder.bean.getMultiSubItemPojo().size());
                                                    multiSubItemPojos = holder.bean.getMultiSubItemPojo();
                                                    multiSubItemPojos.add(multiSubItemPojo);
                                                }
                                                holder.bean.setMultiSubItemPojo(multiSubItemPojos);

                                                if (holder.bean.getMultiSubItemPojo() != null && holder.bean.getMultiSubItemPojo().size() > 0) {
                                                    float TotalHours = 0, TotalAmount = 0;
                                                    for (int i = 0; i < holder.bean.getMultiSubItemPojo().size(); i++) {
                                                        TotalHours = TotalHours + Float.parseFloat(holder.bean.getMultiSubItemPojo().get(i).getHours());
                                                        TotalAmount = TotalAmount + Float.parseFloat(holder.bean.getMultiSubItemPojo().get(i).getAmount());
                                                    }

                                                    EditTextValue.setText(String.valueOf(TotalHours));
                                                    EditTextValue.setEnabled(false);
                                                    EditTextValue.setFocusable(false);
                                                    holder.bean.setVal2(TotalHours);
                                                    EditTextValue.setBackground(null);

                                                    EditTextTotal.setText(String.format("%.2f", TotalAmount));
                                                    EditTextTotal.setEnabled(false);
                                                    EditTextTotal.setFocusable(false);
                                                    holder.bean.setTotal(String.format("%.2f", TotalAmount));
                                                    EditTextTotal.setBackground(null);
                                                    adapter.notifyDataSetChanged();
                                                    if (recyclerViewDynamicAdapter != null)
                                                        recyclerViewDynamicAdapter.notifyDataSetChanged();
                                                }
                                                holder.RecyclerViewDynamic.setAdapter(recyclerViewDynamicAdapter);
                                                if (recyclerViewDynamicAdapter != null)
                                                    recyclerViewDynamicAdapter.notifyDataSetChanged();
                                                adapter.notifyDataSetChanged();
                                            } else {
                                                Alert(getResources().getString(R.string.sorry), getResources().getString(R.string.add_description));
                                            }
                                        } else {
                                            Alert(getResources().getString(R.string.sorry), getResources().getString(R.string.enter_valid_amount));
                                        }
                                    }
                                } else {
                                    Alert(getResources().getString(R.string.sorry), getResources().getString(R.string.enter_valid_hours));
                                }
                            } else {
                                Alert(getResources().getString(R.string.sorry), getResources().getString(R.string.sub_item_title_empty));
                            }
                        }
                    });

                    ButtonCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                        }
                    });
                }
            });
        }

        private void MultipleImage(final EstimationAdapter.Holder holder, int position) {
            if (imageItems != null && imageItems.size() > 0) {
                List<MultiImagePojo> TempImageItems = new ArrayList<>();
                for (int i = 0; i < imageItems.size(); i++) {
                    if (position == imageItems.get(i).getPositionForOperation()) {
                        MultiImagePojo multiImagePojo = new MultiImagePojo();
                        multiImagePojo.setImageName(imageItems.get(i).getImageName());
                        multiImagePojo.setImagePath(imageItems.get(i).getImagePath());
                        multiImagePojo.setPositionForOperation(imageItems.get(i).getPositionForOperation());
                        multiImagePojo.setFileType(imageItems.get(i).getFileType());
                        TempImageItems.add(multiImagePojo);
                    }
                }
                if (TempImageItems.size() > 0) {
                    holder.gridview.setEnabled(false);
                    gridAdapter = new GridViewAdapter(AddEstimateItemActivity.this, R.layout.grid_item, TempImageItems, position);
                    holder.gridview.setAdapter(gridAdapter);
                    gridAdapter.notifyDataSetChanged();
                    holder.EstimationJobPhoto.setVisibility(View.VISIBLE);
                } else {
                    holder.EstimationJobPhoto.setVisibility(View.GONE);
                }
            }
        }

        private void setupItem(final EstimationAdapter.Holder holder, int position) {


            if (showpage == position) {

                holder.overalllayout.setVisibility(View.VISIBLE);
            } else {
                holder.overalllayout.setVisibility(View.GONE);
            }


            if (holder.bean.getCheckedhelper() == 0) {
                holder.checkedhelp.setChecked(false);
            } else {
                holder.checkedhelp.setChecked(true);
            }

            holder.checkedhelp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (holder.bean.getCheckedhelper() == 0) {
                        holder.bean.setCheckedhelper(1);
                        holder.checkedhelp.setChecked(true);
                    } else {
                        holder.bean.setCheckedhelper(0);
                        holder.checkedhelp.setChecked(false);
                    }
                }
            });

            holder.helperdesc.setText("" + holder.bean.getHelpertype());


            if (holder.bean.gethelperTotal() == 0.0) {
                holder.helperedValue2.setText("0");
            } else {
                holder.helperedValue2.setText("" + holder.bean.gethelperTotal());
            }

            holder.EditTextWhatsIncluded.setText(holder.bean.getWhatsincluded());


            /*if (holder.bean.getDeletetype().equals("additional")) {
                holder.headingforestimate.setVisibility(View.VISIBLE);
            } else {
                holder.headingforestimate.setVisibility(View.GONE);
            }*/

            holder.jobtype.setText(holder.bean.getJobtype());
            holder.jobdescription.setText(holder.bean.getJobdescription());

            holder.materialamount.setText(holder.bean.getMaterial());
            holder.materialdesc.setText(holder.bean.getMaterialdescription());


            holder.etVal2.setText(String.valueOf(holder.bean.getVal2()));
            holder.EditTextTotal.setText(/*currency +*/ String.valueOf(holder.bean.getTotal()));
            holder.helperhoursupdate.setText(holder.bean.getHelperhr());


            holder.depositamount.setText(holder.bean.getDepositamount());

            if (items.size() > 1 && position > 0) {
                holder.headingforestimate.setVisibility(View.VISIBLE);
            } else {
                holder.headingforestimate.setVisibility(View.GONE);
            }


            holder.headingforestimate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        additem(AddEstimateItemActivity.this, ServiceConstant.deletestimate, holder.bean.getVal1(), "");

                    } else {
                        final PkDialog mDialog = new PkDialog(AddEstimateItemActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.internet_label));
                        mDialog.setDialogMessage(getResources().getString(R.string.action_no_internet_message));
                        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                            }
                        });
                        mDialog.show();
                    }

                }
            });


        }

        private void setValmaterailTextListeners(final EstimationAdapter.Holder holder) {
            holder.materialamount.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.toString().length() > 0)
                        holder.bean.setMaterial(s.toString());
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }

        private void setDeposiamount(final EstimationAdapter.Holder holder) {
            holder.depositamount.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    String depositvalue = "0";
                    if (s.toString().length() > 0) {
                        depositvalue = s.toString();
                    } else if (s.toString().length() == 0) {
                        depositvalue = "0";
                    }

                    holder.bean.setDepositamount(depositvalue);
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }

        private void setValmateraildecTextListeners(final EstimationAdapter.Holder holder) {
            holder.materialdesc.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.toString().length() > 0)
                        holder.bean.setMaterialdescription(s.toString());
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }

        private void setHelperadd(final EstimationAdapter.Holder holder) {
            holder.helperedValue2.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    double zeroskip = 0.0;
                    if (s.toString().length() == 0) {
                    } else {
                        if (s.toString().equalsIgnoreCase("0.0")) {
                            zeroskip = 0.0;
                        } else {
                            zeroskip = Double.parseDouble(s.toString());
                        }
                    }

                    holder.bean.sethelperTotal(zeroskip);


                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }

        void setWhatsIncluded(final EstimationAdapter.Holder holder) {
            holder.EditTextWhatsIncluded.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    holder.bean.setWhatsincluded(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

        private void setVal2TextListeners(final EstimationAdapter.Holder holder, int postion) {

            holder.etVal2.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    /* When focus is lost check that the text field
                     * has valid values.
                     */
                    if (!hasFocus) {
                        EditText EditTextTotal = v.findViewById(R.id.edValue2);

                        UpdateCategoryMainAmountBasedOnTime(EditTextTotal.getText().toString());
                    }
                }
            });
        }

        @SuppressLint({"DefaultLocale", "SetTextI18n"})
        void UpdateCategoryMainAmountBasedOnTime(String TimeToCal) {
            try {


                String valuie = "1";

                if (TimeToCal.equalsIgnoreCase("")) {
                    valuie = "1";
                } else {
                    valuie = TimeToCal;
                }


                if (valuie.endsWith(".0") || valuie.endsWith(".5") || valuie.endsWith(".") || valuie.length() <= 2) {
                    if (valuie.endsWith(".")) {
                        valuie = valuie.substring(0, (valuie.length() - 1));
                    }
                    holder.bean.setVal2(Float.parseFloat(valuie));
                    String servicetype = holder.bean.getJobtype();
                    if (servicestore.contains(servicetype)) {

                        String gotvalue = "";
                        for (int jk = 0; jk < servicestore.size(); jk++) {
                            String arrayservice = servicestore.get(jk);
                            if (arrayservice.equalsIgnoreCase(servicetype)) {
                                float typingnumber = Float.parseFloat(valuie);
                                if (typingnumber >= Integer.parseInt(fromrangestore.get(jk)) && typingnumber <= Integer.parseInt(torangestore.get(jk))) {
                                    Double addit = (Float.parseFloat(valuie) * Double.parseDouble(estimateamount)) /** (Double.valueOf(rangeamount.get(jk)) / (100))*/;
                                    Log.e("Cal: ", "" + (typingnumber + "*" + estimateamount) /*+ "*" + (rangeamount.get(jk) + "/" + 100)*/);
                                    holder.bean.setTotal((/*currency +*/ String.format("%.2f", addit)));
                                    holder.EditTextTotal.setText(String.format("%.2f", addit));
                                    gotvalue = "yes";
                                }
                            }
                        }

                        if (!gotvalue.equalsIgnoreCase("yes")) {
                            int index = servicestore.indexOf(servicetype);
                            Double addit = (Float.parseFloat(valuie) * Double.parseDouble(defaultestimate.get(index)));
                            Log.e("No Cal: ", "" + valuie + "*" + defaultestimate.get(index));
                            holder.bean.setTotal(String.valueOf(addit));
                            holder.EditTextTotal.setText("" + addit);
                        }
                    }

                } else {

                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.activity_additemclass_work_limit_error), Toast.LENGTH_LONG).show();
//                            holder.etVal2.setText("1.0");
                    holder.etVal2.setText(valuie.substring(0, valuie.length() - 1));
                    holder.etVal2.setSelection(holder.etVal2.getText().length());
                    holder.bean.setVal2(Float.parseFloat(holder.etVal2.getText().toString()));
                    String servicetype = holder.bean.getJobtype();
                    if (servicestore.contains(servicetype)) {
                        String gotvalue = "";
                        for (int jk = 0; jk < servicestore.size(); jk++) {
                            String arrayservice = servicestore.get(jk);
                            if (arrayservice.equalsIgnoreCase(servicetype)) {
                                String from = fromrangestore.get(jk);
                                String to = torangestore.get(jk);
                                int fromvalue = Integer.parseInt(from);
                                int tovalue = Integer.parseInt(to);
                                float typingnumber = Float.parseFloat(holder.etVal2.getText().toString());
                                if (typingnumber >= fromvalue && typingnumber <= tovalue) {
//                                            Double addit = (Double.valueOf(Integer.parseInt("1") * Double.parseDouble(estimateamount))) * Double.valueOf((Integer.valueOf(rangeamount.get(jk)) / 100));
                                    Double addit = (typingnumber * Double.parseDouble(estimateamount)) /* (Double.valueOf(rangeamount.get(jk)) / (100))*/;
                                    Log.e("Cal: ", "" + (typingnumber + "*" + estimateamount) /*+ "*" + (rangeamount.get(jk) + "/" + 100)*/);
                                    holder.bean.setTotal(/*currency +*/ String.format("%.2f", addit));
                                    holder.EditTextTotal.setText(/*currency + */ String.format("%.2f", addit));
                                    gotvalue = "yes";
                                }
                            }

                        }

                        if (!gotvalue.equalsIgnoreCase("yes")) {
                            int index = servicestore.indexOf(servicetype);
                            Double addit = (Float.parseFloat(holder.etVal2.getText().toString()) * Double.parseDouble(defaultestimate.get(index)));
                            Log.e("No Cal: ", "" + holder.etVal2.getText().toString() + "*" + defaultestimate.get(index));
                            holder.bean.setTotal(/*currency +*/ String.valueOf(addit));
                            holder.EditTextTotal.setText(/*currency + */"" + addit);
                        }
                    }
                }
                Log.e("EditTextTotal", "Updated");
            } catch (NumberFormatException e) {
                Log.e(LOG_TAG, "error reading double value: " + TimeToCal);
            }
        }

        SubItemUpdatePojo UpdateSubCategoryAmountBasedOnTime(String TimeToCal) {
            SubItemUpdatePojo subItemUpdatePojo = new SubItemUpdatePojo();
            try {


                String valuie = "1";

                if (TimeToCal.equalsIgnoreCase("")) {
                    valuie = "1";
                } else {
                    valuie = TimeToCal;
                }


                if (valuie.endsWith(".0") || valuie.endsWith(".5") || valuie.endsWith(".") || valuie.length() <= 2) {
                    if (valuie.endsWith(".")) {
                        valuie = valuie.substring(0, (valuie.length() - 1));
                    }
//                    holder.bean.setVal2(Float.parseFloat(valuie));
                    subItemUpdatePojo.setHours(Float.parseFloat(valuie));
                    String servicetype = holder.bean.getJobtype();
                    if (servicestore.contains(servicetype)) {

                        String gotvalue = "";
                        for (int jk = 0; jk < servicestore.size(); jk++) {
                            String arrayservice = servicestore.get(jk);
                            if (arrayservice.equalsIgnoreCase(servicetype)) {
                                float typingnumber = Float.parseFloat(valuie);
                                if (typingnumber >= Integer.parseInt(fromrangestore.get(jk)) && typingnumber <= Integer.parseInt(torangestore.get(jk))) {
                                    Double addit = (Float.parseFloat(valuie) * Double.parseDouble(estimateamount)) /** (Double.valueOf(rangeamount.get(jk)) / (100))*/;
                                    Log.e("Cal: ", "" + (typingnumber + "*" + estimateamount) /*+ "*" + (rangeamount.get(jk) + "/" + 100)*/);
//                                    holder.bean.setTotal((/*currency +*/ String.format("%.2f", addit)));
                                    subItemUpdatePojo.setTotal(String.format("%.2f", addit));
//                                    EditTextGoingToUpdate.setText(String.format("%.2f", addit));
//                                    subItemUpdatePojo.setEditText(String.format("%.2f", addit));
                                    gotvalue = "yes";
                                }
                            }
                        }

                        if (!gotvalue.equalsIgnoreCase("yes")) {
                            int index = servicestore.indexOf(servicetype);
                            Double addit = (Float.parseFloat(valuie) * Double.parseDouble(defaultestimate.get(index)));
                            Log.e("No Cal: ", "" + valuie + "*" + defaultestimate.get(index));
//                            holder.bean.setTotal(String.valueOf(addit));
                            subItemUpdatePojo.setTotal(String.valueOf(addit));
//                            EditTextGoingToUpdate.setText(""+addit);
//                            subItemUpdatePojo.setEditText(String.valueOf(addit));
                        }
                    }

                } else {

                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.activity_additemclass_work_limit_error), Toast.LENGTH_LONG).show();
//                            holder.etVal2.setText("1.0");
                    String NewHour = valuie.substring(0, valuie.length() - 1);
//                    holder.etVal2.setText(valuie.substring(0, valuie.length() - 1));
//                    holder.etVal2.setSelection(holder.etVal2.getText().length());
//                    holder.bean.setVal2(Float.parseFloat(holder.etVal2.getText().toString()));
                    subItemUpdatePojo.setHours(Float.parseFloat(NewHour));
                    String servicetype = holder.bean.getJobtype();
                    if (servicestore.contains(servicetype)) {
                        String gotvalue = "";
                        for (int jk = 0; jk < servicestore.size(); jk++) {
                            String arrayservice = servicestore.get(jk);
                            if (arrayservice.equalsIgnoreCase(servicetype)) {
                                String from = fromrangestore.get(jk);
                                String to = torangestore.get(jk);
                                int fromvalue = Integer.parseInt(from);
                                int tovalue = Integer.parseInt(to);
                                float typingnumber = Float.parseFloat(holder.etVal2.getText().toString());
                                if (typingnumber >= fromvalue && typingnumber <= tovalue) {
//                                            Double addit = (Double.valueOf(Integer.parseInt("1") * Double.parseDouble(estimateamount))) * Double.valueOf((Integer.valueOf(rangeamount.get(jk)) / 100));
                                    Double addit = (typingnumber * Double.parseDouble(estimateamount)) /** (Double.valueOf(rangeamount.get(jk)) / (100))*/;
                                    Log.e("Cal: ", "" + (typingnumber + "*" + estimateamount) /*+ "*" + (rangeamount.get(jk) + "/" + 100)*/);
//                                    holder.bean.setTotal(/*currency +*/ String.format("%.2f", addit));
                                    subItemUpdatePojo.setTotal(String.format("%.2f", addit));
//                                    holder.EditTextTotal.setText(/*currency + */ String.format("%.2f", addit));
//                                    subItemUpdatePojo.setEditText(String.format("%.2f", addit));
                                    gotvalue = "yes";
                                }
                            }

                        }

                        if (!gotvalue.equalsIgnoreCase("yes")) {
                            int index = servicestore.indexOf(servicetype);
                            Double addit = (Float.parseFloat(holder.etVal2.getText().toString()) * Double.parseDouble(defaultestimate.get(index)));
                            Log.e("No Cal: ", "" + holder.etVal2.getText().toString() + "*" + defaultestimate.get(index));
//                            holder.bean.setTotal(/*currency +*/ String.valueOf(addit));
                            subItemUpdatePojo.setTotal(String.valueOf(addit));
//                            holder.EditTextTotal.setText(/*currency + */"" + addit);
//                            subItemUpdatePojo.setEditText(String.valueOf(addit));
                        }
                    }
                }
                Log.e("EditTextTotal", "Updated");
            } catch (NumberFormatException e) {
                Log.e(LOG_TAG, "error reading double value: " + TimeToCal);
            }

            return subItemUpdatePojo;
        }

        private void setjobdescriptionlistener(final EstimationAdapter.Holder holder) {
            holder.jobdescription.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.toString().length() > 0)
                        holder.bean.setJobdescription(s.toString());
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }

        private void sethelperhr(final EstimationAdapter.Holder holder) {
            holder.helperhoursupdate.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {


                    if (s.toString().length() > 0) {
                        if (s.toString().endsWith(".0") || s.toString().endsWith(".5") || s.toString().endsWith(".") || !s.toString().contains(".")) {
                            String value = s.toString();
                            if (value.endsWith(".")) {
                                value = value.substring(0, (value.length() - 1));
                            }

                            float tophours = holder.bean.getVal2();
                            float helperhours = Float.parseFloat(value);
                            if (tophours >= helperhours) {
                                holder.bean.setHelperhr(value);
                            } else {
                                holder.helperhoursupdate.setText("");
                                HNDHelper.showErrorAlert(AddEstimateItemActivity.this, getResources().getString(R.string.activity_additemclass_work_limit_error));
                            }
                        } else {
                            holder.helperhoursupdate.setText("");
                            HNDHelper.showErrorAlert(AddEstimateItemActivity.this, getResources().getString(R.string.activity_additemclass_work_limit_error));
                        }
                    }


                }
            });
        }

        private void sethelpertyping(final EstimationAdapter.Holder holder) {
            holder.helperdesc.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.toString().length() > 0)
                        holder.bean.setHelpertype(s.toString());
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }

        public class Holder {

            EstimationPojo bean;
            CheckBox checkedhelp;
            TextView headingforestimate;
            LinearLayout myjobconverted_cardView_layout, overalllayout;
            EditText etVal2, jobtype, jobdescription, depositamount, materialamount, materialdesc,
                    helperedValue2, helperdesc, helperhoursupdate, EditTextWhatsIncluded, EditTextTotal;
            TextView jobtytpeheading, jobdescriptionheading, amountheading, hourheadining, materialheading, materialdescheading, EstimationJobPhoto;
            MyGridView gridview;
            RecyclerView RecyclerViewDynamic;
            ImageView ImageViewAddSubItem, ImageViewAttachment;

        }
    }

    public class BottomPageNumberAdapter extends RecyclerView.Adapter<BottomPageNumberAdapter.MyViewHolder> {

        private List<viewflip> moviesList;

        BottomPageNumberAdapter(List<viewflip> moviesList) {
            this.moviesList = moviesList;
        }

        @Override
        public BottomPageNumberAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.viewfliprow, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(BottomPageNumberAdapter.MyViewHolder holder, final int position) {
            viewflip movie = moviesList.get(position);
            holder.title.setText(movie.getTitle());


            if (storeunderline == position) {
                holder.title.setPaintFlags(holder.title.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            } else {
                holder.title.setPaintFlags(0);
            }

            holder.clickme.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    storeunderline = position;
                    showpage = position;
                    adapter.notifyDataSetChanged();
                    mAdapter.notifyDataSetChanged();

                    listView.setSelection(0);

                }
            });

        }

        @Override
        public int getItemCount() {
            return moviesList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView title;
            RelativeLayout clickme;

            MyViewHolder(View view) {
                super(view);
                title = view.findViewById(R.id.title);
                clickme = view.findViewById(R.id.clickme);

            }
        }
    }

    public class Joblistadapter extends BaseAdapter {
        private ArrayList<joblistpojo> myOrderArrList;
        private LayoutInflater myInflater;

        Joblistadapter(Context aContext, ArrayList<joblistpojo> aOrderArrList) {
            this.myOrderArrList = aOrderArrList;
            myInflater = LayoutInflater.from(aContext);
        }

        @Override
        public int getCount() {
            return myOrderArrList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = myInflater.inflate(R.layout.categorynameadapter, parent, false);

                holder.categorynameadaptertext = convertView.findViewById(R.id.categorynameadaptertext);


                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.categorynameadaptertext.setText(myOrderArrList.get(position).getOrderServiceType());


            holder.categorynameadaptertext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialogs.dismiss();
                    isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {

                        additem(AddEstimateItemActivity.this, ServiceConstant.addestimate, myOrderArrList.get(position).getOrderId(), myOrderArrList.get(position).getOrderServiceType());

                    } else {
                        final PkDialog mDialog = new PkDialog(AddEstimateItemActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.internet_label));
                        mDialog.setDialogMessage(getResources().getString(R.string.action_no_internet_message));
                        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                            }
                        });
                        mDialog.show();
                    }


                }
            });


            return convertView;
        }

        private class ViewHolder {

            private TextView categorynameadaptertext;


        }


    }

    //Remove this section if you don't want camera code (Start)
    //Got new image taken from camera intent
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode != 0) {
            String picturePath = fileUri.getPath();
            try {
                File actualImage = new File(picturePath);
                //Here I'm adding selected image name and it's parent position
                //We'll add the file in getImages() method
                MultiImagePojo multiImagePojo = new MultiImagePojo();
                multiImagePojo.setImageName(picturePath.substring(picturePath.lastIndexOf("/") + 1));
                multiImagePojo.setPositionForOperation(AddImagePosition);
                multiImagePojo.setFileType("File");
                imageItems.add(multiImagePojo);

                MyCustomCompressClass myCustomCompressClass = new MyCustomCompressClass();
                myCustomCompressClass.constructorWithFixedFileName(this, actualImage);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == 2 && resultCode != 0) {
            String picturePath = "";
            Uri selectedImageUri = data.getData();
            picturePath = getPath(getApplicationContext().getApplicationContext(), selectedImageUri);
            try {
                ///storage/emulated/0/simplecropview/scv20190326_150454.jpeg
                File actualImage = new File(picturePath);
                //Here I'm adding selected image name and it's parent position
                //We'll add the file in getImages() method
                MultiImagePojo multiImagePojo = new MultiImagePojo();
                multiImagePojo.setImageName(picturePath.substring(picturePath.lastIndexOf("/") + 1));
                multiImagePojo.setPositionForOperation(AddImagePosition);
                multiImagePojo.setFileType("File");
                imageItems.add(multiImagePojo);

                MyCustomCompressClass myCustomCompressClass = new MyCustomCompressClass();
                myCustomCompressClass.constructorWithFixedFileName(this, actualImage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void getImages() {


        File fileTarget = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + AddEstimateItemActivity.this.getApplicationContext().getPackageName()
                + "/Files/Compressed");

        File[] files = fileTarget.listFiles();

//        imageItems.clear();

        if (files != null) {
            for (File file : files) {
                if (imageItems != null && imageItems.size() > 0) {
                    for (int i = 0; i < imageItems.size(); i++) {
                        if (imageItems.get(i).getImageName().equals(file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("/") + 1))
                                && imageItems.get(i).getImagePath() == null) {
                            MultiImagePojo multiImagePojo = new MultiImagePojo();
                            multiImagePojo.setImageName(file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("/") + 1));
                            multiImagePojo.setImagePath(file.getAbsolutePath());
                            multiImagePojo.setPositionForOperation(AddImagePosition);
                            multiImagePojo.setFileType("File");
                            imageItems.set(i, multiImagePojo);
                            break;
                        }
                    }
                }
            }
        }
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    public static String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }

    public class GridViewAdapter extends ArrayAdapter {
        private Context context;
        private List<MultiImagePojo> data = new ArrayList<MultiImagePojo>();
        private int resourceId;
        private LayoutInflater inflater;
        private int ParentPosition;

        private GridViewAdapter(Context context, int resourceId, List<MultiImagePojo> data, int ParentPosition) {
            super(context, resourceId, data);
            this.resourceId = resourceId;
            this.context = context;
            this.data = data;
            this.ParentPosition = ParentPosition;
            inflater = LayoutInflater.from(context);

        }

        //I prefer to have Holder to keep all controls
        //So that I can recycle easily in getView
        class ViewHolder {
            ImageView image, close;
            LinearLayout LinearLayoutMainHolder;
        }


        @NonNull
        @Override
        public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
            View row = convertView;
            GridViewAdapter.ViewHolder holder = null;
            if (row == null) {
                row = inflater.inflate(resourceId, parent, false);
                holder = new GridViewAdapter.ViewHolder();
                holder.image = row.findViewById(R.id.image);
                holder.close = row.findViewById(R.id.close);
                holder.LinearLayoutMainHolder = row.findViewById(R.id.LinearLayoutMainHolder);
                row.setTag(holder);
            } else {
                holder = (GridViewAdapter.ViewHolder) row.getTag(); //Easy to recycle view
            }

            if (data.get(position).getPositionForOperation() == ParentPosition) {
                if (data.get(position).getFileType().equals("File")) {
                    Picasso.with(context).load("file://" + data.get(position).getImagePath())
                            .resize(160, 160)
                            .placeholder(R.drawable.nouserimg)
                            .into(holder.image);
                } else {
                    Picasso.with(context).load(data.get(position).getImagePath())
                            .resize(160, 160)
                            .placeholder(R.drawable.nouserimg)
                            .into(holder.image);
                }

                holder.LinearLayoutMainHolder.setVisibility(View.VISIBLE);
                holder.close.setVisibility(View.VISIBLE);
            }


            holder.close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // String imagePath =   parent.getAdapter().getItem(position).toString();

                    final PkDialog mDialog = new PkDialog(AddEstimateItemActivity.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.delete_label));
                    mDialog.setDialogMessage(getResources().getString(R.string.activity_beforephoto_delete_photo));
                    mDialog.setPositiveButton(
                            getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (data.get(position).getFileType().equals("File")) {
                                        File fdelete = new File(data.get(position).getImagePath());
                                        if (fdelete.exists()) {
                                            if (fdelete.delete()) {
                                                for (int i = 0; i < imageItems.size(); i++) {
                                                    if (data.get(position).getImageName().equals(imageItems.get(i).getImageName())) {
                                                        imageItems.remove(i);
                                                        getImages();
                                                        adapter.notifyDataSetChanged();
                                                        gridAdapter.notifyDataSetChanged();
                                                    }
                                                }
                                            }
                                        }
                                    } else if (data.get(position).getFileType().equals("URL")) {
                                        for (int i = 0; i < imageItems.size(); i++) {
                                            if (data.get(position).getImagePath().equals(imageItems.get(i).getImagePath())) {
                                                imageItems.remove(i);
                                                getImages();
                                                adapter.notifyDataSetChanged();
                                                gridAdapter.notifyDataSetChanged();
                                            }
                                        }
                                    }


                            /*gridAdapter = new GridViewAdapter(BeforeAndAfterPhotoActivity.this, R.layout.grid_item, imageItems);
                            gridview.setAdapter(gridAdapter);*/
                                    mDialog.dismiss();
                                }
                            }
                    );
                    mDialog.setNegativeButton(
                            getResources().getString(R.string.ongoing_detail_cancelbtn_label), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();
                                }
                            }
                    );
                    mDialog.show();
                }
            });
            return row;
        }
    }

    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }

    void Alert(String title, String Message) {
        final PkDialog mDialog = new PkDialog(AddEstimateItemActivity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(Message);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    void DeleteTheUnSaveItems() {
        loadingDialog(getResources().getString(R.string.loading_in));
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        String provider_ids = pref.getString("provider_id", "");
        jsonParams.put("provider_id", provider_ids);

        SharedPreferences Job_idpref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
        String Job_id = Job_idpref.getString("jobid", "");
        jsonParams.put("booking_id", Job_id);

        int i = 0;
        boolean CanHitAPI = false;
        if (arrayList.size() > 1) {
            for (int j = 1; j < arrayList.size() && arrayList.size() > 1; j++) {
                if (arrayList.get(j).isCanDelete()) {
                    jsonParams.put("service_id[" + i + "]", arrayList.get(j).getVal1());
                    jsonParams.put("service_type[" + i + "]", arrayList.get(j).getJobtype());
                    i++;
                    CanHitAPI = true;
                }
            }
        }

        if (CanHitAPI) {
            ServiceRequest mservicerequest = new ServiceRequest(AddEstimateItemActivity.this);
            mservicerequest.makeServiceRequest(ServiceConstant.DeleteMultiEstimation, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
                @Override
                public void onCompleteListener(String response) {
                    System.out.println("provider/delete_multi_estitems--------" + response);

                    Log.e("delete_multi_estitems", response);
                    try {
                        JSONObject jobject = new JSONObject(response);
                        if (jobject.getString("status").equalsIgnoreCase("1")) {
                            finish();
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        } else {
                            Alert(getResources().getString(R.string.server_lable_header), jobject.getString("response"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    dismissDialog();
                }

                @Override
                public void onErrorListener() {
                    dismissDialog();
                }
            });
        } else {
            dismissDialog();
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    }

    private void postRequestConfirmBooking(final Context aContext) {

        HashMap<String, String> params = new HashMap<>();
        SharedPreferences Job_idpref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
        String Job_id = Job_idpref.getString("jobid", "");
        params.put("job_id", Job_id);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        String provider_id = pref.getString("provider_id", "");
        params.put("provider_id", provider_id);
        params.put("estimation_itemize", "0");


        for (int jk = 0; jk < arrayList.size(); jk++) {

            params.put("est[" + jk + "][service_id]", arrayList.get(jk).getVal1());
            params.put("est[" + jk + "][description]", arrayList.get(jk).getJobdescription());
            params.put("est[" + jk + "][hours]", "" + arrayList.get(jk).getVal2());
            params.put("est[" + jk + "][material]", arrayList.get(jk).getMaterial());
            params.put("est[" + jk + "][material_desc]", arrayList.get(jk).getMaterialdescription());
            params.put("est[" + jk + "][amount]", "" + arrayList.get(jk).getTotal());

            params.put("est[" + jk + "][depositamount]", "" + arrayList.get(jk).getDepositamount());
            params.put("est[" + jk + "][whatsincluded]", "" + arrayList.get(jk).getWhatsincluded());

            if (arrayList.get(jk).getCheckedhelper() == 1) {

                params.put("est[" + jk + "][helper_amount]", "" + arrayList.get(jk).gethelperTotal());
                params.put("est[" + jk + "][helper_description]", "" + arrayList.get(jk).getHelpertype());
                params.put("est[" + jk + "][helper_hour]", "" + arrayList.get(jk).getHelperhr());

                System.out.println("-------------est[" + jk + "][helper_hour]----------------" + arrayList.get(jk).getetval2());
                System.out.println("-------------est[" + jk + "][helper_amount]----------------" + arrayList.get(jk).gethelperTotal());
                System.out.println("-------------est[" + jk + "][helper_hour]----------------" + arrayList.get(jk).getHelperhr());
            }

            //Following code is used to send multiple Sub Items to API --> Start
            if (arrayList.get(jk).getMultiSubItemPojo() != null && arrayList.get(jk).getMultiSubItemPojo().size() > 0) {
                for (int TempSubIndex = 0; TempSubIndex < arrayList.get(jk).getMultiSubItemPojo().size(); TempSubIndex++) {
                    params.put("est[" + jk + "][sub_items][" + TempSubIndex + "][name]", arrayList.get(jk).getMultiSubItemPojo().get(TempSubIndex).getName());
                    params.put("est[" + jk + "][sub_items][" + TempSubIndex + "][hours]", arrayList.get(jk).getMultiSubItemPojo().get(TempSubIndex).getHours());
                    params.put("est[" + jk + "][sub_items][" + TempSubIndex + "][amount]", arrayList.get(jk).getMultiSubItemPojo().get(TempSubIndex).getAmount());
                    params.put("est[" + jk + "][sub_items][" + TempSubIndex + "][description]", arrayList.get(jk).getMultiSubItemPojo().get(TempSubIndex).getDescription());
                    params.put("est[" + jk + "][sub_items][" + TempSubIndex + "][status]", "0");
                    if (arrayList.get(jk).getMultiSubItemPojo().get(TempSubIndex).getID() != null &&
                            !arrayList.get(jk).getMultiSubItemPojo().get(TempSubIndex).getID().isEmpty())
                        params.put("est[" + jk + "][sub_items][" + TempSubIndex + "][_id]", arrayList.get(jk).getMultiSubItemPojo().get(TempSubIndex).getID());
                }
            }
            //Following code is used to send multiple Sub Items to API --> End

            if (imageItems != null && imageItems.size() > 0) {
                List<String> TempImageItems = new ArrayList<>();
                for (int i = 0; i < imageItems.size(); i++) {
                    if (imageItems.get(i).getPositionForOperation() == jk) {
                        TempImageItems.add(imageItems.get(i).getImagePath());
                    }
                }

                //Following code is used to send multiple images to API --> Start
                JSONArray aJsonArr = null;
                try {
                    aJsonArr = new JSONArray(TempImageItems);

                    for (int a = 0; a < aJsonArr.length(); a++) {
                        if (URLUtil.isValidUrl(aJsonArr.getString(a))) {
                            params.put("est[" + jk + "][estimation_images][" + a + "]", aJsonArr.getString(a));
                        } else {
                            Bitmap src = BitmapFactory.decodeFile(aJsonArr.getString(a));
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            src.compress(Bitmap.CompressFormat.PNG, 100, baos);
                            //  byte[] data = baos.toByteArray();
                            String imgString = Base64.encodeToString(getBytesFromBitmap(src),
                                    Base64.NO_WRAP);
                            params.put("est[" + jk + "][estimation_images][" + a + "]", imgString);
                        }


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        Log.e("Test", params.toString());

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(ServiceConstant.SENDESTIMATE_URL, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                dismissDialog();
                System.out.println("-------------get Confirm Category list Response----------------" + response);
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {
                        Intent intent = new Intent(getApplicationContext(), EstimateBuilderActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("EXIT", true);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    } else {
                        final PkDialog mDialog = new PkDialog(AddEstimateItemActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.action_sorry));
                        mDialog.setDialogMessage(object.getString("response"));
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        mDialog.dismiss();
                                    }
                                }
                        );
                        mDialog.show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block


                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                dismissDialog();
            }
        });
    }

    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.handypro.files.selected");
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
