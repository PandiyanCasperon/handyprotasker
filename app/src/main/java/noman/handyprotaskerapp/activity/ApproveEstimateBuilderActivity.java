package noman.handyprotaskerapp.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import Dialog.PkDialog;
import Utils.ConnectionDetector;
import Utils.CurrencySymbolConverter;
import noman.handyprotaskerapp.DB.EstimateAcceptSQlite;
import noman.handyprotaskerapp.Pojo.ApproveEditBuilderPojo;
import noman.handyprotaskerapp.ProgressDialog1;
import noman.handyprotaskerapp.R;
import noman.handyprotaskerapp.Service.HNDHelper;
import noman.handyprotaskerapp.reschedulepage;
import service.ServiceConstant;
import volley.ServiceRequest;


/**
 * Created by user127 on 16-02-2018.
 */

public class ApproveEstimateBuilderActivity extends AppCompatActivity {


    String storetaskerid, storeestimatehours;
    TextView hintshow;

    LinearLayout imageView;
    LinearLayout displaylist, nodata, downoptions;
    SharedPreferences pref;
    ListView listView;
    TextView done, TextViewTotal, TextViewHandyProSavings, TextViewSubTotal, reject;
    private editestimationadapter adapter;
    ArrayList<ApproveEditBuilderPojo> arrayList = new ArrayList<ApproveEditBuilderPojo>();
    private ConnectionDetector cd;
    private Boolean isInternetPresent = false;
    private ProgressDialog1 myDialog;
    String currency;
    String grandtotal = "0";
    int status = 0;
    String selecteddate, selectedtime;
    private EstimateAcceptSQlite mHelper;
    private SQLiteDatabase dataBase;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.approveestimatepage);

        mHelper = new EstimateAcceptSQlite(this);
        this.deleteDatabase(mHelper.DATABASE_NAME);


        cd = new ConnectionDetector(ApproveEstimateBuilderActivity.this);

        pref = getApplicationContext().getSharedPreferences("logindetails", 0);
        currency = pref.getString("myCurrencySymbol", "");
        imageView = (LinearLayout) findViewById(R.id.backclick);
        listView = (ListView) findViewById(R.id.listview);
        displaylist = (LinearLayout) findViewById(R.id.displaylist);
        downoptions = (LinearLayout) findViewById(R.id.downoptions);
        nodata = (LinearLayout) findViewById(R.id.nodata);
        hintshow = (TextView) findViewById(R.id.hintshow);
        reject = (TextView) findViewById(R.id.reject);
        done = (TextView) findViewById(R.id.done);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    int nozero = 0;
                    for (int jk = 0; jk < arrayList.size(); jk++) {
                        String amount = "" + arrayList.get(jk).getCheckedvalue();
                        if (amount.equalsIgnoreCase("1")) {
                            nozero = 1;
                        }
                    }

                    if (nozero == 1) {
                        float estimatehours = 0;
                        for (int jk = 0; jk < arrayList.size(); jk++) {
                            dataBase = mHelper.getWritableDatabase();
                            ContentValues values = new ContentValues();
                            values.put(EstimateAcceptSQlite.KEY_SERVICEID, arrayList.get(jk).getServiceid());
                            if (arrayList.get(jk).getCheckedvalue() == 0) {
                                values.put(EstimateAcceptSQlite.KEY_STATUS, "2");
                            } else {
                                estimatehours = (estimatehours + arrayList.get(jk).getVal2());

                                values.put(EstimateAcceptSQlite.KEY_STATUS, "" + arrayList.get(jk).getCheckedvalue());

                            }
                            dataBase.insert(EstimateAcceptSQlite.TABLE_NAME, null, values);
                        }


                        SharedPreferences Job_idpref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
                        String Job_id = Job_idpref.getString("jobid", "");

                        SharedPreferences pref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("jobid", Job_id);
                        editor.putString("estimatehours", "" + estimatehours);
                        editor.putString("taskerid", storetaskerid);
                        editor.apply();
                        editor.commit();
                        Intent InfoIntent = new Intent(ApproveEstimateBuilderActivity.this, reschedulepage.class);
                        startActivity(InfoIntent);

                    } else {
                        HNDHelper.showErrorAlert(ApproveEstimateBuilderActivity.this, getResources().getString(R.string.activity_approveestimatepage_choose_cat_estimate));
                    }
                } else {
                    HNDHelper.showErrorAlert(ApproveEstimateBuilderActivity.this, getResources().getString(R.string.no_inetnet_label));
                }

            }
        });
        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                status = 2;
                sendrequest(ApproveEstimateBuilderActivity.this, ServiceConstant.Estimateapproval);
            }
        });

        TextViewSubTotal = (TextView) findViewById(R.id.TextViewSubTotal);
        TextViewHandyProSavings = (TextView) findViewById(R.id.TextViewHandyProSavings);
        TextViewTotal = (TextView) findViewById(R.id.TextViewTotal);

        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            paymentPost(ApproveEstimateBuilderActivity.this, ServiceConstant.USERESTIMATE_URL);
        } else {
            HNDHelper.showErrorAlert(ApproveEstimateBuilderActivity.this, getResources().getString(R.string.no_inetnet_label));
        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ApproveEstimateBuilderActivity.this, EstimateBuilderActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });
    }

    public class editestimationadapter extends ArrayAdapter<ApproveEditBuilderPojo> {

        private ArrayList<ApproveEditBuilderPojo> items;
        private int layoutResourceId;
        private Context context;
        Typeface tf;
        SharedPreferences pref;
        String currency;

        private editestimationadapter(Context context, int layoutResourceId, ArrayList<ApproveEditBuilderPojo> items) {
            super(context, layoutResourceId, items);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.items = items;
            tf = Typeface.createFromAsset(getContext().getAssets(), "Poppins-Regular.ttf");
            pref = context.getSharedPreferences("logindetails", 0);
            currency = pref.getString("myCurrencySymbol", "");
        }

        @NonNull
        @SuppressLint("ViewHolder")
        @Override
        public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
            View row = convertView;
            editestimationadapter.Holder holder = null;

            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new editestimationadapter.Holder();
            holder.bean = items.get(position);

            holder.materialdn = (EditText) row.findViewById(R.id.materialdn);
            holder.etVal1 = (EditText) row.findViewById(R.id.edVal1);
            holder.etVal2 = (EditText) row.findViewById(R.id.edValue2);

            holder.deletecraftman = (CheckBox) row.findViewById(R.id.deletecraftman);


            holder.etVal1.setTypeface(tf);
            holder.etVal2.setTypeface(tf);

            holder.materialdn.setTypeface(tf);


            holder.jobtytpeheading = (TextView) row.findViewById(R.id.jobtytpeheading);
            holder.jobdescriptionheading = (TextView) row.findViewById(R.id.jobdescriptionheading);
            holder.amountheading = (TextView) row.findViewById(R.id.amountheading);
            holder.hourheadining = (TextView) row.findViewById(R.id.hourheadining);
            holder.materialheading = (TextView) row.findViewById(R.id.materialheading);
            holder.materialdescheading = (TextView) row.findViewById(R.id.materialdescheading);
            holder.totalamount = (TextView) row.findViewById(R.id.totalamount);


            holder.materialdhead = (TextView) row.findViewById(R.id.materialdhead);


            holder.materialdhead.setTypeface(tf);

            holder.jobtytpeheading.setTypeface(tf);
            holder.jobdescriptionheading.setTypeface(tf);
            holder.amountheading.setTypeface(tf);
            holder.hourheadining.setTypeface(tf);
            holder.materialheading.setTypeface(tf);
            holder.materialdescheading.setTypeface(tf);
            holder.totalamount.setTypeface(tf);
            holder.totalamount.setTypeface(tf);


            holder.unhideimgae = (ImageView) row.findViewById(R.id.unhideimgae);

            holder.materialdesc = (EditText) row.findViewById(R.id.materialdesc);
            holder.materialamount = (EditText) row.findViewById(R.id.materialamount);
            holder.jobdescription = (EditText) row.findViewById(R.id.jobdescription);

            holder.materialdesc.setTypeface(tf);
            holder.jobdescription.setTypeface(tf);
            holder.materialdescheading.setTypeface(tf);
            holder.materialamount.setTypeface(tf);


            row.setTag(holder);

            setupItem(holder);

            return row;
        }

        private void setupItem(final editestimationadapter.Holder holder) {


            holder.jobdescription.setText(holder.bean.getJobdescription());
            holder.jobtytpeheading.setText(holder.bean.getJobtype());


            switch (holder.bean.getStatus()) {
                case "2":
                    holder.unhideimgae.setBackgroundResource(R.drawable.cancelmark);
                    holder.unhideimgae.setVisibility(View.VISIBLE);
                    holder.deletecraftman.setVisibility(View.GONE);
                    break;
                case "1":
                    holder.unhideimgae.setBackgroundResource(R.drawable.ticked);
                    holder.unhideimgae.setVisibility(View.VISIBLE);
                    holder.deletecraftman.setVisibility(View.GONE);
                    break;
                case "0":
                    holder.unhideimgae.setVisibility(View.GONE);
                    holder.deletecraftman.setVisibility(View.VISIBLE);
                    break;
                default:
                    holder.unhideimgae.setVisibility(View.GONE);
                    holder.deletecraftman.setVisibility(View.GONE);
                    break;
            }


            if (holder.bean.getCheckedvalue() == 0) {
                holder.deletecraftman.setChecked(false);

            } else {
                holder.deletecraftman.setChecked(true);
            }

            holder.deletecraftman.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                    if (buttonView.isChecked()) {
                        holder.bean.setCheckedvalue(1);
                        notifyDataSetChanged();
                    } else {
                        holder.bean.setCheckedvalue(0);
                        notifyDataSetChanged();
                    }
                }
            });

            if (holder.bean.getMaterialdescription().equalsIgnoreCase("") || holder.bean.getMaterialdescription().equalsIgnoreCase(" ")) {
                /*holder.materialdn.setText(getResources().getString(R.string.activity_approveestimatepage_no_material_desc));*/
                holder.materialdn.setText("");
            } else {
                holder.materialdn.setText(holder.bean.getMaterialdescription());
            }


            holder.etVal1.setText(String.valueOf(holder.bean.getVal1()));
            holder.etVal2.setText(String.valueOf(holder.bean.getVal2()));


            holder.totalamount.setText(currency + String.valueOf(holder.bean.getTotal()));
        }

        public class Holder {
            CheckBox deletecraftman;
            ApproveEditBuilderPojo bean;
            EditText etVal1;
            ImageView unhideimgae;
            EditText etVal2, materialdn, jobdescription, materialamount, materialdesc;
            TextView materialdhead, totalamount, jobtytpeheading, jobdescriptionheading, amountheading, hourheadining, materialheading, materialdescheading;

        }


    }

    private void paymentPost(Context mContext, String url) {


        myDialog = new ProgressDialog1(ApproveEstimateBuilderActivity.this);
        myDialog.setCancelable(true);
        myDialog.setCanceledOnTouchOutside(true);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        SharedPreferences senduserid = getApplicationContext().getSharedPreferences("senduserid", MODE_PRIVATE);
        String userid = senduserid.getString("storeuserid", "");
        jsonParams.put("user_id", userid);

        SharedPreferences Job_idpref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
        String Job_id = Job_idpref.getString("jobid", "");
        jsonParams.put("job_id", Job_id);


        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.print("user/get-job-estimation-category" + response);

                String provide_estimation = "", Str_Currency = "", HandyProSavings = "0";


                try {
                    JSONObject jobject = new JSONObject(response);
                    provide_estimation = jobject.getString("provide_estimation");
                    grandtotal = jobject.getString("grandtotal");
                    if (jobject.has("handypro_plus_saving"))
                        HandyProSavings = jobject.getString("handypro_plus_saving");
                    storeestimatehours = jobject.getString("overall_taskhours");
                    if (provide_estimation.equalsIgnoreCase("1")) {
                        String currencyCode = CurrencySymbolConverter.getCurrencySymbol(Str_Currency);

                        JSONArray jarry = jobject.getJSONArray("estimation_category");
                        if (jarry.length() > 0) {
                            for (int i = 0; i < jarry.length(); i++) {
                                JSONObject jobjects_amount = jarry.getJSONObject(i);

                                String title = jobjects_amount.getString("service_type");


                                storetaskerid = jobjects_amount.getString("tasker_id");


                                if (jobjects_amount.has("estimated_details")) {

                                    JSONObject fareobj = jobjects_amount.getJSONObject("estimated_details");

                                    if (fareobj.has("hours") && fareobj.has("totalamount")) {
                                        String hours = fareobj.getString("hours");

                                        String materialamount;
                                        if (fareobj.has("material")) {
                                            materialamount = fareobj.getString("material");
                                        } else {
                                            materialamount = "";
                                        }

                                        String description = fareobj.getString("description");
                                        String material_desc;
                                        if (fareobj.has("material_desc")) {
                                            material_desc = fareobj.getString("material_desc");
                                        } else {
                                            material_desc = "";
                                        }
                                        String totalamount = fareobj.getString("totalamount");

                                        String status;
                                        if (fareobj.has("status")) {
                                            status = fareobj.getString("status");
                                        } else {
                                            status = "";
                                        }

                                        String service_id = fareobj.getString("service_id");
                                        ApproveEditBuilderPojo atomPayment = new ApproveEditBuilderPojo();
                                        atomPayment.setVal2(Float.parseFloat(hours));
                                        atomPayment.setJobtype(title);
                                        atomPayment.setJobdescription(description);
                                        atomPayment.setTotal(Double.parseDouble(totalamount));
                                        atomPayment.setMaterial(materialamount);
                                        atomPayment.setMaterialdescription(material_desc);
                                        atomPayment.setCheckedvalue(0);
                                        atomPayment.setServiceid(service_id);
                                        atomPayment.setStatus(status);

                                        arrayList.add(atomPayment);
                                    }
                                }
                            }
                        }

                        if (arrayList.size() > 0) {

                            if (jobject.has("estimation_status")) {
                                String estimation_status = jobject.getString("estimation_status");
                                hintshow.setVisibility(View.VISIBLE);
                                if (estimation_status.equalsIgnoreCase("0")) {
                                    hintshow.setText(getResources().getString(R.string.activity_approveestimatepage_estim_waiting_approval));
                                    done.setVisibility(View.VISIBLE);
                                    reject.setVisibility(View.VISIBLE);
                                } else if (estimation_status.equalsIgnoreCase("1")) {
                                    hintshow.setText(getResources().getString(R.string.activity_approveestimatepage_estim_accepted));
                                    done.setVisibility(View.GONE);
                                    reject.setVisibility(View.GONE);
                                } else if (estimation_status.equalsIgnoreCase("2")) {
                                    hintshow.setText(getResources().getString(R.string.activity_approveestimatepage_estim_rejected));
                                    done.setVisibility(View.GONE);
                                    reject.setVisibility(View.GONE);
                                }

                            } else {
                                hintshow.setVisibility(View.VISIBLE);
                                hintshow.setText(getResources().getString(R.string.activity_approveestimatepage_estim_waiting_approval));
                                done.setVisibility(View.VISIBLE);
                                reject.setVisibility(View.VISIBLE);
                            }


                            displaylist.setVisibility(View.VISIBLE);
                            nodata.setVisibility(View.GONE);
                            downoptions.setVisibility(View.VISIBLE);

                            TextViewSubTotal.setText(currency + grandtotal);
                            TextViewHandyProSavings.setText(currency + HandyProSavings);
                            int IntSubTotalNumber = 0, IntHandyProSavings = 0;
                            double DoubleSubTotalNumber = 0.0, DoubleHandyProSavings = 0.0;

                            if (grandtotal.length() >= 1) {
                                if (grandtotal.matches("[-+]?[0-9]*\\.?[0-9]+")) {
                                    DoubleSubTotalNumber = Double.parseDouble(grandtotal);
                                } else if (grandtotal.matches("[0-9]+")) {
                                    IntSubTotalNumber = Integer.parseInt(grandtotal);
                                }
                            }

                            if (HandyProSavings.length() >= 1) {
                                if (HandyProSavings.matches("[-+]?[0-9]*\\.?[0-9]+")) {
                                    DoubleHandyProSavings = Double.parseDouble(HandyProSavings);
                                    if (DoubleSubTotalNumber > 0) {
                                        TextViewTotal.setText(currency + String.valueOf(DoubleSubTotalNumber - DoubleHandyProSavings));
                                    } else if (IntSubTotalNumber > 0) {
                                        TextViewTotal.setText(currency + String.valueOf(IntSubTotalNumber - DoubleHandyProSavings));
                                    }
                                } else if (grandtotal.matches("[0-9]+")) {
                                    IntHandyProSavings = Integer.parseInt(HandyProSavings);
                                    if (DoubleSubTotalNumber > 0) {
                                        TextViewTotal.setText(currency + String.valueOf(DoubleSubTotalNumber - IntHandyProSavings));
                                    } else if (IntSubTotalNumber > 0) {
                                        TextViewTotal.setText(currency + String.valueOf(IntSubTotalNumber - DoubleHandyProSavings));
                                    }
                                }
                            }


                            adapter = new editestimationadapter(ApproveEstimateBuilderActivity.this, R.layout.approvereo_item2, arrayList);
                            listView.setAdapter(adapter);

                        } else {

                            downoptions.setVisibility(View.GONE);
                            displaylist.setVisibility(View.GONE);
                            nodata.setVisibility(View.VISIBLE);
                        }

                    } else {
                        final PkDialog mDialog = new PkDialog(ApproveEstimateBuilderActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.failed_label));
                        mDialog.setDialogMessage(getResources().getString(R.string.sorry_invalid_data));
                        mDialog.setPositiveButton(getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                            }
                        });
                        mDialog.show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }


                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    private void sendrequest(final Context aContext, String url) {


        myDialog = new ProgressDialog1(ApproveEstimateBuilderActivity.this);
        myDialog.setCancelable(true);
        myDialog.setCanceledOnTouchOutside(true);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> params = new HashMap<>();
        SharedPreferences senduserid = getApplicationContext().getSharedPreferences("senduserid", MODE_PRIVATE);
        String userid = senduserid.getString("storeuserid", "");
        params.put("user_id", userid);

        SharedPreferences Job_idpref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
        String Job_id = Job_idpref.getString("jobid", "");
        params.put("job_id", Job_id);
        params.put("approval_status", "" + status);

        if (status == 1) {
            params.put("pickup_date", "" + selecteddate);
            params.put("pickup_time", "" + selectedtime);

        }


        if (status == 2) {

            for (int jk = 0; jk < arrayList.size(); jk++) {
                params.put("est[" + jk + "][service_id]", arrayList.get(jk).getServiceid());
                if (arrayList.get(jk).getCheckedvalue() == 0) {
                    params.put("est[" + jk + "][status]", "2");
                } else {
                    params.put("est[" + jk + "][status]", "2");
                }
            }
        } else {
            for (int jk = 0; jk < arrayList.size(); jk++) {
                params.put("est[" + jk + "][service_id]", arrayList.get(jk).getServiceid());
                if (arrayList.get(jk).getCheckedvalue() == 0) {
                    params.put("est[" + jk + "][status]", "2");
                } else {
                    params.put("est[" + jk + "][status]", "" + arrayList.get(jk).getCheckedvalue());
                }
            }
        }


        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------get Confirm Category list Response----------------" + response);
                try {
                    JSONObject object = new JSONObject(response);
                    String message = object.getString("response");
                    if (object.getString("status").equalsIgnoreCase("1")) {
                        if (status == 1) {
                            hintshow.setText(getResources().getString(R.string.activity_approveestimatepage_estim_accepted));
                            done.setVisibility(View.GONE);
                            reject.setVisibility(View.GONE);
                        } else {
                            hintshow.setText(getResources().getString(R.string.activity_approveestimatepage_estim_rejected));
                            done.setVisibility(View.GONE);
                            reject.setVisibility(View.GONE);
                        }

                        Intent intent = new Intent(ApproveEstimateBuilderActivity.this, EstimateBuilderActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();


                    } else {
                        final PkDialog mDialog = new PkDialog(ApproveEstimateBuilderActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.action_sorry));
                        mDialog.setDialogMessage(message);
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.ok_label), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        mDialog.dismiss();
                                    }
                                }
                        );
                        mDialog.show();
                    }
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }


    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
            Intent intent = new Intent(ApproveEstimateBuilderActivity.this, EstimateBuilderActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

}
