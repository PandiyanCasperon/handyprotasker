package noman.handyprotaskerapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.cooltechworks.creditcarddesign.CardSelector;
import com.cooltechworks.creditcarddesign.CreditCardUtils;
import com.cooltechworks.creditcarddesign.CreditCardView;
import com.cooltechworks.creditcarddesign.pager.CardFragmentAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import Utils.ConnectionDetector;
import Utils.SessionManager;
import noman.handyprotaskerapp.R;
import noman.handyprotaskerapp.ReceiveCashPage;
import noman.handyprotaskerapp.ReviwesPage;
import noman.handyprotaskerapp.Service.HNDHelper;
import service.ServiceConstant;
import volley.ServiceRequest;

import static com.cooltechworks.creditcarddesign.CreditCardUtils.CARD_NAME_PAGE;
import static com.cooltechworks.creditcarddesign.CreditCardUtils.EXTRA_CARD_CVV;
import static com.cooltechworks.creditcarddesign.CreditCardUtils.EXTRA_CARD_EXPIRY;
import static com.cooltechworks.creditcarddesign.CreditCardUtils.EXTRA_CARD_HOLDER_NAME;
import static com.cooltechworks.creditcarddesign.CreditCardUtils.EXTRA_CARD_NUMBER;
import static com.cooltechworks.creditcarddesign.CreditCardUtils.EXTRA_ENTRY_START_PAGE;

public class CardPayment extends AppCompatActivity {
    private CreditCardView mCreditCardView;
    int mLastPageSelected = 0;
    TextView activity_card_edit_nextTXT;
    private String mCardNumber;
    private String mCVV;
    private String mCardHolderName;
    private String mExpiry;
    private int mStartPage = 0;
    private CardFragmentAdapter mCardAdapter;
    ConnectionDetector cd;
    Button paycardpayment;
    public static String task_id = "";
    SessionManager sessionManager;
    String str_jobId = "",paymenttype="";
    ArrayList<String> PrepaymentList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_payment);

        initialization();
        Bundle args = savedInstanceState != null ? savedInstanceState : getIntent().getExtras();

        loadPager(args);
        checkParams(args);

        findViewById(R.id.activity_card_edit_previousTXT).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPrevious();
            }
        });


        activity_card_edit_nextTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ViewPager pager = (ViewPager) findViewById(com.cooltechworks.creditcarddesign.R.id.card_field_container_pager);

                int max = pager.getAdapter().getCount();

                if (pager.getCurrentItem() == max - 1) {
                    if (mCardNumber == null) {
                        HNDHelper.showErrorAlert(CardPayment.this, getResources().getString(R.string.activity_enter_card_number));
                        pager.setCurrentItem(0);
                    } else if (mExpiry == null) {
                        HNDHelper.showErrorAlert(CardPayment.this, getResources().getString(R.string.activity_enter_card_expiry));
                        pager.setCurrentItem(1);
                    } else if (mExpiry.length() < 5) {
                        HNDHelper.showErrorAlert(CardPayment.this, getResources().getString(R.string.activity_enter_card_expirdy));
                        pager.setCurrentItem(1);
                    } else if (mCVV == null) {
                        HNDHelper.showErrorAlert(CardPayment.this, getResources().getString(R.string.activity_enter_card_cvv_number));
                        pager.setCurrentItem(2);
                    } else if (mCardHolderName == null) {
                        HNDHelper.showErrorAlert(CardPayment.this, getResources().getString(R.string.activity_enter_card_holder_name));
                        pager.setCurrentItem(3);
                    } else {
                        if (cd.isConnectingToInternet()) {
                           // postRequestSaveCardData(EditCardActivity.this, ServiceConstant.AddStripeCard);
                        } else {
                            HNDHelper.showErrorAlert(CardPayment.this, getResources().getString(R.string.nointernet_text));
                        }
                    }
                } else {
                    showNext();
                }
            }

        });


        paycardpayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(paymenttype.equalsIgnoreCase("pre_payment_cash_initiated")){

                    postprepayment(ServiceConstant.PRECARDPAYMENT);
                }
                else{
                    postcardpayment(ServiceConstant.CARDPAYMENT);
                }


            }
        });


    }

    public void initialization(){

        activity_card_edit_nextTXT = (TextView)findViewById(R.id.activity_card_edit_nextTXT);
        mCreditCardView = (CreditCardView) findViewById(R.id.activity_card_edit_credit_card_view);
        paycardpayment = (Button)findViewById(R.id.paycardpayment);
        sessionManager = new SessionManager(this);
        cd = new ConnectionDetector(CardPayment.this);

        Intent i = getIntent();
        task_id = i.getStringExtra("jobId");
        paymenttype = i.getStringExtra("Type");
        PrepaymentList = getIntent().getStringArrayListExtra("PrepaymentList");

        SharedPreferences pref = getApplicationContext().getSharedPreferences("ongoingpage", MODE_PRIVATE);
        str_jobId = pref.getString("jobid", "");

    }



    private void checkParams(Bundle bundle) {
        if (bundle == null) {
            return;
        }
        mCardHolderName = bundle.getString(EXTRA_CARD_HOLDER_NAME);
        mCVV = bundle.getString(EXTRA_CARD_CVV);
        mExpiry = bundle.getString(EXTRA_CARD_EXPIRY);
        mCardNumber = bundle.getString(EXTRA_CARD_NUMBER);
        mStartPage = bundle.getInt(EXTRA_ENTRY_START_PAGE);

        final int maxCvvLength = CardSelector.selectCard(mCardNumber).getCvvLength();
        if (mCVV != null && mCVV.length() > maxCvvLength) {
            mCVV = mCVV.substring(0, maxCvvLength);
        }

        mCreditCardView.setCVV(mCVV);
        mCreditCardView.setCardHolderName(mCardHolderName);
        mCreditCardView.setCardExpiry(mExpiry);
        mCreditCardView.setCardNumber(mCardNumber);

        if (mCardAdapter != null) {
            mCreditCardView.post(new Runnable() {
                @Override
                public void run() {
                    mCardAdapter.setMaxCVV(maxCvvLength);
                    mCardAdapter.notifyDataSetChanged();
                }
            });
        }

        int cardSide = bundle.getInt(CreditCardUtils.EXTRA_CARD_SHOW_CARD_SIDE, CreditCardUtils.CARD_SIDE_FRONT);
        if (cardSide == CreditCardUtils.CARD_SIDE_BACK) {
            mCreditCardView.showBack();
        }
        if (mStartPage > 0 && mStartPage <= CARD_NAME_PAGE) {
            getViewPager().setCurrentItem(mStartPage);
        }
    }

    ViewPager getViewPager() {
        return (ViewPager) findViewById(R.id.card_field_container_pager);
    }
    public void showNext() {
        final ViewPager pager = (ViewPager) findViewById(R.id.card_field_container_pager);
        CardFragmentAdapter adapter = (CardFragmentAdapter) pager.getAdapter();

        int max = adapter.getCount();
        int currentIndex = pager.getCurrentItem();

        if (currentIndex + 1 < max) {
            pager.setCurrentItem(currentIndex + 1);
        } else {
            setKeyboardVisibility(false);
            // completed the card entry.
        }

        refreshNextButton();
    }

    public void showPrevious() {
        final ViewPager pager = (ViewPager) findViewById(R.id.card_field_container_pager);
        int currentIndex = pager.getCurrentItem();

        if (currentIndex == 0) {
            setResult(RESULT_CANCELED);
            finish();
        }

        if (currentIndex - 1 >= 0) {
            pager.setCurrentItem(currentIndex - 1);
        }

        refreshNextButton();
    }
    public void loadPager(Bundle bundle) {
        ViewPager pager = getViewPager();
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                mCardAdapter.focus(position);

                /*if ((mCreditCardView.getCardType() != CreditCardUtils.CardType.AMEX_CARD) && (position == 2)) {
                    mCreditCardView.showBack();
                } else if (((position == 1) || (position == 3)) && (mLastPageSelected == 2) && (mCreditCardView.getCardType() != CreditCardUtils.CardType.AMEX_CARD)) {
                    mCreditCardView.showFront();
                }*/

                mCreditCardView.showFront();

                mLastPageSelected = position;

                refreshNextButton();

            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        pager.setOffscreenPageLimit(4);

        mCardAdapter = new CardFragmentAdapter(getSupportFragmentManager(), bundle);
        mCardAdapter.setOnCardEntryCompleteListener(new CardFragmentAdapter.ICardEntryCompleteListener() {
            @Override
            public void onCardEntryComplete(int currentIndex) {
                showNext();
            }

            @Override
            public void onCardEntryEdit(int currentIndex, String entryValue) {
                switch (currentIndex) {
                    case 0:
                        mCardNumber = entryValue.replace(CreditCardUtils.SPACE_SEPERATOR, "");
                        mCreditCardView.setCardNumber(mCardNumber);
                        if (mCardAdapter != null) {
                            mCardAdapter.setMaxCVV(CardSelector.selectCard(mCardNumber).getCvvLength());
                        }
                        break;
                    case 1:
                        mExpiry = entryValue;
                        mCreditCardView.setCardExpiry(entryValue);
                        break;
                    case 2:
                        mCVV = entryValue;
                        mCreditCardView.setCVV(entryValue);
                        break;
                    case 3:
                        mCardHolderName = entryValue;
                        mCreditCardView.setCardHolderName(entryValue);

                        break;
                }
            }
        });
        pager.setAdapter(mCardAdapter);

    }

    public void refreshNextButton() {
        ViewPager pager = (ViewPager) findViewById(R.id.card_field_container_pager);

        int max = pager.getAdapter().getCount();

        int text = R.string.next;

        if (pager.getCurrentItem() == max - 1) {
            text = R.string.done;
        }

        ((TextView) findViewById(R.id.activity_card_edit_nextTXT)).setText(text);
    }

    private void setKeyboardVisibility(boolean visible) {
        final EditText editText = (EditText) findViewById(R.id.card_number_field);

        if (!visible) {

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        } else {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void postcardpayment(String url){

        HashMap<String, String> jsonParams = new HashMap<>();
        task_id = sessionManager.getTaskid();
        String[] splitString = mExpiry.split("/");
        jsonParams.put("taskid", task_id);
        jsonParams.put("card_number", mCardNumber);
        jsonParams.put("exp_month", splitString[0]);
        jsonParams.put("exp_year", "20"+mExpiry.substring(3));
        jsonParams.put("cvc_number",mCVV);
        System.out.println("-------------task_id----------------" +task_id);
        System.out.println("-------------card_number----------------" + mCardNumber);
        System.out.println("-------------exp_month----------------" + splitString[0]);
        System.out.println("-------------exp_year----------------" + "20"+mExpiry.substring(3));
        System.out.println("-------------cvc_number----------------" + mCVV);
        ServiceRequest mRequest = new ServiceRequest(this);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------add Stripe Card Response----------------" + response);

                String S_response="";
                try {
                    JSONObject object = new JSONObject(response);
                    S_response = object.getString("response");
                    if (object.getString("status").equalsIgnoreCase("1")) {

                        Toast.makeText(getApplicationContext(),S_response,Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(CardPayment.this, ReviwesPage.class);
                        intent.putExtra("jobId", str_jobId);
                        startActivity(intent);

                    } else {
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {

            }
        });
    }

    public void postprepayment(String url){

        HashMap<String, String> jsonParams = new HashMap<>();
        task_id = sessionManager.getTaskid();
        String[] splitString = mExpiry.split("/");
        jsonParams.put("task_id", task_id);
        jsonParams.put("card_number", mCardNumber);
        jsonParams.put("exp_month", splitString[0]);
        jsonParams.put("exp_year", "20"+mExpiry.substring(3));
        jsonParams.put("cvc_number",mCVV);
        for (int i = 0; i < PrepaymentList.size(); i++) {
            jsonParams.put("prepayment_id[" + i + "]", PrepaymentList.get(i));
            System.out.println("-------------prepayment_id[\" + i + \"]----------------" + PrepaymentList.get(i));
        }
        System.out.println("-------------task_id----------------" +task_id);
        System.out.println("-------------card_number----------------" + mCardNumber);
        System.out.println("-------------exp_month----------------" + splitString[0]);
        System.out.println("-------------exp_year----------------" + "20"+mExpiry.substring(3));
        System.out.println("-------------cvc_number----------------" + mCVV);

        ServiceRequest mRequest = new ServiceRequest(this);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------add Stripe Card Response----------------" + response);

                String S_response="";
                try {
                    JSONObject object = new JSONObject(response);
                    S_response = object.getString("response");
                    if (object.getString("status").equalsIgnoreCase("1")) {

                        Toast.makeText(getApplicationContext(),S_response,Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(CardPayment.this, ReviwesPage.class);
                        intent.putExtra("jobId", str_jobId);
                        startActivity(intent);

                    }
                    else if(object.getString("status").equalsIgnoreCase("11")) {
                        Toast.makeText(getApplicationContext(),S_response,Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(CardPayment.this,MyJobs_OnGoingDetailActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    else if(object.getString("status").equalsIgnoreCase("0")) {
                        Toast.makeText(getApplicationContext(),S_response,Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {

            }
        });
    }


}