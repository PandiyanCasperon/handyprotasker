package noman.handyprotaskerapp.activity;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Window;

import noman.handyprotaskerapp.R;
import service.ServiceConstant;


/**
 */
public class BaseActivity extends AppCompatActivity implements ServiceConstant {
    private Dialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void showDialog(String message) {
        dialog = new Dialog(this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public void dismissDialog() {
        try {
            if (dialog != null)
                dialog.dismiss();
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        //  Toast.makeText(getApplicationContext(),"resume",Toast.LENGTH_LONG).show();

    }
}
