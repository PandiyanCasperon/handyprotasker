package noman.handyprotaskerapp.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import Dialog.PkDialog;
import Dialog.PkLoadingDialog;
import Utils.ConnectionDetector;
import Utils.CurrencySymbolConverter;
import Utils.SessionManager;
import noman.handyprotaskerapp.Adapter.PaymentFareSummeryAdapter;
import noman.handyprotaskerapp.Pojo.PaymentFareSummeryPojo;
import noman.handyprotaskerapp.R;
import noman.handyprotaskerapp.WidgetSupport.listhei;
import service.ServiceConstant;
import volley.ServiceRequest;


public class TransactionDetailActivity extends AppCompatActivity {
    private String myUsedIdStr = "", myBookingIdStr = "";
    private TextView myHeaderTitleTXT;
    private ImageView myBackIMG;
    private RelativeLayout myBackLAY;
    private boolean isInternetPresent = false;
    private ConnectionDetector myConnectionDetector;
    private RelativeLayout myInternalLAY;
    private String myRefreshStr = "normal", myCurrencySymbol = "";
    private ServiceRequest myRequest;
    private PkLoadingDialog myLoadingDialog;
    private TextView myCategoryNameTXT, myTaskerNameTXT, myAddressTXT, myTotalHourTXT, myPerhourTXT, myHourlyRateTXT,
            myTaskAmountTXT, myServiceTaxTXT, myTotalAmountTXT, myBookingIDTXT, myBookingTaskTimeTXT, myPaymentModeTXT,material_fees;
    private LinearLayout material_layout;
    private String lat_address="",long_address="",Address="", currencyCode;
    private SessionManager mySession;
    listhei fare_listview;
    PaymentFareSummeryAdapter adapter;
    private ArrayList<PaymentFareSummeryPojo> farelist, TempFareList;
    private int PreAmount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_detail);

        farelist = new ArrayList<PaymentFareSummeryPojo>();
        TempFareList = new ArrayList<PaymentFareSummeryPojo>();

        initializeHeaderBar();
        classAndWidgetInitialize();




        paymentPost(TransactionDetailActivity.this, ServiceConstant.PAYMENT_URL);



    }


    private void paymentPost(Context mContext, String url) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        String provider_ids=pref.getString("provider_id","");
        jsonParams.put("provider_id", provider_ids);
        jsonParams.put("job_id", myBookingIdStr);

        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("payment", response);

                String Str_status = "", Str_response = "",user_approval_status="", Str_jobDescription = "", Str_NeedPayment = "", Str_Currency = "", Str_BtnGroup = "";


                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject object = jobject.getJSONObject("response");
                        JSONObject object2 = object.getJSONObject("job");
                        Str_jobDescription = object2.getString("job_summary");
                        Str_NeedPayment = object2.getString("need_payment");
                        Str_Currency = object2.getString("currency");
                        currencyCode = CurrencySymbolConverter.getCurrencySymbol(Str_Currency);

                        Str_BtnGroup = object2.getString("btn_group");

                        if(object2.has("user_pre_amount")){
                            JSONArray user_pre_amount_obj = object2.getJSONArray("user_pre_amount");
                            for (int i = 0; i < user_pre_amount_obj.length() ; i++) {
                                if (user_pre_amount_obj.getJSONObject(i).has("payment_status")
                                        && user_pre_amount_obj.getJSONObject(i).has("pre_amount")
                                        && user_pre_amount_obj.getJSONObject(i).getString("payment_status").equals("Accepted")){
                                    PreAmount = PreAmount + Integer.parseInt(user_pre_amount_obj.getJSONObject(i).getString("pre_amount"));
                                }

                            }
                        }

                        JSONArray jarry = object2.getJSONArray("billing");

                        if (jarry.length() > 0) {

                            for (int i = 0; i < jarry.length(); i++) {
                                JSONObject jobjects_amount = jarry.getJSONObject(i);
                                PaymentFareSummeryPojo pojo = new PaymentFareSummeryPojo();

                                String title = jobjects_amount.getString("title");

                                String desc="";
                                if(jobjects_amount.has("description"))
                                {
                                    desc=jobjects_amount.getString("description");
                                }
                                else
                                {
                                    desc="";
                                }


                                pojo.setPayment_desc(desc);
                                pojo.setPayment_title(jobjects_amount.getString("title"));

                                if(title.contains("Hours")||title.contains("Payment mode") || title.contains("Task type")||title.contains("Start Date")|| title.contains("Time")||title.contains("Month")|| title.contains("End Date")){
                                    pojo.setPayment_amount(jobjects_amount.getString("amount"));
                                }
                                else {
                                    pojo.setPayment_amount(currencyCode + jobjects_amount.getString("amount"));
                                }
                                farelist.add(pojo);
                            }
                        }

                    } else {
                        Str_response = jobject.getString("response");
                    }

                    System.out.println("payment1---------------------------");

                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (Str_status.equalsIgnoreCase("1")) {

                    System.out.println();

                    System.out.println();
                    fare_listview.setEnabled(false);

                    if (PreAmount != 0 && PreAmount > 0){
                        PaymentFareSummeryPojo pojo = new PaymentFareSummeryPojo();
                        pojo.setPayment_title("Prepayment");
                        pojo.setPayment_amount(currencyCode + String.valueOf(PreAmount));
                        TempFareList.set((TempFareList.size()-1), pojo);
                        pojo = new PaymentFareSummeryPojo();
                        pojo.setPayment_title(farelist.get(farelist.size()-1).getPayment_title());
                        pojo.setPayment_amount(farelist.get(farelist.size()-1).getPayment_amount());
                        TempFareList.add(pojo);
                        adapter = new PaymentFareSummeryAdapter(TransactionDetailActivity.this, TempFareList);
                    }else if (PreAmount == 0){
                        adapter = new PaymentFareSummeryAdapter(TransactionDetailActivity.this, farelist);
                    }

                    fare_listview.setAdapter(adapter);
//                    if (Str_NeedPayment.equalsIgnoreCase("1")) {
//                        Rl_layout_farepayment_methods.setVisibility(View.VISIBLE);
//                    } else {
//                        Rl_layout_farepayment_methods.setVisibility(View.GONE);
//                    }
                }
            }

            @Override
            public void onErrorListener() {

            }
        });
    }


    private void initializeHeaderBar() {

        fare_listview = (listhei) findViewById(R.id.cancelreason_listView);
        RelativeLayout headerBar = (RelativeLayout) findViewById(R.id.headerBar_layout);
        myBackLAY = (RelativeLayout) headerBar.findViewById(R.id.headerBar_left_layout);
        myBackIMG = (ImageView) headerBar.findViewById(R.id.headerBar_imageView);
        myHeaderTitleTXT = (TextView) headerBar.findViewById(R.id.headerBar_title_textView);
        myHeaderTitleTXT.setText(getResources().getString(R.string.activity_transaction_TXT_view_task_details));
        myBackIMG.setImageResource(R.drawable.backarrow);
        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });
    }


    private void classAndWidgetInitialize() {
        myConnectionDetector = new ConnectionDetector(TransactionDetailActivity.this);
        myRequest = new ServiceRequest(TransactionDetailActivity.this);
        mySession = new SessionManager(TransactionDetailActivity.this);
        isInternetPresent = myConnectionDetector.isConnectingToInternet();

        myInternalLAY = (RelativeLayout) findViewById(R.id.transaction_detail_noInternet_layout);
        myCategoryNameTXT = (TextView) findViewById(R.id.screen_transaction_detail_TXT_categoryname);
        myTaskerNameTXT = (TextView) findViewById(R.id.screen_transaction_detail_TXT_taskername);
        myAddressTXT = (TextView) findViewById(R.id.screen_transaction_detail_TXT_address);
        myTotalHourTXT = (TextView) findViewById(R.id.screen_transaction_detail_TXT_totalhours);
        myPerhourTXT = (TextView) findViewById(R.id.screen_transaction_detail_TXT_perhour);
        myHourlyRateTXT = (TextView) findViewById(R.id.screen_transaction_detail_TXT_hourlyrate);
        myTaskAmountTXT = (TextView) findViewById(R.id.screen_transaction_detail_TXT_taskamount);
        myServiceTaxTXT = (TextView) findViewById(R.id.screen_transaction_detail_TXT_servicetax);
        myTotalAmountTXT = (TextView) findViewById(R.id.screen_transaction_detail_TXT_totalamount);
        myBookingIDTXT = (TextView) findViewById(R.id.screen_transaction_detail_TXT_bookingid);
        myBookingTaskTimeTXT = (TextView) findViewById(R.id.screen_transaction_detail_TXT_bookingtasktime);
        myPaymentModeTXT = (TextView) findViewById(R.id.screen_transaction_detail_TXT_payment_mode);
        material_fees=(TextView)findViewById(R.id.material_fees);
        material_layout=(LinearLayout)findViewById(R.id.material_layout);
        HashMap<String, String> aAmountMap = mySession.getWalletDetails();
        String aCurrencyCode = aAmountMap.get(SessionManager.KEY_CURRENCY_CODE);
        myCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(aCurrencyCode);

        getIntentValues();
        getTransactionDetailsData();
    }


    private void getIntentValues() {
        if (getIntent() != null) {
            myUsedIdStr = getIntent().getExtras().getString("ProviderId");
            myBookingIdStr = getIntent().getExtras().getString("BookingId");
        }
    }

    private void getTransactionDetailsData() {
        if (isInternetPresent) {
            myInternalLAY.setVisibility(View.GONE);
            getDetailData();
        } else {
            // mySwipeLAY.setEnabled(true);
            myInternalLAY.setVisibility(View.VISIBLE);
        }
    }

    private void getDetailData() {
        startLoading();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("provider_id", myUsedIdStr);
        jsonParams.put("booking_id", myBookingIdStr);

        myRequest.makeServiceRequest(ServiceConstant.TRANSACTION_DETAIL_URL, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

                    @Override
                    public void onCompleteListener(String response) {
                        Log.e("response", response);
                        String sStatus = "";
                        try {
                            JSONObject aObject = new JSONObject(response);
                            sStatus = aObject.getString("status");
                            if (sStatus.equalsIgnoreCase("1")) {
                                JSONObject response_Object = aObject.getJSONObject("response");
                                if (response_Object.length() > 0) {
                                    JSONArray aJobsArray = response_Object.getJSONArray("jobs");
                                    if (aJobsArray.length() > 0) {
                                        JSONObject aArrayObject = aJobsArray.getJSONObject(0);
                                        myBookingIDTXT.setText(aArrayObject.getString("job_id"));
                                        String service="";
                                        JSONArray service_type = aArrayObject.getJSONArray("category_name");
                                        for (int b = 0; b < service_type.length(); b++)
                                        {

                                            String value=""+service_type.getString(b);


                                            service+=value+",";
                                        }

                                        if(service.endsWith(","))
                                        {
                                            service=service.substring(0,service.length()-1);
                                        }

                                        myCategoryNameTXT.setText(service);
                                        myTotalAmountTXT.setText(myCurrencySymbol + " " + aArrayObject.getString("total_amount"));
                                        myTaskerNameTXT.setText(aArrayObject.getString("user_name"));
                                        //myAddressTXT.setText(aArrayObject.getString("location"));
                                        myTotalHourTXT.setText(aArrayObject.getString("total_hrs"));
                                        lat_address=aArrayObject.getString("lat_provider");
                                        long_address=aArrayObject.getString("lng_provider");
                                        myPerhourTXT.setText(myCurrencySymbol + aArrayObject.getString("per_hour"));
                                        myHourlyRateTXT.setText(myCurrencySymbol + aArrayObject.getString("min_hrly_rate"));
                                        myTaskAmountTXT.setText(myCurrencySymbol + aArrayObject.getString("task_amount"));
                                        myServiceTaxTXT.setText(myCurrencySymbol + aArrayObject.getString("service_tax"));
                                        myPaymentModeTXT.setText(aArrayObject.getString("payment_mode"));
                                        myBookingTaskTimeTXT.setText(aArrayObject.getString("booking_time"));
                                        String material_fee = aArrayObject.getString("meterial_fee");
                                        if (material_fee.equalsIgnoreCase("")) {
                                            material_fees.setText("---");
                                        } else {
                                            material_fees.setText(myCurrencySymbol + aArrayObject.getString("meterial_fee"));
                                        }
                                        if(!lat_address.equalsIgnoreCase("")&&!long_address.equalsIgnoreCase("")){
                                            Address=getCompleteAddressString(Double.parseDouble(lat_address), Double.parseDouble(long_address));
                                            myAddressTXT.setText(Address);
                                        }else{
                                            myAddressTXT.setText("---");
                                        }
                                    }
                                }
                            } else {
                                myBookingIDTXT.setText("---");
                                myCategoryNameTXT.setText("---");
                                myTaskerNameTXT.setText("---");
                                myAddressTXT.setText("---");
                                myTotalHourTXT.setText("---");
                                myPerhourTXT.setText("---");
                                myHourlyRateTXT.setText("---");
                                myTaskAmountTXT.setText("---");
                                myServiceTaxTXT.setText("---");
                                myTotalAmountTXT.setText("---");
                                myBookingTaskTimeTXT.setText("---");
                                myPaymentModeTXT.setText("---");
                                material_fees.setText("---");
                                String sResponse = aObject.getString("response");
                                alert(getResources().getString(R.string.my_rides_rating_header_sorry_textview), sResponse);
                            }
                            // loadInfoData(myTransactionInfoList);
                            stopLoading();
                        } catch (JSONException e) {
                            stopLoading();
                            e.printStackTrace();
                        }
                    }


                    @Override
                    public void onErrorListener() {
                        stopLoading();
                    }
                }

        );
    }

    private void startLoading() {
        if (myRefreshStr.equalsIgnoreCase("normal")) {
            myLoadingDialog = new PkLoadingDialog(TransactionDetailActivity.this);
            myLoadingDialog.show();
        } else {
            //  mySwipeLAY.setRefreshing(true);
        }
    }

    private void stopLoading() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (myRefreshStr.equalsIgnoreCase("normal")) {
                    myLoadingDialog.dismiss();
                } else {
                    //     mySwipeLAY.setRefreshing(false);
                }
            }
        }, 250);
    }

    private void alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(TransactionDetailActivity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //-------------Method to get Complete Address------------
    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        String loc_addr="";
        Geocoder geocoder = new Geocoder(TransactionDetailActivity.this, Locale.getDefault());
        try {
            List<android.location.Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                android.location.Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");
                loc_addr=returnedAddress.getAddressLine(0);

            } else {
                Log.e("Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Current loction address", "Canont get Address!");
        }
        return loc_addr;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            onBackPressed();
            finish();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

            return true;
        }
        return false;
    }
}
