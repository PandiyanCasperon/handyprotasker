package noman.handyprotaskerapp.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.provider.Settings;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import Dialog.PkDialog;
import Utils.MyCustomCompressClass;
import noman.handyprotaskerapp.R;
import service.ServiceConstant;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class CaptureSignActivity extends AppCompatActivity {


    public static String tempDir;


    LinearLayout mContent;
    signature mSignature;
    Button mClear, mGetSign, mCancel,capture_photo;

    public int count = 1;
    public String current = null;
    private Bitmap mBitmap;
    View mView;
    File mypath;
    private String uniqueId;
    private EditText yourName;
    TextView jobidnumber, textchange, clientname, jobtytpeheading;
    String startjob = "";
    String[] permissionsRequired = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};
    private static int MY_PERMISSIONS_REQUEST_LOCATION = 123;
    private static int REQUEST_PERMISSION_SETTING = 151;
    private static final int PICK_CAMERA_IMAGE = 1;
    private static final int PICK_GALLERY_IMAGE = 2;

    private Uri fileUri; // file url to store image/video
    private static String IMAGE_DIRECTORY_NAME;
    BroadcastReceiver receiver;
    private TextView update_amount_tv;

    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signature);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        SharedPreferences pref1 = getApplicationContext().getSharedPreferences("jjjooo", 0);
        String jjoobbidd = pref1.getString("jobbidd", "");

        Bundle extras = getIntent().getExtras();


        if (extras != null && extras.containsKey("startjob")) {
            startjob = extras.getString("startjob");
        }


        // init_modal_bottomsheet();

        tempDir = Environment.getExternalStorageDirectory() + "/" + getResources().getString(R.string.signature) + "/";
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        File directory = cw.getDir(getResources().getString(R.string.signature), Context.MODE_PRIVATE);


        prepareDirectory();
        uniqueId = getTodaysDate() + "_" + getCurrentTime() + "_" + Math.random();
        current = uniqueId + ".png";
        mypath = new File(directory, current);

        jobidnumber = (TextView) findViewById(R.id.jobidnumber);
        jobidnumber.setText(jjoobbidd);

        mContent = (LinearLayout) findViewById(R.id.linearLayout);
        mSignature = new signature(this, null);
        mSignature.setBackgroundColor(Color.WHITE);
        mContent.addView(mSignature, ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);
        mClear = (Button) findViewById(R.id.clear);
        mGetSign = (Button) findViewById(R.id.getsign);
        mGetSign.setVisibility(View.GONE);
        mCancel = (Button) findViewById(R.id.cancel);
        capture_photo=(Button)findViewById(R.id.capture_photo);

        textchange = (TextView) findViewById(R.id.textchange);
        clientname = (TextView) findViewById(R.id.clientname);
        jobtytpeheading = (TextView) findViewById(R.id.jobtytpeheading);
        update_amount_tv=(TextView)findViewById(R.id.update_amount_tv);

        if(getIntent().getExtras().containsKey("page")){

            String Page=getIntent().getStringExtra("page");
            String Updated_amount=getIntent().getStringExtra("amount");

            if(Page.equals("change_order")){

                update_amount_tv.setVisibility(VISIBLE);
                update_amount_tv.setText(getResources().getString(R.string.updatedamount)+" "+Updated_amount);
            }

        } else {

            update_amount_tv.setVisibility(GONE);
        }

        mView = mContent;


        if (startjob != null && startjob.equals("st")) {
            textchange.setText(getResources().getString(R.string.authorizesignaturenutre));
        }

        if (extras != null && extras.containsKey("From") && extras.getString("From").equals("EstimateBuilder")) {
            clientname.setVisibility(View.GONE);
            jobtytpeheading.setVisibility(View.VISIBLE);
            jobidnumber.setVisibility(View.GONE);
            jobtytpeheading.setText(jjoobbidd);
            textchange.setText(getResources().getString(R.string.you_are_accepting) + " " + jjoobbidd + " " +
                    getResources().getString(R.string.tearms_and_condition) + " " + ServiceConstant.EmptyBaseURL);
        } else if (extras != null && extras.containsKey("From") && extras.getString("From").equals("MoreInfoPage")) {
            clientname.setVisibility(View.GONE);
            jobtytpeheading.setVisibility(View.VISIBLE);
            jobidnumber.setVisibility(View.GONE);
            jobtytpeheading.setText(jjoobbidd);
            textchange.setText(getResources().getString(R.string.you_authorizing));
        }

        yourName = (EditText) findViewById(R.id.yourName);

        mClear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.v("log_tag", "Panel Cleared");
                mSignature.clear();
                mGetSign.setEnabled(false);
                mGetSign.setVisibility(View.GONE);
            }
        });

        mGetSign.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mView.setDrawingCacheEnabled(true);
                mSignature.save(mView);

            }
        });

        mCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

        capture_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ActivityCompat.requestPermissions(
                        CaptureSignActivity.this,
                        permissionsRequired,
                        MY_PERMISSIONS_REQUEST_LOCATION
                );

            }
        });


        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                Log.e("imageCaptured","");

            }
        };

    }


    public class signature extends View {
        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();

        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();

        public signature(Context context, AttributeSet attrs) {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        @SuppressLint("WrongThread")
        public void save(View v) {
            Log.v("log_tag", "Width: " + v.getWidth());
            Log.v("log_tag", "Height: " + v.getHeight());
            if (mBitmap == null) {
                mBitmap = Bitmap.createBitmap(mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);
                ;
            }
            Canvas canvas = new Canvas(mBitmap);
            try {
                FileOutputStream mFileOutStream = new FileOutputStream(mypath);

                v.draw(canvas);
                mBitmap.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream);
                mFileOutStream.flush();
                mFileOutStream.close();
              //  String url = MediaStore.Images.Media.insertImage(getContentResolver(), mBitmap, "title", null);
                Log.v("log_tag", "url: " + mypath.toString());

                Bitmap src = BitmapFactory.decodeFile(mypath.toString());
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                src.compress(Bitmap.CompressFormat.PNG, 100, baos);
                //  byte[] data = baos.toByteArray();
                String imgString = Base64.encodeToString(getBytesFromBitmap(src),
                        Base64.NO_WRAP);
                Log.v("log_tag", "Base64: " + imgString);


                SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("changordersign", 0);
                SharedPreferences.Editor prefeditor = prefjobid.edit();
                prefeditor.putString("sign", mypath.toString());
                prefeditor.apply();
                prefeditor.commit();


                /*overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();*/

                Intent intent = getIntent();
                setResult(RESULT_OK, intent);
                finish();

                //In case you want to delete the file
                //boolean deleted = mypath.delete();
                //Log.v("log_tag","deleted: " + mypath.toString() + deleted);
                //If you want to convert the image to string use base64 converter

            } catch (Exception e) {
                Log.v("log_tag", e.toString());
            }
        }

        public void clear() {
            path.reset();
            invalidate();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawPath(path, paint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float eventX = event.getX();
            float eventY = event.getY();
            mGetSign.setEnabled(true);
            mGetSign.setVisibility(View.VISIBLE);

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:

                case MotionEvent.ACTION_UP:

                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++) {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;

                default:
                    debug("Ignored touch event: " + event.toString());
                    return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void debug(String string) {
        }

        private void expandDirtyRect(float historicalX, float historicalY) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX;
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX;
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY;
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY) {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }

    private boolean captureSignature() {

        boolean error = false;
        String errorMessage = "";


        if (yourName.getText().toString().equalsIgnoreCase("")) {
            errorMessage = errorMessage + "Please enter your Name\n";
            error = true;
        }

        if (error) {
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP, 105, 50);
            toast.show();
        }

        return error;
    }

    private String getTodaysDate() {

        final Calendar c = Calendar.getInstance();
        int todaysDate = (c.get(Calendar.YEAR) * 10000) +
                ((c.get(Calendar.MONTH) + 1) * 100) +
                (c.get(Calendar.DAY_OF_MONTH));
        Log.w("DATE:", String.valueOf(todaysDate));
        return (String.valueOf(todaysDate));

    }

    private String getCurrentTime() {

        final Calendar c = Calendar.getInstance();
        int currentTime = (c.get(Calendar.HOUR_OF_DAY) * 10000) +
                (c.get(Calendar.MINUTE) * 100) +
                (c.get(Calendar.SECOND));
        Log.w("TIME:", String.valueOf(currentTime));
        return (String.valueOf(currentTime));

    }


    private boolean prepareDirectory() {
        try {
            if (makedirs()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }
    }

    private boolean makedirs() {
        File tempdir = new File(tempDir);
        if (!tempdir.exists())
            tempdir.mkdirs();

        if (tempdir.isDirectory()) {
            File[] files = tempdir.listFiles();
            for (File file : files) {
                if (!file.delete()) {
                    System.out.println("Failed to delete " + file);
                }
            }
        }
        return (tempdir.isDirectory());
    }

    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            if (checkPermissions()) {
                PicSelectionPopUp();
            } else {
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setIcon(R.drawable.craftmanappmainlogo);
                alertDialog.setTitle("Alert!!!");
                alertDialog.setMessage("Storage Read, Write and Camera permission is required to read files from your device. Enable it to get basic details\n\nThank you");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Proceed", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        intent.setData(Uri.fromParts("package", getPackageName(), null));
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                    }
                });
            }
        }
    }

    private boolean checkPermissions() {
        boolean Status = false;
        Status = ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED;

        Status = ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED;

        return Status;
    }


    void PicSelectionPopUp() {
        final PkDialog mDialog = new PkDialog(CaptureSignActivity.this);
        mDialog.setDialogTitle(getResources().getString(R.string.select_label));
        mDialog.setDialogMessage(getResources().getString(R.string.activity_beforephoto_choose_to_add_photos));
        mDialog.setPositiveButton(
                getResources().getString(R.string.cameraword), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();

                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

                        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

                        // start the image capture Intent
                        startActivityForResult(intent, PICK_CAMERA_IMAGE);


                    }
                }
        );
        mDialog.setNegativeButton(
                getResources().getString(R.string.galleryword), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();

                        Intent i = new Intent(
                                Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, PICK_GALLERY_IMAGE);
                    }
                }
        );

        mDialog.show();
    }


    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        File compressedImage;


        if (requestCode == 1 && resultCode != 0) {
            String picturePath = fileUri.getPath();

            try {
                /*File actualImage = new File(picturePath);
                MyCustomCompressClass myCustomCompressClass = new MyCustomCompressClass();
                myCustomCompressClass.constructor(this, actualImage);*/

                SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("changordersign", 0);
                SharedPreferences.Editor prefeditor = prefjobid.edit();
                prefeditor.putString("sign", picturePath);
                prefeditor.apply();
                prefeditor.commit();
                Intent intent = getIntent();
                setResult(RESULT_OK, intent);
                finish();


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == 2 && resultCode != 0) {
            String picturePath = "";
            Uri selectedImageUri = data.getData();
            picturePath = getPath(getApplicationContext().getApplicationContext(), selectedImageUri);
            try {
              /*  File actualImage = new File(picturePath);
                MyCustomCompressClass myCustomCompressClass = new MyCustomCompressClass();
                myCustomCompressClass.constructor(this, actualImage);*/

                SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("changordersign", 0);
                SharedPreferences.Editor prefeditor = prefjobid.edit();
                prefeditor.putString("sign", picturePath);
                prefeditor.apply();
                prefeditor.commit();
                Intent intent = getIntent();
                setResult(RESULT_OK, intent);
                finish();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }

    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.handypro.files.selected");
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
