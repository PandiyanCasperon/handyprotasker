package noman.handyprotaskerapp.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.HashMap;

import Dialog.PkDialog;
import Utils.ConnectionDetector;
import Utils.CurrencySymbolConverter;
import Utils.HideSoftKeyboard;
import Utils.SessionManager;
import fcm.MyFirebaseMessagingService;
import noman.handyprotaskerapp.Navigationdrawer;
import noman.handyprotaskerapp.R;
import noman.handyprotaskerapp.termsprivacy;
import service.ServiceConstant;
import socket.SocketHandler;
import volley.ServiceRequest;

import static service.ServiceConstant.LOGIN_URL;


/**
 * Created by user88 on 11/28/2015.
 */
public class LoginActivity extends AppCompatActivity {

    public static final int RequestPermissionCode = 7;
    RelativeLayout Rl_layout_no_account_signup, Rl_layout_forgotpassword;
    EditText Et_login_email, Et_login_password;
    Button Bt_login;
    private Dialog dialog;
    private SocketHandler socketHandler;
    private SessionManager session;
    private String Gcm_Id;
    public static String Appmail = "", google_api = "";
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    ImageView imageView;
    LinearLayout backarrowlogin;
    Typeface tf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loginpage);
        tf = Typeface.createFromAsset(getAssets(), "Poppins-Regular.ttf");


        if (Build.VERSION.SDK_INT >= 23) {

            if (CheckingPermissionIsEnabledOrNot()) {
                // Toast.makeText(LoginActivity.this, "All Permissions Granted Successfully", Toast.LENGTH_LONG).show();
            }

            // If, If permission is not enabled then else condition will execute.
            else {
                //Calling method to enable permission.
                RequestMultiplePermission();

            }


        }


        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);

        String provider_id = pref.getString("provider_id", "");
        if (provider_id.equals("") || provider_id.length() < 2) {

        } else {


            String termspage = pref.getString("termsandconditions", "");
            if (termspage.equals("1")) {
                Intent intent = new Intent(LoginActivity.this, Navigationdrawer.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(LoginActivity.this, termsprivacy.class);
                startActivity(intent);
                finish();
            }


        }


        initilize();
        //Hide the keyboard of click the outside
        HideSoftKeyboard.setupUI(
                LoginActivity.this.getWindow().getDecorView(),
                LoginActivity.this);

        backarrowlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //finish();
            }
        });

        Bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Et_login_email.getText().toString().length() == 0) {


                    final PkDialog mDialog = new PkDialog(LoginActivity.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.action_sorry));
                    mDialog.setCancelOnTouchOutside(false);
                    mDialog.setDialogMessage(getResources().getString(R.string.mainpage_logineithemail_validate_lable));
                    mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();

                        }
                    });
                    mDialog.show();


                } else if (Et_login_password.getText().toString().length() == 0) {


                    final PkDialog mDialog = new PkDialog(LoginActivity.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.action_sorry));
                    mDialog.setCancelOnTouchOutside(false);
                    mDialog.setDialogMessage(getResources().getString(R.string.mainpage_login_password_validate_lable));
                    mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();

                        }
                    });
                    mDialog.show();


                } else {

                    login();
                }
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Rl_layout_forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, FotgotPasswordActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });


    }

    public void initilize() {
        cd = new ConnectionDetector(LoginActivity.this);
        session = new SessionManager(LoginActivity.this);
        socketHandler = SocketHandler.getInstance(LoginActivity.this);
        Rl_layout_no_account_signup = (RelativeLayout) findViewById(R.id.loginpage_no_account_label);
        Et_login_email = (EditText) findViewById(R.id.loginpage_email_edittext_label);
        Et_login_password = (EditText) findViewById(R.id.loginpage_password_edittext_label);
        Bt_login = (Button) findViewById(R.id.logipage_login_label);
        Rl_layout_forgotpassword = (RelativeLayout) findViewById(R.id.loginpage_forgt_password);
        imageView = (ImageView) findViewById(R.id.imageView);
        backarrowlogin = (LinearLayout) findViewById(R.id.backarrowlogin);

        Et_login_email.setImeOptions(EditorInfo.IME_ACTION_DONE);
        Et_login_password.setImeOptions(EditorInfo.IME_ACTION_DONE);

        isInternetPresent = cd.isConnectingToInternet();


        if (isInternetPresent) {
            AppInfo();
        }


    }

    public void login() {

        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {

            dialog = new Dialog(LoginActivity.this);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_loading);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();

            Gcm_Id = MyFirebaseMessagingService.getRegistrationId(LoginActivity.this);
            if(Gcm_Id==null){

                Gcm_Id="";
            }
            PostRequest();
            System.out.println("--------------login-------------------" + LOGIN_URL);
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
        }
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(LoginActivity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(
                getResources().getString(R.string.action_ok), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                    }
                }
        );

        mDialog.show();
    }

    //-----------------------Code for login post request-----------------

    private void PostRequest() {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("email", Et_login_email.getText().toString());
        jsonParams.put("password", Et_login_password.getText().toString());
        jsonParams.put("gcm_id", Gcm_Id);

        System.out.println("Gcm-----------$$$$$$$$$" + Gcm_Id);
        System.out.println("email-----------$$$$$$$" + Et_login_email.getText().toString());
        System.out.println("password-----------$$$$$" + Et_login_password.getText().toString());


        System.out.println("loadin-----------");
        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.logging_in));

        ServiceRequest mservicerequest = new ServiceRequest(LoginActivity.this);

        mservicerequest.makeServiceRequest(ServiceConstant.LOGIN_URL, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {
                Log.e("loginresponse", response);

                System.out.println("response---------" + response);
                String Str_status = "", Str_response = "", Str_providerid = "", Str_socky_id = "", Str_provider_name = "", Str_provider_email = "", Str_provider_img = "", Str_key = "";
                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");

                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject object = jobject.getJSONObject("response");
                        Str_providerid = object.getString("provider_id");
                        //Str_socky_id = object.getString("soc_key");
                        Str_provider_name = object.getString("provider_name");
                        Str_provider_email = object.getString("email");
                        Str_provider_img = object.getString("provider_image");
                        // Str_key = object.getString("key");

                        String ScurrencyCode = object.getString("currency");

                        String availability = object.getString("availability");
                        String termsandconditions = "0", franchiseeid = "0";

                        if (object.has("termsandconditions")) {
                            termsandconditions = object.getString("termsandconditions");
                        }


                        if (object.has("franchisee")) {
                            franchiseeid = object.getString("franchisee");
                        }


                        String myCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(ScurrencyCode);
                        session.createWalletSession(ScurrencyCode);


                        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("provider_id", Str_providerid);
                        editor.putString("provider_image", Str_provider_img);
                        editor.putString("provider_name", Str_provider_name);
                        editor.putString("email", Str_provider_email);
                        editor.putString("currency", ScurrencyCode);
                        editor.putString("myCurrencySymbol", myCurrencySymbol);
                        editor.putString("termsandconditions", termsandconditions);
                        editor.putString("franchiseeid", franchiseeid);
                        editor.putString("GCMID", Gcm_Id);
                        editor.apply();


                        SharedPreferences pref1 = getApplicationContext().getSharedPreferences("available", MODE_PRIVATE);
                        SharedPreferences.Editor editor1 = pref1.edit();
                        editor1.putString("status", availability);
                        editor1.apply();

                        SharedPreferences pref1ss = getApplicationContext().getSharedPreferences("dd", MODE_PRIVATE);
                        SharedPreferences.Editor editor1ss = pref1ss.edit();
                        editor1ss.putString("ddd", "in");
                        editor1ss.apply();
                        editor1ss.commit();


                        System.out.println("provider-----------" + Str_providerid);
                        System.out.println("provider_name-----------" + Str_provider_name);
                        System.out.println("email-----------" + Str_provider_email);
                        System.out.println("providerimg-----------" + Str_provider_img);
                        System.out.println("key-----------" + Str_key);
                    } else {
                        Str_response = jobject.getString("response");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();

                if (Str_status.equalsIgnoreCase("1")) {

                    SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
                    String termspage = pref.getString("termsandconditions", "");
                    if (termspage.equals("1")) {
                        Intent intent = new Intent(LoginActivity.this, Navigationdrawer.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(LoginActivity.this, termsprivacy.class);
                        startActivity(intent);
                        finish();
                    }


                } else if (Str_status.equalsIgnoreCase("3")) {
                    dialog.dismiss();
                    login();
                } else {
                    final PkDialog mdialog = new PkDialog(LoginActivity.this);
                    mdialog.setDialogTitle(getResources().getString(R.string.server_lable_header));
                    mdialog.setDialogMessage(Str_response);
                    mdialog.setCancelOnTouchOutside(false);
                    mdialog.setPositiveButton(
                            getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mdialog.dismiss();
                                }
                            }
                    );
                    mdialog.show();
                }


            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });


    }

    //----------------------------------------------------------------------App Info Url------------------------------------------------------
    private void AppInfo() {


        ServiceRequest mservicerequest = new ServiceRequest(LoginActivity.this);

        mservicerequest.makeServiceRequest(ServiceConstant.App_Info, Request.Method.POST, null, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {
                Log.e("loginresponse", response);

                System.out.println("response---------" + response);
                String Str_status = "";

                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");

                    if (Str_status.equalsIgnoreCase("1")) {

                        Appmail = jobject.getString("email_address");

                        if (jobject.has("google_api"))
                            google_api = jobject.getString("google_api");

                        SharedPreferences pref = getApplicationContext().getSharedPreferences("AppInfo", MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("email", Appmail);
                        //For the security reason, I'm here encoding the code.
                        editor.putString("code", Base64.encodeToString(google_api.getBytes("UTF-8"), Base64.DEFAULT));
                        editor.apply();
                        editor.commit();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {

            }
        });


    }

    private void RequestMultiplePermission() {

        // Creating String Array with Permissions.
        ActivityCompat.requestPermissions(LoginActivity.this, new String[]
                {
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.CALL_PHONE
                }, RequestPermissionCode);

    }


    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == RequestPermissionCode) {
            if (grantResults.length > 0) {

                boolean CameraPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean RecordAudioPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                boolean loc1 = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                boolean loc2 = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                boolean call = grantResults[4] == PackageManager.PERMISSION_GRANTED;


                if (CameraPermission && RecordAudioPermission && loc1 && loc2 && call) {

                    // Toast.makeText(LoginActivity.this, "Permission Granted", Toast.LENGTH_LONG).show();
                } else {
                    // Toast.makeText(LoginActivity.this,"Permission Denied",Toast.LENGTH_LONG).show();

                }
            }
        }
    }

    // Checking permission is enabled or not using function starts from here.
    public boolean CheckingPermissionIsEnabledOrNot() {

        int FirstPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA);
        int SecondPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int SecondPermissionResult1 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int loc1 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION);
        int loc2 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        int call = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE);


        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED &&
                SecondPermissionResult == PackageManager.PERMISSION_GRANTED &&
                SecondPermissionResult1 == PackageManager.PERMISSION_GRANTED &&
                loc1 == PackageManager.PERMISSION_GRANTED &&
                loc2 == PackageManager.PERMISSION_GRANTED &&
                call == PackageManager.PERMISSION_GRANTED;
    }

}
