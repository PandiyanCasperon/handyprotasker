package noman.handyprotaskerapp.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Dialog.PkDialog;
import Dialog.onthePkDialog;
import Map.GPSTracker;
import Utils.ConnectionDetector;
import Utils.CurrencySymbolConverter;
import Utils.DynamicHeightListview;
import Utils.GMapV2GetRouteDirection;
import Utils.SessionManager;
import Utils.onItemRemoveClickListener;
import de.hdodenhof.circleimageview.CircleImageView;
import noman.handyprotaskerapp.Adapter.AcceptImageAdapter;
import noman.handyprotaskerapp.Adapter.AfterImageAdapter;
import noman.handyprotaskerapp.Adapter.BeforeImageAdapter;
import noman.handyprotaskerapp.Adapter.FranchiseListAdapter;
import noman.handyprotaskerapp.Adapter.MoreIconsAdapter;
import noman.handyprotaskerapp.Adapter.MultiplePaymentRequestAdapter;
import noman.handyprotaskerapp.Adapter.PaymentFareSummeryAdapter;
import noman.handyprotaskerapp.Adapter.ServiceTypeArrayListAdapter;
import noman.handyprotaskerapp.OtpPage;
import noman.handyprotaskerapp.Pojo.AfterImagePojo;
import noman.handyprotaskerapp.Pojo.BeforeImagePojo;
import noman.handyprotaskerapp.Pojo.FranchiseDescPojo;
import noman.handyprotaskerapp.Pojo.MoreIconsPojo;
import noman.handyprotaskerapp.Pojo.MyOrdersPojo;
import noman.handyprotaskerapp.Pojo.PaymentFareSummeryPojo;
import noman.handyprotaskerapp.Pojo.User_pre_amount_list;
import noman.handyprotaskerapp.Pojo.acceptimage;
import noman.handyprotaskerapp.Pojo.changeorderpojo;
import noman.handyprotaskerapp.Pojo.joblistpojo;
import noman.handyprotaskerapp.ProgressDialog1;
import noman.handyprotaskerapp.R;
import noman.handyprotaskerapp.Service.HNDHelper;
import noman.handyprotaskerapp.ShakingBell;
import noman.handyprotaskerapp.Track_your_ride;
import noman.handyprotaskerapp.WidgetSupport.listhei;
import service.ServiceConstant;
import socket.ChatMessageServicech;
import socket.SocketHandler;
import socket.SocketManager;
import volley.ServiceRequest;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


/**
 * Created by user88 on 12/12/2015.
 */


public class MyJobs_OnGoingDetailActivity extends AppCompatActivity implements com.google.android.gms.location.LocationListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, SeekBar.OnSeekBarChangeListener, onItemRemoveClickListener, OnMapReadyCallback {

    SharedPreferences pref;
    int multipleschedulessize = 0;
    private final static int REQUEST_LOCATION = 199;
    public Button detail_btn2, detail_btn1;
    public static String str_jobId = "", jobtype = "", sendaddress = "", senddatetime = "", sendphoto = "", sendusername = "";
    public static String Str_Userid = "";
    public static String provider_id = "", currency;
    public static Location myLocation;
    public static String mTaskID;
    public static Dialog moreAddressDialog;
    public ListView list;
    public static boolean item_add_bollean = false;
    public RelativeLayout aCancelLAY;
    public RelativeLayout aOKLAY;
    public CheckBox addmaterial;
    int firsttimealonebox = 0;
    public AppCompatActivity job_page_activity;
    private SocketManager smanager;
    final int PERMISSION_REQUEST_CODE = 111;
    final int PERMISSION_REQUEST_CODES = 222;
    final int PERMISSION_REQUEST_LOCATION = 333;
    Handler handler = new Handler();
    String saveoveralljobcount = "0";
    String task_id, user_id;
    String changeordervisible;
    String duration = "0";
    String provide_estimation = "";
    String before_status = "";
    String before_array = "0";
    Joblistadapter myOrderAdapterss;
    ListView joblist;
    LinearLayout hidemyprocrewnotes;
    String totalcost = "", procrew_notes = "";
    LinearLayout whatsincluded;
    String after_status = "";
    String after_array = "", partners_payment_status = "";
    String storewhatsincluded = "";
    int storewhatsincludedlenghth = 0;
    int billsize = 0;
    String storemybase64 = "";
    String sendlat = "";
    String jobinstruction, description, clientname;
    String sendlng = "";
    SupportMapFragment mapFragment;
    GPSTracker gps;
    int statushide = 0;
    String zipcodestore;
    String saveprovide_estimatio = "0";
    double myLatitude, myLongitude;
    LinearLayout hidemap;
    String saveresforward;
    String usersharelat;
    String address = "";
    String jsonformat = "";
    List<acceptimage> movieList = new ArrayList<>();
    List<BeforeImagePojo> beforemovieList = new ArrayList<>();
    List<AfterImagePojo> aftermovieList = new ArrayList<>();
    SessionManager session;
    ConnectionDetector cd;
    String storeresponseforpass = "";
    SharedPreferences saveim;
    GMapV2GetRouteDirection v2GetRouteDirection;
    //  public static MaterialAddAdapter aAdapter;
    CheckBox checkedhelp;
    ArrayList<String> listItems;
    LinearLayout openlist, craftmanlayout;
    String saveoverallestimatecount = "0";
    CircleImageView imag;
    TextView usernameee;
    LinearLayout back_ongoingback;
    LinearLayout changeorder, dateselect, LinearLayoutRequestPayment;
    ArrayList<changeorderpojo> getarrayList = new ArrayList<changeorderpojo>();
    ListView activity_my_orders_listView;
    ImageView imagechange, billpay, lotjob;
    TextView nooftaskers;
    listhei joblistmore;
    LinearLayout showphoto;
    ShakingBell shakingBell;
    String procrewnotes = "";
    SwipeRefreshLayout swipeRefreshLayout;
    LinearLayout helperlayout, LinearLayoutMoreIcon;
    TextView helpercount, viewprocrewnotes, TextViewHeaderTitle;
    LinearLayout writtenauth;
    MarkerOptions mm = new MarkerOptions();
    Marker drivermarker;
    JSONObject job = new JSONObject();
    ArrayList<PaymentFareSummeryPojo> BillingList;
    private TextView jobtypetext, Tv_ongoingDetail_jobdate, ongoing_details_dateTv,
            Tv_ongoing_jobtype, Tv_ongoing_detail_jobinstruction, details_orderid_textView;
    private ArrayList<MyOrdersPojo> myOrdersArrList;
    private ArrayList<joblistpojo> myjobList;
    private ImageView Img_Chat, Img_Call, Img_Message, Img_Email, estimate;
    private String Str_usermobile = "";
    private String destination_lat, EnableEstimateIcon = "";
    private String destination_long, cancel_reson = "";
    private ProgressDialog1 myDialog;
    private Handler mHandler;
    private double strlat, strlon;
    private PendingResult<LocationSettingsResult> result;
    private ArrayList<String> ServiceTypeArrayList;
    private Boolean isInternetPresent = false;
    private Marker currentMarker;
    private StringRequest postrequest;
    private String Str_job_group = "";
    private double MyCurrent_lat = 0.0, MyCurrent_long = 0.0;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private EditText Et_jobfare;
    private EditText Et_job_cost;
    private TextView Tv_OrderId;
    private String Str_Orderid = "";
    private ImageView track_location;
    private ImageView pdf_view_image,document;
    private SocketHandler socketHandler;
    private View moreAddressView;
    private String Str_user_email = "";
    private String share_text = "";
    private RelativeLayout show_moretext_layout;
    private TextView show_more;
    private String Job_Instruction = "";
    private String pending_multi_schedule = null, not_start_multi_schedule = null, completed_multi_schedule = null;
    private PopupWindow mypopupWindow;
    private ArrayList<MoreIconsPojo> moreIconsArrayList;
    private boolean IcInfoIconDisplay = false, IcPrepaymentIconDisplay = false, IcEstimateIconDisplay = false,
            IcMultiScheduleIconDisplay = false, IcChangeOrderIconDisplay = false, IcMaterialIconDisplay = false,
            IcLocationIconDisplay = false,IcLocationDocuments=true;
    private int IconEstimate = 0;
    private LinearLayout myViewLAY;
    private String myFranchiseDescriptionStr = "", myMultiStatusStr = "";
    private DynamicHeightListview myFranchiseLV;
    private ArrayList<FranchiseDescPojo> myFranchiseInfoList = new ArrayList<>();
    private String Pdf_view_status = "";
    private String myAnswerStr = "";
    private Runnable timedTask = new Runnable() {
        public void run() {


            shakingBell.shake(1);

            //  Toast.makeText(getApplicationContext(),""+myLatitude,Toast.LENGTH_LONG).show();

            if (myLatitude == 0.0) {
                gps = new GPSTracker(MyJobs_OnGoingDetailActivity.this);
                if (gps.isgpsenabled() && gps.canGetLocation()) {
                    myLatitude = gps.getLatitude();
                    myLongitude = gps.getLongitude();

                }
            }

            SharedPreferences showpoupup = getApplicationContext().getSharedPreferences("reload", 0);
            String show = showpoupup.getString("page", "");
            if (show.equalsIgnoreCase("1")) {
                SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("page", "0");
                editor.apply();


                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    myjobOngoingDetail(MyJobs_OnGoingDetailActivity.this, ServiceConstant.MYJOB_DETAIL_INFORMATION_URL);
                } else {
                    final PkDialog mDialog = new PkDialog(MyJobs_OnGoingDetailActivity.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.internet_label));
                    mDialog.setCancelOnTouchOutside(false);
                    mDialog.setDialogMessage(getResources().getString(R.string.action_no_internet_message));
                    mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                            finish();
                        }
                    });
                    mDialog.show();
                }
            }


            handler.postDelayed(timedTask, 4000);
        }
    };

    public static void AlertShow(String title, String message, Context context) {
        final PkDialog mDialog = new PkDialog(context);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(context.getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myjobs_ongoing_detail);


        saveim = getApplicationContext().getSharedPreferences("changordersign", 0);
        SharedPreferences.Editor prefeditord = saveim.edit();
        prefeditord.clear();
        prefeditord.apply();

        movieList.clear();
        beforemovieList.clear();
        aftermovieList.clear();

        myOrdersArrList = new ArrayList<MyOrdersPojo>();

        myjobList = new ArrayList<joblistpojo>();

        checkedhelp = findViewById(R.id.checkedhelp);
        job_page_activity = MyJobs_OnGoingDetailActivity.this;
        pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        provider_id = pref.getString("provider_id", "");
        currency = pref.getString("myCurrencySymbol", "");
        initialize();
        try {
            setLocationRequest();
            buildGoogleApiClient();
        } catch (Exception e) {
            e.printStackTrace();
        }
        initilizeMap();

        checkedhelp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (checkedhelp.isChecked()) {
                    System.out.println("Checked");


                    SharedPreferences pref1 = getApplicationContext().getSharedPreferences("jjjooo", 0);
                    SharedPreferences.Editor editor1 = pref1.edit();
                    editor1.putString("jobbidd", "" + str_jobId);
                    editor1.apply();
                    editor1.commit();


                    Intent intent = new Intent(getApplicationContext(), CaptureSignActivity.class);
                    startActivityForResult(intent, ServiceConstant.GetSignatureRequestCode);

                } else {

                    System.out.println("Un-Checked");
                }
            }
        });


        Img_Call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Str_usermobile != null) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        // Marshmallow+
                        if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                            requestPermission();
                        } else {
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:" + Str_usermobile));
                            startActivity(callIntent);
                        }
                    } else {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + Str_usermobile));
                        startActivity(callIntent);
                    }

                } else {
                    Alert(MyJobs_OnGoingDetailActivity.this.getResources().getString(R.string.server_lable_header), MyJobs_OnGoingDetailActivity.this.getResources().getString(R.string.arrived_alert_content1));
                }


            }
        });

        estimate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences pref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("jobid", str_jobId);
                editor.putString("category", jobtypetext.getText().toString());
                editor.putString("location", Tv_ongoing_jobtype.getText().toString());
                editor.putString("date", ongoing_details_dateTv.getText().toString());
                editor.putString("currentamt", "0");
                editor.apply();
                editor.commit();

                Intent intent = new Intent(getApplicationContext(), EstimateBuilderActivity.class);
                startActivity(intent);

                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        changeorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences pref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("jobid", str_jobId);
                editor.putString("category", jobtypetext.getText().toString());
                editor.putString("location", Tv_ongoing_jobtype.getText().toString());
                editor.putString("date", ongoing_details_dateTv.getText().toString());
                editor.putString("currentamt", "0");
                editor.apply();
                editor.commit();

                Intent intent = new Intent(getApplicationContext(), ChangeOrderActivity.class);
                startActivity(intent);

                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        });


        Img_Message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    share_text = getResources().getString(R.string.ongoing_detail_shar_text);
                    sms_sendMsg(share_text);

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Your call has failed...", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        });


        Img_Email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{Str_user_email});
                i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.ongoing_detail_subject));
                i.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.ongoing_detail_text) + " ");
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    // Toast.makeText(SettingsPage.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    swipeRefreshLayout.setRefreshing(true);
                    myjobOngoingDetail(MyJobs_OnGoingDetailActivity.this, ServiceConstant.MYJOB_DETAIL_INFORMATION_URL);

                } else {
                    final PkDialog mDialog = new PkDialog(MyJobs_OnGoingDetailActivity.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.internet_label));
                    mDialog.setCancelOnTouchOutside(false);
                    mDialog.setDialogMessage(getResources().getString(R.string.action_no_internet_message));
                    mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                            finish();
                        }
                    });
                    mDialog.show();
                }
            }
        });

        LinearLayoutRequestPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!myDialog.isShowing()) {
                    myDialog.show();
                }
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("provider_id", provider_id);
                jsonParams.put("job_id", str_jobId);

                ServiceRequest mservicerequest = new ServiceRequest(MyJobs_OnGoingDetailActivity.this);
                mservicerequest.makeServiceRequest(ServiceConstant.PreRequestDisplay, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
                    @Override
                    public void onCompleteListener(String response) {
                        myDialog.dismiss();
                        Log.e("provider/pre_amount", response);
                        //{"total_amount":"0","balance_amount":"0","pre_amount":"0"}
                        String total_amount = "", balance_amount = "", pre_amount = "";
                        User_pre_amount_list[] user_pre_amount_list_main = new User_pre_amount_list[0];
                        try {
                            JSONObject jobject = new JSONObject(response);
                            if (jobject.has("status") && jobject.getString("status").equals("1")) {
                                if (jobject.has("total_amount"))
                                    total_amount = currency + jobject.getString("total_amount");
                                if (jobject.has("balance_amount"))
                                    balance_amount = currency + jobject.getString("balance_amount");
                                if (jobject.has("pre_amount"))
                                    pre_amount = currency + jobject.getString("pre_amount");
                                if (jobject.has("user_pre_amount_list")) {
                                    JSONArray user_pre_amount_list = jobject.getJSONArray("user_pre_amount_list");
                                    if (user_pre_amount_list.length() > 0) {
                                        user_pre_amount_list_main = new User_pre_amount_list[user_pre_amount_list.length()];
                                        for (int i = 0; i < user_pre_amount_list.length(); i++) {
                                            User_pre_amount_list user_pre_amount_list1 = new User_pre_amount_list();
                                            user_pre_amount_list1.set_id(user_pre_amount_list.getJSONObject(i).getString("_id"));
                                            user_pre_amount_list1.setPre_amount(user_pre_amount_list.getJSONObject(i).getString("pre_amount"));
                                            user_pre_amount_list1.setPayment_status(user_pre_amount_list.getJSONObject(i).getString("payment_status"));
                                            user_pre_amount_list1.setTime(user_pre_amount_list.getJSONObject(i).getString("time"));
                                            user_pre_amount_list1.setDate(user_pre_amount_list.getJSONObject(i).getString("date"));
                                            user_pre_amount_list_main[i] = user_pre_amount_list1;
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        final AlertDialog.Builder builder = new AlertDialog.Builder(MyJobs_OnGoingDetailActivity.this);
                        // Get the layout inflater
                        LayoutInflater inflater = MyJobs_OnGoingDetailActivity.this.getLayoutInflater();
                        // Inflate the layout for the dialog
                        // Pass null as the parent view because its going in the dialog layout
                        View RequestPaymentView = inflater.inflate(R.layout.dialog_custom_payment_request, null);
                        TextView TextViewJobAmount = RequestPaymentView.findViewById(R.id.TextViewJobAmount);
                        TextViewJobAmount.setText(total_amount);
                        TextView TextViewRequestAmount = RequestPaymentView.findViewById(R.id.TextViewRequestAmount);
                        TextViewRequestAmount.setText(pre_amount);
                        TextView TextViewBalanceAmount = RequestPaymentView.findViewById(R.id.TextViewBalanceAmount);
                        TextViewBalanceAmount.setText(balance_amount);
                        final EditText EditTextRequestAmount = RequestPaymentView.findViewById(R.id.EditTextRequestAmount);
                        Button ButtonRequest = RequestPaymentView.findViewById(R.id.ButtonRequest);
                        Button ButtonRequestCashCheque = RequestPaymentView.findViewById(R.id.ButtonRequestCashCheque);

                        final String finalBalance_amount = balance_amount;
                        RecyclerView RecyclerViewMultiplePaymentRequest = RequestPaymentView.findViewById(R.id.RecyclerViewMultiplePaymentRequest);
                        LinearLayoutManager llm = new LinearLayoutManager(MyJobs_OnGoingDetailActivity.this);
                        llm.setOrientation(LinearLayoutManager.VERTICAL);

                        RecyclerViewMultiplePaymentRequest.setLayoutManager(llm);
                        RecyclerViewMultiplePaymentRequest.setHasFixedSize(true);

                        // Get your views by using view.findViewById() here and do your listeners.
                        /*TextView TextViewJobAmount = RequestPaymentView.findViewById(R.id.TextViewJobAmount);
                        EditText EditTextRequestAmount = RequestPaymentView.findViewsWithText(R.id.EditTextRequestAmount);
                        Button ButtonRequest = RequestPaymentView.findViewById(R.id.ButtonRequest);*/
                        // Set the dialog layout
                        builder.setView(RequestPaymentView);
                        builder.create();
                        final AlertDialog alertDialog = builder.show();

                        if (user_pre_amount_list_main.length > 0) {
                            MultiplePaymentRequestAdapter multiplePaymentRequestAdapter = new MultiplePaymentRequestAdapter(MyJobs_OnGoingDetailActivity.this, user_pre_amount_list_main, currency, new MultiplePaymentRequestAdapter.MultiplePaymentRequestAdapterInterface() {
                                @Override
                                public void Refresh(final String PrePaymentID) {
                                    new AlertDialog.Builder(MyJobs_OnGoingDetailActivity.this)
                                            .setTitle("Delete")
                                            .setMessage("Are you sure want to delete this request?")

                                            // Specifying a listener allows you to take an action before dismissing the dialog.
                                            // The dialog is automatically dismissed when a dialog button is clicked.
                                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // Continue with delete operation
                                                    if (!myDialog.isShowing()) {
                                                        myDialog.show();
                                                    }
                                                    HashMap<String, String> params = new HashMap<>();
                                                    params.put("job_id", str_jobId);
                                                    params.put("prepayment_id[" + 0 + "]", PrePaymentID);
                                                    ServiceRequest mservicerequest = new ServiceRequest(MyJobs_OnGoingDetailActivity.this);
                                                    mservicerequest.makeServiceRequest(ServiceConstant.PreRequestDelete, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                                                        @Override
                                                        public void onCompleteListener(String response) {
                                                            myDialog.dismiss();
                                                            Log.e("delete-payment", response);
                                                            try {
                                                                JSONObject jobject = new JSONObject(response);
                                                                if (jobject.has("status") && jobject.getString("status").equals("1")) {
                                                                    alertDialog.dismiss();
                                                                    LinearLayoutRequestPayment.performClick();
                                                                    Toast.makeText(MyJobs_OnGoingDetailActivity.this, jobject.getString("message"), Toast.LENGTH_SHORT).show();
                                                                } else {
                                                                    Toast.makeText(MyJobs_OnGoingDetailActivity.this, "Something went wrong! Try again", Toast.LENGTH_SHORT).show();
                                                                }
                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }

                                                        @Override
                                                        public void onErrorListener() {
                                                            myDialog.dismiss();
                                                        }
                                                    });
                                                }
                                            })

                                            // A null listener allows the button to dismiss the dialog and take no further action.
                                            .setNegativeButton(android.R.string.no, null)
                                            .setIcon(R.drawable.craftmanappmainlogo)
                                            .show();
                                }
                            });
                            RecyclerViewMultiplePaymentRequest.setAdapter(multiplePaymentRequestAdapter);
                            RecyclerViewMultiplePaymentRequest.setVisibility(VISIBLE);
                        } else {
                            RecyclerViewMultiplePaymentRequest.setVisibility(GONE);
                        }


                        ButtonRequest.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ValidEditTextAmount(alertDialog, EditTextRequestAmount.getText().toString(), finalBalance_amount, "ButtonRequest");
                            }
                        });

                        ButtonRequestCashCheque.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ValidEditTextAmount(alertDialog, EditTextRequestAmount.getText().toString(), finalBalance_amount, "ButtonRequestCashCheque");
                            }
                        });

                    }

                    @Override
                    public void onErrorListener() {
                        myDialog.dismiss();
                    }
                });
            }
        });

    }

    private void sms_sendMsg(String share_text) {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (!checkSmsPermission()) {
                requestPermissionSMS();
            } else {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.putExtra("address", Str_usermobile);
                intent.putExtra("sms_body", share_text);
                intent.setData(Uri.parse("smsto:" + Str_usermobile));
                startActivity(intent);
            }
        } else {
            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
            sendIntent.putExtra("sms_body", share_text);
            sendIntent.putExtra("address", Str_usermobile);
            sendIntent.setType("vnd.android-dir/mms-sms");
            startActivity(sendIntent);
        }
    }

    private boolean checkSmsPermission() {
        int result = ContextCompat.checkSelfPermission(MyJobs_OnGoingDetailActivity.this, Manifest.permission.SEND_SMS);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissionSMS() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, PERMISSION_REQUEST_CODES);
    }

    private void initialize() {

        TextViewHeaderTitle = findViewById(R.id.TextViewHeaderTitle);
        TextViewHeaderTitle.setText(getResources().getString(R.string.TextViewHeaderTitleString));
        details_orderid_textView = findViewById(R.id.details_orderid_textView);
        writtenauth = findViewById(R.id.writtenauth);

        hidemyprocrewnotes = findViewById(R.id.hidemyprocrewnotes);
        helpercount = findViewById(R.id.helpercount);
        helperlayout = findViewById(R.id.helpercountlayout);
        viewprocrewnotes = findViewById(R.id.viewprocrewnotes);

        myViewLAY = findViewById(R.id.myjobs_ongoing_detail_LAY_view_notes);
        myFranchiseLV = findViewById(R.id.myjobs_ongoing_detail_LV_view_notes);


        viewprocrewnotes.setPaintFlags(viewprocrewnotes.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        hidemyprocrewnotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final PkDialog mDialog = new PkDialog(MyJobs_OnGoingDetailActivity.this);
                mDialog.setDialogTitle(getResources().getString(R.string.PROCREW_NOTES));
                mDialog.setDialogMessage(procrew_notes);
                mDialog.setPositiveButton(
                        getResources().getString(R.string.action_ok), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                            }
                        }
                );
                mDialog.show();
            }
        });

        viewprocrewnotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final PkDialog mDialog = new PkDialog(MyJobs_OnGoingDetailActivity.this);
                mDialog.setDialogTitle(getResources().getString(R.string.PROCREW_NOTES));
                mDialog.setDialogMessage(procrew_notes);
                mDialog.setPositiveButton(
                        getResources().getString(R.string.action_ok), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                            }
                        }
                );
                mDialog.show();
            }
        });


        lotjob = findViewById(R.id.lotjob);

        lotjob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(MyJobs_OnGoingDetailActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.fullscreendialog);


                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                dialog.getWindow().setAttributes(lp);
                dialog.show();

                joblistmore = dialog.findViewById(R.id.jobrecycler_view);
                myOrderAdapterss = new Joblistadapter(MyJobs_OnGoingDetailActivity.this, myjobList, dialog);
                joblistmore.setEnabled(false);
                joblistmore.setAdapter(myOrderAdapterss);


            }
        });

        myViewLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final PkDialog mDialog = new PkDialog(MyJobs_OnGoingDetailActivity.this);
                mDialog.setDialogTitle(getResources().getString(R.string.description));
                mDialog.setDialogMessage(myFranchiseDescriptionStr);
                mDialog.setPositiveButton(
                        getResources().getString(R.string.action_ok), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                            }
                        }
                );
                mDialog.show();
            }
        });


        shakingBell = findViewById(R.id.shakeBell);
        shakingBell.shake(1);
        swipeRefreshLayout = findViewById(R.id.myjob_ongoing_swipe_refresh_layout);
        billpay = findViewById(R.id.billpay);
        imagechange = findViewById(R.id.imagechange);
        openlist = findViewById(R.id.openlist);
        craftmanlayout = findViewById(R.id.craftmanlayout);
        estimate = findViewById(R.id.estimate);
        activity_my_orders_listView = findViewById(R.id.activity_my_orders_listView);
        hidemap = findViewById(R.id.hidemap);
        nooftaskers = findViewById(R.id.nooftaskers);
        dateselect = findViewById(R.id.dateselect);
        showphoto = findViewById(R.id.showphoto);
        whatsincluded = findViewById(R.id.whatsincluded);


        billpay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyJobs_OnGoingDetailActivity.this, BillAttachmentActivity.class);
                SharedPreferences pref1 = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
                SharedPreferences.Editor editor1 = pref1.edit();
                editor1.putString("jobid", str_jobId);
                editor1.putString("photostatus", "1");
                editor1.putString("zipcode", "" + zipcodestore);
                editor1.putString("storeresponse", "" + storeresponseforpass);
                editor1.putString("afterr", "0");

                editor1.apply();
                editor1.commit();
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });


        showphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RecyclerView RecyclerViewServiceType, recyclerView, beforerecyclerView, afterrecycler_view;
                TextView tellabout, whatsimportant, whats, custom_text_view, Whatsincluded,
                        jobphotos, ajobphotos, bjobphotos, TextViewJobID, TextViewClientName,
                        TextViewClientNameLabel, TextViewJobServiceType, BillingListViewTitle;
                LinearLayout userphoto, beforephoto, afterphoto, whatsincluded, LinearLayoutBillingListView;
                listhei BillingListView;


                View modalbottomsheet = getLayoutInflater().inflate(R.layout.bottomdialog_layout, null);
                BottomSheetDialog dialog = new BottomSheetDialog(MyJobs_OnGoingDetailActivity.this);
                dialog.setContentView(modalbottomsheet);
                dialog.setCanceledOnTouchOutside(true);
                dialog.setCancelable(true);


                custom_text_view = modalbottomsheet.findViewById(R.id.custom_text_view);
                custom_text_view.setPaintFlags(custom_text_view.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

                Whatsincluded = modalbottomsheet.findViewById(R.id.Whatsincluded);
                Whatsincluded.setPaintFlags(Whatsincluded.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


                jobphotos = modalbottomsheet.findViewById(R.id.jobphotos);
                jobphotos.setPaintFlags(jobphotos.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


                bjobphotos = modalbottomsheet.findViewById(R.id.bjobphotos);
                bjobphotos.setPaintFlags(bjobphotos.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

                ajobphotos = modalbottomsheet.findViewById(R.id.ajobphotos);
                ajobphotos.setPaintFlags(bjobphotos.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

                BillingListViewTitle = modalbottomsheet.findViewById(R.id.BillingListViewTitle);
                BillingListViewTitle.setPaintFlags(bjobphotos.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

                TextViewJobID = modalbottomsheet.findViewById(R.id.TextViewJobID);
                TextViewJobID.setText("");
                TextViewJobID.setText(details_orderid_textView.getText().toString().toUpperCase() + " " + Tv_OrderId.getText().toString());

                TextViewClientNameLabel = modalbottomsheet.findViewById(R.id.TextViewClientNameLabel);
                TextViewClientNameLabel.setPaintFlags(bjobphotos.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

                TextViewClientName = modalbottomsheet.findViewById(R.id.TextViewClientName);
                TextViewClientName.setText("");
                TextViewClientName.setText(usernameee.getText().toString().toUpperCase());

                TextViewJobServiceType = modalbottomsheet.findViewById(R.id.TextViewJobServiceType);
                TextViewJobServiceType.setPaintFlags(bjobphotos.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


                tellabout = modalbottomsheet.findViewById(R.id.tellabout);
                whatsimportant = modalbottomsheet.findViewById(R.id.whatsimportant);

                SpannableStringBuilder builders = new SpannableStringBuilder();
                String string1 = getResources().getString(R.string.job_des) + " : ";
                SpannableString redSpannable = new SpannableString(string1);
                redSpannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.red)), 0, string1.length(), 0);
                builders.append(redSpannable);

                String string2 = "\n\u2022 " + description + myAnswerStr;
                SpannableString whiteSpannable = new SpannableString(string2);
                whiteSpannable.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, string2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                //  whiteSpannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.black)), 0, string2.length(), 0);
                builders.append(whiteSpannable);
                tellabout.setText(builders, TextView.BufferType.SPANNABLE);


                SpannableStringBuilder builders1 = new SpannableStringBuilder();
                String string3 = getResources().getString(R.string.bottomdialog_whats_important) + " : ";
                SpannableString redSpannable2 = new SpannableString(string3);
                redSpannable2.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.red)), 0, string3.length(), 0);
                builders1.append(redSpannable2);

                String string4 = jobinstruction;
                SpannableString whiteSpannable1 = new SpannableString(string4);
//                whiteSpannable.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, string2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                whiteSpannable1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.black)), 0, string4.length(), 0);
                builders1.append(whiteSpannable1);
                whatsimportant.setText(builders1, TextView.BufferType.SPANNABLE);

                userphoto = modalbottomsheet.findViewById(R.id.userphoto);
                beforephoto = modalbottomsheet.findViewById(R.id.beforephoto);
                afterphoto = modalbottomsheet.findViewById(R.id.afterphoto);
                whatsincluded = modalbottomsheet.findViewById(R.id.whatsincluded);
                LinearLayoutBillingListView = modalbottomsheet.findViewById(R.id.LinearLayoutBillingListView);
                whats = modalbottomsheet.findViewById(R.id.whats);
                BillingListView = modalbottomsheet.findViewById(R.id.BillingListView);

                whats.setText(storewhatsincluded);
                if (storewhatsincludedlenghth > 0) {
                    whatsincluded.setVisibility(View.VISIBLE);
                } else {
                    whatsincluded.setVisibility(GONE);
                }

                recyclerView = modalbottomsheet.findViewById(R.id.recycler_view);
                RecyclerViewServiceType = modalbottomsheet.findViewById(R.id.RecyclerViewServiceType);
                LinearLayoutManager llm = new LinearLayoutManager(MyJobs_OnGoingDetailActivity.this);
                llm.setOrientation(LinearLayoutManager.VERTICAL);

                RecyclerViewServiceType.setLayoutManager(llm);
                RecyclerViewServiceType.setHasFixedSize(true);
                beforerecyclerView = modalbottomsheet.findViewById(R.id.beforerecycler_view);
                afterrecycler_view = modalbottomsheet.findViewById(R.id.afterrecycler_view);

                if (movieList.size() > 0) {
                    userphoto.setVisibility(View.VISIBLE);
                    LinearLayoutManager layoutManager
                            = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                    AcceptImageAdapter mAdapter = new AcceptImageAdapter(movieList, getApplicationContext());
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(mAdapter);
                } else {
                    userphoto.setVisibility(GONE);
                    recyclerView.setVisibility(GONE);
                }


                if (beforemovieList.size() > 0) {
                    beforephoto.setVisibility(View.VISIBLE);
                    LinearLayoutManager layoutManager
                            = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                    BeforeImageAdapter mAdapter = new BeforeImageAdapter(beforemovieList, getApplicationContext());
                    beforerecyclerView.setLayoutManager(layoutManager);
                    beforerecyclerView.setAdapter(mAdapter);
                } else {
                    beforephoto.setVisibility(GONE);
                    beforerecyclerView.setVisibility(GONE);
                }


                if (aftermovieList.size() > 0) {
                    afterphoto.setVisibility(View.VISIBLE);
                    LinearLayoutManager layoutManager
                            = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                    AfterImageAdapter mAdapter = new AfterImageAdapter(aftermovieList, getApplicationContext());
                    afterrecycler_view.setLayoutManager(layoutManager);
                    afterrecycler_view.setAdapter(mAdapter);
                } else {
                    afterphoto.setVisibility(GONE);
                    afterrecycler_view.setVisibility(GONE);
                }


                if (ServiceTypeArrayList != null && ServiceTypeArrayList.size() > 0) {
                    RecyclerViewServiceType.setAdapter(new ServiceTypeArrayListAdapter(MyJobs_OnGoingDetailActivity.this, ServiceTypeArrayList));
                }

                if (BillingList != null && BillingList.size() > 0) {
                    LinearLayoutBillingListView.setVisibility(VISIBLE);
                    BillingListView.setAdapter(new PaymentFareSummeryAdapter(MyJobs_OnGoingDetailActivity.this, BillingList));
                } else {
                    LinearLayoutBillingListView.setVisibility(GONE);
                }
                dialog.show();
            }
        });

        dateselect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*SharedPreferences pref = getApplicationContext().getSharedPreferences("AppInfo", 0);
                Appinfoemail = pref.getString("email", "");*/

                final PkDialog mDialog = new PkDialog(MyJobs_OnGoingDetailActivity.this);
                mDialog.setDialogTitle(getResources().getString(R.string.message_label));
                mDialog.setCancelOnTouchOutside(false);
                mDialog.setDialogMessage("To reschedule your appointment, please email us");
                mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                        Intent emailIntent = new Intent(Intent.ACTION_SEND);
                        //  emailIntent.putExtra(Intent.EXTRA_EMAIL, email);
                        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"hpsupport@handypro.com"});
                        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Handy Pro");
                        emailIntent.setType("text/plain");
                        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
                        final PackageManager pm = getPackageManager();
                        final List<ResolveInfo> matches = pm.queryIntentActivities(emailIntent, 0);
                        ResolveInfo best = null;
                        for (final ResolveInfo info : matches)
                            if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail"))
                                best = info;
                        if (best != null)
                            emailIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
                        startActivity(emailIntent);
                    }
                });
                mDialog.setNegativeButton("CANCEL", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mDialog.dismiss();
                    }
                });
                mDialog.show();
            }
        });

        if (statushide == 0) {
            imagechange.setBackgroundResource(R.drawable.rightopen);
        } else {
            imagechange.setBackgroundResource(R.drawable.expandarrow);
        }

        craftmanlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (statushide == 0) {
                    imagechange.setBackgroundResource(R.drawable.rightopen);
                    statushide = 1;
                    hidemap.setVisibility(GONE);
                    activity_my_orders_listView.setVisibility(View.VISIBLE);
                } else {
                    imagechange.setBackgroundResource(R.drawable.expandarrow);
                    statushide = 0;
                    hidemap.setVisibility(View.VISIBLE);
                    activity_my_orders_listView.setVisibility(GONE);
                }


            }
        });


        changeorder = findViewById(R.id.layout_back_ddongoingback);
        LinearLayoutRequestPayment = findViewById(R.id.LinearLayoutRequestPayment);
        LinearLayoutMoreIcon = findViewById(R.id.LinearLayoutMoreIcon);

        session = new SessionManager(MyJobs_OnGoingDetailActivity.this);
        cd = new ConnectionDetector(MyJobs_OnGoingDetailActivity.this);
        gps = new GPSTracker(MyJobs_OnGoingDetailActivity.this);
        mHandler = new Handler();
        socketHandler = SocketHandler.getInstance(this);

        // get user data from session


        System.out.println("sessionproviderId=-------------------" + provider_id);


        SharedPreferences pref = getApplicationContext().getSharedPreferences("ongoingpage", MODE_PRIVATE);
        str_jobId = pref.getString("jobid", "");


        jobtypetext = findViewById(R.id.jobtype);
        System.out.println("dispaly jobid-------------------" + str_jobId);


        Tv_ongoingDetail_jobdate = findViewById(R.id.ongoing_details_date);
        imag = findViewById(R.id.imag);

        back_ongoingback = findViewById(R.id.layout_back_ongoingback);
        back_ongoingback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences pref1 = getApplicationContext().getSharedPreferences("needrefresh", 0);
                String whichclass = pref1.getString("yesno", " ");
                if ("0".equals(whichclass) || "2".equals(whichclass)) {/* finish();*/
                    //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    Intent intent = new Intent(getApplicationContext(), MyJobsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("status", "0");
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                } else if ("1".equals(whichclass)) {
                    Intent intent = new Intent(getApplicationContext(), WeeklyCalendarActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("EXIT", true);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                } else if ("3".equals(whichclass)) {
                    Intent intent = new Intent(getApplicationContext(), MyJobsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("status", "0");
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                } else {
                    Intent intent = new Intent(getApplicationContext(), MyJobsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("status", "0");
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }


            }
        });


        usernameee = findViewById(R.id.username);
        usernameee.setSelected(true);

        Tv_ongoing_jobtype = findViewById(R.id.ongoing_detail_jobtype);


        ongoing_details_dateTv = findViewById(R.id.ongoing_details_dateTv);


        track_location = findViewById(R.id.track_location);
        pdf_view_image = findViewById(R.id.pdf_view_image);
        document = findViewById(R.id.document);
        Tv_ongoing_detail_jobinstruction = findViewById(R.id.ongoing_details_instruction);

        pdf_view_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(MyJobs_OnGoingDetailActivity.this,
                        EstimationPreviewWebPageActivity.class).putExtra("Job_ID", str_jobId));
            }
        });

        document.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MyJobs_OnGoingDetailActivity.this,GetDouments.class);
                intent.putExtra("Job_ID",str_jobId);
                startActivity(intent);
            }
        });

        track_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String status = detail_btn2.getText().toString();

                if (status.equalsIgnoreCase(getResources().getString(R.string.ongoing_detail_accept_label)) || status.equalsIgnoreCase(getResources().getString(R.string.ongoing_detail_startoff_label))
                        || status.equalsIgnoreCase(getResources().getString(R.string.ongoing_detail_arrived_label))) {
                    if (cd.isConnectingToInternet()) {
                        CheckPermissions();
                    } else {
                        Alert(getResources().getString(R.string.sorry), getResources().getString(R.string.alert_nointernet));
                    }


                } else {

                    Alert(getResources().getString(R.string.sorry), getResources().getString(R.string.live_tracking_alert));
                }


            }
        });


        Tv_OrderId = findViewById(R.id.details_orderid);
        Tv_OrderId.setText(str_jobId);
        Img_Chat = findViewById(R.id.detail_chat_img);
        Img_Call = findViewById(R.id.detail_phone_img);
        Img_Message = findViewById(R.id.detail_message_img);
        Img_Email = findViewById(R.id.detail_email_img);

        detail_btn1 = findViewById(R.id.job_detail_btn1);


        Img_Chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ChatMessageServicech.user_id = "";
                ChatMessageServicech.task_id = "";
                Intent intent = new Intent(MyJobs_OnGoingDetailActivity.this, ChatPageActivity.class);
                intent.putExtra("chatpage", true);
                intent.putExtra("JOBID", str_jobId);
                intent.putExtra("TaskerId", user_id);
                intent.putExtra("TaskId", task_id);
                startActivity(intent);
            }
        });

        LinearLayoutMoreIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View ButtonView) {
                /*PopupMenu popup = new PopupMenu(MyJobs_OnGoingDetailActivity.this, v);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.my_job_on_going_more_icons, popup.getMenu());
                popup.show();*/

                LayoutInflater inflater = (LayoutInflater)
                        getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View MoreIconView = inflater.inflate(R.layout.my_job_on_going_more_icons, null);

                RecyclerView RecyclerViewMoreIcons = MoreIconView.findViewById(R.id.RecyclerViewMoreIcons);
                LinearLayoutManager llm = new LinearLayoutManager(MyJobs_OnGoingDetailActivity.this);
                llm.setOrientation(LinearLayoutManager.VERTICAL);

                RecyclerViewMoreIcons.setLayoutManager(llm);
                RecyclerViewMoreIcons.setHasFixedSize(true);
                ArrayList<MoreIconsPojo> noRepeat = new ArrayList<>();
                moreIconsArrayList = new ArrayList<>();

                if (myMultiStatusStr.equalsIgnoreCase("1"))
                {


                   /* if (IcEstimateIconDisplay) {
                        MoreIconsPojo EstimateMoreIconsPojo = new MoreIconsPojo();
                        if (IconEstimate == 0) {
                            EstimateMoreIconsPojo = new MoreIconsPojo("Show", "Estimation", R.drawable.ic_magnify);
                        } else {
                            if (IconEstimate == R.drawable.ic_edit) {
                                EstimateMoreIconsPojo = new MoreIconsPojo("Show", "Edit Estimation", IconEstimate);
                            } else if (IconEstimate == R.drawable.ic_magnify) {
                                EstimateMoreIconsPojo = new MoreIconsPojo("Show", "View Estimation", IconEstimate);
                            }
                        }
                        moreIconsArrayList.add(EstimateMoreIconsPojo);
                    }*/

                   /* if (IcChangeOrderIconDisplay) {
                        MoreIconsPojo moreIconsPojo = new MoreIconsPojo("Show", "Change Order", R.drawable.ic_change_order);
                        moreIconsArrayList.add(moreIconsPojo);
                    }
                    if (IcMaterialIconDisplay) {
                        MoreIconsPojo BillIconsPojo1 = new MoreIconsPojo("Show", "Receipt", R.drawable.ic_receipt);
                        moreIconsArrayList.add(BillIconsPojo1);
                    }*/
                    if (IcLocationIconDisplay) {
                        MoreIconsPojo moreIconsPojo1 = new MoreIconsPojo("Show", "Route", R.drawable.ic_route);
                        moreIconsArrayList.add(moreIconsPojo1);
                    }

                    if (IcLocationDocuments) {
                        MoreIconsPojo documentpojo = new MoreIconsPojo("Show", "Documents", R.drawable.document);
                        moreIconsArrayList.add(documentpojo);
                    }
                }
                else
                {
                    if (IcInfoIconDisplay) {
                    MoreIconsPojo moreIconsPojoInfo = new MoreIconsPojo("Show", "Job Info", R.drawable.ic_info);
                    moreIconsArrayList.add(moreIconsPojoInfo);
                        if (IcPrepaymentIconDisplay) {
                            MoreIconsPojo moreIconsPojo = new MoreIconsPojo("Show", "Prepayment", R.drawable.ic_request_payment);
                            moreIconsArrayList.add(moreIconsPojo);
                        }
                        if (IcEstimateIconDisplay) {
                            MoreIconsPojo EstimateMoreIconsPojo = new MoreIconsPojo();
                            if (IconEstimate == 0) {
                                EstimateMoreIconsPojo = new MoreIconsPojo("Show", "Estimation", R.drawable.ic_magnify);
                            } else {
                                if (IconEstimate == R.drawable.ic_edit) {
                                    EstimateMoreIconsPojo = new MoreIconsPojo("Show", "Edit Estimation", IconEstimate);
                                } else if (IconEstimate == R.drawable.ic_magnify) {
                                    EstimateMoreIconsPojo = new MoreIconsPojo("Show", "View Estimation", IconEstimate);
                                }
                            }
                            moreIconsArrayList.add(EstimateMoreIconsPojo);
                        }
                        if (IcMultiScheduleIconDisplay) {
                            MoreIconsPojo moreIconsPojo = new MoreIconsPojo("Show", "Multi Schedule", R.drawable.ic_multi_schedule);
                            moreIconsArrayList.add(moreIconsPojo);
                        }
                        if (IcChangeOrderIconDisplay) {
                            MoreIconsPojo moreIconsPojo = new MoreIconsPojo("Show", "Change Order", R.drawable.ic_change_order);
                            moreIconsArrayList.add(moreIconsPojo);
                        }
                        if (IcMaterialIconDisplay) {
                            MoreIconsPojo BillIconsPojo1 = new MoreIconsPojo("Show", "Receipt", R.drawable.ic_receipt);
                            moreIconsArrayList.add(BillIconsPojo1);
                        }
                        if (IcLocationIconDisplay) {
                            MoreIconsPojo moreIconsPojo1 = new MoreIconsPojo("Show", "Route", R.drawable.ic_route);
                            moreIconsArrayList.add(moreIconsPojo1);
                        }
                        if (Pdf_view_status.equals("1")) {

                            MoreIconsPojo moreIconsPojo1 = new MoreIconsPojo("Show", "Estimation PDF", R.drawable.ic_estimation_preview);
                            moreIconsArrayList.add(moreIconsPojo1);
                        }
                        if (IcLocationDocuments) {
                            MoreIconsPojo documentpojo = new MoreIconsPojo("Show", "Documents", R.drawable.document);
                            moreIconsArrayList.add(documentpojo);
                        }
                }


                }





                RecyclerViewMoreIcons.setAdapter(new MoreIconsAdapter(MyJobs_OnGoingDetailActivity.this, moreIconsArrayList, new MoreIconsAdapter.ClickListen() {
                    @Override
                    public void OnClickMainHolder(int SelectedIcon) {
                        mypopupWindow.dismiss();
                        switch (SelectedIcon) {
                            case R.drawable.ic_info:
                                showphoto.performClick();
                                break;
                            case R.drawable.ic_request_payment:
                                LinearLayoutRequestPayment.performClick();
                                break;
                            case R.drawable.ic_edit:
                            case R.drawable.ic_magnify:
                                estimate.performClick();
                                break;
                            case R.drawable.ic_multi_schedule:
                                lotjob.performClick();
                                break;
                            case R.drawable.ic_change_order:
                                changeorder.performClick();
                                break;
                            case R.drawable.ic_receipt:
                                billpay.performClick();
                                break;
                            case R.drawable.ic_route:
                                track_location.performClick();
                                break;
                            case R.drawable.ic_estimation_preview:
                                pdf_view_image.performClick();
                                break;
                            case R.drawable.document:
                                document.performClick();
                                break;
                        }
                    }
                }));

                mypopupWindow = new PopupWindow(MoreIconView, RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT, true);
                mypopupWindow.showAsDropDown(ButtonView, -153, 0);
            }
        });


        detail_btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (detail_btn1.getText().toString().equalsIgnoreCase("IGNORE")) {
                    HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.acceptable_job_cant_be_cancelled));
                } else if (detail_btn1.getText().toString().equalsIgnoreCase("REJECT")) {
                    HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.acceptable_job_cant_be_cancelled));
                } else if (detail_btn1.getText().toString().equalsIgnoreCase("Cancel")) {
                    detail_btn2.setVisibility(View.VISIBLE);
                    HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.acceptable_job_cant_be_cancelled));
                } else if (detail_btn1.getText().toString().equalsIgnoreCase("Cancel Reason")) {
                    detail_btn2.setVisibility(GONE);
                    HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.acceptable_job_cant_be_cancelled));
                } else if (detail_btn1.getText().toString().equalsIgnoreCase("More Info")) {
                    {
                        Intent intent = new Intent(MyJobs_OnGoingDetailActivity.this, MoreInfoCompletePageActivity.class);
                        SharedPreferences pref1 = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
                        SharedPreferences.Editor editor1 = pref1.edit();
                        editor1.putString("jobid", str_jobId);
                        editor1.putString("photostatus", "1");
                        editor1.putString("completed", "1");
                        editor1.putString("jobinstruction", jobinstruction);
                        editor1.putString("description", description);
                        editor1.putString("storewhatsincluded", storewhatsincluded);
                        editor1.putString("saveresforward", saveresforward);
                        editor1.putString("clientname", clientname);
                        editor1.apply();
                        editor1.commit();
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    }
                }
            }
        });

        detail_btn2 = findViewById(R.id.job_detail_btn2);
        detail_btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                cd = new ConnectionDetector(MyJobs_OnGoingDetailActivity.this);
                isInternetPresent = cd.isConnectingToInternet();

                if (detail_btn2.getText().toString().equalsIgnoreCase("ACCEPT")) {
                    if (isInternetPresent) {
                        buttonsClickActions(MyJobs_OnGoingDetailActivity.this, ServiceConstant.ACCEPT_JOB_URL, "accept");
                        System.out.println("--------------accept-------------------" + ServiceConstant.ACCEPT_JOB_URL);
                    } else {
                        Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                    }
                } else if (detail_btn2.getText().toString().equalsIgnoreCase("On My Way")) {

                   if(firsttimealonebox == 0)
                   {
                       firsttimealonebox ++;
                       final onthePkDialog mdialog = new onthePkDialog(MyJobs_OnGoingDetailActivity.this);
                       mdialog.setDialogTitle("The client will receive a text that you are on your way.");
                       mdialog.setDialogMessage("Are you sure you want to continue?");
                       mdialog.setCancelOnTouchOutside(false);
                       mdialog.setPositiveButton(
                               "NO", new View.OnClickListener() {
                                   @Override
                                   public void onClick(View v) {
                                       mdialog.dismiss();
                                   }
                               }
                       );
                       mdialog.setNegativeButton(
                               "YES", new View.OnClickListener() {
                                   @Override
                                   public void onClick(View v) {
                                       mdialog.dismiss();
                                   }
                               }
                       );
                       mdialog.show();
                   }
else
                   {
                       if (isInternetPresent) {

                        gps = new GPSTracker(MyJobs_OnGoingDetailActivity.this);
                        if (gps.isgpsenabled() && gps.canGetLocation()) {
                            myLatitude = gps.getLatitude();
                            myLongitude = gps.getLongitude();

                            if (myLatitude == 0.0) {
                                //   HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.fetching_location_wait_TXT));
                                usersharelat = destination_lat + "," + destination_long;

                                String concatdes = myLatitude + "," + myLongitude;

                                String orgin = "origin=" + usersharelat;
                                String destination = "destination=" + concatdes;


                                System.out.println("orgin--->" + orgin + "  " + destination);

                                postRequestMyOrderList(MyJobs_OnGoingDetailActivity.this, "https://maps.googleapis.com/maps/api/directions/json?" + orgin + "&" + destination + "&sensor=false");
                            } else {
                                usersharelat = destination_lat + "," + destination_long;

                                String concatdes = myLatitude + "," + myLongitude;

                                String orgin = "origin=" + usersharelat;
                                String destination = "destination=" + concatdes;

                                myDialog = new ProgressDialog1(MyJobs_OnGoingDetailActivity.this);
                                if (!myDialog.isShowing()) {
                                    myDialog.show();
                                }
                                System.out.println("orgin--->" + orgin + "  " + destination);
                                duration = "few min";
                                buttonsClickActions(MyJobs_OnGoingDetailActivity.this, ServiceConstant.STARTOFFJOB_JOB_URL, "startoff");

                                //  postRequestMyOrderList(MyJobs_OnGoingDetailActivity.this, "https://maps.googleapis.com/maps/api/directions/json?" + orgin + "&" + destination + "&sensor=false");

                            }


                        } else {
                            CheckPermissions();
                        }


                        System.out.println("--------------startoff url-------------------" + ServiceConstant.STARTOFFJOB_JOB_URL);


                    } else {
                        Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                    }
                   }





                   /* if (isInternetPresent) {

                        gps = new GPSTracker(MyJobs_OnGoingDetailActivity.this);
                        if (gps.isgpsenabled() && gps.canGetLocation()) {
                            myLatitude = gps.getLatitude();
                            myLongitude = gps.getLongitude();

                            if (myLatitude == 0.0) {
                                //   HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.fetching_location_wait_TXT));
                                usersharelat = destination_lat + "," + destination_long;

                                String concatdes = myLatitude + "," + myLongitude;

                                String orgin = "origin=" + usersharelat;
                                String destination = "destination=" + concatdes;


                                System.out.println("orgin--->" + orgin + "  " + destination);

                                postRequestMyOrderList(MyJobs_OnGoingDetailActivity.this, "https://maps.googleapis.com/maps/api/directions/json?" + orgin + "&" + destination + "&sensor=false");
                            } else {
                                usersharelat = destination_lat + "," + destination_long;

                                String concatdes = myLatitude + "," + myLongitude;

                                String orgin = "origin=" + usersharelat;
                                String destination = "destination=" + concatdes;

                                myDialog = new ProgressDialog1(MyJobs_OnGoingDetailActivity.this);
                                if (!myDialog.isShowing()) {
                                    myDialog.show();
                                }
                                System.out.println("orgin--->" + orgin + "  " + destination);
                                duration = "few min";
                                buttonsClickActions(MyJobs_OnGoingDetailActivity.this, ServiceConstant.STARTOFFJOB_JOB_URL, "startoff");

                                //  postRequestMyOrderList(MyJobs_OnGoingDetailActivity.this, "https://maps.googleapis.com/maps/api/directions/json?" + orgin + "&" + destination + "&sensor=false");

                            }


                        } else {
                            CheckPermissions();
                        }


                        System.out.println("--------------startoff url-------------------" + ServiceConstant.STARTOFFJOB_JOB_URL);


                    } else {
                        Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                    }*/
                } else if (detail_btn2.getText().toString().equalsIgnoreCase("Arrived")) {

                    if (isInternetPresent) {

                        buttonsClickActions(MyJobs_OnGoingDetailActivity.this, ServiceConstant.ARRIVED_JOB_URL, "startoff");


                    } else {
                        Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                    }


                } else if (detail_btn2.getText().toString().equalsIgnoreCase("Start")) {
                    if (isInternetPresent) {
                        //If Multiple schedule job is equal to 0 means come to below block(Not multiple day jobs) --> Start
                        if (multipleschedulessize == 0) {
                            if (saveoveralljobcount.equals("0")) {
                                if (saveoverallestimatecount.equals("1")) {
                                    if (before_status.equalsIgnoreCase("0") || before_status.equalsIgnoreCase("2")) {
                                        if (before_status.equalsIgnoreCase("0")) {
                                            if (!before_array.equalsIgnoreCase("0")) {
                                                HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.waiting_for_user_approval));
                                            } else {
                                                SharedPreferences pref;
                                                pref = getApplicationContext().getSharedPreferences("testpu", MODE_PRIVATE);
                                                SharedPreferences.Editor editor = pref.edit();
                                                editor.clear();
                                                editor.apply();
                                                SharedPreferences pref1 = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
                                                SharedPreferences.Editor editor1 = pref1.edit();
                                                editor1.putString("jobid", str_jobId);
                                                editor1.putString("photostatus", "0");
                                                editor1.putString("jobinstruction", jobinstruction);
                                                editor1.putString("description", description);
                                                editor1.putString("storewhatsincluded", storewhatsincluded);
                                                editor1.putString("saveresforward", saveresforward);
                                                editor1.putString("clinetname", clientname);
                                                editor1.apply();
                                                Intent intent = new Intent(MyJobs_OnGoingDetailActivity.this, BeforeAndAfterPhotoActivity.class);
                                                startActivity(intent);
                                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                                finish();
                                            }
                                        } else {
                                            HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.waiting_for_user_approval));
                                        }
                                    } else {
                                        if (isInternetPresent) {
                                            buttonsClickActions(MyJobs_OnGoingDetailActivity.this, ServiceConstant.STARTJOB_URL, "startjob");
                                        } else {
                                            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                                        }

                                    }


                                } else {
                                    SharedPreferences pref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
                                    SharedPreferences.Editor editor = pref.edit();
                                    editor.putString("jobid", str_jobId);
                                    editor.putString("category", jobtypetext.getText().toString());
                                    editor.putString("location", Tv_ongoing_jobtype.getText().toString());
                                    editor.putString("date", ongoing_details_dateTv.getText().toString());
                                    editor.putString("currentamt", "0");
                                    editor.apply();
                                    Intent intent = new Intent(getApplicationContext(), EstimateBuilderActivity.class);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            } else {

                                if (before_status.equalsIgnoreCase("0") || before_status.equalsIgnoreCase("2")) {


                                    if (before_status.equalsIgnoreCase("0")) {
                                        if (!before_array.equalsIgnoreCase("0")) {
                                            HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.waiting_for_user_approval));
                                        } else {


                                            SharedPreferences pref;
                                            pref = getApplicationContext().getSharedPreferences("testpu", MODE_PRIVATE);
                                            SharedPreferences.Editor editor = pref.edit();
                                            editor.clear();
                                            editor.apply();
                                            SharedPreferences pref1 = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
                                            SharedPreferences.Editor editor1 = pref1.edit();
                                            editor1.putString("jobid", str_jobId);
                                            editor1.putString("photostatus", "0");
                                            editor1.putString("jobinstruction", jobinstruction);
                                            editor1.putString("description", description);
                                            editor1.putString("storewhatsincluded", storewhatsincluded);
                                            editor1.putString("saveresforward", saveresforward);
                                            editor1.putString("clinetname", clientname);
                                            editor1.apply();
                                            Intent intent = new Intent(MyJobs_OnGoingDetailActivity.this, BeforeAndAfterPhotoActivity.class);
                                            startActivity(intent);
                                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                            finish();
                                        }
                                    } else {
                                        HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.waiting_for_user_approval));
                                    }


                                } else {
                                    if (isInternetPresent) {
                                        buttonsClickActions(MyJobs_OnGoingDetailActivity.this, ServiceConstant.STARTJOB_URL, "startjob");
                                    } else {
                                        Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                                    }
                                }

                            }
                        }
                        //If Multiple schedule job is equal to 0 means come to below block(Not multiple day jobs)--> End
                        else {
                            if (saveoveralljobcount.equals("0")) {
                                if (saveoverallestimatecount.equals("1")) {
                                    if (before_status.equalsIgnoreCase("0") || before_status.equalsIgnoreCase("2")) {
                                        if (before_status.equalsIgnoreCase("0")) {

                                            if (!before_array.equalsIgnoreCase("0")) {
                                                HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.waiting_for_user_approval));
                                            } else {
                                                SharedPreferences pref;
                                                pref = getApplicationContext().getSharedPreferences("testpu", MODE_PRIVATE);
                                                SharedPreferences.Editor editor = pref.edit();
                                                editor.clear();
                                                SharedPreferences pref1 = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
                                                SharedPreferences.Editor editor1 = pref1.edit();
                                                editor1.putString("jobid", str_jobId);
                                                editor1.putString("photostatus", "0");
                                                editor1.putString("jobinstruction", jobinstruction);
                                                editor1.putString("description", description);
                                                editor1.putString("storewhatsincluded", storewhatsincluded);
                                                editor1.putString("saveresforward", saveresforward);
                                                editor1.putString("clinetname", clientname);
                                                editor1.apply();
                                                Intent intent = new Intent(MyJobs_OnGoingDetailActivity.this, BeforeAndAfterPhotoActivity.class);
                                                startActivity(intent);
                                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                            }
                                        } else {
                                            HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.waiting_for_user_approval));
                                        }
                                    } else if (before_status.equalsIgnoreCase("1")) {
                                        final Dialog dialog = new Dialog(MyJobs_OnGoingDetailActivity.this);
                                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                        dialog.setCancelable(true);
                                        dialog.setContentView(R.layout.fullscreendialog);


                                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                        lp.copyFrom(dialog.getWindow().getAttributes());
                                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                                        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                                        dialog.getWindow().setAttributes(lp);
                                        dialog.show();

                                        joblistmore = dialog.findViewById(R.id.jobrecycler_view);
                                        myOrderAdapterss = new Joblistadapter(MyJobs_OnGoingDetailActivity.this, myjobList, dialog);
                                        joblistmore.setEnabled(false);
                                        joblistmore.setAdapter(myOrderAdapterss);
                                    } else {
                                        if (isInternetPresent) {
                                            buttonsClickActions(MyJobs_OnGoingDetailActivity.this, ServiceConstant.STARTJOB_URL, "startjob");
                                        } else {
                                            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                                        }

                                    }
                                } else {
                                    SharedPreferences pref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
                                    SharedPreferences.Editor editor = pref.edit();
                                    editor.putString("jobid", str_jobId);
                                    editor.putString("category", jobtypetext.getText().toString());
                                    editor.putString("location", Tv_ongoing_jobtype.getText().toString());
                                    editor.putString("date", ongoing_details_dateTv.getText().toString());
                                    editor.putString("currentamt", "0");
                                    editor.apply();
                                    Intent intent = new Intent(getApplicationContext(), EstimateBuilderActivity.class);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            } else {
                                if (before_status.equalsIgnoreCase("0") || before_status.equalsIgnoreCase("2")) {
                                    if (before_status.equalsIgnoreCase("0")) {


                                        if (!before_array.equalsIgnoreCase("0")) {
                                            HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.waiting_for_user_approval));
                                        } else {
                                            final Dialog dialog = new Dialog(MyJobs_OnGoingDetailActivity.this);
                                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                            dialog.setCancelable(true);
                                            dialog.setContentView(R.layout.fullscreendialog);

                                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                            lp.copyFrom(dialog.getWindow().getAttributes());
                                            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                                            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                                            dialog.getWindow().setAttributes(lp);
                                            dialog.show();
                                            joblistmore = dialog.findViewById(R.id.jobrecycler_view);
                                            myOrderAdapterss = new Joblistadapter(MyJobs_OnGoingDetailActivity.this, myjobList, dialog);
                                            joblistmore.setEnabled(false);
                                            joblistmore.setAdapter(myOrderAdapterss);
                                        }
                                    } else {
                                        HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.waiting_for_user_approval));
                                    }
                                } else {
                                    if (isInternetPresent) {
                                        buttonsClickActions(MyJobs_OnGoingDetailActivity.this, ServiceConstant.STARTJOB_URL, "startjob");
                                    } else {
                                        Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                                    }
                                }

                            }


                        }

                    } else {
                        Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                    }
                }
                else if (detail_btn2.getText().toString().equalsIgnoreCase("Complete Job")) {

                    if (isInternetPresent) {
                        /*if (completed_multi_schedule != null && completed_multi_schedule.equalsIgnoreCase("1")){*/
                        if (pending_multi_schedule != null && pending_multi_schedule.equalsIgnoreCase("1")) {
                            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.pending_multi_schedule));
                        } else if (not_start_multi_schedule != null && not_start_multi_schedule.equalsIgnoreCase("1")) {
                            final PkDialog mDialog = new PkDialog(MyJobs_OnGoingDetailActivity.this);
                            mDialog.setDialogTitle(getResources().getString(R.string.alert_label_title));
                            mDialog.setDialogMessage(getResources().getString(R.string.not_start_multi_schedule));
                            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    not_start_multi_schedule = null;
                                    detail_btn2.performClick();
                                    mDialog.dismiss();
                                }
                            });
                            mDialog.setNegativeButton(getResources().getString(R.string.ongoing_detail_cancelbtn_label), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();
                                }
                            });
                            mDialog.show();
                        } else if (after_status.equalsIgnoreCase("0") || after_status.equalsIgnoreCase("2")) {

                            if (after_status.equalsIgnoreCase("0") && !after_array.equalsIgnoreCase("0")) {
                                HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.waiting_for_user_approval));
                            } else {
                                SharedPreferences changeorderdd = getApplicationContext().getSharedPreferences("valueofchangeorder", MODE_PRIVATE);
                                String arrayvalue = changeorderdd.getString("value", "");

                                if (arrayvalue.equalsIgnoreCase("")) {

                                    /*if (billsize > 0) {
                                        SharedPreferences pref;
                                        pref = getApplicationContext().getSharedPreferences("testpu", MODE_PRIVATE);
                                        SharedPreferences.Editor editor = pref.edit();
                                        editor.clear();
                                        SharedPreferences pref1 = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
                                        SharedPreferences.Editor editor1 = pref1.edit();
                                        editor1.putString("jobid", str_jobId);
                                        editor1.putString("photostatus", "1");

                                        editor1.putString("jobinstruction", jobinstruction);
                                        editor1.putString("description", description);
                                        editor1.putString("storewhatsincluded", storewhatsincluded);
                                        editor1.putString("saveresforward", saveresforward);
                                        editor1.putString("clinetname", clientname);
                                        editor1.apply();
                                        Intent intent = new Intent(MyJobs_OnGoingDetailActivity.this, BeforeAndAfterPhotoActivity.class);
                                        startActivity(intent);
                                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                        finish();
                                    } else {


                                    }*/

                                    SharedPreferences pref;
                                    pref = getApplicationContext().getSharedPreferences("testpu", MODE_PRIVATE);
                                    SharedPreferences.Editor editor = pref.edit();
                                    editor.clear();
                                    SharedPreferences pref1 = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
                                    SharedPreferences.Editor editor1 = pref1.edit();
                                    editor1.putString("jobid", str_jobId);
                                    editor1.putString("photostatus", "1");
                                    editor1.putString("clinetname", clientname);
                                    editor1.putString("jobinstruction", jobinstruction);
                                    editor1.putString("description", description);
                                    editor1.putString("storewhatsincluded", storewhatsincluded);
                                    editor1.putString("saveresforward", saveresforward);
                                    editor1.putString("zipcode", "" + zipcodestore);
                                    editor1.putString("storeresponse", "" + storeresponseforpass);
                                    editor1.putString("afterr", "1");
                                    editor1.apply();


                                    Intent intent = new Intent(MyJobs_OnGoingDetailActivity.this, BillAttachmentActivity.class);

                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                                }
                            }
                        } else {
                            if (isInternetPresent) {

                                if (checkedhelp.isChecked()) {
                                    if (storemybase64.equals("") || storemybase64.equals(" ")) {
                                        HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.pleasecap));
                                    } else {
                                        buttonsClickActions(MyJobs_OnGoingDetailActivity.this, ServiceConstant.JOBCOMPLETE_URL, "completejob");
                                    }
                                } else {
                                    buttonsClickActions(MyJobs_OnGoingDetailActivity.this, ServiceConstant.JOBCOMPLETE_URL, "completejob");
                                }


                            } else {
                                Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                            }

                        }
                    } else {
                        Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                    }
                } else if (detail_btn2.getText().toString().equalsIgnoreCase("Payment")) {
                    {
                        if (partners_payment_status.equalsIgnoreCase("0")) {
                            HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.wait_till_craftmen_cmplt_job));
                        } else {
                            Intent intent = new Intent(MyJobs_OnGoingDetailActivity.this, MoreInfoPageActivity.class);
                            SharedPreferences pref1 = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
                            SharedPreferences.Editor editor1 = pref1.edit();
                            editor1.putString("jobid", str_jobId);
                            editor1.putString("photostatus", "1");
                            editor1.putString("completed", "0");
                            editor1.putString("jobinstruction", jobinstruction);
                            editor1.putString("description", description);
                            editor1.putString("user_id", user_id);
                            editor1.putString("storewhatsincluded", storewhatsincluded);
                            editor1.putString("saveresforward", saveresforward);
                            editor1.putString("clinetname", clientname);
                            editor1.apply();
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        }
                    }
                }
            }
        });


        show_moretext_layout = findViewById(R.id.show_moretext_layout);
        show_more = findViewById(R.id.show_more);


    }

    @SuppressLint("RestrictedApi")
    private void setLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    //------------------------Job Complete------------------


    //-----------------------------------------------------------Connect to Chat---------------------------------------------------

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void initilizeMap() {


        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.myjob_ongoing_detail_map);
        mapFragment.getMapAsync(MyJobs_OnGoingDetailActivity.this);

        //     googleMap = ((MapFragment) MyJobs_OnGoingDetailActivity.this.getFragmentManager().findFragmentById(R.id.myjob_ongoing_detail_map)).getMap();
        // check if map is created successfully or not

    }

    //--------------Alert Method-----------
    private void Alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(MyJobs_OnGoingDetailActivity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void detailButtons(String name1, int color1, String name2, int color2) {
        detail_btn1.setText(name1);
        // detail_btn1.setBackgroundColor(color1);
        detail_btn2.setText(name2);
        // detail_btn2.setBackgroundColor(color2);
    }

    //-------------------------code for myjobs ongoing detail----------------
    private void myjobOngoingDetail(Context mContext, String url) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("provider_id", provider_id);
        jsonParams.put("job_id", str_jobId);
        myDialog = new ProgressDialog1(MyJobs_OnGoingDetailActivity.this);
        myDialog.setCancelable(true);
        myDialog.setCanceledOnTouchOutside(true);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
//                moreIconsArrayList = new ArrayList<>();
                saveresforward = response;
                swipeRefreshLayout.setRefreshing(false);
                System.out.println("--------------reponsedetail-------------------" + response);
                storeresponseforpass = response;
                Log.e("detail", response);
                String Str_status = "", Str_jobId = "", Str_Response = "", Str_currencycode = "", Str_jobdate = "", Str_jobtime = "", Str_jobtype = "",
                        Str_jobinstruction = "", Str_jobstatus = "", Str_jobusername = "", Str_userimg = "", Str_job_userrating = "", Str_job_location = "", Str_location_lattitude = "", Str_location_longitude = "";

                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");

                    if (Str_status.equalsIgnoreCase("1")) {
                        IcInfoIconDisplay = true;
                        movieList.clear();
                        beforemovieList.clear();
                        aftermovieList.clear();
                        //detail_btn1.setVisibility(View.VISIBLE);
                        detail_btn2.setVisibility(View.VISIBLE);


                        JSONObject object = jobject.getJSONObject("response");

                        if (object.has("completed_multi_schedule")) {
                            completed_multi_schedule = object.getString("completed_multi_schedule");
                        } else {
                            completed_multi_schedule = null;
                        }

                        if (object.has("pending_multi_schedule")) {
                            pending_multi_schedule = object.getString("pending_multi_schedule");
                        } else {
                            pending_multi_schedule = null;
                        }

                        if (object.has("not_start_multi_schedule")) {
                            not_start_multi_schedule = object.getString("not_start_multi_schedule");
                        } else {
                            not_start_multi_schedule = null;
                        }

                        JSONObject object2 = object.getJSONObject("job");

                        if (object2.has("booking_type")) {
                            if (object2.getString("booking_type").equalsIgnoreCase("Job")
                                    || object2.getString("booking_type").equalsIgnoreCase("estimatejob")) {
                                details_orderid_textView.setText(getResources().getString(R.string.jobs_orderid_label));
                            } else if (object2.getString("booking_type").equalsIgnoreCase("Estimate")) {
                                details_orderid_textView.setText(getResources().getString(R.string.estimation_orderid_label));
                            }

                        }

                        if (object2.has("pdfview")) {

                            Pdf_view_status = object2.getString("pdfview");

                        } else {

                            Pdf_view_status = "";
                        }

                        session.setInvoiceAlert(false);

                        JSONArray aMiscellanousArray = object2.getJSONArray("miscellaneous");
                        if (aMiscellanousArray.length() > 0) {
                            for (int y = 0; y < aMiscellanousArray.length(); y++) {
                                JSONObject aMiscellanousObject = aMiscellanousArray.getJSONObject(y);
                                if (aMiscellanousObject.getString("status").equalsIgnoreCase("0")) {
                                    session.setInvoiceAlert(true);
                                }
                            }
                        }


                        //Based on the key EnablePreAmountRequest available, I'm enabling the toolbar button id=LinearLayoutRequestPayment
                        //                            LinearLayoutRequestPayment.setVisibility(VISIBLE);
                        //                            LinearLayoutRequestPayment.setVisibility(View.GONE);
                        IcPrepaymentIconDisplay = object2.has("EnablePreAmountRequest");

                        if (object2.has("ChangeEstimateIcon") && object2.getString("ChangeEstimateIcon").equals("1")) {
                            estimate.setImageResource(R.drawable.ic_magnify);
                            IconEstimate = R.drawable.ic_magnify;
                        } else {
                            estimate.setImageResource(R.drawable.ic_edit);
                            IconEstimate = R.drawable.ic_edit;
                        }

                        if (object2.has("EnableEstimateIcon") && object2.getString("EnableEstimateIcon").equals("1")) {
//                            estimate.setVisibility(VISIBLE);
                            IcEstimateIconDisplay = true;
                            EnableEstimateIcon = object2.getString("EnableEstimateIcon");
                        } else {
//                            estimate.setVisibility(View.GONE);
                            IcEstimateIconDisplay = false;
                            EnableEstimateIcon = "";
                        }

                        if (object2.has("billing")) {
                            JSONArray jarry = object2.getJSONArray("billing");
                            if (jarry.length() > 0) {
                                BillingList = new ArrayList<>();
                                for (int i = 0; i < jarry.length(); i++) {
                                    JSONObject jobjects_amount = jarry.getJSONObject(i);
                                    PaymentFareSummeryPojo pojo = new PaymentFareSummeryPojo();

                                    String title = jobjects_amount.getString("title");

                                    String desc = "";
                                    if (jobjects_amount.has("description")) {
                                        desc = jobjects_amount.getString("description");
                                    } else {
                                        desc = "";
                                    }

                                    pojo.setPayment_desc(desc);
                                    {
                                        if (jobjects_amount.getString("title").equalsIgnoreCase("Balance amount")) {
                                            if (jobjects_amount.getString("amount").matches("[0-9.]*")) {
                                                Double balanceamo = Double.parseDouble(jobjects_amount.getString("amount"));
                                            }
                                        }

                                        checkedhelp.setChecked(false);
                                        pojo.setPayment_title(jobjects_amount.getString("title"));

                                        if (title.contains("Hours") || title.contains("Payment mode") || title.contains("Task type") || title.contains("Start Date") || title.contains("Time") || title.contains("Month") || title.contains("End Date")) {
                                            pojo.setPayment_amount(jobjects_amount.getString("amount"));
                                        } else {
                                            pojo.setPayment_amount(CurrencySymbolConverter.getCurrencySymbol(object2.getString("currency_code")) + jobjects_amount.getString("amount"));
                                        }
                                    }
                                    BillingList.add(pojo);
                                }
                            }
                        }


                        if (object2.has("procrew_notes")) {
                            JSONObject procrew_notesobj = object2.getJSONObject("procrew_notes");
                            JSONArray additional_notes = procrew_notesobj.getJSONArray("additional_notes");
                            if (additional_notes.length() > 0) {
                                for (int i = 0; i < additional_notes.length(); i++) {
                                    if (additional_notes.getJSONObject(i).has("content")) {
                                        if (i > 0) {
                                            procrew_notes = procrew_notes.concat(", " + additional_notes.getJSONObject(i).getString("content"));
                                        } else {
                                            procrew_notes = procrew_notes.concat(additional_notes.getJSONObject(i).getString("content"));
                                        }

                                        if (procrew_notes.length() > 2) {
                                            hidemyprocrewnotes.setVisibility(View.VISIBLE);
                                        } else {
                                            hidemyprocrewnotes.setVisibility(GONE);
                                        }

                                    } else {
                                        hidemyprocrewnotes.setVisibility(GONE);
                                    }
                                }
                            } else if (procrew_notesobj.has("content")) {
                                procrew_notes = procrew_notesobj.getString("content");
                                if (procrew_notes.length() > 2) {
                                    hidemyprocrewnotes.setVisibility(View.VISIBLE);
                                } else {
                                    hidemyprocrewnotes.setVisibility(GONE);
                                }
                            }
                        } else {
                            hidemyprocrewnotes.setVisibility(GONE);
                        }


                        if (object2.has("task_cost")) {
                            totalcost = object2.getString("task_cost");

                        } else {

                        }


                        if (object2.has("bills")) {
                            JSONArray billtyp = object2.getJSONArray("bills");
                            billsize = billtyp.length();
                        } else {
                            billsize = 0;
                        }


                        if (object2.has("multipleschedules")) {
                            JSONArray multipleschedules = object2.getJSONArray("multipleschedules");

                            multipleschedulessize = multipleschedules.length();

                            if (multipleschedules.length() > 0) {

                                myjobList.clear();
                                //  JSONArray joblistcategory = object2.getJSONArray("helper_category");
                                for (int b1 = 0; b1 < multipleschedules.length(); b1++) {
                                    JSONObject getarrayvalue = multipleschedules.getJSONObject(b1);
                                    String date = "" + getarrayvalue.getString("date");
                                    String starttime = "" + getarrayvalue.getString("starttime");
                                    String endtime = "" + getarrayvalue.getString("endtime");
                                    String schedulestatus = "" + getarrayvalue.getString("schedulestatus");

                                    String schedule_id = "" + getarrayvalue.getString("_id");


                                    joblistpojo aOrderPojo = new joblistpojo();

                                    aOrderPojo.setOrderId(date);
                                    aOrderPojo.setOrderServiceType("Job time : " + starttime + " to " + endtime);
                                    aOrderPojo.setOrderStatus(schedulestatus);


                                    aOrderPojo.setOrderBookingDate(schedule_id);


                                    myjobList.add(aOrderPojo);
                                }

//                                lotjob.setVisibility(View.VISIBLE);
                                IcMultiScheduleIconDisplay = true;
                            } else {
//                                lotjob.setVisibility(View.GONE);
                                IcMultiScheduleIconDisplay = false;
                            }

                        } else {
//                            lotjob.setVisibility(View.GONE);
                            IcMultiScheduleIconDisplay = false;
                        }
                        if (object2.has("multischedule_description")) {
                            myFranchiseDescriptionStr = object2.getString("multischedule_description");
                        }
                        if (object2.has("multischedule")) {
                            myMultiStatusStr = object2.getString("multischedule");
                        }


                        myFranchiseInfoList.clear();
                        if (object2.has("multipleschedules")) {
                            JSONArray aMulitpleScheduleArray = object2.getJSONArray("multipleschedules");
                            if (aMulitpleScheduleArray.length() > 0) {
                                for (int y = 0; y < aMulitpleScheduleArray.length(); y++) {
                                    JSONObject aMulitpleObject = aMulitpleScheduleArray.getJSONObject(y);
                                    FranchiseDescPojo aPojo = new FranchiseDescPojo();
                                    aPojo.setFranchiseDescription(aMulitpleObject.getString("description"));
                                    aPojo.setFranchiseId(aMulitpleObject.getString("_id"));
                                    aPojo.setFranchiseStartTime(aMulitpleObject.getString("starttime"));
                                    aPojo.setFranchiseEndTime(aMulitpleObject.getString("endtime"));
                                    aPojo.setFranchiseStartIndex(aMulitpleObject.getString("startindex"));
                                    aPojo.setFranchiseEndIdex(aMulitpleObject.getString("endindex"));
                                    aPojo.setFranchsiseHours(aMulitpleObject.getString("hours"));
                                    aPojo.setFranchiseDate(aMulitpleObject.getString("date"));
                                    aPojo.setFranchiseTasker(aMulitpleObject.getString("tasker"));
                                    aPojo.setFranchiseTaskerName(aMulitpleObject.getString("taskername"));
                                    aPojo.setFranchiseScheduleStatus(aMulitpleObject.getString("schedulestatus"));
                                    if (aMulitpleObject.getString("tasker").equalsIgnoreCase(provider_id)) {
                                        myFranchiseInfoList.add(aPojo);
                                    }
                                    loadData(myFranchiseInfoList);
                                }
                            }
                        }


                        String role = object2.getString("role");
                        partners_payment_status = object.getString("partners_payment_status");

                        if (role.equalsIgnoreCase("Helper")) {
                            finish();
                        } else {

                        }

                        JSONArray jobservice_type1 = object2.getJSONArray("helper_category");


                        if (jobservice_type1.length() == 0) {
                            helperlayout.setVisibility(GONE);


                        } else {
                            helperlayout.setVisibility(View.VISIBLE);
                            helpercount.setText(jobservice_type1.length() + " " + getResources().getString(R.string.helper_assigned_job));
                        }


                        JSONArray service_type1 = object.getJSONArray("partners");


                        if (service_type1.length() > 0 || jobservice_type1.length() > 0) {

                            craftmanlayout.setVisibility(View.VISIBLE);
                            statushide = 0;
                            if (statushide == 0) {
                                imagechange.setBackgroundResource(R.drawable.rightopen);
                            } else {
                                imagechange.setBackgroundResource(R.drawable.expandarrow);
                            }

                            if (statushide == 0) {
                                imagechange.setBackgroundResource(R.drawable.rightopen);
                                statushide = 0;
                                hidemap.setVisibility(GONE);
                                activity_my_orders_listView.setVisibility(View.VISIBLE);
                            } else {
                                imagechange.setBackgroundResource(R.drawable.expandarrow);
                                statushide = 1;
                                hidemap.setVisibility(View.VISIBLE);
                                activity_my_orders_listView.setVisibility(GONE);
                            }

                        } else {
                            helperlayout.setVisibility(GONE);
                            craftmanlayout.setVisibility(GONE);
                            imagechange.setBackgroundResource(R.drawable.expandarrow);
                            statushide = 1;
                            hidemap.setVisibility(View.VISIBLE);
                            activity_my_orders_listView.setVisibility(GONE);
                        }
                        if (service_type1.length() > 0) {
                            nooftaskers.setText(service_type1.length() + " " + getResources().getString(R.string.crftmn_available));
                        } else {
                            nooftaskers.setText("");
                        }

                        myOrdersArrList.clear();


                        for (int b1 = 0; b1 < jobservice_type1.length(); b1++) {
                            JSONObject getarrayvalue = jobservice_type1.getJSONObject(b1);
                            String jobiddd = "" + getarrayvalue.getString("service_type");
                            String service_id = "" + getarrayvalue.getString("service_id");
                            String orderstatus = "";
                            if (getarrayvalue.getString("helper_status").equalsIgnoreCase("2")) {
                                orderstatus = "Start Off";
                            } else if (getarrayvalue.getString("helper_status").equalsIgnoreCase("3")) {
                                orderstatus = "Arrived";
                            } else if (getarrayvalue.getString("helper_status").equalsIgnoreCase("4")) {
                                orderstatus = "Start job";
                            } else if (getarrayvalue.getString("helper_status").equalsIgnoreCase("5")) {
                                orderstatus = "Complete job";
                            } else if (getarrayvalue.getString("helper_status").equalsIgnoreCase("6")) {
                                orderstatus = "Job completed";
                            }


                            String helpername = "" + getarrayvalue.getString("helpername");
                            String helperimage = "" + getarrayvalue.getString("helperimage");
                            String service_type = "" + getarrayvalue.getString("service_type");


                            MyOrdersPojo aOrderPojo = new MyOrdersPojo();
                            aOrderPojo.setOrderId(str_jobId);
                            aOrderPojo.setOrderServiceType(service_type);
                            aOrderPojo.setTaskname(helpername);
                            aOrderPojo.setUserphoto(helperimage);
                            aOrderPojo.setOrderStatus(orderstatus);
                            myOrdersArrList.add(aOrderPojo);

                        }


                        for (int b1 = 0; b1 < service_type1.length(); b1++) {
                            JSONObject getarrayvalue = service_type1.getJSONObject(b1);
                            String jobiddd = "" + getarrayvalue.getString("job_id");


                            String orderstatus = "Job status : " + getarrayvalue.getString("job_status");
                            String service = "";
                            JSONArray service_type = getarrayvalue.getJSONArray("category_name");
                            for (int b = 0; b < service_type.length(); b++) {
                                String value = "" + service_type.getString(b);
                                service += value + ",";
                            }
                            if (service.endsWith(",")) {
                                service = service.substring(0, service.length() - 1);
                            }


                            String tasknamee = getarrayvalue.getString("tasker_name");
                            String taskphotoo = getarrayvalue.getString("tasker_image");
                            if (taskphotoo.startsWith("http:") || taskphotoo.startsWith("https:")) {
                            } else {
                                taskphotoo = ServiceConstant.Review_image + taskphotoo;
                            }

                            MyOrdersPojo aOrderPojo = new MyOrdersPojo();
                            aOrderPojo.setOrderId(jobiddd);
                            aOrderPojo.setOrderServiceType(service);
                            aOrderPojo.setTaskname(tasknamee);
                            aOrderPojo.setUserphoto(taskphotoo);
                            aOrderPojo.setOrderStatus(orderstatus);
                            myOrdersArrList.add(aOrderPojo);

                        }


                        if (service_type1.length() > 0 || jobservice_type1.length() > 0) {
                            MyOrdersAdapter myOrderAdapter = new MyOrdersAdapter(MyJobs_OnGoingDetailActivity.this, myOrdersArrList);
                            activity_my_orders_listView.setAdapter(myOrderAdapter);
                        }


                        Str_Userid = object2.getString("user_id");


                        jobinstruction = object2.getString("instruction");
                        description = object2.getString("job_description");

                        before_status = object2.getString("before_status");

                        after_status = object2.getString("after_status");

                        JSONArray taskimage_type = object2.getJSONArray("user_taskimages");
                        for (int b = 0; b < taskimage_type.length(); b++) {
                            String value = "" + taskimage_type.getString(b);

                            if (value.startsWith("http://") || value.startsWith("https://")) {
                                value = value;
                            } else {
                                value = ServiceConstant.BASE_URLIMAGE + value;
                            }
                            acceptimage movie = new acceptimage(value);
                            movieList.add(movie);
                        }


                        JSONArray beforetaskimage_type = object2.getJSONArray("before_taskimages");
                        before_array = "" + beforetaskimage_type.length();

                        for (int b = 0; b < beforetaskimage_type.length(); b++) {
                            String value = "" + beforetaskimage_type.getString(b);

                            if (value.startsWith("http://") || value.startsWith("https://")) {
                                value = value;
                            } else {
                                value = ServiceConstant.BASE_URLIMAGE + value;
                            }
                            BeforeImagePojo beforemovie = new BeforeImagePojo(value, "");
                            beforemovieList.add(beforemovie);
                        }


                        JSONArray aftertaskimage_type = object2.getJSONArray("after_taskimages");
                        after_array = "" + aftertaskimage_type.length();

                        if (aftertaskimage_type.length() > 0) {
                            writtenauth.setVisibility(GONE);
                        }

                        for (int b = 0; b < aftertaskimage_type.length(); b++) {
                            String value = "" + aftertaskimage_type.getString(b);

                            if (value.startsWith("http://") || value.startsWith("https://")) {
                                value = value;
                            } else {
                                value = ServiceConstant.BASE_URLIMAGE + value;
                            }
                            AfterImagePojo beforemovie = new AfterImagePojo(value);
                            aftermovieList.add(beforemovie);
                        }

                        JSONArray categoryinclude = object2.getJSONArray("category");
                        storewhatsincludedlenghth = categoryinclude.length();
                        storewhatsincluded = "";
                        for (int z = 0; z < categoryinclude.length(); z++) {
                            JSONObject getarrayvaluex = categoryinclude.getJSONObject(z);
                            String name = "" + getarrayvaluex.getString("name");

                            if (getarrayvaluex.has("whatsincluded")) {
                                String whatsincluded = "" + getarrayvaluex.getString("whatsincluded");
                                if (categoryinclude.length() == (z + 1)) {
                                    storewhatsincluded += name + "-" + whatsincluded;
                                } else {
                                    storewhatsincluded += name + "-" + whatsincluded + "\n";
                                }

                            } else {

                            }


                        }


                        Str_currencycode = object2.getString("currency_code");
                        Str_jobdate = object2.getString("job_date");
                        Str_jobtime = object2.getString("job_time");

                        task_id = "" + object2.getString("task_id");

                        session.setTaskid(task_id);
                        user_id = "" + object2.getString("user_id");


                        if (object2.has("estimation_status")) {
                            saveoverallestimatecount = object2.getString("estimation_status");
                        } else {
                            saveoverallestimatecount = "0";
                        }
                        saveoveralljobcount = object2.getString("overall_job_count");
                        String service = "";
                        JSONArray service_type = object2.getJSONArray("job_type");
                        ServiceTypeArrayList = new ArrayList<>();
                        for (int b = 0; b < service_type.length(); b++) {
                            ServiceTypeArrayList.add(service_type.getString(b));
                            String value = "" + service_type.getString(b);
                            service += value + ",";
                        }
                        if (service.endsWith(",")) {
                            service = service.substring(0, service.length() - 1);
                        }
                        myAnswerStr = "";
                        JSONArray aCategoryArray = object2.getJSONArray("catdata");
                        if (aCategoryArray.length() > 0) {
                            for (int j = 0; j < aCategoryArray.length(); j++) {
                                JSONObject aCategoryObject = aCategoryArray.getJSONObject(j);
                                JSONArray aAnswerArray = aCategoryObject.getJSONArray("answer");
                                if (aAnswerArray.length() > 0) {
                                    for (int h = 0; h < aAnswerArray.length(); h++) {
                                        if (myAnswerStr.equalsIgnoreCase("")) {
                                            myAnswerStr = "\n\u2022 " + aAnswerArray.getString(h);
                                        } else {
                                            myAnswerStr = myAnswerStr + "\n\u2022 " + aAnswerArray.getString(h);
                                        }
                                    }
                                }
                            }
                        }
                        changeordervisible = object2.getString("change_order");

                        Str_jobtype = service;
                        Str_jobinstruction = object2.getString("instruction");
                        Job_Instruction = object2.getString("instruction");
                        Str_jobstatus = object2.getString("job_status");
                        Str_jobusername = object2.getString("user_name");
                        clientname = object2.getString("user_name");
                        Str_userimg = object2.getString("user_image");
                        Str_job_userrating = object2.getString("user_ratings");
                        Str_user_email = object2.getString("user_email");
                        Str_usermobile = object2.getString("user_mobile");
                        Str_job_location = object2.getString("job_location");
                        Str_job_group = object2.getString("btn_group");
                        zipcodestore = object2.getString("zipcode");
                        destination_lat = object2.getString("location_lat");
                        destination_long = object2.getString("location_lon");

                        if (object2.has("provide_estimation")) {
                            String provide_estimation = object2.getString("provide_estimation");
                            saveprovide_estimatio = provide_estimation;
                        } else {
                            saveprovide_estimatio = "0";
                        }
                        if (object2.has("overall_est_count")) {


                            if (saveoveralljobcount.equals("0")) {
                                provide_estimation = object2.getString("overall_est_count");
                                //                                    estimate.setVisibility(View.VISIBLE);
                                //                                    estimate.setVisibility(View.GONE);
                                IcEstimateIconDisplay = !provide_estimation.equalsIgnoreCase("0") && !saveprovide_estimatio.equals("0");
                            } else {

                                provide_estimation = object2.getString("overall_est_count");


                                //                                    estimate.setVisibility(View.GONE);
                                //                                    estimate.setVisibility(View.VISIBLE);
                                IcEstimateIconDisplay = !provide_estimation.equalsIgnoreCase("0");
                            }

                        }
                        jobtype = Str_jobtype;
                        sendaddress = Str_job_location;
                        senddatetime = Str_jobdate + " " + Str_jobtime;
                        sendphoto = Str_userimg;

                        sendusername = Str_jobusername;

                        if (object2.has("cancelreason")) {
                            cancel_reson = object2.getString("cancelreason");
                        }

                        strlat = Double.parseDouble(destination_lat);
                        strlon = Double.parseDouble(destination_long);
                        mTaskID = object2.getString("task_id");
                    } else {
                        Str_Response = jobject.getString("response");
                    }

                    if (Str_status.equalsIgnoreCase("1")) {

                        Tv_ongoing_detail_jobinstruction.setText(Str_jobinstruction);
                        String length = String.valueOf(Tv_ongoing_detail_jobinstruction.getText().toString().length());
                        if (Str_jobinstruction.length() > 30) {
                            show_moretext_layout.setVisibility(View.VISIBLE);
                        } else {
                            show_moretext_layout.setVisibility(GONE);
                        }
                        //-------------------code for set marker-------------------------

                        mapFragment.getMapAsync(new OnMapReadyCallback() {
                            @Override
                            public void onMapReady(GoogleMap googleMap) {
                                googleMap.clear();
                                googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

                                googleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng((strlat), (strlon)))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin)));
                                // Move the camera to last position with a zoom level
                                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng((strlat), (strlon))).zoom(17).build();
                                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                            }
                        });


                        //                            ChangeOrderActivity.setVisibility(View.GONE);
                        //                            ChangeOrderActivity.setVisibility(View.VISIBLE);
                        IcChangeOrderIconDisplay = !changeordervisible.equals("0");

                        if (Str_job_group.equalsIgnoreCase("1")) {
                            writtenauth.setVisibility(GONE);
//                            ChangeOrderActivity.setVisibility(View.GONE);
                            IcChangeOrderIconDisplay = false;
                            detailButtons(getResources().getString(R.string.ongoing_detail_rejectlabel), Color.parseColor("#ff0000"), getResources().getString(R.string.ongoing_detail_accept_label), Color.parseColor("#248423"));
//                            track_location.setVisibility(View.VISIBLE);
                            IcLocationIconDisplay = true;
//                            billpay.setVisibility(View.GONE);
                            IcMaterialIconDisplay = false;
                            //                                estimate.setVisibility(View.VISIBLE);
                            //                                estimate.setVisibility(View.GONE);
                            IcEstimateIconDisplay = saveoverallestimatecount.equals("1");
                        } else if (Str_job_group.equalsIgnoreCase("2")) {
                            writtenauth.setVisibility(GONE);
//                            ChangeOrderActivity.setVisibility(View.GONE);
                            IcChangeOrderIconDisplay = false;
                            detailButtons(getResources().getString(R.string.ongoing_detail_cancelbtn_label), Color.parseColor("#ff0000"), getResources().getString(R.string.ongoing_detail_startoff_label), Color.parseColor("#248423"));
//                            track_location.setVisibility(View.VISIBLE);
                            IcLocationIconDisplay = true;
//                            billpay.setVisibility(View.GONE);
                            IcMaterialIconDisplay = false;
                            //                                estimate.setVisibility(View.VISIBLE);
                            //                                estimate.setVisibility(View.GONE);
                            IcEstimateIconDisplay = saveoverallestimatecount.equals("1");

                        } else if (Str_job_group.equalsIgnoreCase("3")) {
                            writtenauth.setVisibility(GONE);
//                            ChangeOrderActivity.setVisibility(View.GONE);
                            IcChangeOrderIconDisplay = false;
                            detailButtons(getResources().getString(R.string.ongoing_detail_cancelbtn_label), Color.parseColor("#ff0000"), getResources().getString(R.string.ongoing_detail_arrived_label), Color.parseColor("#248423"));
//                            track_location.setVisibility(View.VISIBLE);
                            IcLocationIconDisplay = true;
//                            billpay.setVisibility(View.GONE);
                            IcMaterialIconDisplay = false;
                            //                                estimate.setVisibility(View.VISIBLE);
                            //                                estimate.setVisibility(View.GONE);
                            IcEstimateIconDisplay = saveoverallestimatecount.equals("1");
                        } else if (Str_job_group.equalsIgnoreCase("4")) {

                            writtenauth.setVisibility(GONE);

                            //#193-Github --> By the client confirmation ChangeOrderActivity was hidden here.
//                            ChangeOrderActivity.setVisibility(View.GONE);
                            IcChangeOrderIconDisplay = false;
                            //#193-Github --> By the client confirmation this following lines are blocked --> Start
                            /*ChangeOrderActivity.setVisibility(View.VISIBLE);

                            if (totalcost.equals("") || totalcost.equals("0") || totalcost.equals("0.0")) {
                                ChangeOrderActivity.setVisibility(View.GONE);
                            } else {
                                ChangeOrderActivity.setVisibility(View.VISIBLE);
                            }*/
                            //#193-Github --> By the client confirmation this following lines are blocked --> End

                            detailButtons(getResources().getString(R.string.ongoing_detail_cancelbtn_label), Color.parseColor("#ff0000"), getResources().getString(R.string.ongoing_detail_startjob_label), Color.parseColor("#248423"));
//                            track_location.setVisibility(View.GONE);
                            IcLocationIconDisplay = false;
                            //                                billpay.setVisibility(View.VISIBLE);
                            //                                billpay.setVisibility(View.GONE);
                            IcMaterialIconDisplay = details_orderid_textView.getText().toString().equalsIgnoreCase(getResources().getString(R.string.jobs_orderid_label));

                            if (EnableEstimateIcon.equals("1")) {
                                if (saveoveralljobcount.equals("0")) {

                                    if (!provide_estimation.equalsIgnoreCase("0") && !saveprovide_estimatio.equals("0")) {
//                                        estimate.setVisibility(View.VISIBLE);
                                        IcEstimateIconDisplay = true;

                                    } else {
                                        // estimate.setVisibility(View.GONE);
                                    }
                                } else {
                                    // ChangeOrderActivity.setVisibility(View.VISIBLE);
                                    //estimate.setVisibility(View.GONE);
                                }
                            } else {
//                                estimate.setVisibility(View.GONE);
                                IcEstimateIconDisplay = false;
                            }


                        } else if (Str_job_group.equalsIgnoreCase("5")) {

//                            ChangeOrderActivity.setVisibility(View.VISIBLE);
                            IcChangeOrderIconDisplay = true;
                            // detail_btn1.setVisibility(View.GONE);
                            detail_btn2.setVisibility(View.VISIBLE);
                            detail_btn2.setText(getResources().getString(R.string.ongoing_detail_jobcompleted_label));
                            //  detail_btn1.setBackgroundColor(getResources().getColor(R.color.layout_completejob_btn_color));
//                            track_location.setVisibility(View.GONE);
                            IcLocationIconDisplay = false;
//                            billpay.setVisibility(View.VISIBLE);
                            IcMaterialIconDisplay = true;

                            //                                estimate.setVisibility(View.GONE);
                            //                                estimate.setVisibility(View.VISIBLE);
                            IcEstimateIconDisplay = !provide_estimation.equals("0");

                        } else if (Str_job_group.equalsIgnoreCase("6")) {
                            writtenauth.setVisibility(GONE);
                            dateselect.setVisibility(GONE);
//                            ChangeOrderActivity.setVisibility(View.GONE);
                            IcChangeOrderIconDisplay = false;
                            //  detail_btn1.setVisibility(View.GONE);
                            detail_btn2.setVisibility(View.VISIBLE);
                            detail_btn2.setText(getResources().getString(R.string.ongoing_detail_payment_label));
                            //  detail_btn2.setBackgroundColor(getResources().getColor(R.color.layout_payment_btn_color));
//                            track_location.setVisibility(View.GONE);
                            IcLocationIconDisplay = false;
//                            billpay.setVisibility(View.VISIBLE);
                            IcMaterialIconDisplay = true;
                            //                                estimate.setVisibility(View.GONE);
                            //                                estimate.setVisibility(View.VISIBLE);
                            IcEstimateIconDisplay = !provide_estimation.equals("0");

                        } else if (Str_job_group.equalsIgnoreCase("7")) {
                            writtenauth.setVisibility(GONE);
                            dateselect.setVisibility(GONE);
//                            ChangeOrderActivity.setVisibility(View.GONE);
                            IcChangeOrderIconDisplay = false;
                            detail_btn2.setVisibility(GONE);
                            detail_btn1.setVisibility(View.VISIBLE);
                            detail_btn1.setText(getResources().getString(R.string.ongoing_detail_canceled_job_label));
                            //  detail_btn1.setBackgroundColor(getResources().getColor(R.color.app_cancel_btn_color));
                            Img_Chat.setEnabled(false);
                            Img_Call.setEnabled(false);
                            Img_Email.setEnabled(false);
                            Img_Message.setEnabled(false);
                            track_location.setEnabled(false);
//                            track_location.setVisibility(View.GONE);
                            IcLocationIconDisplay = false;
//                            billpay.setVisibility(View.VISIBLE);
                            IcMaterialIconDisplay = true;
//                            estimate.setVisibility(View.GONE);
                            IcEstimateIconDisplay = false;
                        } else {

                            writtenauth.setVisibility(GONE);
                            dateselect.setVisibility(GONE);
//                            ChangeOrderActivity.setVisibility(View.GONE);
                            IcChangeOrderIconDisplay = false;
                            detail_btn1.setText(getResources().getString(R.string.ongoing_detail_more_label));
                            detail_btn2.setVisibility(GONE);
                            detail_btn1.setVisibility(View.VISIBLE);
                            //detail_btn1.setBackgroundColor(getResources().getColor(R.color.layout_moreinfo_btn_color));
//                            track_location.setVisibility(View.GONE);
                            IcLocationIconDisplay = false;
//                            billpay.setVisibility(View.VISIBLE);
                            IcMaterialIconDisplay = true;
                            //                                estimate.setVisibility(View.GONE);
                            //                                estimate.setVisibility(View.VISIBLE);
                            IcEstimateIconDisplay = !provide_estimation.equals("0");
                        }

                    } else {
                        final PkDialog mdialog = new PkDialog(MyJobs_OnGoingDetailActivity.this);
                        mdialog.setDialogTitle(getResources().getString(R.string.server_lable_header));
                        mdialog.setDialogMessage(Str_Response);
                        mdialog.setCancelOnTouchOutside(false);
                        mdialog.setPositiveButton(getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        /*Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.putExtra("EXIT", true);
                                        startActivity(intent);
                                        finish();*/

                                        Intent intent = new Intent(getApplicationContext(), WeeklyCalendarActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.putExtra("EXIT", true);
                                        startActivity(intent);
                                        finish();
                                        mdialog.dismiss();
                                    }
                                }
                        );
                        mdialog.show();
                    }

                    if (myMultiStatusStr.equalsIgnoreCase("1")) {
                        myViewLAY.setVisibility(VISIBLE);
                        LinearLayoutMoreIcon.setVisibility(GONE);
                        detail_btn1.setVisibility(GONE);
                        detail_btn2.setVisibility(GONE);
                        LinearLayoutMoreIcon.setVisibility(VISIBLE);
                    } else {
                        myViewLAY.setVisibility(GONE);
                        LinearLayoutMoreIcon.setVisibility(VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (sendphoto.isEmpty()) {
                    imag.setImageResource(R.drawable.ic_no_user);
                } else {
                    Picasso.with(MyJobs_OnGoingDetailActivity.this)
                            .load(sendphoto)


                            .placeholder(R.drawable.ic_no_user)   // optional
                            .error(R.drawable.ic_no_user)      // optional
                            // optional
                            .into(imag);
                }
                usernameee.setText(sendusername);
                jobtypetext.setText(jobtype);
                if (sendaddress.length() < 2) {
                    Tv_ongoing_jobtype.setText(getResources().getString(R.string.no_address_available));
                } else {
                    Tv_ongoing_jobtype.setText(sendaddress);
                }
                ongoing_details_dateTv.setText(senddatetime.replace("/", "."));
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {

                /*Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);
                finish();*/

                Intent intent = new Intent(getApplicationContext(), WeeklyCalendarActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    private void loadData(ArrayList<FranchiseDescPojo> aFranchiseInfoList) {
        if (aFranchiseInfoList.size() > 0) {
            FranchiseListAdapter aAdapter = new FranchiseListAdapter(MyJobs_OnGoingDetailActivity.this, aFranchiseInfoList);
            myFranchiseLV.setAdapter(aAdapter);
        }
    }

    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkAccessFineLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkAccessCoarseLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void CheckPermissions() {
        if (!checkAccessFineLocationPermission() || !checkAccessCoarseLocationPermission()) {
            requestPermissionLocation();
        } else {
            enableGpsService();
        }
    }

    private void enableGpsService() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final com.google.android.gms.common.api.Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Intent intent = new Intent(MyJobs_OnGoingDetailActivity.this, Track_your_ride.class);
                        intent.putExtra("LAT", destination_lat);
                        intent.putExtra("LONG", destination_long);
                        intent.putExtra("Userid", Str_Userid);
                        intent.putExtra("tasker", provider_id);
                        intent.putExtra("task", mTaskID);
                        intent.putExtra("address", sendaddress);
                        startActivity(intent);
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(MyJobs_OnGoingDetailActivity.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODES);
    }

    private void requestPermissionLocation() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + Str_usermobile));
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(callIntent);
                }
                break;


            case PERMISSION_REQUEST_CODES:

                if (Build.VERSION.SDK_INT >= 23) {

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.putExtra("address", Str_usermobile);
                    intent.putExtra("sms_body", share_text);
                    intent.setData(Uri.parse("smsto:" + Str_usermobile));
                    startActivity(intent);


                } else {
                    Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                    sendIntent.putExtra("sms_body", share_text);
                    sendIntent.putExtra("address", Str_usermobile);
                    sendIntent.setType("vnd.android-dir/mms-sms");
                    startActivity(sendIntent);
                }
                break;
            case PERMISSION_REQUEST_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {

            movieList.clear();
            beforemovieList.clear();
            aftermovieList.clear();
            myjobOngoingDetail(MyJobs_OnGoingDetailActivity.this, ServiceConstant.MYJOB_DETAIL_INFORMATION_URL);
            System.out.println("--------------detail-------------------" + ServiceConstant.MYJOB_DETAIL_INFORMATION_URL);
        } else {
            final PkDialog mDialog = new PkDialog(MyJobs_OnGoingDetailActivity.this);
            mDialog.setDialogTitle(getResources().getString(R.string.internet_label));
            mDialog.setCancelOnTouchOutside(false);
            mDialog.setDialogMessage(getResources().getString(R.string.action_no_internet_message));
            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                    finish();
                }
            });
            mDialog.show();
        }
    }

    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences pref1 = getApplicationContext().getSharedPreferences("openclosed", 0);
        SharedPreferences.Editor editor1 = pref1.edit();
        editor1.putString("status", "0");
        editor1.apply();
        editor1.commit();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    protected void onStop() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();

        try {
            handler.removeCallbacks(timedTask);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //  mHandler.removeCallbacks(mHandlerTask);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();


    }

    @Override
    public void onConnected(Bundle bundle) {
        if (gps != null && gps.canGetLocation() && gps.isgpsenabled()) {
        }

        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            myLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);

            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(getApplicationContext(), "Connection lost", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    //---------------------------------------------Material Fees Submit Request--------------------------------------------------------

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    private void buttonsClickActions(Context mContext, String url, String key) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("provider_id", provider_id);
        jsonParams.put("job_id", str_jobId);

        if (key.equalsIgnoreCase("completejob")) {
            jsonParams.put("signature", storemybase64);
        }


        if (key.equalsIgnoreCase("startjob")) {
            jsonParams.put("jobstatus", "0");
        }

        jsonParams.put("eta", duration);

        if (key.equalsIgnoreCase("accept")) {
            jsonParams.put("provider_lat", String.valueOf(MyCurrent_lat));
            jsonParams.put("provider_lon", String.valueOf(MyCurrent_long));
        }
        if (key.equalsIgnoreCase("startoff")) {
            jsonParams.put("provider_lat", String.valueOf(MyCurrent_lat));
            jsonParams.put("provider_lon", String.valueOf(MyCurrent_long));
        }

        System.out.println("curentlat-----------" + MyCurrent_lat);
        System.out.println("curentlong-----------" + MyCurrent_long);
        System.out.println("provider_id-----------" + provider_id);
        System.out.println("job_id-----------" + str_jobId);


        if (!myDialog.isShowing()) {
            myDialog.show();
        }


        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                Log.e("clickresponse------", response);

                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

                String Str_status = "", username = "", user_email = "", user_phoneno = "", user_img = "", user_reviwe = "", jobId = "", user_location = "", jobtime = "", job_lat = "", job_long = "",
                        Str_message = "", Str_btngroup = "";

                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");

                    if (Str_status.equalsIgnoreCase("1")) {


                        JSONObject object = jobject.getJSONObject("response");
                        Str_message = object.getString("message");
                        Str_btngroup = object.getString("btn_group");


                        if (Str_message.contains("Start Off")) {
                            if (object.has("DisplayMessage")) {
                                Str_message = object.getString("DisplayMessage");
                            } else {
                                Str_message = getResources().getString(R.string.startoff);
                            }
                        } else if (Str_message.contains("Arrived")) {
                            Str_message = getResources().getString(R.string.arrived);
                        } else if (Str_message.contains("Job Started")) {
                            Str_message = getResources().getString(R.string.startjob);
                        } else if (Str_message.contains("Job has been completed")) {
                            Str_message = getResources().getString(R.string.completejob);
                        }


                        final PkDialog mdialog = new PkDialog(MyJobs_OnGoingDetailActivity.this);
                        mdialog.setDialogTitle(getResources().getString(R.string.action_loading_sucess));
                        mdialog.setDialogMessage(Str_message);
                        mdialog.setPositiveButton(getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mdialog.dismiss();

                                    }
                                }
                        );
                        mdialog.show();

                    } else {
                        Str_message = jobject.getString("response");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (Str_status.equalsIgnoreCase("1")) {


                    isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        swipeRefreshLayout.setRefreshing(true);
                        myjobOngoingDetail(MyJobs_OnGoingDetailActivity.this, ServiceConstant.MYJOB_DETAIL_INFORMATION_URL);

                    } else {
                        final PkDialog mDialog = new PkDialog(MyJobs_OnGoingDetailActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.internet_label));
                        mDialog.setCancelOnTouchOutside(false);
                        mDialog.setDialogMessage(getResources().getString(R.string.action_no_internet_message));
                        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                                finish();
                            }
                        });
                        mDialog.show();
                    }
                } else {
                    Alert(getResources().getString(R.string.server_lable_header), Str_message);
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }
        });
    }

    @Override
    public void onRemoveListener(int position) {

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR


            SharedPreferences pref1 = getApplicationContext().getSharedPreferences("needrefresh", 0);
            String whichclass = pref1.getString("yesno", " ");
            switch (whichclass) {
                case "0":
                case "2": {
               /* finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);*/

                    Intent intent = new Intent(getApplicationContext(), MyJobsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("status", "0");
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                    break;
                }
                case "1": {

                /*Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);*/

                    Intent intent = new Intent(getApplicationContext(), WeeklyCalendarActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("EXIT", true);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    break;
                }
                case "3": {

                    Intent intent = new Intent(getApplicationContext(), MyJobsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("status", "0");
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    break;
                }
                default: {
                    Intent intent = new Intent(getApplicationContext(), MyJobsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("status", "0");
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    break;
                }
            }


            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        googleMap.getUiSettings().setAllGesturesEnabled(true);

        if (googleMap == null) {
            Toast.makeText(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.ongoing_detail_map_doesnotcreate_label), Toast.LENGTH_SHORT).show();
        }

        // Changing map type
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        // Showing / hiding your current location

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(false);
        // Enable / Disable zooming controls
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        // Enable / Disable my location button
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        // Enable / Disable Compass icon
        googleMap.getUiSettings().setCompassEnabled(false);
        // Enable / Disable Rotate gesture
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        // Enable / Disable zooming functionality
        googleMap.getUiSettings().setZoomGesturesEnabled(true);

        if (gps.canGetLocation()) {
            double Dlatitude = gps.getLatitude();
            double Dlongitude = gps.getLongitude();
            MyCurrent_lat = Dlatitude;
            MyCurrent_long = Dlongitude;
            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Dlatitude, Dlongitude)).zoom(17).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            // create marker double Dlatitude = gps.getLatitude();

            MarkerOptions marker = new MarkerOptions().position(new LatLng(Dlatitude, Dlongitude));
            // marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.maps));
            //  currentMarker = googleMap.addMarker(marker);
            System.out.println("currntlat----------" + MyCurrent_lat);
            System.out.println("currntlon----------" + MyCurrent_long);

        } else {
            //show gps alert
           /* alert_layout.setVisibility(View.VISIBLE);
            alert_textview.setText(getResources().getString(R.string.alert_gpsEnable));*/
        }
        setLocationRequest();
    }

    private void postRequestMyOrderList(final Context aContext, String url) {
        myDialog = new ProgressDialog1(MyJobs_OnGoingDetailActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<>();


        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------full details----------------" + response);


                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }


                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("OK")) {

                        JSONArray jarry = object.getJSONArray("routes");
                        for (int b = 0; b < jarry.length(); b++) {
                            JSONObject object2 = jarry.getJSONObject(b);
                            JSONArray legsjarry = object2.getJSONArray("legs");
                            for (int c = 0; c < legsjarry.length(); c++) {
                                JSONObject legsobject2 = legsjarry.getJSONObject(c);
                                JSONObject distanceobject2 = legsobject2.getJSONObject("duration");
                                duration = distanceobject2.getString("text");

                                System.out.println("-------------duration----------------" + duration);
                                buttonsClickActions(MyJobs_OnGoingDetailActivity.this, ServiceConstant.STARTOFFJOB_JOB_URL, "startoff");
                            }

                        }


                    } else {
                        duration = "few min";
                        buttonsClickActions(MyJobs_OnGoingDetailActivity.this, ServiceConstant.STARTOFFJOB_JOB_URL, "startoff");
                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }
                }


            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                    buttonsClickActions(MyJobs_OnGoingDetailActivity.this, ServiceConstant.STARTOFFJOB_JOB_URL, "startoff");
                }
            }
        });

    }

    private void multiplestatus(Context mContext, String url, final int key, String scheduleid, final String status, final Dialog dialog) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("provider_id", provider_id);
        jsonParams.put("job_id", str_jobId);


        jsonParams.put("schedule_id", scheduleid);
        jsonParams.put("status", status);


        if (!myDialog.isShowing()) {
            myDialog.show();
        }


        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                Log.e("clickresponse------", response);

                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

                dialog.dismiss();

                String Str_status = "", username = "", user_email = "", user_phoneno = "", user_img = "", user_reviwe = "", jobId = "", user_location = "", jobtime = "", job_lat = "", job_long = "",
                        Str_message = "", Str_btngroup = "";

                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");

                    if (Str_status.equalsIgnoreCase("1")) {
                        if (status.equals("1")) {
                            myjobList.get(key).setOrderStatus("1");
                            myOrderAdapterss.notifyDataSetChanged();

                            isInternetPresent = cd.isConnectingToInternet();
                            if (isInternetPresent) {
//                                onBackPressed();
                                swipeRefreshLayout.setRefreshing(true);
                                myjobOngoingDetail(MyJobs_OnGoingDetailActivity.this, ServiceConstant.MYJOB_DETAIL_INFORMATION_URL);
                            } else {
                                final PkDialog mDialog = new PkDialog(MyJobs_OnGoingDetailActivity.this);
                                mDialog.setDialogTitle(getResources().getString(R.string.internet_label));
                                mDialog.setCancelOnTouchOutside(false);
                                mDialog.setDialogMessage(getResources().getString(R.string.action_no_internet_message));
                                mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mDialog.dismiss();
                                        finish();
                                    }
                                });
                                mDialog.show();
                            }

                            if (detail_btn2.getText().toString().equalsIgnoreCase("Start")) {
                                if (saveoveralljobcount.equals("0")) {
                                    if (saveoverallestimatecount.equals("1")) {
                                        if (before_status.equalsIgnoreCase("0") || before_status.equalsIgnoreCase("2")) {
                                            if (before_status.equalsIgnoreCase("0")) {
                                                if (!before_array.equalsIgnoreCase("0")) {
                                                    HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.waiting_for_user_approval));
                                                } else {
                                                    SharedPreferences pref;
                                                    pref = getApplicationContext().getSharedPreferences("testpu", MODE_PRIVATE);
                                                    SharedPreferences.Editor editor = pref.edit();
                                                    editor.clear();
                                                    SharedPreferences pref1 = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
                                                    SharedPreferences.Editor editor1 = pref1.edit();
                                                    editor1.putString("jobid", str_jobId);
                                                    editor1.putString("photostatus", "0");
                                                    editor1.putString("jobinstruction", jobinstruction);
                                                    editor1.putString("description", description);
                                                    editor1.putString("storewhatsincluded", storewhatsincluded);
                                                    editor1.putString("saveresforward", saveresforward);
                                                    editor1.putString("clinetname", clientname);
                                                    editor1.apply();
                                                    Intent intent = new Intent(MyJobs_OnGoingDetailActivity.this, BeforeAndAfterPhotoActivity.class);
                                                    startActivity(intent);
                                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                                    finish();
                                                }
                                            } else {
                                                HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.waiting_for_user_approval));
                                            }
                                        } else {
                                            if (isInternetPresent) {
                                                buttonsClickActions(MyJobs_OnGoingDetailActivity.this, ServiceConstant.STARTJOB_URL, "startjob");
                                            } else {
                                                Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                                            }

                                        }


                                    } else {
                                        SharedPreferences pref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
                                        SharedPreferences.Editor editor = pref.edit();
                                        editor.putString("jobid", str_jobId);
                                        editor.putString("category", jobtypetext.getText().toString());
                                        editor.putString("location", Tv_ongoing_jobtype.getText().toString());
                                        editor.putString("date", ongoing_details_dateTv.getText().toString());
                                        editor.putString("currentamt", "0");
                                        editor.apply();
                                        editor.commit();
                                        Intent intent = new Intent(getApplicationContext(), EstimateBuilderActivity.class);
                                        startActivity(intent);
                                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                    }
                                } else {

                                    if (before_status.equalsIgnoreCase("0") || before_status.equalsIgnoreCase("2")) {


                                        if (before_status.equalsIgnoreCase("0")) {
                                            if (!before_array.equalsIgnoreCase("0")) {
                                                HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.waiting_for_user_approval));
                                            } else {


                                                SharedPreferences pref;
                                                pref = getApplicationContext().getSharedPreferences("testpu", MODE_PRIVATE);
                                                SharedPreferences.Editor editor = pref.edit();
                                                editor.clear();
                                                editor.apply();
                                                SharedPreferences pref1 = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
                                                SharedPreferences.Editor editor1 = pref1.edit();
                                                editor1.putString("jobid", str_jobId);
                                                editor1.putString("photostatus", "0");
                                                editor1.putString("jobinstruction", jobinstruction);
                                                editor1.putString("description", description);
                                                editor1.putString("storewhatsincluded", storewhatsincluded);
                                                editor1.putString("saveresforward", saveresforward);
                                                editor1.putString("clinetname", clientname);
                                                editor1.apply();
                                                Intent intent = new Intent(MyJobs_OnGoingDetailActivity.this, BeforeAndAfterPhotoActivity.class);
                                                startActivity(intent);
                                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                                finish();
                                            }
                                        } else {
                                            HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.waiting_for_user_approval));
                                        }


                                    } else {
                                        if (isInternetPresent) {
                                            buttonsClickActions(MyJobs_OnGoingDetailActivity.this, ServiceConstant.STARTJOB_URL, "startjob");
                                        } else {
                                            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                                        }
                                    }

                                }
                            }


                        } else if (status.equals("2")) {
                            myjobList.get(key).setOrderStatus("2");
                            myOrderAdapterss.notifyDataSetChanged();

                            isInternetPresent = cd.isConnectingToInternet();
                            if (isInternetPresent) {
//                                onBackPressed();
                                swipeRefreshLayout.setRefreshing(true);
                                myjobOngoingDetail(MyJobs_OnGoingDetailActivity.this, ServiceConstant.MYJOB_DETAIL_INFORMATION_URL);
                            } else {
                                final PkDialog mDialog = new PkDialog(MyJobs_OnGoingDetailActivity.this);
                                mDialog.setDialogTitle(getResources().getString(R.string.internet_label));
                                mDialog.setCancelOnTouchOutside(false);
                                mDialog.setDialogMessage(getResources().getString(R.string.action_no_internet_message));
                                mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mDialog.dismiss();
                                        finish();
                                    }
                                });
                                mDialog.show();
                            }
                        }
                    } else {
                        Str_message = jobject.getString("response");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (Str_status.equalsIgnoreCase("1")) {


                } else {
                    Alert(getResources().getString(R.string.server_lable_header), Str_message);
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }
        });
    }

    public class MyOrdersAdapter extends BaseAdapter {
        private ArrayList<MyOrdersPojo> myOrderArrList;
        private LayoutInflater myInflater;
        private Context myContext;

        public MyOrdersAdapter(Context aContext, ArrayList<MyOrdersPojo> aOrderArrList) {
            this.myOrderArrList = aOrderArrList;
            this.myContext = aContext;
            myInflater = LayoutInflater.from(aContext);
        }

        @Override
        public int getCount() {
            return myOrderArrList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            MyOrdersAdapter.ViewHolder holder;
            if (convertView == null) {
                holder = new MyOrdersAdapter.ViewHolder();
                convertView = myInflater.inflate(R.layout.layout_inflate_my_orders_list_item, parent, false);

                holder.jobid = convertView.findViewById(R.id.details_orderid);
                holder.imag = convertView.findViewById(R.id.imag);
                holder.jobtype = convertView.findViewById(R.id.jobtype);
                holder.username = convertView.findViewById(R.id.username);

                holder.jobstatus = convertView.findViewById(R.id.jobstatus);
                convertView.setTag(holder);

            } else {
                holder = (MyOrdersAdapter.ViewHolder) convertView.getTag();
            }

            holder.jobid.setText(myOrderArrList.get(position).getOrderId());
            holder.jobtype.setText(myOrderArrList.get(position).getOrderServiceType());
            holder.username.setText(myOrderArrList.get(position).getTaskname());
            holder.jobstatus.setText(myOrderArrList.get(position).getOrderStatus());


            if (myOrderArrList.get(position).getUserphoto().contains("http")) {
                Picasso.with(myContext).load(myOrderArrList.get(position).getUserphoto())
                        .resize(500, 500)
                        .onlyScaleDown()
                        .placeholder(R.drawable.ic_no_user)
                        .into(holder.imag);
            } else {
                if (!myOrderArrList.get(position).getUserphoto().isEmpty()) {
                    Picasso.with(myContext).load(/*ServiceConstant.Review_image +*/ myOrderArrList.get(position).getUserphoto())
                            .resize(500, 500)
                            .onlyScaleDown()
                            .placeholder(R.drawable.ic_no_user)
                            .into(holder.imag);
                } else {
                    Picasso.with(myContext).load(ServiceConstant.Review_image + myOrderArrList.get(position).getUserphoto())
                            .resize(500, 500)
                            .onlyScaleDown()
                            .placeholder(R.drawable.ic_no_user)
                            .into(holder.imag);
                }

            }


            return convertView;
        }

        private class ViewHolder {

            private TextView jobid, jobtype, username, jobstatus;
            private CircleImageView imag;

        }


    }

    public class Joblistadapter extends BaseAdapter {
        private ArrayList<joblistpojo> myOrderArrList;
        private LayoutInflater myInflater;
        private Context myContext;
        private Dialog dialog;

        private Joblistadapter(Context aContext, ArrayList<joblistpojo> aOrderArrList, Dialog dialog) {
            this.myOrderArrList = aOrderArrList;
            this.myContext = aContext;
            myInflater = LayoutInflater.from(aContext);
            this.dialog = dialog;
        }

        @Override
        public int getCount() {
            return myOrderArrList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            Joblistadapter.ViewHolder holder;
            if (convertView == null) {
                holder = new Joblistadapter.ViewHolder();
                convertView = myInflater.inflate(R.layout.joblistadapter, parent, false);

                holder.jobid = convertView.findViewById(R.id.details_orderid);
                holder.imag = convertView.findViewById(R.id.imag);
                holder.jobtype = convertView.findViewById(R.id.jobtype);
                holder.username = convertView.findViewById(R.id.username);

                holder.jobstatus = convertView.findViewById(R.id.jobstatus);
                convertView.setTag(holder);

            } else {
                holder = (Joblistadapter.ViewHolder) convertView.getTag();
            }

            holder.jobid.setText(myOrderArrList.get(position).getOrderId());
            holder.jobtype.setText(myOrderArrList.get(position).getOrderServiceType());
            holder.jobstatus.setText(myOrderArrList.get(position).getOrderStatus());

            switch (myOrderArrList.get(position).getOrderStatus()) {
                case "2":
                    holder.username.setText(getResources().getString(R.string.jobcompleted));
                    break;
                case "1":
                    holder.username.setText(getResources().getString(R.string.completejobb));
                    break;
                default:
                    holder.username.setText(getResources().getString(R.string.startjobb));
                    break;
            }

            holder.username.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    boolean CanStartNextSchedule = true;
                    if (myOrderArrList.size() > 0) {
                        for (int i = 0; i < myOrderArrList.size(); i++) {
                            if (myOrderArrList.get(i).getOrderStatus().equalsIgnoreCase("1")) {
                                CanStartNextSchedule = false;
                                break;
                            } else {
                                CanStartNextSchedule = true;
                            }
                        }
                    }
                    if (after_array.equalsIgnoreCase("0")) {
                        if (myOrderArrList.get(position).getOrderStatus().equals("0")) {
                            if (detail_btn2.getText().toString().equalsIgnoreCase("Start")) {
                                if (saveoveralljobcount.equals("0")) {
                                    if (saveoverallestimatecount.equals("1")) {
                                        if (before_status.equalsIgnoreCase("0") || before_status.equalsIgnoreCase("2")) {
                                            if (before_status.equalsIgnoreCase("0")) {

                                                if (!before_array.equalsIgnoreCase("0")) {
                                                    HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.waiting_for_user_approval));
                                                } else {
                                                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.CantStartScheduleNow));
                                                }
                                            } else {
                                                HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.waiting_for_user_approval));
                                            }
                                        } else if (before_status.equalsIgnoreCase("1")) {
                                            multiplestatus(MyJobs_OnGoingDetailActivity.this, ServiceConstant.STARTSTATUSS, position, myOrderArrList.get(position).getOrderBookingDate(), "1", dialog);
                                        } else {
                                            if (isInternetPresent) {
                                                buttonsClickActions(MyJobs_OnGoingDetailActivity.this, ServiceConstant.STARTJOB_URL, "startjob");
                                            } else {
                                                Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                                            }
                                        }
                                    } else {
                                        SharedPreferences pref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
                                        SharedPreferences.Editor editor = pref.edit();
                                        editor.putString("jobid", str_jobId);
                                        editor.putString("category", jobtypetext.getText().toString());
                                        editor.putString("location", Tv_ongoing_jobtype.getText().toString());
                                        editor.putString("date", ongoing_details_dateTv.getText().toString());
                                        editor.putString("currentamt", "0");
                                        editor.apply();
                                        editor.commit();
                                        Intent intent = new Intent(getApplicationContext(), EstimateBuilderActivity.class);
                                        startActivity(intent);
                                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                    }
                                } else {
                                    if (before_status.equalsIgnoreCase("0") || before_status.equalsIgnoreCase("2")) {
                                        if (before_status.equalsIgnoreCase("0")) {
                                            if (!before_array.equalsIgnoreCase("0")) {
                                                HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.waiting_for_user_approval));
                                            }
                                        } else {
                                            HNDHelper.showErrorAlert(MyJobs_OnGoingDetailActivity.this, getResources().getString(R.string.waiting_for_user_approval));
                                        }
                                    } else if (before_status.equalsIgnoreCase("1")) {
                                        multiplestatus(MyJobs_OnGoingDetailActivity.this, ServiceConstant.STARTSTATUSS, position, myOrderArrList.get(position).getOrderBookingDate(), "1", dialog);
                                    } else {
                                        if (isInternetPresent) {
                                            buttonsClickActions(MyJobs_OnGoingDetailActivity.this, ServiceConstant.STARTJOB_URL, "startjob");
                                        } else {
                                            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                                        }
                                    }
                                }
                            } else if (detail_btn2.getText().toString().equalsIgnoreCase("Complete Job") && myOrderArrList.get(position).getOrderStatus().equals("0") && CanStartNextSchedule) {
                                multiplestatus(MyJobs_OnGoingDetailActivity.this, ServiceConstant.STARTSTATUSS, position, myOrderArrList.get(position).getOrderBookingDate(), "1", dialog);
                            } else if (!CanStartNextSchedule) {
                                Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.CanStartNextSchedule));
                            } else if (detail_btn2.getText().toString().equalsIgnoreCase("On My Way")
                                    || detail_btn2.getText().toString().equalsIgnoreCase("Arrived")) {
                                Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.CantStartScheduleNow));
                            }
                        } else if (myOrderArrList.get(position).getOrderStatus().equals("1")) {
                            multiplestatus(MyJobs_OnGoingDetailActivity.this, ServiceConstant.STARTSTATUSS, position, myOrderArrList.get(position).getOrderBookingDate(), "2", dialog);
                        }
                    } else {
                        Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.cant_start_multi_schedule));
                    }
                }
            });


            return convertView;
        }

        private class ViewHolder {

            private TextView jobid, jobtype, username, jobstatus;
            private CircleImageView imag;

        }


    }

    void ValidEditTextAmount(final AlertDialog alertDialog, String Amount, String finalBalance_amount, String RequestType) {
        if (Amount.length() > 0) {
            if (!Amount.equals("0")) {
                PrepaymentAPICall(alertDialog, Amount, RequestType);
                /*if (Float.parseFloat(finalBalance_amount.replace(currency, ""))>Float.parseFloat(Amount)){

                }else {
                    Toast.makeText(MyJobs_OnGoingDetailActivity.this, "Requesting amount should be less than balance amount", Toast.LENGTH_SHORT).show();
                }*/
            } else {
                Toast.makeText(MyJobs_OnGoingDetailActivity.this, "Amount should be greater than zero", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(MyJobs_OnGoingDetailActivity.this, "Enter the valid amount", Toast.LENGTH_SHORT).show();
        }
    }

    void PrepaymentAPICall(final AlertDialog alertDialog, String EditTextRequestAmount, final String RequestType) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("provider_id", provider_id);
        jsonParams.put("job_id", str_jobId);
        jsonParams.put("pre_amount", EditTextRequestAmount);
        ServiceRequest mservicerequest = new ServiceRequest(MyJobs_OnGoingDetailActivity.this);
        mservicerequest.makeServiceRequest(ServiceConstant.PreRequestRequest, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                //{"status":1,"response":"Payment Request sent, please wait."}
                Log.e("pre_amount/request", response);
                String Str_status, total_amount = "", balance_amount = "", pre_amount = "";
                try {
                    JSONObject jobject = new JSONObject(response);
                    if (jobject.has("status") && jobject.getString("status").equals("1")) {
                        Toast.makeText(MyJobs_OnGoingDetailActivity.this, jobject.getString("response"), Toast.LENGTH_SHORT).show();
                        alertDialog.dismiss();
                        if (RequestType.equals("ButtonRequestCashCheque") && jobject.has("PrepaymentID")) {
                            Intent intent = new Intent(getApplicationContext(), OtpPage.class);
                            intent.putExtra("jobId", str_jobId);
                            intent.putExtra("type","prepayment");
                            intent.putExtra("Type", "pre_payment_cash_initiated");
                            ArrayList<String> PrepaymentList = new ArrayList<>();
                            PrepaymentList.add(jobject.getString("PrepaymentID"));
                            intent.putStringArrayListExtra("PrepaymentList", PrepaymentList);
                            startActivity(intent);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                alertDialog.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ServiceConstant.GetSignatureRequestCode && resultCode == RESULT_OK) {
            String bitmaa = saveim.getString("sign", "");
            if (bitmaa.equals("") || bitmaa.equals(" ")) {
                storemybase64 = "";
                // setsignimage.setVisibility(View.GONE);
            } else {
                Bitmap src = BitmapFactory.decodeFile(bitmaa);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                src.compress(Bitmap.CompressFormat.PNG, 100, baos);
                //  byte[] data = baos.toByteArray();
                storemybase64 = Base64.encodeToString(getBytesFromBitmap(src),
                        Base64.NO_WRAP);
                Log.v("log_tag", "Base64: " + storemybase64);

            }


            Intent intent = new Intent(MyJobs_OnGoingDetailActivity.this, ChatMessageServicech.class);
            startService(intent);


            SharedPreferences pref1 = getApplicationContext().getSharedPreferences("openclosed", 0);
            SharedPreferences.Editor editor1 = pref1.edit();
            editor1.putString("status", "1");
            editor1.apply();

            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("page", "0");
            prefeditor.apply();
            prefeditor.commit();

            handler.post(timedTask);
        }
    }
}