package noman.handyprotaskerapp.activity;

/**
 * Created by user127 on 02-04-2018.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import Dialog.LoadingDialog;
import Dialog.PkDialog;
import Utils.ConnectionDetector;
import Utils.CurrencySymbolConverter;
import Utils.SessionManager;
import noman.handyprotaskerapp.Adapter.PaymentFareSummeryAdapter;
import noman.handyprotaskerapp.Pojo.BeforeImagePojo;
import noman.handyprotaskerapp.Pojo.PaymentFareSummeryPojo;
import noman.handyprotaskerapp.ProgressDialog1;
import noman.handyprotaskerapp.R;
import noman.handyprotaskerapp.WidgetSupport.listhei;
import service.ServiceConstant;
import socket.SocketHandler;
import volley.ServiceRequest;


/**
 * Created by user88 on 1/8/2016.
 */
public class MoreInfoCompletePageActivity extends AppCompatActivity {
    private ConnectionDetector cd;
    private Context context;
    private SessionManager session;
    private float PreAmount = 0;

    private Boolean isInternetPresent = false;
    private boolean show_progress_status = false;
    private Handler mHandler;
    private ProgressDialog1 myDialog;
    private String provider_id = "", currencyCode;
    listhei fare_listview;
    LinearLayout Rl_layout_main;

    TextView bjobphotos;

    private TextView Tv_JobId, jobinvoice;
    private String asyntask_name = "normal";

    PaymentFareSummeryAdapter adapter;
    private ArrayList<PaymentFareSummeryPojo> farelist, TempFareList;


    private LoadingDialog dialog;
    private String Job_id = "";
    private boolean isPaymetFare = false;
    TextView custom_text_view, jobid, whatsimportant, tellabout, jobphoto, faredetails, beforejobphoto, clientname, clientnamevalue;


    private RelativeLayout Rl_layout_back;
    ScrollView fixmetop;
    LinearLayout signlinear;
    List<BeforeImagePojo> beforemovieList = new ArrayList<>();

    private SocketHandler socketHandler;
    String jobinstruction, service, description, storewhatsincluded, saveresforward, jobcompleted;
    SharedPreferences pref1;
    RecyclerView beforerecycler_view;
    private String myAnswerStr = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.moreinfoone);
        this.myDialog = new ProgressDialog1(getApplicationContext());
        this.myDialog.setCancelable(true);
        this.myDialog.setCanceledOnTouchOutside(true);

        pref1 = getApplicationContext().getSharedPreferences("arrivedjobid", 0);


        jobcompleted = pref1.getString("jobinstruction", "");


        jobinstruction = pref1.getString("jobinstruction", "");
        description = pref1.getString("description", "");
        storewhatsincluded = pref1.getString("storewhatsincluded", "");
        saveresforward = pref1.getString("saveresforward", "");

        fixmetop = (ScrollView) findViewById(R.id.fixmetop);


        signlinear = (LinearLayout) findViewById(R.id.signlinear);
        beforerecycler_view = (RecyclerView) findViewById(R.id.beforerecycler_view);
        bjobphotos = (TextView) findViewById(R.id.bjobphotos);

        bjobphotos.setPaintFlags(bjobphotos.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        clientname = (TextView) findViewById(R.id.clientname);
        clientnamevalue = (TextView) findViewById(R.id.clientnamevalue);

        String clientnamev = pref1.getString("clientname", "");
        clientnamevalue.setText(clientnamev);

        clientname.setPaintFlags(clientname.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        TextView jonspecialin = (TextView) findViewById(R.id.jonspecialin);
        jonspecialin.setPaintFlags(jonspecialin.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        TextView Whatsincludedtext = (TextView) findViewById(R.id.Whatsincludedtext);
        Whatsincludedtext.setPaintFlags(Whatsincludedtext.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        TextView whatsincluded = (TextView) findViewById(R.id.whats);
        whatsincluded.setText(storewhatsincluded);

        if (storewhatsincluded.length() > 0) {
            whatsincluded.setVisibility(View.VISIBLE);
            Whatsincludedtext.setVisibility(View.VISIBLE);
        } else {
            whatsincluded.setVisibility(View.GONE);
            Whatsincludedtext.setVisibility(View.GONE);
        }


        whatsimportant = (TextView) findViewById(R.id.whatsimportant);
        tellabout = (TextView) findViewById(R.id.tellabout);

        whatsimportant.setText("Instruction: " + jobinstruction);


        try {
            JSONObject jobject = new JSONObject(saveresforward);
            String Str_status = jobject.getString("status");

            if (Str_status.equalsIgnoreCase("1")) {


                JSONObject object = jobject.getJSONObject("response");
                JSONObject object2 = object.getJSONObject("job");


                service = "";
                JSONArray service_type = object2.getJSONArray("job_type");
                for (int b = 0; b < service_type.length(); b++) {
                    String value = "" + service_type.getString(b);
                    service += value + " ,";
                }
                if (service.endsWith(",")) {
                    service = service.substring(0, service.length() - 1);
                }

                JSONArray aCategoryArray = object2.getJSONArray("catdata");
                if (aCategoryArray.length() > 0) {
                    for (int j = 0; j < aCategoryArray.length(); j++) {
                        JSONObject aCategoryObject = aCategoryArray.getJSONObject(j);
                        JSONArray aAnswerArray = aCategoryObject.getJSONArray("answer");
                        if (aAnswerArray.length() > 0) {
                            for (int h = 0; h < aAnswerArray.length(); h++) {
                                if (myAnswerStr.equalsIgnoreCase("")) {
                                    myAnswerStr = "\n\u2022 " + aAnswerArray.getString(h);
                                } else {
                                    myAnswerStr = myAnswerStr + "\n\u2022 " + aAnswerArray.getString(h);
                                }
                            }
                        }
                    }
                }

                tellabout.setText("Description: " + "\n\u2022 " + description + myAnswerStr);

//                JSONArray aCategoryArray = object2.getJSONArray("catdata");
//                String aCreateStr = "", aFinalDisplyStr = "";
//                if (aCategoryArray.length() > 0) {
//                    for (int r = 0; r < aCategoryArray.length(); r++) {
//                        String aAnswerStr = "";
//                        JSONObject aCategoryObject = aCategoryArray.getJSONObject(r);
//                        aCreateStr = "♦ " + aCategoryObject.getString("name");
//                        JSONArray aAnswerArray = aCategoryObject.getJSONArray("answer");
//                        if (aAnswerArray.length() > 0) {
//                            for (int b = 0; b < aAnswerArray.length(); b++) {
//                                if (aAnswerStr.equalsIgnoreCase("")) {
//                                    aAnswerStr = "\t\t\t\u2022 " + aAnswerArray.getString(b);
//                                } else {
//                                    aAnswerStr = aAnswerStr + "\n\t\t\t\u2022 " + aAnswerArray.getString(b);
//                                }
//                            }
//                            aFinalDisplyStr = aFinalDisplyStr + aCreateStr + "\n" + aAnswerStr + "\n";
//                        } else {
//                            aFinalDisplyStr = aFinalDisplyStr + aCreateStr + "\n";
//                        }
//                    }
//                }
//

                TextView servicetypp = (TextView) findViewById(R.id.servicetypp);
                servicetypp.setText(service);


                TextView servicetype = (TextView) findViewById(R.id.servicetype);
                servicetype.setPaintFlags(servicetype.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


            }
        } catch (Exception e) {

        }
        initialize();

        Rl_layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });


    }

    private void initialize() {
        session = new SessionManager(MoreInfoCompletePageActivity.this);
        cd = new ConnectionDetector(MoreInfoCompletePageActivity.this);
        mHandler = new Handler();
        socketHandler = SocketHandler.getInstance(this);

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        provider_id = user.get(SessionManager.KEY_PROVIDERID);
        farelist = new ArrayList<PaymentFareSummeryPojo>();
        TempFareList = new ArrayList<PaymentFareSummeryPojo>();

        SharedPreferences pickup;
        pickup = getApplicationContext().getSharedPreferences("arrivedjobid", 0); // 0 - for private mode
        Job_id = pickup.getString("jobid", "");


        fare_listview = (listhei) findViewById(R.id.cancelreason_listView);
        Tv_JobId = (TextView) findViewById(R.id.paymentfare_jobId_);


        jobinvoice = (TextView) findViewById(R.id.jobinvoice);
        jobinvoice.setPaintFlags(jobinvoice.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        Rl_layout_back = (RelativeLayout) findViewById(R.id.layout_jobfare_back);


        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            paymentPost(MoreInfoCompletePageActivity.this, ServiceConstant.PAYMENT_URL);
            System.out.println("--------------payment-------------------" + ServiceConstant.PAYMENT_URL);
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
        }

    }

    //----------------Loading Method-----------
    Runnable dialogRunnable = new Runnable() {
        @Override
        public void run() {
            dialog = new LoadingDialog(MoreInfoCompletePageActivity.this);
            dialog.setLoadingTitle(getResources().getString(R.string.loading_in));
            dialog.show();
        }
    };

    //--------------Alert Method-----------
    private void Alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(MoreInfoCompletePageActivity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //----------------------Post method for Payment Fare------------
    private void paymentPost(Context mContext, String url) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        String provider_ids = pref.getString("provider_id", "");
        jsonParams.put("provider_id", provider_ids);
        jsonParams.put("job_id", Job_id);

        System.out.println("provider_id------------" + provider_id);
        System.out.println("job_id------------" + Job_id);

        mHandler.post(dialogRunnable);

        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("payment", response);

                String Str_status = "", Str_response = "", user_approval_status = "", Str_jobDescription = "", Str_NeedPayment = "", Str_Currency = "", Str_BtnGroup = "";


                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject object = jobject.getJSONObject("response");
                        JSONObject object2 = object.getJSONObject("job");
                        Str_jobDescription = object2.getString("job_summary");
                        Str_NeedPayment = object2.getString("need_payment");
                        Str_Currency = object2.getString("currency");

                        if (object2.has("user_approval_status")) {
                            user_approval_status = object2.getString("user_approval_status");
                        }

                        // Currency currencycode = Currency.getInstance(getLocale(Str_Currency));

                        currencyCode = CurrencySymbolConverter.getCurrencySymbol(Str_Currency);

                        Str_BtnGroup = object2.getString("btn_group");


                        beforemovieList.clear();

                        if (object2.has("signatures")) {

                            if (object2.isNull("signatures")) {

                            } else {
                                String signaturesjj = object2.getString("signatures");

                                JSONObject signaturesjjobj = new JSONObject(signaturesjj);
                                if (signaturesjjobj.has("ChangeOrderActivity")) {
                                    String changeorder = signaturesjjobj.getString("ChangeOrderActivity");
                                    if (changeorder.equals("") || changeorder.equals(" ")) {
                                    } else {
                                        BeforeImagePojo beforemovie = new BeforeImagePojo(/*ServiceConstant.BASE_URLIMAGE+*/changeorder, "Change Order");
                                        beforemovieList.add(beforemovie);
                                    }
                                }

                                if (signaturesjjobj.has("startjob")) {
                                    String startjob = signaturesjjobj.getString("startjob");
                                    if (startjob.equals("") || startjob.equals(" ")) {
                                    } else {
                                        BeforeImagePojo beforemovie = new BeforeImagePojo(/*ServiceConstant.BASE_URLIMAGE+*/startjob, "Start Job");
                                        beforemovieList.add(beforemovie);
                                    }
                                }

                                if (signaturesjjobj.has("completejob")) {
                                    String completejob = signaturesjjobj.getString("completejob");
                                    if (completejob.equals("") || completejob.equals(" ")) {
                                    } else {
                                        BeforeImagePojo beforemovie = new BeforeImagePojo(/*ServiceConstant.BASE_URLIMAGE+*/completejob, "Complete Job");
                                        beforemovieList.add(beforemovie);
                                    }
                                }

                                if (signaturesjjobj.has("estimateApproval")) {
                                    String estimateApproval = signaturesjjobj.getString("estimateApproval");
                                    if (estimateApproval.equals("") || estimateApproval.equals(" ")) {
                                    } else {
                                        BeforeImagePojo beforemovie = new BeforeImagePojo(/*ServiceConstant.BASE_URLIMAGE+*/estimateApproval, "Estimate Approval");
                                        beforemovieList.add(beforemovie);
                                    }
                                }


                                if (beforemovieList.size() > 0) {
                                    signlinear.setVisibility(View.VISIBLE);
                                    LinearLayoutManager layoutManager
                                            = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                                    beforeimageadapter mAdapter = new beforeimageadapter(beforemovieList, getApplicationContext());
                                    beforerecycler_view.setLayoutManager(layoutManager);
                                    beforerecycler_view.setAdapter(mAdapter);
                                } else {
                                    signlinear.setVisibility(View.GONE);
                                    beforerecycler_view.setVisibility(View.GONE);
                                }


                            }
                        }

                        if (object2.has("user_pre_amount")) {
                            JSONArray user_pre_amount_obj = object2.getJSONArray("user_pre_amount");
                            for (int i = 0; i < user_pre_amount_obj.length(); i++) {
                                if (user_pre_amount_obj.getJSONObject(i).has("payment_status")
                                        && user_pre_amount_obj.getJSONObject(i).has("pre_amount")
                                        && user_pre_amount_obj.getJSONObject(i).getString("payment_status").equals("Accepted")) {
                                    PreAmount = PreAmount + Float.parseFloat(user_pre_amount_obj.getJSONObject(i).getString("pre_amount"));
                                }

                            }
                        }


                        JSONArray jarry = object2.getJSONArray("billing");

                        if (jarry.length() > 0) {

                            for (int i = 0; i < jarry.length(); i++) {
                                JSONObject jobjects_amount = jarry.getJSONObject(i);
                                PaymentFareSummeryPojo pojo = new PaymentFareSummeryPojo();

                                String title = jobjects_amount.getString("title");

                                String desc = "";
                                if (jobjects_amount.has("description")) {
                                    desc = jobjects_amount.getString("description");
                                } else {
                                    desc = "";
                                }


                                pojo.setPayment_desc(desc);
                                if (user_approval_status.equalsIgnoreCase("1")) {


                                    if (jobjects_amount.getString("title").equalsIgnoreCase("Total amount")) {
                                        pojo.setPayment_title(jobjects_amount.getString("title"));

                                        if (title.contains("Hours") || title.contains("Payment mode") || title.contains("Task type") || title.contains("Start Date") || title.contains("Time") || title.contains("Month") || title.contains("End Date")) {
                                            pojo.setPayment_amount("-");
                                        } else {
                                            pojo.setPayment_amount("-");
                                        }
                                    } else {

                                        if (jobjects_amount.has("description")) {

                                            pojo.setPayment_title(jobjects_amount.getString("title"));

                                            if (title.contains("Hours") || title.contains("Payment mode") || title.contains("Task type") || title.contains("Start Date") || title.contains("Time") || title.contains("Month") || title.contains("End Date")) {
                                                pojo.setPayment_amount("-");
                                            } else {
                                                pojo.setPayment_amount("-");
                                            }

                                        } else {
                                            pojo.setPayment_title(jobjects_amount.getString("title"));

                                            if (title.contains("Hours") || title.contains("Payment mode") || title.contains("Task type") || title.contains("Start Date") || title.contains("Time") || title.contains("Month") || title.contains("End Date")) {
                                                pojo.setPayment_amount(jobjects_amount.getString("amount"));
                                            } else {
                                                pojo.setPayment_amount(currencyCode + jobjects_amount.getString("amount"));
                                            }
                                        }

                                    }

                                } else {

                                    pojo.setPayment_title(jobjects_amount.getString("title"));

                                    if (title.contains("Hours") || title.contains("Payment mode") || title.contains("Task type") || title.contains("Start Date") || title.contains("Time") || title.contains("Month") || title.contains("End Date")) {
                                        pojo.setPayment_amount(jobjects_amount.getString("amount"));
                                    } else {
                                        pojo.setPayment_amount(currencyCode + jobjects_amount.getString("amount"));
                                    }
                                }


                                System.out.println("payment1---------------------------" + jobjects_amount.getString("title"));
                                farelist.add(pojo);
                                TempFareList.add(pojo);
                            }
                            show_progress_status = true;
                        } else {
                            show_progress_status = false;
                        }

                        fixmetop.fullScroll(ScrollView.FOCUS_UP);
                    } else {
                        Str_response = jobject.getString("response");
                    }

                    System.out.println("payment1---------------------------");

                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (Str_status.equalsIgnoreCase("1")) {

                    System.out.println();


                    Tv_JobId.setText(Job_id);

                    System.out.println();
                    fare_listview.setEnabled(false);

                    if (PreAmount != 0 && PreAmount > 0) {
                        PaymentFareSummeryPojo pojo = new PaymentFareSummeryPojo();
                        pojo.setPayment_title("Prepayment");
                        pojo.setPayment_amount(currencyCode + String.valueOf(PreAmount));
                        TempFareList.set((TempFareList.size() - 1), pojo);
                        pojo = new PaymentFareSummeryPojo();
                        pojo.setPayment_title(farelist.get(farelist.size() - 1).getPayment_title());
                        pojo.setPayment_amount(farelist.get(farelist.size() - 1).getPayment_amount());
                        TempFareList.add(pojo);
                        adapter = new PaymentFareSummeryAdapter(MoreInfoCompletePageActivity.this, TempFareList);
                    } else if (PreAmount == 0) {
                        adapter = new PaymentFareSummeryAdapter(MoreInfoCompletePageActivity.this, farelist);
                    }

                    fare_listview.setAdapter(adapter);

//                    if (Str_NeedPayment.equalsIgnoreCase("1")) {
//                        Rl_layout_farepayment_methods.setVisibility(View.VISIBLE);
//                    } else {
//                        Rl_layout_farepayment_methods.setVisibility(View.GONE);
//                    }


                } else {
                    Alert(getResources().getString(R.string.server_lable_header), Str_response);
                }

                dialog.dismiss();

            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    //method to convert currency code to currency symbol
    private static Locale getLocale(String strCode) {

        for (Locale locale : NumberFormat.getAvailableLocales()) {
            String code = NumberFormat.getCurrencyInstance(locale).getCurrency().getCurrencyCode();
            if (strCode.equals(code)) {
                return locale;
            }
        }
        return null;
    }


    @Override
    public void onResume() {
        super.onResume();
//starting XMPP service
      /*  if (!socketHandler.getSocketManager().isConnected){
            socketHandler.getSocketManager().connect();
        }*/
    }

    public class beforeimageadapter extends RecyclerView.Adapter<beforeimageadapter.MyViewHolder> {

        private List<BeforeImagePojo> moviesList;
        Context ctxx;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public ImageView image;
            TextView textme;

            public MyViewHolder(View view) {
                super(view);
                image = (ImageView) view.findViewById(R.id.image);
                textme = (TextView) view.findViewById(R.id.textme);
            }
        }


        public beforeimageadapter(List<BeforeImagePojo> moviesList, Context ctx) {
            this.moviesList = moviesList;
            this.ctxx = ctx;
        }

        @Override
        public beforeimageadapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.imte, parent, false);

            return new beforeimageadapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(beforeimageadapter.MyViewHolder holder, int position) {
            BeforeImagePojo movie = moviesList.get(position);

            holder.textme.setText(movie.getTextname());
            if (movie.getTitle().isEmpty()) {
                holder.image.setImageResource(R.drawable.noimageavailable);
            } else {
                Picasso.with(ctxx)
                        .load(movie.getTitle())
                        .placeholder(R.drawable.noimageavailable)   // optional
                        .error(R.drawable.noimageavailable)      // optional
                        // optional
                        .into(holder.image);
            }


        }

        @Override
        public int getItemCount() {
            return moviesList.size();
        }
    }
}
