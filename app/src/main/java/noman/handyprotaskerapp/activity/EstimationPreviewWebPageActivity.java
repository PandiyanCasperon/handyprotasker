package noman.handyprotaskerapp.activity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.Request;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import Dialog.LoadingDialog;
import Dialog.PkDialog;
import Utils.ConnectionDetector;
import noman.handyprotaskerapp.R;
import service.ServiceConstant;
import volley.ServiceRequest;

public class EstimationPreviewWebPageActivity extends AppCompatActivity {
    private WebView WebViewMain;
    private ConnectionDetector cd;
    private LoadingDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estimation_preview_web_page);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setTitle(getResources().getString(R.string.estimation_preview));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        cd = new ConnectionDetector(EstimationPreviewWebPageActivity.this);

        WebViewMain = findViewById(R.id.WebViewMain);

        WebViewMain.getSettings().setBuiltInZoomControls(true);
        if (getIntent().hasExtra("Job_ID")) {
            if (cd.isConnectingToInternet()) {
                loadingDialog();
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("job_id", getIntent().getStringExtra("Job_ID"));
                ServiceRequest mservicerequest = new ServiceRequest(this);
                mservicerequest.makeServiceRequest(ServiceConstant.ESTIMATION_PREVIEW, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
                    @Override
                    public void onCompleteListener(String response) {
                        dismissDialog();

                        try {
                            JSONObject resJsonObject = new JSONObject(response);
                            if (resJsonObject.getString("status").equals("1")) {
                                if (resJsonObject.has("result")) {
                                    WebViewMain.loadDataWithBaseURL(null, resJsonObject.getString("result"), "text/html; charset=UTF-8;", "UTF-8", null);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorListener() {
                        Toast.makeText(EstimationPreviewWebPageActivity.this, "Something went wrong, please try again!", Toast.LENGTH_SHORT).show();
                        dismissDialog();
                    }
                });

            } else {
                final PkDialog mDialog = new PkDialog(EstimationPreviewWebPageActivity.this);
                mDialog.setDialogTitle(getResources().getString(R.string.internet_label));
                mDialog.setDialogMessage(getResources().getString(R.string.no_inetnet_label));
                mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                    }
                });
                mDialog.show();
            }
        }


    }

    private void loadingDialog() {

        dialog = new LoadingDialog(EstimationPreviewWebPageActivity.this);
        dialog.setLoadingTitle(getResources().getString(R.string.loading_in));
        dialog.show();
    }

    private void dismissDialog() {

        dialog.dismiss();
    }
}
