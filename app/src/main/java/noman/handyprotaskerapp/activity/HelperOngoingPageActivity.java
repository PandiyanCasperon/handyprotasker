package noman.handyprotaskerapp.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Dialog.PkDialog;
import Map.GPSTracker;
import Utils.ConnectionDetector;
import Utils.GMapV2GetRouteDirection;
import Utils.SessionManager;
import Utils.onItemRemoveClickListener;
import de.hdodenhof.circleimageview.CircleImageView;
import me.drakeet.materialdialog.MaterialDialog;
import noman.handyprotaskerapp.Adapter.AcceptImageAdapter;
import noman.handyprotaskerapp.Adapter.AfterImageAdapter;
import noman.handyprotaskerapp.Adapter.BeforeImageAdapter;
import noman.handyprotaskerapp.Service.HNDHelper;
import noman.handyprotaskerapp.Pojo.AfterImagePojo;
import noman.handyprotaskerapp.Pojo.BeforeImagePojo;
import noman.handyprotaskerapp.Pojo.MyOrdersPojo;
import noman.handyprotaskerapp.Pojo.acceptimage;
import noman.handyprotaskerapp.Pojo.changeorderpojo;
import noman.handyprotaskerapp.Pojo.joblistpojo;
import noman.handyprotaskerapp.ProgressDialog1;
import noman.handyprotaskerapp.R;
import noman.handyprotaskerapp.ShakingBell;
import noman.handyprotaskerapp.Track_your_ride;
import noman.handyprotaskerapp.WidgetSupport.listhei;
import service.ServiceConstant;
import socket.ChatMessageServicech;
import socket.SocketHandler;
import socket.SocketManager;
import volley.ServiceRequest;


/**
 * Created by user88 on 12/12/2015.
 */
public class HelperOngoingPageActivity extends AppCompatActivity implements com.google.android.gms.location.LocationListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, SeekBar.OnSeekBarChangeListener, onItemRemoveClickListener, OnMapReadyCallback {

    private TextView jobtypetext, ongoing_details_dateTv, Tv_ongoing_jobtype, Tv_ongoing_detail_jobinstruction;
    Handler handler = new Handler();
    String saveoveralljobcount = "0";
    String task_id, user_id;
    String changeordervisible;
    String duration = "0";
    MyOrdersAdapter myOrderAdapter;
    String before_status = "";
    String before_array = "";
    listhei joblistmore;
    LinearLayout whatsincluded;
    String after_status = "";
    String after_array = "";

    String storewhatsincluded = "";
    int storewhatsincludedlenghth = 0;

    private ArrayList<MyOrdersPojo> myOrdersArrList;
    private ImageView Img_Chat, Img_Call, Img_Message, Img_Email;
    private String Str_usermobile = "";
    private String destination_lat;
    private String destination_long, cancel_reson = "";
    String sendlat = "";
    String jobinstruction, description;
    String sendlng = "";
    SupportMapFragment mapFragment;
    GPSTracker gps;
    int statushide = 0;
    private ProgressDialog1 myDialog;

    private Handler mHandler;
    String zipcodestore;
    String saveprovide_estimatio = "0";
    public static String str_jobId = "", jobtype = "", sendaddress = "", senddatetime = "", sendphoto = "", sendusername = "";
    double myLatitude, myLongitude;
    LinearLayout hidemap;
    private double strlat, strlon;


    String usersharelat;

    String address = "";
    String jsonformat = "";
    final int PERMISSION_REQUEST_CODE = 111;
    List<acceptimage> movieList = new ArrayList<>();
    List<BeforeImagePojo> beforemovieList = new ArrayList<>();
    List<AfterImagePojo> aftermovieList = new ArrayList<>();
    final int PERMISSION_REQUEST_CODES = 222;
    final int PERMISSION_REQUEST_LOCATION = 333;
    private final static int REQUEST_LOCATION = 199;
    private PendingResult<LocationSettingsResult> result;
    SessionManager session;
    ConnectionDetector cd;
    private Boolean isInternetPresent = false;
    private String pending_multi_schedule = null, not_start_multi_schedule = null, completed_multi_schedule = null;


    private Marker currentMarker;


    public static String Str_Userid = "";

    public static MaterialDialog completejob_dialog;
    private ArrayList<joblistpojo> myjobList;
    private StringRequest postrequest;
    public static String provider_id = "";
    private String Str_job_group = "";
    ImageView lotjob;
    private double MyCurrent_lat = 0.0, MyCurrent_long = 0.0;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    public static Location myLocation;
    public static String mTaskID;
    private EditText Et_jobfare;
    private EditText Et_job_cost;
    private TextView Tv_OrderId;
    private String Str_Orderid = "";
    private ImageView track_location;
    Joblistadapter myOrderAdapterss;
    private SocketHandler socketHandler;
    GMapV2GetRouteDirection v2GetRouteDirection;
    static SocketManager smanager;
    private View moreAddressView;
    //  public static MaterialAddAdapter aAdapter;

    public static Dialog moreAddressDialog;
    ArrayList<String> listItems;
    public static ListView list;

    public static boolean item_add_bollean = false;

    public static RelativeLayout aCancelLAY;
    public static RelativeLayout aOKLAY;
    public static CheckBox addmaterial;
    private String Str_user_email = "";
    LinearLayout openlist, craftmanlayout;
    String saveoverallestimatecount = "0";

    public static AppCompatActivity job_page_activity;
    private String share_text = "";

    private RelativeLayout show_moretext_layout;
    private TextView show_more;
    private String Job_Instruction = "";
    CircleImageView imag;
    TextView usernameee;
    LinearLayout back_ongoingback;

    ArrayList<changeorderpojo> getarrayList = new ArrayList<changeorderpojo>();
    ListView activity_my_orders_listView;
    ImageView imagechange;
    TextView nooftaskers, details_purchaseid_textView, details_purchaseid,helpername;
    LinearLayout showphoto;
    ShakingBell shakingBell;
    SwipeRefreshLayout swipeRefreshLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.helperlayout);

        myjobList = new ArrayList<joblistpojo>();

        movieList.clear();
        beforemovieList.clear();
        aftermovieList.clear();
        myOrdersArrList = new ArrayList<MyOrdersPojo>();
        job_page_activity = HelperOngoingPageActivity.this;
        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        provider_id = pref.getString("provider_id", "");
        initialize();
        try {
            setLocationRequest();
            buildGoogleApiClient();
        } catch (Exception e) {
            e.printStackTrace();
        }
        initilizeMap();


        Img_Call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Str_usermobile != null) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        // Marshmallow+
                        if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                            requestPermission();
                        } else {
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:" + Str_usermobile));
                            startActivity(callIntent);
                        }
                    } else {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + Str_usermobile));
                        startActivity(callIntent);
                    }

                } else {
                    Alert(HelperOngoingPageActivity.this.getResources().getString(R.string.server_lable_header), HelperOngoingPageActivity.this.getResources().getString(R.string.arrived_alert_content1));
                }


            }
        });


        Img_Message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    share_text = getResources().getString(R.string.ongoing_detail_shar_text);
                    sms_sendMsg(share_text);

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Your call has failed...", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        });


        Img_Email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{Str_user_email});
                i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.ongoing_detail_subject));
                i.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.ongoing_detail_text) + " ");
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    // Toast.makeText(SettingsPage.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {

                    swipeRefreshLayout.setRefreshing(true);
                    myjobOngoingDetail(HelperOngoingPageActivity.this, ServiceConstant.MYJOB_DETAIL_INFORMATION_URL);

                } else {
                    final PkDialog mDialog = new PkDialog(HelperOngoingPageActivity.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.internet_label));
                    mDialog.setCancelOnTouchOutside(false);
                    mDialog.setDialogMessage(getResources().getString(R.string.action_no_internet_message));
                    mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                            finish();
                        }
                    });
                    mDialog.show();
                }
            }
        });

    }


    private void sms_sendMsg(String share_text) {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (!checkSmsPermission()) {
                requestPermissionSMS();
            } else {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.putExtra("address", Str_usermobile);
                intent.putExtra("sms_body", share_text);
                intent.setData(Uri.parse("smsto:" + Str_usermobile));
                startActivity(intent);
            }
        } else {
            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
            sendIntent.putExtra("sms_body", share_text);
            sendIntent.putExtra("address", Str_usermobile);
            sendIntent.setType("vnd.android-dir/mms-sms");
            startActivity(sendIntent);
        }
    }


    public void showalert(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(HelperOngoingPageActivity.this);
        builder.setTitle(getString(R.string.dialog_title));
        builder.setMessage(message);

        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        String negativeText = getString(android.R.string.cancel);


        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
    }


    private boolean checkSmsPermission() {
        int result = ContextCompat.checkSelfPermission(HelperOngoingPageActivity.this, Manifest.permission.SEND_SMS);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }


    private void requestPermissionSMS() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, PERMISSION_REQUEST_CODES);
    }


    private void initialize() {


        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.myjob_ongoing_swipe_refresh_layout);
        shakingBell = (ShakingBell) findViewById(R.id.shakeBell);
        shakingBell.shake(1);
        imagechange = (ImageView) findViewById(R.id.imagechange);
        openlist = (LinearLayout) findViewById(R.id.openlist);
        craftmanlayout = (LinearLayout) findViewById(R.id.craftmanlayout);

        activity_my_orders_listView = (ListView) findViewById(R.id.activity_my_orders_listView);
        hidemap = (LinearLayout) findViewById(R.id.hidemap);
        nooftaskers = (TextView) findViewById(R.id.nooftaskers);
        details_purchaseid_textView = (TextView) findViewById(R.id.details_purchaseid_textView);
        details_purchaseid = (TextView) findViewById(R.id.details_purchaseid);
        helpername = (TextView)findViewById(R.id.helpername);

        showphoto = (LinearLayout) findViewById(R.id.showphoto);
        whatsincluded = (LinearLayout) findViewById(R.id.whatsincluded);


        lotjob = (ImageView) findViewById(R.id.lotjob);

        lotjob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(HelperOngoingPageActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.fullscreendialog);
                joblistmore = (listhei) dialog.findViewById(R.id.jobrecycler_view);
                myOrderAdapterss = new Joblistadapter(HelperOngoingPageActivity.this, myjobList);
                joblistmore.setEnabled(false);
                joblistmore.setAdapter(myOrderAdapterss);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                dialog.getWindow().setAttributes(lp);
                dialog.show();
            }
        });


        showphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RecyclerView recyclerView, beforerecyclerView, afterrecycler_view;
                TextView tellabout, whatsimportant, whats;
                LinearLayout userphoto, beforephoto, afterphoto, whatsincluded;

                View modalbottomsheet = getLayoutInflater().inflate(R.layout.bottomdialog_layout, null);
                BottomSheetDialog dialog = new BottomSheetDialog(HelperOngoingPageActivity.this);
                dialog.setContentView(modalbottomsheet);
                dialog.setCanceledOnTouchOutside(true);
                dialog.setCancelable(true);

                tellabout = (TextView) modalbottomsheet.findViewById(R.id.tellabout);
                whatsimportant = (TextView) modalbottomsheet.findViewById(R.id.whatsimportant);

                tellabout.setText(description);
                whatsimportant.setText(jobinstruction);

                userphoto = (LinearLayout) modalbottomsheet.findViewById(R.id.userphoto);
                beforephoto = (LinearLayout) modalbottomsheet.findViewById(R.id.beforephoto);
                afterphoto = (LinearLayout) modalbottomsheet.findViewById(R.id.afterphoto);
                whatsincluded = (LinearLayout) modalbottomsheet.findViewById(R.id.whatsincluded);
                whats = (TextView) modalbottomsheet.findViewById(R.id.whats);

                whats.setText(storewhatsincluded);
                if (storewhatsincludedlenghth > 0) {
                    whatsincluded.setVisibility(View.VISIBLE);
                } else {
                    whatsincluded.setVisibility(View.GONE);
                }

                recyclerView = (RecyclerView) modalbottomsheet.findViewById(R.id.recycler_view);
                beforerecyclerView = (RecyclerView) modalbottomsheet.findViewById(R.id.beforerecycler_view);
                afterrecycler_view = (RecyclerView) modalbottomsheet.findViewById(R.id.afterrecycler_view);

                if (movieList.size() > 0) {
                    userphoto.setVisibility(View.VISIBLE);
                    LinearLayoutManager layoutManager
                            = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                    AcceptImageAdapter mAdapter = new AcceptImageAdapter(movieList, getApplicationContext());
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(mAdapter);
                } else {
                    userphoto.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);
                }


                if (beforemovieList.size() > 0) {
                    beforephoto.setVisibility(View.VISIBLE);
                    LinearLayoutManager layoutManager
                            = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                    BeforeImageAdapter mAdapter = new BeforeImageAdapter(beforemovieList, getApplicationContext());
                    beforerecyclerView.setLayoutManager(layoutManager);
                    beforerecyclerView.setAdapter(mAdapter);
                } else {
                    beforephoto.setVisibility(View.GONE);
                    beforerecyclerView.setVisibility(View.GONE);
                }


                if (aftermovieList.size() > 0) {
                    afterphoto.setVisibility(View.VISIBLE);
                    LinearLayoutManager layoutManager
                            = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                    AfterImageAdapter mAdapter = new AfterImageAdapter(aftermovieList, getApplicationContext());
                    afterrecycler_view.setLayoutManager(layoutManager);
                    afterrecycler_view.setAdapter(mAdapter);
                } else {
                    afterphoto.setVisibility(View.GONE);
                    afterrecycler_view.setVisibility(View.GONE);
                }


                dialog.show();


            }
        });


        if (statushide == 0) {
            imagechange.setBackgroundResource(R.drawable.rightopen);
        } else {
            imagechange.setBackgroundResource(R.drawable.expandarrow);
        }

        openlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (statushide == 0) {
                    imagechange.setBackgroundResource(R.drawable.rightopen);
                    statushide = 1;
                    hidemap.setVisibility(View.GONE);
                    activity_my_orders_listView.setVisibility(View.VISIBLE);
                } else {
                    imagechange.setBackgroundResource(R.drawable.expandarrow);
                    statushide = 0;
                    hidemap.setVisibility(View.VISIBLE);
                    activity_my_orders_listView.setVisibility(View.GONE);
                }


            }
        });


        session = new SessionManager(HelperOngoingPageActivity.this);
        cd = new ConnectionDetector(HelperOngoingPageActivity.this);
        gps = new GPSTracker(HelperOngoingPageActivity.this);
        mHandler = new Handler();
        socketHandler = SocketHandler.getInstance(this);

        // get user data from session


        System.out.println("sessionproviderId=-------------------" + provider_id);


        SharedPreferences pref = getApplicationContext().getSharedPreferences("ongoingpage", MODE_PRIVATE);
        str_jobId = pref.getString("jobid", "");


        jobtypetext = (TextView) findViewById(R.id.jobtype);
        System.out.println("dispaly jobid-------------------" + str_jobId);

        imag = (CircleImageView) findViewById(R.id.imag);

        back_ongoingback = (LinearLayout) findViewById(R.id.layout_back_ongoingback);
        back_ongoingback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences pref1 = getApplicationContext().getSharedPreferences("needrefresh", 0);
                String whichclass = pref1.getString("yesno", " ");
                if (whichclass.equals("0") || whichclass.equals("2")) {
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                } else if (whichclass.equals("1")) {

                    /*Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("EXIT", true);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);*/

                    Intent intent = new Intent(getApplicationContext(), WeeklyCalendarActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("EXIT", true);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                } else if (whichclass.equals("3")) {

                    Intent intent = new Intent(getApplicationContext(), MyJobsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("status", "0");
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }


            }
        });


        usernameee = (TextView) findViewById(R.id.username);

        Tv_ongoing_jobtype = (TextView) findViewById(R.id.ongoing_detail_jobtype);


        ongoing_details_dateTv = (TextView) findViewById(R.id.ongoing_details_dateTv);


        track_location = (ImageView) findViewById(R.id.track_location);
        Tv_ongoing_detail_jobinstruction = (TextView) findViewById(R.id.ongoing_details_instruction);

        track_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (cd.isConnectingToInternet()) {
                    CheckPermissions();
                } else {
                    Alert(getResources().getString(R.string.sorry), getResources().getString(R.string.alert_nointernet));
                }


            }
        });


        Tv_OrderId = (TextView) findViewById(R.id.details_orderid);
        Tv_OrderId.setText(str_jobId);
        Img_Chat = (ImageView) findViewById(R.id.detail_chat_img);
        Img_Call = (ImageView) findViewById(R.id.detail_phone_img);
        Img_Message = (ImageView) findViewById(R.id.detail_message_img);
        Img_Email = (ImageView) findViewById(R.id.detail_email_img);


        Img_Chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ChatMessageServicech.user_id = "";
                ChatMessageServicech.task_id = "";
                Intent intent = new Intent(HelperOngoingPageActivity.this, ChatPageActivity.class);
                intent.putExtra("chatpage", true);
                intent.putExtra("JOBID", str_jobId);
                intent.putExtra("TaskerId", user_id);
                intent.putExtra("TaskId", task_id);
                startActivity(intent);
            }
        });


        show_moretext_layout = (RelativeLayout) findViewById(R.id.show_moretext_layout);
        show_more = (TextView) findViewById(R.id.show_more);
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {

            movieList.clear();
            beforemovieList.clear();
            aftermovieList.clear();
            myjobOngoingDetail(HelperOngoingPageActivity.this, ServiceConstant.MYJOB_DETAIL_INFORMATION_URL);
            System.out.println("--------------detail-------------------" + ServiceConstant.MYJOB_DETAIL_INFORMATION_URL);
        } else {
            final PkDialog mDialog = new PkDialog(HelperOngoingPageActivity.this);
            mDialog.setDialogTitle(getResources().getString(R.string.internet_label));
            mDialog.setCancelOnTouchOutside(false);
            mDialog.setDialogMessage(getResources().getString(R.string.action_no_internet_message));
            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                    finish();
                }
            });
            mDialog.show();
        }


    }


    private void setLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    protected void startLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            if (result == PackageManager.PERMISSION_GRANTED) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

            }
        }

    }


    private void initilizeMap() {


        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.myjob_ongoing_detail_map);
        mapFragment.getMapAsync(HelperOngoingPageActivity.this);

        //     googleMap = ((MapFragment) HelperOngoingPageActivity.this.getFragmentManager().findFragmentById(R.id.myjob_ongoing_detail_map)).getMap();
        // check if map is created successfully or not

    }


    //--------------Alert Method-----------
    private void Alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(HelperOngoingPageActivity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    //-----------------------------------------------------------Connect to Chat---------------------------------------------------


    //-------------------------code for myjobs ongoing detail----------------
    private void myjobOngoingDetail(Context mContext, String url) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("provider_id", provider_id);
        jsonParams.put("job_id", str_jobId);
        myDialog = new ProgressDialog1(HelperOngoingPageActivity.this);
        myDialog.setCancelable(true);
        myDialog.setCanceledOnTouchOutside(true);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                swipeRefreshLayout.setRefreshing(false);
                System.out.println("--------------reponsedetail-------------------" + response);
                Log.e("detail", response);
                String Str_status = "", Str_jobId = "", Str_Response = "", Str_currencycode = "", Str_jobdate = "", Str_jobtime = "", Str_jobtype = "",
                        Str_jobinstruction = "", Str_jobstatus = "", Str_jobusername = "", Str_userimg = "", Str_job_userrating = "", Str_job_location = "", Str_location_lattitude = "", Str_location_longitude = "";

                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");

                    if (Str_status.equalsIgnoreCase("1")) {
                        movieList.clear();
                        beforemovieList.clear();
                        aftermovieList.clear();
                        JSONObject object = jobject.getJSONObject("response");
                        JSONObject object2 = object.getJSONObject("job");
                        JSONArray service_type1 = object2.getJSONArray("helper_category");

                        if (object.has("completed_multi_schedule")) {
                            completed_multi_schedule = object.getString("completed_multi_schedule");
                        } else {
                            completed_multi_schedule = null;
                        }

                        if (object.has("pending_multi_schedule")) {
                            pending_multi_schedule = object.getString("pending_multi_schedule");
                        } else {
                            pending_multi_schedule = null;
                        }

                        if (object.has("not_start_multi_schedule")) {
                            not_start_multi_schedule = object.getString("not_start_multi_schedule");
                        } else {
                            not_start_multi_schedule = null;
                        }

                        if (object2.has("purchase_order")) {
                            details_purchaseid_textView.setVisibility(View.VISIBLE);
                            details_purchaseid.setVisibility(View.VISIBLE);
                            details_purchaseid.setText(object2.getString("purchase_order"));
                        } else {
                            details_purchaseid_textView.setVisibility(View.GONE);
                            details_purchaseid.setVisibility(View.GONE);
                        }

                        if (object2.has("parent_tasker")) {
                            helpername.setText(object2.getString("parent_tasker"));
                        } else {
                            helpername.setVisibility(View.GONE);
                        }


                        if (object2.has("helperschedules")) {
                            JSONArray multipleschedules = object2.getJSONArray("helperschedules");
                            if (multipleschedules.length() > 0) {
                                myjobList.clear();
                                for (int b1 = 0; b1 < multipleschedules.length(); b1++) {
                                    JSONObject getarrayvalue = multipleschedules.getJSONObject(b1);
                                    String date = "" + getarrayvalue.getString("date");
                                    String starttime = "" + getarrayvalue.getString("starttime");
                                    String endtime = "" + getarrayvalue.getString("endtime");
                                    String schedulestatus = "" + getarrayvalue.getString("schedulestatus");
                                    String schedule_id = "" + getarrayvalue.getString("_id");
                                    joblistpojo aOrderPojo = new joblistpojo();
                                    aOrderPojo.setOrderId(date);
                                    aOrderPojo.setOrderServiceType("Job time : " + starttime + " to " + endtime);
                                    aOrderPojo.setOrderStatus(schedulestatus);
                                    aOrderPojo.setOrderBookingDate(schedule_id);
                                    myjobList.add(aOrderPojo);
                                }
                                lotjob.setVisibility(View.VISIBLE);
                            } else {
                                lotjob.setVisibility(View.GONE);
                            }

                        } else {
                            lotjob.setVisibility(View.GONE);
                        }


                        if (service_type1.length() == 0) {
                            craftmanlayout.setVisibility(View.GONE);
                            imagechange.setBackgroundResource(R.drawable.expandarrow);
                            statushide = 1;
                            hidemap.setVisibility(View.VISIBLE);
                            activity_my_orders_listView.setVisibility(View.GONE);


                        } else {
                            craftmanlayout.setVisibility(View.VISIBLE);
                            statushide = 0;
                            if (statushide == 0) {
                                imagechange.setBackgroundResource(R.drawable.rightopen);
                            } else {
                                imagechange.setBackgroundResource(R.drawable.expandarrow);
                            }

                            if (statushide == 0) {
                                imagechange.setBackgroundResource(R.drawable.rightopen);
                                statushide = 0;
                                hidemap.setVisibility(View.GONE);
                                activity_my_orders_listView.setVisibility(View.VISIBLE);
                            } else {
                                imagechange.setBackgroundResource(R.drawable.expandarrow);
                                statushide = 1;
                                hidemap.setVisibility(View.VISIBLE);
                                activity_my_orders_listView.setVisibility(View.GONE);
                            }
                        }

                        nooftaskers.setText(getResources().getString(R.string.helper_assigned_1) + " " + service_type1.length() + " " + getResources().getString(R.string.headerBar_name_category_label));
                        myOrdersArrList.clear();
                        for (int b1 = 0; b1 < service_type1.length(); b1++) {
                            JSONObject getarrayvalue = service_type1.getJSONObject(b1);
                            String jobiddd = "" + getarrayvalue.getString("service_type");
                            String service_id = "" + getarrayvalue.getString("service_id");
                            String orderstatus = "";
                            if (getarrayvalue.getString("helper_status").equalsIgnoreCase("2")) {
                                orderstatus = "Start Off";
                            } else if (getarrayvalue.getString("helper_status").equalsIgnoreCase("3")) {
                                orderstatus = "Arrived";
                            } else if (getarrayvalue.getString("helper_status").equalsIgnoreCase("4")) {
                                orderstatus = "Start job";
                            } else if (getarrayvalue.getString("helper_status").equalsIgnoreCase("5")) {
                                orderstatus = "Complete job";
                            } else if (getarrayvalue.getString("helper_status").equalsIgnoreCase("6")) {
                                orderstatus = "Job completed";
                            }


                            MyOrdersPojo aOrderPojo = new MyOrdersPojo();
                            aOrderPojo.setOrderId(service_id);
                            aOrderPojo.setOrderServiceType(jobiddd);
                            aOrderPojo.setTaskname(getarrayvalue.getString("helper_status"));
                            aOrderPojo.setUserphoto("");
                            aOrderPojo.setOrderStatus(orderstatus);
                            myOrdersArrList.add(aOrderPojo);

                        }


                        if (service_type1.length() > 0) {
                            myOrderAdapter = new MyOrdersAdapter(HelperOngoingPageActivity.this, myOrdersArrList);
                            activity_my_orders_listView.setAdapter(myOrderAdapter);
                        }

                        Str_Userid = object2.getString("user_id");

                        String role = object2.getString("role");


                        if (role.equalsIgnoreCase("Helper")) {

                        } else {
                            finish();
                        }


                        jobinstruction = object2.getString("instruction");
                        description = object2.getString("job_description");

                        before_status = object2.getString("before_status");

                        after_status = object2.getString("after_status");


                        JSONArray taskimage_type = object2.getJSONArray("user_taskimages");
                        for (int b = 0; b < taskimage_type.length(); b++) {
                            String value = "" + taskimage_type.getString(b);

                            if (value.startsWith("http://") || value.startsWith("https://")) {
                                value = value;
                            } else {
                                value = ServiceConstant.BASE_URLIMAGE + value;
                            }
                            acceptimage movie = new acceptimage(value);
                            movieList.add(movie);
                        }


                        JSONArray beforetaskimage_type = object2.getJSONArray("before_taskimages");
                        before_array = "" + beforetaskimage_type.length();

                        for (int b = 0; b < beforetaskimage_type.length(); b++) {
                            String value = "" + beforetaskimage_type.getString(b);

                            if (value.startsWith("http://") || value.startsWith("https://")) {
                                value = value;
                            } else {
                                value = ServiceConstant.BASE_URLIMAGE + value;
                            }
                            BeforeImagePojo beforemovie = new BeforeImagePojo(value, "");
                            beforemovieList.add(beforemovie);
                        }


                        JSONArray aftertaskimage_type = object2.getJSONArray("after_taskimages");
                        after_array = "" + aftertaskimage_type.length();

                        for (int b = 0; b < aftertaskimage_type.length(); b++) {
                            String value = "" + aftertaskimage_type.getString(b);

                            if (value.startsWith("http://") || value.startsWith("https://")) {
                                value = value;
                            } else {
                                value = ServiceConstant.BASE_URLIMAGE + value;
                            }
                            AfterImagePojo beforemovie = new AfterImagePojo(value);
                            aftermovieList.add(beforemovie);
                        }

                        JSONArray categoryinclude = object2.getJSONArray("category");
                        storewhatsincludedlenghth = categoryinclude.length();
                        storewhatsincluded = "";
                        for (int z = 0; z < categoryinclude.length(); z++) {
                            JSONObject getarrayvaluex = categoryinclude.getJSONObject(z);
                            String name = "" + getarrayvaluex.getString("name");

                            if (getarrayvaluex.has("whatsincluded")) {
                                String whatsincluded = "" + getarrayvaluex.getString("whatsincluded");
                                storewhatsincluded += name + "-" + whatsincluded + "\n";
                            } else {

                            }


                        }


                        Str_currencycode = object2.getString("currency_code");
                        Str_jobdate = object2.getString("job_date");
                        Str_jobtime = object2.getString("job_time");

                        task_id = "" + object2.getString("task_id");
                        user_id = "" + object2.getString("user_id");


                        if (object2.has("estimation_status")) {
                            saveoverallestimatecount = object2.getString("estimation_status");
                        } else {
                            saveoverallestimatecount = "";
                        }
                        saveoveralljobcount = object2.getString("overall_job_count");
                        String service = "";
                        JSONArray service_type = object2.getJSONArray("job_type");
                        for (int b = 0; b < service_type.length(); b++) {
                            String value = "" + service_type.getString(b);
                            service += value + ",";
                        }
                        if (service.endsWith(",")) {
                            service = service.substring(0, service.length() - 1);
                        }


                        changeordervisible = object2.getString("change_order");


                        Str_jobtype = service;
                        Str_jobinstruction = object2.getString("instruction");
                        Job_Instruction = object2.getString("instruction");
                        Str_jobstatus = object2.getString("job_status");
                        Str_jobusername = object2.getString("user_name");
                        Str_userimg = object2.getString("user_image");
                        Str_job_userrating = object2.getString("user_ratings");
                        Str_user_email = object2.getString("user_email");
                        Str_usermobile = object2.getString("user_mobile");
                        Str_job_location = object2.getString("job_location");
                        Str_job_group = object2.getString("btn_group");
                        zipcodestore = object2.getString("zipcode");
                        destination_lat = object2.getString("location_lat");
                        destination_long = object2.getString("location_lon");
                        if (object2.has("provide_estimation")) {
                            String provide_estimation = object2.getString("provide_estimation");
                            saveprovide_estimatio = provide_estimation;
                        } else {
                            saveprovide_estimatio = "0";
                        }

                        jobtype = Str_jobtype;
                        sendaddress = Str_job_location;
                        senddatetime = Str_jobdate + " " + Str_jobtime;
                        sendphoto = Str_userimg;
                        sendusername = Str_jobusername;
                        if (object2.has("cancelreason")) {
                            cancel_reson = object2.getString("cancelreason");
                        }

                        strlat = Double.parseDouble(destination_lat);
                        strlon = Double.parseDouble(destination_long);
                        mTaskID = object2.getString("task_id");
                    } else {
                        Str_Response = jobject.getString("response");
                    }

                    if (Str_status.equalsIgnoreCase("1")) {

                        Tv_ongoing_detail_jobinstruction.setText(Str_jobinstruction);
                        String length = String.valueOf(Tv_ongoing_detail_jobinstruction.getText().toString().length());
                        if (Str_jobinstruction.length() > 30) {
                            show_moretext_layout.setVisibility(View.VISIBLE);
                        } else {
                            show_moretext_layout.setVisibility(View.GONE);
                        }
                        //-------------------code for set marker-------------------------

                        mapFragment.getMapAsync(new OnMapReadyCallback() {
                            @Override
                            public void onMapReady(GoogleMap googleMap) {
                                googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

                                googleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng((strlat), (strlon)))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin)));
                                // Move the camera to last position with a zoom level
                                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng((strlat), (strlon))).zoom(17).build();
                                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                            }
                        });


                    } else {
                        final PkDialog mdialog = new PkDialog(HelperOngoingPageActivity.this);
                        mdialog.setDialogTitle(getResources().getString(R.string.server_lable_header));
                        mdialog.setDialogMessage(Str_Response);
                        mdialog.setCancelOnTouchOutside(false);
                        mdialog.setPositiveButton(getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        /*Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.putExtra("EXIT", true);
                                        startActivity(intent);*/

                                        Intent intent = new Intent(getApplicationContext(), WeeklyCalendarActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.putExtra("EXIT", true);
                                        startActivity(intent);
                                        finish();
                                        mdialog.dismiss();
                                    }
                                }
                        );
                        mdialog.show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (sendphoto.isEmpty()) {
                    imag.setImageResource(R.drawable.ic_no_user);
                } else {
                    Picasso.with(HelperOngoingPageActivity.this)
                            .load(sendphoto)

                            .placeholder(R.drawable.ic_no_user)   // optional
                            .error(R.drawable.ic_no_user)      // optional
                            // optional
                            .into(imag);
                }
                usernameee.setText(sendusername);
                jobtypetext.setText(jobtype);
                if (sendaddress.length() < 2) {
                    Tv_ongoing_jobtype.setText(getResources().getString(R.string.no_address_available));
                } else {
                    Tv_ongoing_jobtype.setText(sendaddress);
                }
                ongoing_details_dateTv.setText(senddatetime.replace("/", "."));
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {

                /*Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);*/

                Intent intent = new Intent(getApplicationContext(), WeeklyCalendarActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }


    //-----------------------code for detail ButtonAction Post request---------------------


    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkAccessFineLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkAccessCoarseLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void CheckPermissions() {
        if (!checkAccessFineLocationPermission() || !checkAccessCoarseLocationPermission()) {
            requestPermissionLocation();
        } else {
            enableGpsService();
        }
    }

    private void enableGpsService() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final com.google.android.gms.common.api.Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Intent intent = new Intent(HelperOngoingPageActivity.this, Track_your_ride.class);
                        intent.putExtra("LAT", destination_lat);
                        intent.putExtra("LONG", destination_long);
                        intent.putExtra("Userid", Str_Userid);
                        intent.putExtra("tasker", provider_id);
                        intent.putExtra("task", mTaskID);
                        intent.putExtra("address", sendaddress);
                        startActivity(intent);
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(HelperOngoingPageActivity.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }


    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODES);
    }

    private void requestPermissionLocation() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + Str_usermobile));
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(callIntent);
                }
                break;


            case PERMISSION_REQUEST_CODES:

                if (Build.VERSION.SDK_INT >= 23) {

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.putExtra("address", Str_usermobile);
                    intent.putExtra("sms_body", share_text);
                    intent.setData(Uri.parse("smsto:" + Str_usermobile));
                    startActivity(intent);


                } else {
                    Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                    sendIntent.putExtra("sms_body", share_text);
                    sendIntent.putExtra("address", Str_usermobile);
                    sendIntent.setType("vnd.android-dir/mms-sms");
                    startActivity(sendIntent);
                }
                break;
            case PERMISSION_REQUEST_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
                break;
        }
    }


    public static void AlertShow(String title, String message, Context context) {
        final PkDialog mDialog = new PkDialog(context);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(context.getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences pref1 = getApplicationContext().getSharedPreferences("openclosed", 0);
        SharedPreferences.Editor editor1 = pref1.edit();
        editor1.putString("status", "1");
        editor1.apply();
        editor1.commit();

        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
        SharedPreferences.Editor prefeditor = prefjobid.edit();
        prefeditor.putString("page", "0");
        prefeditor.apply();
        prefeditor.commit();

        handler.post(timedTask);
    }


    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences pref1 = getApplicationContext().getSharedPreferences("openclosed", 0);
        SharedPreferences.Editor editor1 = pref1.edit();
        editor1.putString("status", "0");
        editor1.apply();
        editor1.commit();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    protected void onStop() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();

        try {
            handler.removeCallbacks(timedTask);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //  mHandler.removeCallbacks(mHandlerTask);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();


    }

    @Override
    public void onConnected(Bundle bundle) {
        if (gps != null && gps.canGetLocation() && gps.isgpsenabled()) {
        }

        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            myLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);

            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(getApplicationContext(), "Connection lost", Toast.LENGTH_LONG).show();
    }

    MarkerOptions mm = new MarkerOptions();
    Marker drivermarker;
    JSONObject job = new JSONObject();

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    //---------------------------------------------Material Fees Submit Request--------------------------------------------------------


    @Override
    public void onRemoveListener(int position) {

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR


            SharedPreferences pref1 = getApplicationContext().getSharedPreferences("needrefresh", 0);
            String whichclass = pref1.getString("yesno", " ");
            if (whichclass.equals("0") || whichclass.equals("2")) {
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            } else if (whichclass.equals("1")) {

                /*Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);*/
                Intent intent = new Intent(getApplicationContext(), WeeklyCalendarActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else if (whichclass.equals("3")) {

                Intent intent = new Intent(getApplicationContext(), MyJobsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("status", "0");
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }


            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        googleMap.getUiSettings().setAllGesturesEnabled(true);

        if (googleMap == null) {
            Toast.makeText(HelperOngoingPageActivity.this, getResources().getString(R.string.ongoing_detail_map_doesnotcreate_label), Toast.LENGTH_SHORT).show();
        }

        // Changing map type
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        // Showing / hiding your current location

        googleMap.setMyLocationEnabled(false);
        // Enable / Disable zooming controls
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        // Enable / Disable my location button
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        // Enable / Disable Compass icon
        googleMap.getUiSettings().setCompassEnabled(false);
        // Enable / Disable Rotate gesture
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        // Enable / Disable zooming functionality
        googleMap.getUiSettings().setZoomGesturesEnabled(true);

        if (gps.canGetLocation()) {
            double Dlatitude = gps.getLatitude();
            double Dlongitude = gps.getLongitude();
            MyCurrent_lat = Dlatitude;
            MyCurrent_long = Dlongitude;
            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Dlatitude, Dlongitude)).zoom(17).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            // create marker double Dlatitude = gps.getLatitude();

            MarkerOptions marker = new MarkerOptions().position(new LatLng(Dlatitude, Dlongitude));
            // marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.maps));
            //  currentMarker = googleMap.addMarker(marker);
            System.out.println("currntlat----------" + MyCurrent_lat);
            System.out.println("currntlon----------" + MyCurrent_long);

        } else {
            //show gps alert
           /* alert_layout.setVisibility(View.VISIBLE);
            alert_textview.setText(getResources().getString(R.string.alert_gpsEnable));*/
        }
        setLocationRequest();
    }


    public class MyOrdersAdapter extends BaseAdapter {
        private ArrayList<MyOrdersPojo> myOrderArrList;
        private LayoutInflater myInflater;
        private Context myContext;

        public MyOrdersAdapter(Context aContext, ArrayList<MyOrdersPojo> aOrderArrList) {
            this.myOrderArrList = aOrderArrList;
            this.myContext = aContext;
            myInflater = LayoutInflater.from(aContext);
        }

        @Override
        public int getCount() {
            return myOrderArrList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        private class ViewHolder {

            private TextView jobtype, jobstatus,helpername;


        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final MyOrdersAdapter.ViewHolder holder;
            if (convertView == null) {
                holder = new MyOrdersAdapter.ViewHolder();
                convertView = myInflater.inflate(R.layout.adpaterforhelpe, parent, false);


                holder.jobtype = (TextView) convertView.findViewById(R.id.jobtype);
                holder.helpername = (TextView) convertView.findViewById(R.id.helpername);

                holder.jobstatus = (TextView) convertView.findViewById(R.id.jobstatus);
                convertView.setTag(holder);

            } else {
                holder = (MyOrdersAdapter.ViewHolder) convertView.getTag();
            }


            holder.jobtype.setText(myOrderArrList.get(position).getOrderServiceType());

            holder.jobstatus.setText(myOrderArrList.get(position).getOrderStatus());


            holder.jobstatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (isInternetPresent) {
                        int statuss = Integer.parseInt(myOrderArrList.get(position).getTaskname());
                        if (statuss >= 6) {
                            HNDHelper.showErrorAlert(HelperOngoingPageActivity.this, getResources().getString(R.string.job_completed_TXT));
                        } else {
                            buttonsClickActions(HelperOngoingPageActivity.this, ServiceConstant.helperupdatestatusurl, "" + (statuss + 1), position, myOrderArrList.get(position).getOrderId());
                        }

                    } else {
                        Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                    }
                }
            });


            return convertView;
        }


    }


    private void buttonsClickActions(Context mContext, String url, final String key, final int postion, String orderid) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("provider_id", provider_id);
        jsonParams.put("job_id", str_jobId);
        jsonParams.put("status", key);
        jsonParams.put("service_id", orderid);


        if (!myDialog.isShowing()) {
            myDialog.show();
        }


        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                Log.e("clickresponse------", response);

                String Str_status = "",
                        Str_message = "", Str_btngroup = "";

                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");

                    if (Str_status.equalsIgnoreCase("1")) {


                        Str_message = jobject.getString("response");


                        int add = Integer.parseInt(key);
                        myOrdersArrList.get(postion).setTaskname("" + (add));


                        String orderstatus = "";
                        if (key.equalsIgnoreCase("2")) {
                            orderstatus = "Start Off";
                        } else if (key.equalsIgnoreCase("3")) {
                            orderstatus = "Arrived";
                        } else if (key.equalsIgnoreCase("4")) {
                            orderstatus = "Start job";
                        } else if (key.equalsIgnoreCase("5")) {
                            orderstatus = "Complete job";
                        } else if (key.equalsIgnoreCase("6")) {
                            orderstatus = "Job completed";
                        }

                        myOrdersArrList.get(postion).setOrderStatus(orderstatus);

                        myOrderAdapter.notifyDataSetChanged();


                        final PkDialog mdialog = new PkDialog(HelperOngoingPageActivity.this);
                        mdialog.setDialogTitle(getResources().getString(R.string.action_loading_sucess));
                        mdialog.setDialogMessage(Str_message);
                        mdialog.setPositiveButton(getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mdialog.dismiss();

                                    }
                                }
                        );
                        mdialog.show();

                    } else {
                        Str_message = jobject.getString("response");
                        final PkDialog mdialog = new PkDialog(HelperOngoingPageActivity.this);
                        mdialog.setDialogTitle(getResources().getString(R.string.action_loading_sucess));
                        mdialog.setDialogMessage(Str_message);
                        mdialog.setPositiveButton(getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mdialog.dismiss();

                                    }
                                }
                        );
                        mdialog.show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }
        });
    }


    private Runnable timedTask = new Runnable() {
        public void run() {

            shakingBell.shake(1);


            //  Toast.makeText(getApplicationContext(),""+myLatitude,Toast.LENGTH_LONG).show();

            if (myLatitude == 0.0) {
                gps = new GPSTracker(HelperOngoingPageActivity.this);
                if (gps.isgpsenabled() && gps.canGetLocation()) {
                    myLatitude = gps.getLatitude();
                    myLongitude = gps.getLongitude();

                } else {

                }
            } else {

            }

            SharedPreferences showpoupup = getApplicationContext().getSharedPreferences("reload", 0);
            String show = showpoupup.getString("page", "");
            if (show.equalsIgnoreCase("1")) {
                SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("page", "0");
                editor.commit();
                editor.apply();


                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {


                    myjobOngoingDetail(HelperOngoingPageActivity.this, ServiceConstant.MYJOB_DETAIL_INFORMATION_URL);

                } else {
                    final PkDialog mDialog = new PkDialog(HelperOngoingPageActivity.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.internet_label));
                    mDialog.setCancelOnTouchOutside(false);
                    mDialog.setDialogMessage(getResources().getString(R.string.action_no_internet_message));
                    mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                            finish();
                        }
                    });
                    mDialog.show();
                }


            }


            handler.postDelayed(timedTask, 4000);
        }
    };


    public class Joblistadapter extends BaseAdapter {
        private ArrayList<joblistpojo> myOrderArrList;
        private LayoutInflater myInflater;
        private Context myContext;

        public Joblistadapter(Context aContext, ArrayList<joblistpojo> aOrderArrList) {
            this.myOrderArrList = aOrderArrList;
            this.myContext = aContext;
            myInflater = LayoutInflater.from(aContext);
        }

        @Override
        public int getCount() {
            return myOrderArrList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            Joblistadapter.ViewHolder holder;
            if (convertView == null) {
                holder = new Joblistadapter.ViewHolder();
                convertView = myInflater.inflate(R.layout.joblistadapter, parent, false);

                holder.jobid = (TextView) convertView.findViewById(R.id.details_orderid);
                holder.imag = (CircleImageView) convertView.findViewById(R.id.imag);
                holder.jobtype = (TextView) convertView.findViewById(R.id.jobtype);
                holder.helpername = (TextView) convertView.findViewById(R.id.helpername);

                holder.username = (TextView) convertView.findViewById(R.id.username);

                holder.jobstatus = (TextView) convertView.findViewById(R.id.jobstatus);
                convertView.setTag(holder);

            } else {
                holder = (Joblistadapter.ViewHolder) convertView.getTag();
            }

            holder.jobid.setText(myOrderArrList.get(position).getOrderId());
            holder.jobtype.setText(myOrderArrList.get(position).getOrderServiceType());
            holder.jobstatus.setText(myOrderArrList.get(position).getOrderStatus());

            switch (myOrderArrList.get(position).getOrderStatus()) {
                case "2":
                    holder.username.setText(getResources().getString(R.string.jobcompleted));
                    break;
                case "1":
                    holder.username.setText(getResources().getString(R.string.completejobb));
                    break;
                default:
                    holder.username.setText(getResources().getString(R.string.startjobb));
                    break;
            }

            holder.username.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

//                    HelperOngoingPageActivity.this.myOrdersArrList
                    if (myOrderArrList.get(position).getOrderStatus().equals("0")) {
                        multiplestatus(HelperOngoingPageActivity.this, ServiceConstant.helperstatusupdate, position, myOrderArrList.get(position).getOrderBookingDate(), "1");
                    } else if (myOrderArrList.get(position).getOrderStatus().equals("1")) {
                        multiplestatus(HelperOngoingPageActivity.this, ServiceConstant.helperstatusupdate, position, myOrderArrList.get(position).getOrderBookingDate(), "2");
                    }


                }
            });


            return convertView;
        }

        private class ViewHolder {

            private TextView jobid, jobtype, username, jobstatus,helpername;
            private CircleImageView imag;

        }


    }


    private void multiplestatus(Context mContext, String url, final int key, String scheduleid, final String status) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("provider_id", provider_id);
        jsonParams.put("job_id", str_jobId);
        jsonParams.put("schedule_id", scheduleid);
        jsonParams.put("status", status);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                Log.e("clickresponse------", response);

                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

                String Str_status = "", username = "", user_email = "", user_phoneno = "", user_img = "", user_reviwe = "", jobId = "", user_location = "", jobtime = "", job_lat = "", job_long = "",
                        Str_message = "", Str_btngroup = "";

                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");

                    if (Str_status.equalsIgnoreCase("1")) {

                        if (status.equals("1")) {
                            myjobList.get(key).setOrderStatus("1");
                            myOrderAdapterss.notifyDataSetChanged();
                        } else if (status.equals("2")) {
                            myjobList.get(key).setOrderStatus("2");
                            myOrderAdapterss.notifyDataSetChanged();
                        }

                    } else {
                        Str_message = jobject.getString("response");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (Str_status.equalsIgnoreCase("1")) {


                } else {
                    Alert(getResources().getString(R.string.server_lable_header), Str_message);
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }
        });
    }


}