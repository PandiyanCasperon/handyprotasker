package noman.handyprotaskerapp.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.android.volley.Request;
import com.rackspira.kristiawan.rackmonthpicker.RackMonthPicker;
import com.rackspira.kristiawan.rackmonthpicker.listener.DateMonthDialogListener;
import com.rackspira.kristiawan.rackmonthpicker.listener.OnCancelMonthDialogListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import Dialog.LoadingDialog;
import Dialog.PkDialog;
import Utils.ConnectionDetector;
import de.hdodenhof.circleimageview.CircleImageView;
import noman.handyprotaskerapp.WidgetSupport.LinearLayoutManagerWithSmoothScroller;
import noman.handyprotaskerapp.Pojo.MoviePojo;
import noman.handyprotaskerapp.Pojo.Movie1Pojo;
import noman.handyprotaskerapp.Navigationdrawer;
import noman.handyprotaskerapp.R;
import noman.handyprotaskerapp.RecyclerItemClickListener;
import noman.handyprotaskerapp.RecyclerViewPositionHelper;
import noman.handyprotaskerapp.RecyclerViewPositionHelper1;
import noman.handyprotaskerapp.DB.ChangeOrderSQlite;
import service.ServiceConstant;
import volley.ServiceRequest;


public class MainActivity extends BaseActivity {

    RecyclerView recyclerView1,recyclerVie2;
    ArrayList<String> list = new ArrayList<String>();
    private MoviesAdapter mAdapter;
    private MoviesAdapter1 mAdapter1;
    LinearLayout card_view;
    private List<MoviePojo> moviePojoList = new ArrayList<>();
    private List<Movie1Pojo> movieList2 = new ArrayList<>();
    String selectedpos="",overallresponse="";
    LinearLayout checkinternet;
    TextView date;
    int differ=0;
    String joinall="";
    LinearLayout tou;
    ArrayList<String> availday = new ArrayList<String>();
    ArrayList<String> jobidd = new ArrayList<String>();
    ArrayList<String> mechanicstype = new ArrayList<String>();
    ArrayList<String> bookingmodelist = new ArrayList<String>();
    ArrayList<String> colourchange = new ArrayList<String>();

    ArrayList<String> ascraftmanorhelper = new ArrayList<String>();


    Typeface tf;
    String cdate;
    TextView choosemonth;
    String passyear;
    String passmonth;
    LinearLayout backlay;
    String displaymonth;
    ConnectionDetector cd;
    boolean isInternetPresent;
    TextView hintdisplay;
    ImageView back,imageclcik;
    LoadingDialog       dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        cd = new ConnectionDetector(MainActivity.this);
        isInternetPresent = cd.isConnectingToInternet();
        ChangeOrderSQlite mHelper=new ChangeOrderSQlite(MainActivity.this);
        deleteDatabase(mHelper.DATABASE_NAME);
        SharedPreferences pref1 = getApplicationContext().getSharedPreferences("needrefresh", 0);
        SharedPreferences.Editor editor1 = pref1.edit();
        editor1.putString("yesno", "0");
        editor1.apply();
        editor1.commit();




        tf = Typeface.createFromAsset(getAssets(), "Poppins-Regular.ttf");
        displaymonth = new SimpleDateFormat("MMM yyyy", Locale.getDefault()).format(new Date());

        cdate = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(new Date());
        passyear= new SimpleDateFormat("yyyy", Locale.getDefault()).format(new Date());
        passmonth= new SimpleDateFormat("MM", Locale.getDefault()).format(new Date());





        differ=0;
        moviePojoList.clear();
        movieList2.clear();
        selectedpos="";
        overallresponse="";
        joinall="";

        init();




       /* imageclcik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                YearMonthPickerDialog yearMonthPickerDialog = new YearMonthPickerDialog(MainActivity.this, new YearMonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onYearMonthSet(int year, int month) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, month);

                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");
                        SimpleDateFormat storemonth = new SimpleDateFormat("MM");

                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("MMM yyyy");

                        passyear=dateFormat.format(calendar.getTime());
                        passmonth=storemonth.format(calendar.getTime());


                        choosemonth.setText(dateFormat1.format(calendar.getTime()));













                        moviePojoList.clear();

                        movieList2.clear();


                        selectedpos="";
                        differ=0;
                        joinall="";

                        availday.clear();

                        list.clear();




                        prepareMovieData1();
                        prepareMovieData();


                        mAdapter = new MoviesAdapter(moviePojoList,MainActivity.this);
                        mAdapter1 = new MoviesAdapter1(movieList2);

                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerView1.setLayoutManager(mLayoutManager);
                        recyclerView1.setItemAnimator(new DefaultItemAnimator());
                        recyclerView1.setAdapter(mAdapter);



                        RecyclerView.LayoutManager mLayoutManager1 =
                                new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                        recyclerVie2.setLayoutManager(mLayoutManager1);
                        recyclerVie2.setAdapter(mAdapter1);

                    }
                });

                yearMonthPickerDialog.show();
            }
        });

*/
        choosemonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final RackMonthPicker rackMonthPicker = new RackMonthPicker(MainActivity.this)
                        .setLocale(Locale.ENGLISH)
                        .setColorTheme(R.color.orangecol)
                        .setPositiveButton(new DateMonthDialogListener() {
                            @Override
                            public void onDateMonth(int month, int startDate, int endDate, int year, String monthLabel) {
                                System.out.println(month);

                                System.out.println(year);
                                System.out.println(monthLabel);







                                if(month<10)
                                {
                                    passmonth="0"+month;
                                }
                                else
                                {
                                    passmonth=""+month;
                                }
                                passyear=""+year;


                                if (isInternetPresent)
                                {
                                    checkinternet.setVisibility(View.GONE);
                                    myNewLeadsPostRequest(MainActivity.this, ServiceConstant.NEWLEADS_URL);
                                }
                                else
                                {
                                    card_view.setVisibility(View.GONE);
                                    checkinternet.setVisibility(View.VISIBLE);
                                }


                                choosemonth.setText(monthLabel);
                                moviePojoList.clear();
                                movieList2.clear();
                                selectedpos = "";
                                differ = 0;
                                joinall = "";
                                availday.clear();
                                list.clear();
                                prepareMovieData1();
                                prepareMovieData();
                                mAdapter = new MoviesAdapter(moviePojoList, MainActivity.this);
                                mAdapter1 = new MoviesAdapter1(movieList2);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                recyclerView1.setLayoutManager(mLayoutManager);
                                recyclerView1.setItemAnimator(new DefaultItemAnimator());
                                recyclerView1.setAdapter(mAdapter);
                                RecyclerView.LayoutManager mLayoutManager1 =
                                        new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                                recyclerVie2.setLayoutManager(mLayoutManager1);
                                recyclerVie2.setAdapter(mAdapter1);


                            }
                        })
                        .setNegativeButton(new OnCancelMonthDialogListener() {
                            @Override
                            public void onCancel(AlertDialog dialog) {
                                dialog.dismiss();
                            }
                        });
                rackMonthPicker.show();
            }
        });



     /*   choosemonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                YearMonthPickerDialog yearMonthPickerDialog = new YearMonthPickerDialog(MainActivity.this, new YearMonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onYearMonthSet(int year, int month) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, month);

                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");
                        SimpleDateFormat storemonth = new SimpleDateFormat("MM");
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("MMM yyyy");
                        passyear=dateFormat.format(calendar.getTime());
                        passmonth=storemonth.format(calendar.getTime());


                        if (isInternetPresent)
                        {
                            checkinternet.setVisibility(View.GONE);
                            myNewLeadsPostRequest(MainActivity.this, ServiceConstant.NEWLEADS_URL);
                        }
                        else
                        {
                            card_view.setVisibility(View.GONE);
                            checkinternet.setVisibility(View.VISIBLE);
                        }


                        choosemonth.setText(dateFormat1.format(calendar.getTime()));
                       *//* moviePojoList.clear();
                        movieList2.clear();
                        selectedpos="";
                        differ=0;
                        joinall="";
                        availday.clear();
                        list.clear();*//*
                       *//* prepareMovieData1();
                        prepareMovieData();*//*
                       *//* mAdapter = new MoviesAdapter(moviePojoList,MainActivity.this);
                        mAdapter1 = new MoviesAdapter1(movieList2);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerView1.setLayoutManager(mLayoutManager);
                        recyclerView1.setItemAnimator(new DefaultItemAnimator());
                        recyclerView1.setAdapter(mAdapter);
                        RecyclerView.LayoutManager mLayoutManager1 =
                                new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                        recyclerVie2.setLayoutManager(mLayoutManager1);
                        recyclerVie2.setAdapter(mAdapter1);*//*

                    }
                });

                yearMonthPickerDialog.show();
            }
        });*/




       recyclerVie2.setOnScrollListener(new RecyclerView.OnScrollListener() {
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                differ=1;

            }
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                RecyclerViewPositionHelper1 mRecyclerViewHelper;
                mRecyclerViewHelper = RecyclerViewPositionHelper1.createHelper(recyclerVie2);
                int firstVisibleItem = mRecyclerViewHelper.findFirstVisibleItemPosition();
                if(differ==1)
                {
                  String joinallq=availday.get(firstVisibleItem);
                    System.out.println("-------->Recycler 2 scrolling now--"+joinallq);
                    joinall=joinallq;
                    mAdapter1.notifyDataSetChanged();
                    if(list.contains(joinallq))
                    {
                        int position = list.indexOf(joinallq);
                        recyclerView1.scrollToPosition(position);

                    }
                }
            }
        });


        recyclerView1.setOnScrollListener(new RecyclerView.OnScrollListener() {
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                System.out.println("-------->Recycler 1 scroll state changed");
                differ=0;

            }



            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                RecyclerViewPositionHelper mRecyclerViewHelper;
                mRecyclerViewHelper = RecyclerViewPositionHelper.createHelper(recyclerView);
                int firstVisibleItem = mRecyclerViewHelper.findFirstVisibleItemPosition();
                if(differ==1)
                {
                }
                else
                {
                    // weekCalendar.setSelectedDate(new DateTime().plusDays(50));
                    joinall=list.get(firstVisibleItem);
                    date.setText(""+list.get(firstVisibleItem));
                    mAdapter1.notifyDataSetChanged();
                    if(availday.contains(list.get(firstVisibleItem)))
                    {
                        int position = availday.indexOf(list.get(firstVisibleItem));
                        recyclerVie2.scrollToPosition(position);
                    }
                }

            }
        });
    }

    private void init() {
        hintdisplay=(TextView) findViewById(R.id.hintdisplay);
        card_view=(LinearLayout) findViewById(R.id.card_view);
        imageclcik=(ImageView) findViewById(R.id.imageclcik);
        date=(TextView)findViewById(R.id.date);
        tou = (LinearLayout) findViewById(R.id.tou);
        recyclerVie2= (RecyclerView) findViewById(R.id.recyclerviewdate);
        recyclerView1 = (RecyclerView) findViewById(R.id.recyclerview);
        backlay= (LinearLayout) findViewById(R.id.backlay);
        choosemonth=(TextView)findViewById(R.id.choosemonth);
        checkinternet= (LinearLayout) findViewById(R.id.checkinternet);
        if (isInternetPresent)
        {
            checkinternet.setVisibility(View.GONE);
            myNewLeadsPostRequest(MainActivity.this, ServiceConstant.NEWLEADS_URL);
        }
        else
        {
            card_view.setVisibility(View.GONE);
            checkinternet.setVisibility(View.VISIBLE);
        }
        choosemonth.setText(displaymonth);
        back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Navigationdrawer.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
        backlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Navigationdrawer.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
        recyclerView1.addOnItemTouchListener(
                new RecyclerItemClickListener(MainActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        // TODO Handle item click
                        // Toast.makeText(getApplicationContext(),""+position,Toast.LENGTH_LONG).show();


                        if(ascraftmanorhelper.get(position).equalsIgnoreCase("Helper"))
                        {
                            if (isInternetPresent) {

                                SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                                SharedPreferences.Editor prefeditor = prefjobid.edit();
                                prefeditor.putString("jobid", jobidd.get(position));
                                prefeditor.apply();
                                prefeditor.commit();

                                Intent intent = new Intent(MainActivity.this, HelperOngoingPageActivity.class);
                                intent.putExtra("JobId", jobidd.get(position));
                                startActivity(intent);
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            } else {
                                Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));

                            }
                        }
                        else
                        {
                            if (isInternetPresent) {

                                SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                                SharedPreferences.Editor prefeditor = prefjobid.edit();
                                prefeditor.putString("jobid", jobidd.get(position));
                                prefeditor.apply();
                                prefeditor.commit();

                                Intent intent = new Intent(MainActivity.this, MyJobs_OnGoingDetailActivity.class);
                                intent.putExtra("JobId", jobidd.get(position));
                                startActivity(intent);
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            } else {
                                Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));

                            }
                        }




                    }
                })
        );
    }
    private void Alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(MainActivity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }
    private void prepareMovieData() {
        MoviePojo moviePojo;
        try {
            JSONObject obj1 = new JSONObject(overallresponse);
            JSONObject obj2 = new JSONObject(obj1.getString("response"));
            JSONArray m_jArry = obj2.getJSONArray("jobs");
            for (int i = 0; i < m_jArry.length(); i++)
            {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                String booking_time = jo_inside.getString("booking_time_std");
                String jobidbooking_time = "JOB ID : "+jo_inside.getString("job_id")+"\n"+booking_time;
                String category_name ;
                String service="";
                JSONArray service_type = jo_inside.getJSONArray("category_name");
                for (int b = 0; b < service_type.length(); b++)
                {
                    String value=""+service_type.getString(b);
                    //JSONObject serobj = aMainArr.getJSONObject(b);
                    service+=value+",";
                }
                if(service.endsWith(","))
                {
                    service=service.substring(0,service.length()-1);
                }
                String location = jo_inside.getString("location");

                if(location.length()<2)
                {
                    location=getResources().getString(R.string.no_address_available);
                }

                String user_image = jo_inside.getString("user_image");
                String user_name = jo_inside.getString("user_name");
                String bookingmode = jo_inside.getString("job_status");

                String colourchangevalue = jo_inside.getString("overall_est_count");
                String acceptedestimates  = jo_inside.getString("acceptedestimates");

                String role = jo_inside.getString("role");


                String bookingtime=booking_time.substring(0,10).replace("/",".");
                if(bookingtime.contains("."+passmonth+"."+passyear))
                {
                    moviePojo = new MoviePojo(jobidbooking_time, location, service,user_image,user_name,bookingmode,colourchangevalue, acceptedestimates,role);
                    moviePojoList.add(moviePojo);
                    System.out.println("booking_time--->"+bookingtime);
                    list.add(bookingtime);
                    jobidd.add(jo_inside.getString("job_id"));
                    mechanicstype.add(service);
                    bookingmodelist.add(bookingmode);
                    colourchange.add(colourchangevalue);
                    ascraftmanorhelper.add(role);
                }
            }
            if(moviePojoList.size()==0)
            {
                hintdisplay.setVisibility(View.VISIBLE);
            }
            else
            {
                hintdisplay.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        mAdapter = new MoviesAdapter(moviePojoList,MainActivity.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView1.setLayoutManager(mLayoutManager);
        recyclerView1.setItemAnimator(new DefaultItemAnimator());
        recyclerView1.setAdapter(mAdapter);
    }

    private void prepareMovieData1() {
// Create a calendar object and set year and month
        Calendar mycal;
// Get the number of days in that month

        String reducemonth;
        if (passmonth.startsWith("0")) {
            reducemonth = passmonth.substring(1, passmonth.length());
        } else {
            reducemonth = passmonth;
        }
        int minusmonth = ((Integer.parseInt(reducemonth)) - 1);
        mycal = new GregorianCalendar(Integer.parseInt(passyear), minusmonth, 1);
        int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH); // 28
        //  Toast.makeText(getApplicationContext(),""+daysInMonth,Toast.LENGTH_LONG).show();
        for (int p = 1; p <= daysInMonth; p++) {
            String vak = "";
            if (p <= 9) {
                vak = "0" + p;
            } else {
                vak = "" + p;
            }
            availday.add(vak + "." + passmonth + "." + passyear);
            String dayOfWeek;
            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
            Date date = null;
            Calendar calender = new GregorianCalendar();
            try {
                date = format.parse(passyear + "/" + passmonth + "/" + vak);
            } catch (ParseException e) {
                e.printStackTrace();
                dayOfWeek = null;
            }
            calender.setTime(date);
            switch (calender.get(Calendar.DAY_OF_WEEK)) {
                case Calendar.MONDAY:
                    dayOfWeek = "Monday";
                    break;
                case Calendar.TUESDAY:
                    dayOfWeek = "Tuesday";
                    break;
                case Calendar.WEDNESDAY:
                    dayOfWeek = "Wednesday";
                    break;
                case Calendar.THURSDAY:
                    dayOfWeek = "Thursday";
                    break;
                case Calendar.FRIDAY:
                    dayOfWeek = "Friday";
                    break;
                case Calendar.SATURDAY:
                    dayOfWeek = "Saturday";
                    break;
                case Calendar.SUNDAY:
                    dayOfWeek = "Sunday";
                    break;

                default:
                    dayOfWeek = null;
                    break;
            }
            Movie1Pojo movie1Pojo;
            movie1Pojo = new Movie1Pojo(vak + "." + passmonth + "." + passyear, "Action & Adventure", dayOfWeek);
            movieList2.add(movie1Pojo);
        }
        mAdapter1 = new MoviesAdapter1(movieList2);
        RecyclerView.LayoutManager mLayoutManager1 =
                new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerVie2.setLayoutManager(mLayoutManager1);
        recyclerVie2.setAdapter(mAdapter1);
    }






    public class MoviesAdapter1 extends RecyclerView.Adapter<MoviesAdapter1.MyViewHolder> {
        private List<Movie1Pojo> moviesList;
        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView title, year, genre;
            public MyViewHolder(View view) {
                super(view);
                title = (TextView) view.findViewById(R.id.title);
                genre = (TextView) view.findViewById(R.id.genre);
                year = (TextView) view.findViewById(R.id.year);
                title.setTypeface(tf);
                year.setTypeface(tf);
            }
        }
        public MoviesAdapter1(List<Movie1Pojo> moviesList) {
            this.moviesList = moviesList;
            tf = Typeface.createFromAsset(getAssets(), "Poppins-Medium.ttf");
        }
        @Override
        public MoviesAdapter1.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.lay2, parent, false);

            return new MoviesAdapter1.MyViewHolder(itemView);
        }
        @Override
        public void onBindViewHolder(final MoviesAdapter1.MyViewHolder holder, int position) {
            final Movie1Pojo movie1Pojo = moviesList.get(position);
            holder.title.setText(movie1Pojo.getTitle().substring(0,2));

            if(joinall.equals(movie1Pojo.getTitle()))
            {
                holder.title.setTextColor(Color.WHITE);
                holder.title.setBackgroundResource(R.drawable.ovalcolour);
            }
            else
            {
                holder.title.setTextColor(Color.BLACK);
                holder.title.setBackgroundResource(R.drawable.white);
            }

            if(cdate.equals(movie1Pojo.getTitle()))
            {
                holder.title.setPaintFlags(holder.title.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
            }
            else
            {
                holder.title.setPaintFlags(View.INVISIBLE);
            }
            holder.genre.setText(movie1Pojo.getGenre());
            holder.year.setText(movie1Pojo.getYear().substring(0,3));
            holder.title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    joinall= movie1Pojo.getTitle();
                    date.setText(""+joinall);
                    selectedpos="";
                    if(list.contains(joinall))
                    {
                        int position = list.indexOf(joinall);
                        //recyclerView1.scrollToPosition(position);
                        recyclerView1.setLayoutManager(new LinearLayoutManagerWithSmoothScroller(MainActivity.this));
                        recyclerView1.scrollToPosition(position);
                        for(int jk=0;jk<list.size();jk++)
                        {
                            if(list.get(jk).equals(joinall))
                            {
                                selectedpos+=jk+",";
                            }
                        }
                        differ=1;
                        recyclerView1.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                    }
                    mAdapter.notifyDataSetChanged();
                    mAdapter1.notifyDataSetChanged();

                }
            });

        }

        @Override
        public int getItemCount() {
            return moviesList.size();
        }
    }








    public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MyViewHolder> {

        private List<MoviePojo> moviesList;
        Typeface tf;
        Context ctx;
        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView title, year, genre,username,deny,helperorcraftman;
            LinearLayout fulllayback;
            CircleImageView imag;

            public MyViewHolder(View view) {
                super(view);
                title = (TextView) view.findViewById(R.id.title);
                genre = (TextView) view.findViewById(R.id.genre);
                year = (TextView) view.findViewById(R.id.year);
                username= (TextView) view.findViewById(R.id.username);
                deny= (TextView) view.findViewById(R.id.deny);
                helperorcraftman= (TextView) view.findViewById(R.id.helperorcraftman);

                fulllayback= (LinearLayout) view.findViewById(R.id.fulllayback);

                imag = (CircleImageView) view.findViewById(R.id.imag);

                title.setTypeface(tf);
                genre.setTypeface(tf);
                year.setTypeface(tf);
                deny.setTypeface(tf);
                helperorcraftman.setTypeface(tf);
                username.setTypeface(tf);
            }
        }


        public MoviesAdapter(List<MoviePojo> moviesList, Context ctx1) {
            this.moviesList = moviesList;
            this.ctx=ctx1;
            tf = Typeface.createFromAsset(ctx.getAssets(), "Poppins-Medium.ttf");
        }

        @Override
        public MoviesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.movie_list_row, parent, false);

            return new MoviesAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MoviesAdapter.MyViewHolder holder, int position) {
            MoviePojo moviePojo = moviesList.get(position);
            holder.title.setText(moviePojo.getTitle());//.replace("/",".")
            holder.genre.setText(moviePojo.getGenre());
            holder.year.setText(moviePojo.getYear());
            holder.username.setText(moviePojo.getUser_name());



            holder.deny.setText(moviePojo.getBookingmode());

            if(moviePojo.getCraftmanhelper().equalsIgnoreCase("Helper"))
            {
                holder.helperorcraftman.setText(getResources().getString(R.string.main_activity_assigned_as)+ moviePojo.getCraftmanhelper());
            }
            else
            {
                holder.helperorcraftman.setText("");
            }

            if(Integer.parseInt(moviePojo.getColourchangevalue()) > 0 && Integer.parseInt(moviePojo.getColourChangeConditionValue()) > 0)
            {
                holder.fulllayback.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.commoncolourback));


            }
            else
            {
                holder.fulllayback.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.orangecol));
            }


            if (moviePojo.getUser_image().isEmpty())
            {
                holder.imag.setImageResource(R.drawable.ic_no_user);
            }
            else
            {
                Picasso.with(ctx)
                        .load(moviePojo.getUser_image())
                        .resize(100,100)
                        .placeholder(R.drawable.ic_no_user)   // optional
                            // optional
                        // optional
                        .into( holder.imag);
            }

        }

        @Override
        public int getItemCount() {
            return moviesList.size();
        }
    }




    private void myNewLeadsPostRequest(Context mContext, String url)
    {

        dialog = new LoadingDialog(MainActivity.this);
        dialog.setLoadingTitle(getResources().getString(R.string.loading_in));
        dialog.show();

        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        String provider_id=pref.getString("provider_id","");
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("provider_id", provider_id);
        jsonParams.put("page", "1");
        jsonParams.put("perPage", "100");

        jsonParams.put("month", passmonth);
        jsonParams.put("year", passyear);


        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------reponsenewleads-------------------" + response);
                overallresponse=response;
                Log.e("new", response);
                String Str_status = "", Str_totaljobs = "", Str_Response = "";
                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");

                    if (Str_status.equalsIgnoreCase("1"))
                    {

                        moviePojoList.clear();
                        movieList2.clear();
                        selectedpos="";
                        differ=0;
                        joinall="";
                        availday.clear();
                        list.clear();


                        card_view.setVisibility(View.VISIBLE);
                        dialog.dismiss();
                        prepareMovieData1();
                        prepareMovieData();
                    }
                    else
                    {
                        card_view.setVisibility(View.GONE);
                        dialog.dismiss();
                    }



                } catch (Exception e) {
                    dialog.dismiss();
                    e.printStackTrace();
                }
                dismissDialog();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences pref1 = getApplicationContext().getSharedPreferences("openclosed", 0);
        SharedPreferences.Editor editor1 = pref1.edit();
        editor1.putString("status", "1");
        editor1.apply();
        editor1.commit();
    }


    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences pref1 = getApplicationContext().getSharedPreferences("openclosed", 0);
        SharedPreferences.Editor editor1 = pref1.edit();
        editor1.putString("status", "0");
        editor1.apply();
        editor1.commit();
    }


    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
            Intent intent = new Intent(getApplicationContext(), Navigationdrawer.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("EXIT", true);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }








}
