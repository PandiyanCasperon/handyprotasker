package noman.handyprotaskerapp.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.RectF;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;
import com.android.volley.Request;
import com.rackspira.kristiawan.rackmonthpicker.RackMonthPicker;
import com.rackspira.kristiawan.rackmonthpicker.listener.DateMonthDialogListener;
import com.rackspira.kristiawan.rackmonthpicker.listener.OnCancelMonthDialogListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import Dialog.LoadingDialog;
import Dialog.PkDialog;
import Utils.ConnectionDetector;
import noman.handyprotaskerapp.Navigationdrawer;
import noman.handyprotaskerapp.Pojo.Not_working_hours;
import noman.handyprotaskerapp.Pojo.WeekViewPojo;
import noman.handyprotaskerapp.R;
import service.ServiceConstant;
import volley.ServiceRequest;

public class WeeklyCalendarActivity extends BaseActivity {

    LinearLayout backlay;
    ImageView back;
    TextView choosemonth;
    String overallresponse = "";
    LoadingDialog dialog;
    WeekView weekView;
    List<WeekViewPojo> weekViewList;
    LinearLayout LinearLayoutSelectMonth, LinearLayoutCheckInternet;
    boolean IsServerRequestLoading = false, IsWeekViewScrolling = true;
    ConnectionDetector cd;
    int id = 0;
    LinearLayout myRefreshLAY;
    private String myMonthStr = "", myYearStr = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.weekly_calendar_activity);

        SharedPreferences pref1 = getApplicationContext().getSharedPreferences("needrefresh", 0);
        SharedPreferences.Editor editor1 = pref1.edit();
        editor1.putString("yesno", "1");
        editor1.apply();
        editor1.commit();

        cd = new ConnectionDetector(WeeklyCalendarActivity.this);

        backlay = (LinearLayout) findViewById(R.id.backlay);
        back = (ImageView) findViewById(R.id.back);
        choosemonth = (TextView) findViewById(R.id.choosemonth);
        LinearLayoutSelectMonth = (LinearLayout) findViewById(R.id.LinearLayoutSelectMonth);
        LinearLayoutCheckInternet = (LinearLayout) findViewById(R.id.LinearLayoutCheckInternet);
        myRefreshLAY = (LinearLayout) findViewById(R.id.weekly_calendar_activit_LAY_refresh);


        choosemonth.setText(new SimpleDateFormat("MMM yyyy", Locale.getDefault()).format(new Date()));
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Navigationdrawer.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        backlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Navigationdrawer.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        // Get a reference for the week view in the layout.
        weekView = (WeekView) findViewById(R.id.weekView);

        weekView.setNumberOfVisibleDays(4);

        myRefreshLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ServerRequest(WeeklyCalendarActivity.this, ServiceConstant.NEWLEADS_URL,
                        myMonthStr, myYearStr);
            }
        });

        //This following code is use to show calendar hours from 12 AM - 11 PM (Left - Y Axis) --> Start
        /*int height = weekView.getMeasuredHeight();
        int textSize = weekView.getTextSize();
        int padding = weekView.getHeaderRowPadding();
        height = height - textSize - (2 * padding);
        weekView.setHourHeight(height / 24);*/
        //This following code is use to show calendar hours from 12 AM - 11 PM (Left - Y Axis) --> End

        // Set an action when any event is clicked.
        weekView.setOnEventClickListener(new WeekView.EventClickListener() {
            @Override
            public void onEventClick(WeekViewEvent event, RectF eventRect) {


//                Toast.makeText(WeeklyCalendarActivity.this, "Clicked "
//                        + weekViewList.get((int)(event.getId())).getJob_id(), Toast.LENGTH_SHORT).show();

                if (event.getId() != -1) {
                    if (weekViewList.get((int) (event.getId())).getRole().equalsIgnoreCase("")) {
                        if (cd.isConnectingToInternet()) {
                            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                            SharedPreferences.Editor prefeditor = prefjobid.edit();
                            prefeditor.putString("jobid", weekViewList.get((int) (event.getId())).getJob_id());
                            prefeditor.apply();

                            Intent intent = new Intent(WeeklyCalendarActivity.this, HelperOngoingPageActivity.class);
                            intent.putExtra("JobId", weekViewList.get((int) (event.getId())).getJob_id());
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        } else {
                            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                        }
                    } else {
                        if (cd.isConnectingToInternet()) {

                            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                            SharedPreferences.Editor prefeditor = prefjobid.edit();
                            prefeditor.putString("jobid", weekViewList.get((int) (event.getId())).getJob_id());
                            prefeditor.apply();
                            prefeditor.commit();

                            if (event.getId() != -1) {
                                Intent intent = new Intent(WeeklyCalendarActivity.this, MyJobs_OnGoingDetailActivity.class);
                                intent.putExtra("JobId", weekViewList.get((int) (event.getId())).getJob_id());
                                startActivity(intent);
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            }

                        } else {
                            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                        }
                    }
                }
            }
        });

        // The week view has infinite scrolling horizontally. We have to provide the events of a
        // month every time the month changes on the week view.
        weekView.setMonthChangeListener(new MonthLoader.MonthChangeListener() {
            @Override
            public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {
                // Populate the week view with some events.
//                Toast.makeText(WeeklyCalendarActivity.this, "newMonth: "+newMonth+"  newYear: "+newYear, Toast.LENGTH_SHORT).show();
                List<WeekViewEvent> events = null;
                if (weekViewList == null) {
                    Calendar startTime = Calendar.getInstance();
                    startTime.setTime(new Date());
                    startTime.set(Calendar.HOUR_OF_DAY, 3);
                    startTime.set(Calendar.MINUTE, 0);
                    startTime.set(Calendar.MONTH, newMonth - 1);
                    startTime.set(Calendar.YEAR, newYear);

                    Calendar endTime = (Calendar) startTime.clone();
                    endTime.set(Calendar.HOUR_OF_DAY, 3);
                    endTime.set(Calendar.MINUTE, 1);

                    WeekViewEvent EventsToLoad = new WeekViewEvent(1, "", startTime, endTime);
                    EventsToLoad.setColor(getResources().getColor(R.color.white));
                    events = new ArrayList<WeekViewEvent>();
                    //Based on mNameColor, I'm setting the color of text color in WeekView.java - 833
                    //So only you can use Black and White color. If need more color, based on the condition set color in WeekView.java - 833
                    EventsToLoad.setmNameColor("White");
                    events.add(EventsToLoad);

                } else {
                    events = new ArrayList<WeekViewEvent>();
                    events.clear();
                    //Following process us to fetch the graftman non working days from weekViewList(Response) -- Start
                    if (weekViewList.size() > 0 && weekViewList.get(0).getNot_working_hours().length > 0) {
                        for (int i = 0; i < weekViewList.get(0).getNot_working_hours().length; i++) {
                            Date DateToProcess = StringToDate(weekViewList.get(0).getNot_working_hours()[i].getDate(), "yyyy-MM-dd");

                            //This setMonthChangeListener will call 3 times totally(Current, Previous and Next month)
                            //So to add the event only to current month, I'm checking the below condition.
                            //Once newMonth and newYear match with our weekViewList.get(0).getNot_working_hours()[i].getDate() means allow inside.
                            if (Integer.parseInt(DateToString(DateToProcess, "MM")) == newMonth && Integer.parseInt(DateToString(DateToProcess, "yyyy")) == newYear) {
                                Calendar startTime = Calendar.getInstance();
                                startTime.setTime(new Date());
                                //Here we adding the 'from' time of the NotWorkingHours to startTime - Start
                                SimpleDateFormat ResultFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
                                Date DateStartTime = StringToDate(weekViewList.get(0).getNot_working_hours()[i].getFromTime(), "hh:mm a");
                                //Adding Hours to startTime
                                startTime.set(Calendar.HOUR_OF_DAY, Integer.parseInt(ResultFormat.format(DateStartTime).split(":")[0]));
                                //Adding Minutes to startTime
                                startTime.set(Calendar.MINUTE, Integer.parseInt(ResultFormat.format(DateStartTime).split(":")[1]));
                                //Adding the 'from' time of the NotWorkingHours to startTime - End
                                startTime.set(Calendar.DATE, Integer.parseInt(DateToString(DateToProcess, "dd")));
                                startTime.set(Calendar.MONTH, Integer.parseInt(DateToString(DateToProcess, "MM")) - 1);
                                startTime.set(Calendar.YEAR, Integer.parseInt(DateToString(DateToProcess, "yyyy")));

                                Calendar endTime = (Calendar) startTime.clone();
                                //Here we adding the 'to' time of the NotWorkingHours to endTime - Start
                                Date DateToTime = StringToDate(weekViewList.get(0).getNot_working_hours()[i].getToTime(), "hh:mm a");
                                endTime.set(Calendar.HOUR_OF_DAY, Integer.parseInt(ResultFormat.format(DateToTime).split(":")[0]));
                                endTime.set(Calendar.MINUTE, Integer.parseInt(ResultFormat.format(DateToTime).split(":")[1]));
                                //Adding the 'to' time of the NotWorkingHours to endTime - End

                                WeekViewEvent EventsToLoad = new WeekViewEvent(-1, weekViewList.get(0).getNot_working_hours()[i].getMemo_description(), startTime, endTime);
                                EventsToLoad.setColor(getResources().getColor(R.color.yellow));
                                //Based on mNameColor, I'm setting the color of text color in WeekView.java - 833
                                //So only you can use Black and White color. If need more color, based the condition set color in WeekView.java - 833
                                EventsToLoad.setmNameColor("Black");
                                events.add(EventsToLoad);
                            }
                        }
                    }

                    for (int i = 0; i < weekViewList.size(); i++) {
                        Date DateToProcess = StringToDate(weekViewList.get(i).getBooking_time_std().split(",")[0], "dd/MM/yyyy");

                        //This setMonthChangeListener will call 3 times totally(Current, Previous and Next month)
                        //So to add the event only to current month, I'm checking the below condition.
                        //Once newMonth and newYear match with our weekViewList.get(i).getBooking_time_std() means allow inside.
                        if (Integer.parseInt(DateToString(DateToProcess, "MM")) == newMonth && Integer.parseInt(DateToString(DateToProcess, "yyyy")) == newYear) {
                            Calendar startTime = Calendar.getInstance();
                            startTime.setTime(new Date());
                            //Here we adding the 'from' time of the job to startTime - Start
                            SimpleDateFormat ResultFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
                            Date DateStartTime = StringToDate(weekViewList.get(i).getFromTime(), "hh:mm a");
                            //Adding Hours to startTime
                            startTime.set(Calendar.HOUR_OF_DAY, Integer.parseInt(ResultFormat.format(DateStartTime).split(":")[0]));
                            //Adding Minutes to startTime
                            startTime.set(Calendar.MINUTE, Integer.parseInt(ResultFormat.format(DateStartTime).split(":")[1]));
                            //Adding the 'from' time of the job to startTime - End
                            startTime.set(Calendar.DATE, Integer.parseInt(DateToString(DateToProcess, "dd")));
                            startTime.set(Calendar.MONTH, Integer.parseInt(DateToString(DateToProcess, "MM")) - 1);
                            startTime.set(Calendar.YEAR, Integer.parseInt(DateToString(DateToProcess, "yyyy")));

                            Calendar endTime = (Calendar) startTime.clone();
                            //Here we adding the 'to' time of the job to endTime - Start
                            Date DateToTime = StringToDate(weekViewList.get(i).getToTime(), "hh:mm a");

                            //In order to handle the job which start and end less than a 30 min, I'm checking the following condition.
                            long diffInMillies = Math.abs(DateToTime.getTime() - DateStartTime.getTime());
                            if (TimeUnit.MILLISECONDS.toMinutes(diffInMillies) > 30) {
                                //If difference is above 30 min means continue the process.
                                endTime.set(Calendar.HOUR_OF_DAY, Integer.parseInt(ResultFormat.format(DateToTime).split(":")[0]));
                                endTime.set(Calendar.MINUTE, Integer.parseInt(ResultFormat.format(DateToTime).split(":")[1]));
                            } else {
                                //If difference is less than 30 min means I'm adding 30 min with start time and setting it to as end time.
                                Calendar calendar = Calendar.getInstance();
                                calendar.setTime(DateStartTime);
                                calendar.add(Calendar.MINUTE, 30);
                                endTime.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY));
                                endTime.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE));
                            }
                            //Adding the 'to' time of the job to endTime - End

                            //In order to get specific value from weekViewList(list), we are increasing id value at runtime.
                            //At new server request, we need to reassign id as 0. You can find this in the server response function.
                            WeekViewEvent EventsToLoad = new WeekViewEvent(id++, weekViewList.get(i).getJob_id() + "\n\n" + weekViewList.get(i).getUser_name(), startTime, endTime);
                            switch (weekViewList.get(i).getColor()) {
                                case "Blue":
                                    EventsToLoad.setColor(getResources().getColor(R.color.commoncolourback));
                                    break;
                                case "Orange":
                                    EventsToLoad.setColor(getResources().getColor(R.color.orangecol));
                                    break;
                            }
                            //Based on mNameColor, I'm setting the color of text color in WeekView.java - 833
                            //So only you can use Black and White color. If need more color, based the condition set color in WeekView.java - 833
                            EventsToLoad.setmNameColor("White");
                            events.add(EventsToLoad);
                        }
                    }
                }
                return events;
            }
        });

        weekView.setScrollListener(new WeekView.ScrollListener() {
            @Override
            public void onFirstVisibleDayChanged(Calendar newFirstVisibleDay, Calendar oldFirstVisibleDay) {
//                Log.e("List", newFirstVisibleDay.get(Calendar.MONTH) + " " +newFirstVisibleDay.get(Calendar.YEAR));

                //At the first time of view creation, newFirstVisibleDay & oldFirstVisibleDay will be null so we are checking not null.
                //When month get changes, we have to load new data to view. In order to do it checking NewMonth with OldMonth
                if ((newFirstVisibleDay != null && oldFirstVisibleDay != null) && (newFirstVisibleDay.get(Calendar.MONTH) != oldFirstVisibleDay.get(Calendar.MONTH)) && !IsServerRequestLoading) {
                    IsServerRequestLoading = true;
                    Log.e("WeeklyCalendarActivity", "onFirstVisibleDayChanged");
                    //Based on the calendar view scroll in next line we are setting the text to TextView(choosemonth)
                    choosemonth.setText(DateToString(newFirstVisibleDay.getTime(), "MMM yyyy"));

                    myMonthStr = String.valueOf(newFirstVisibleDay.get(Calendar.MONTH) + 1);
                    myYearStr = String.valueOf(newFirstVisibleDay.get(Calendar.YEAR));

                    ServerRequest(WeeklyCalendarActivity.this, ServiceConstant.NEWLEADS_URL,
                            String.valueOf(newFirstVisibleDay.get(Calendar.MONTH) + 1),
                            String.valueOf(newFirstVisibleDay.get(Calendar.YEAR)));
                }
            }
        });


        LinearLayoutSelectMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int MonthToSetInMonthPicker = Integer.parseInt(new SimpleDateFormat("MM", Locale.getDefault()).format(new Date()));
                if (!choosemonth.getText().toString().isEmpty())
                    MonthToSetInMonthPicker = Integer.parseInt(DateToString(StringToDate(choosemonth.getText().toString(), "MMM yyyy"), "MM"));
                final RackMonthPicker rackMonthPicker = new RackMonthPicker(WeeklyCalendarActivity.this)
                        .setLocale(Locale.ENGLISH)
                        .setColorTheme(R.color.orangecol)
                        .setSelectedMonth(MonthToSetInMonthPicker - 1)
                        .setPositiveButton(new DateMonthDialogListener() {
                            @Override
                            public void onDateMonth(int month, int startDate, int endDate, int year, String monthLabel) {
                                //Based on SelectMonth, we are updating the view to specific month
                                Calendar startTime = Calendar.getInstance();
                                startTime.setTime(new Date());
                                startTime.set(Calendar.DATE, 1);
                                startTime.set(Calendar.MONTH, month - 1);
                                weekView.goToDate(startTime);
                                //Updating the view end
                                choosemonth.setText(monthLabel.replace(",", ""));
                                Log.e("WeeklyCalendarActivity", "LinearLayoutSelectMonth");
                                IsServerRequestLoading = true;
                                myMonthStr = String.valueOf(month);
                                myYearStr = String.valueOf(year);

                                ServerRequest(WeeklyCalendarActivity.this, ServiceConstant.NEWLEADS_URL,
                                        String.valueOf(month), String.valueOf(year));
                            }
                        })
                        .setNegativeButton(new OnCancelMonthDialogListener() {
                            @Override
                            public void onCancel(AlertDialog dialog) {
                                dialog.dismiss();
                            }
                        });
                rackMonthPicker.show();
            }
        });
        myMonthStr = DateToString(new Date(), "MM");
        myYearStr = DateToString(new Date(), "yyyy");
        Log.e("WeeklyCalendarActivity", "OnCreate");
        ServerRequest(WeeklyCalendarActivity.this, ServiceConstant.NEWLEADS_URL, DateToString(new Date(), "MM"), DateToString(new Date(), "yyyy"));

    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences pref1 = getApplicationContext().getSharedPreferences("openclosed", 0);
        SharedPreferences.Editor editor1 = pref1.edit();
        editor1.putString("status", "1");
        editor1.apply();
        editor1.commit();
    }


    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences pref1 = getApplicationContext().getSharedPreferences("openclosed", 0);
        SharedPreferences.Editor editor1 = pref1.edit();
        editor1.putString("status", "0");
        editor1.apply();
        editor1.commit();
    }

    private void ServerRequest(Context mContext, String url, String QueryMonth, String QueryYear) {
        if (cd.isConnectingToInternet()) {
            dialog = new LoadingDialog(WeeklyCalendarActivity.this);
            dialog.setLoadingTitle(getResources().getString(R.string.loading_in));
            dialog.show();

            SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
            String provider_id = pref.getString("provider_id", "");
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("provider_id", provider_id);
            jsonParams.put("page", "1");
            jsonParams.put("perPage", "100");

            jsonParams.put("month", QueryMonth);
            jsonParams.put("year", QueryYear);

            ServiceRequest mservicerequest = new ServiceRequest(mContext);
            mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

                @Override
                public void onCompleteListener(String response) {
                    //id value is reassigned here because new value is received from server.
                    id = 0;
                    LinearLayoutCheckInternet.setVisibility(View.GONE);
                    weekView.setVisibility(View.VISIBLE);
                    dialog.dismiss();
                    IsServerRequestLoading = false;
                    System.out.println("--------------WeeklyCalendarActivity-------------------" + response);
                    overallresponse = response;
                    Log.e("WeeklyCalendarActivity", response);
                    try {
                        JSONObject jobject = new JSONObject(response);
                        if (jobject.getString("status").equalsIgnoreCase("1")) {
                            JSONObject obj1 = new JSONObject(overallresponse);
                            JSONObject obj2 = new JSONObject(obj1.getString("response"));

                            //Fetching the CraftMan not working hours from response and storying it in Array
                            JSONArray NotWorkingArray = obj2.getJSONArray("not_working_hours");
                            Not_working_hours[] Not_working_hoursArray = new Not_working_hours[0];
                            if (NotWorkingArray.length() > 0) {
                                Not_working_hoursArray = new Not_working_hours[NotWorkingArray.length()];
                                for (int lo = 0; lo < NotWorkingArray.length(); lo++) {
                                    JSONObject NotWorkingArray_Inside = NotWorkingArray.getJSONObject(lo);
                                    Not_working_hours not_working_hours = new Not_working_hours();
                                    if (NotWorkingArray_Inside.has("FromTime"))
                                        not_working_hours.setFromTime(NotWorkingArray_Inside.getString("FromTime"));
                                    if (NotWorkingArray_Inside.has("ToTime"))
                                        not_working_hours.setToTime(NotWorkingArray_Inside.getString("ToTime"));
                                    if (NotWorkingArray_Inside.has("memo_description"))
                                        not_working_hours.setMemo_description(NotWorkingArray_Inside.getString("memo_description"));
                                    if (NotWorkingArray_Inside.has("date")) {
                                        if (!NotWorkingArray_Inside.getString("date").contains("Invalid date")) {
                                            not_working_hours.setDate(NotWorkingArray_Inside.getString("date").split("T")[0]);
                                        }
                                    }
                                    Not_working_hoursArray[lo] = not_working_hours;
                                }
                            }

                            JSONArray m_jArry = obj2.getJSONArray("jobs");
                            if (m_jArry.length() > 0) {
                                weekViewList = new ArrayList<>();
                                for (int i = 0; i < m_jArry.length(); i++) {
                                    JSONObject jo_inside = m_jArry.getJSONObject(i);
                                    if (!jo_inside.getString("btn_group").equals("8")) {
                                        if (!jo_inside.getString("booking_time_std").contains("Invalid date")) {
                                            String ColorToSetInCalendar = "Blue";
//                                            if (jo_inside.has("check")) {
//                                                ColorToSetInCalendar = "Orange";
//                                            } else if (Integer.parseInt(jo_inside.getString("overall_est_count")) > 0) {
//                                                if (Integer.parseInt(jo_inside.getString("acceptedestimates")) > 0 ||
//                                                        Integer.parseInt(jo_inside.getString("overall_job_count")) > 0) {
//                                                    ColorToSetInCalendar = "Blue";
//                                                } else {
//                                                    ColorToSetInCalendar = "Orange";
//                                                }
//                                            }

                                            if ((Integer.parseInt(jo_inside.getString("overall_est_count")) > 0 && Integer.parseInt(jo_inside.getString("acceptedestimates")) > 0)
                                                    || Integer.parseInt(jo_inside.getString("overall_job_count")) > 0) {
                                                ColorToSetInCalendar = "Blue";
                                            } else {
                                                ColorToSetInCalendar = "Orange";
                                            }

                                            WeekViewPojo weekViewPojo = new WeekViewPojo(jo_inside.getString("job_id"),
                                                    jo_inside.getString("user_name"),
                                                    jo_inside.getString("booking_time_std"),
                                                    ColorToSetInCalendar,
                                                    jo_inside.getString("role"),
                                                    jo_inside.getString("FromTime"),
                                                    jo_inside.getString("ToTime"),
                                                    Not_working_hoursArray);
                                            weekViewList.add(weekViewPojo);
                                        }

                                    }
                                }
                            } else {
                                Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.no_data));
                            }
                            weekView.notifyDatasetChanged();
                        } else {
                            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.no_data));
                        }
                    } catch (Exception e) {
                        dialog.dismiss();
                        IsServerRequestLoading = false;
                        e.printStackTrace();
                    }
                }

                @Override
                public void onErrorListener() {
                    dialog.dismiss();
                    IsServerRequestLoading = false;
                }
            });
        } else {
            LinearLayoutCheckInternet.setVisibility(View.VISIBLE);
            weekView.setVisibility(View.GONE);
        }
    }

    private void Alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(WeeklyCalendarActivity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    String DateToString(Date date, String Format) {
        return new SimpleDateFormat(Format, Locale.US).format(date);
    }

    Date StringToDate(String date, String Format) {
        Date OutPutDate = null;
        try {
            OutPutDate = new SimpleDateFormat(Format, Locale.US).parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return OutPutDate;
    }
}
