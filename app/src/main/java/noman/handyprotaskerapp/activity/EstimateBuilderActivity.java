package noman.handyprotaskerapp.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Dialog.LoadingDialog;
import Dialog.PkDialog;
import Utils.ConnectionDetector;
import Utils.CurrencySymbolConverter;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import noman.handyprotaskerapp.Adapter.EstimateAdapter;
import noman.handyprotaskerapp.Adapter.RecyclerViewDynamicAdapter;
import noman.handyprotaskerapp.Pojo.EstimatedDetails;
import noman.handyprotaskerapp.Pojo.MultiSubItemPojo;
import noman.handyprotaskerapp.Pojo.editbuilderpojo;
import noman.handyprotaskerapp.R;
import noman.handyprotaskerapp.Service.HNDHelper;
import noman.handyprotaskerapp.WidgetSupport.MyGridView;
import noman.handyprotaskerapp.WidgetSupport.listhei;
import noman.handyprotaskerapp.viewestimatebuilder;
import service.ServiceConstant;
import volley.ServiceRequest;

import static android.view.View.VISIBLE;

/**
 * Created by user127 on 16-02-2018.
 */

public class EstimateBuilderActivity extends AppCompatActivity {

    TextView details_orderid, ongoing_detail_jobtype, ongoing_details_dateTv, username;
    LinearLayout imageView;
    LinearLayout displaylist, nodata, downoptions;
    SharedPreferences pref;
    String dataisthere;
    listhei listView;
    String storemybase64 = "";
    TextView done, TextViewSubTotal, TextViewHandyProSavings, TextViewHandyProSavingsLabel,
            TextViewTotal, TextViewHandyCouponNameLabel, TextViewCouponName, TextViewHandyCouponAmountLabel, TextViewCouponAmount;
    ArrayList<editbuilderpojo> arrayList = new ArrayList<editbuilderpojo>();
    SharedPreferences saveim;
    String tjonbid, taddress, tdatetime, tusernamed, tuserphoto, userEmail, franchisee_email;
    String currency;
    String grandtotal = "0", HandyProSavings = "0";
    LinearLayout edit, written;
    TextView hintshow, estimatehead, writtenitemize;
    LinearLayout toplay;
    String storeuserid = "";
    CircleImageView imag;
    CheckBox itemize;
    LinearLayout hideit;
    ImageView setsignimage, ImageViewHistory, ImageViewEdit;
    LinearLayout viewallestimate;
    private editestimationadapter adapter;
    private ConnectionDetector cd;
    private Boolean isInternetPresent = false;
    private LoadingDialog dialog;
    private List<MultiSubItemPojo> MainMultSubItemsList;
    RecyclerViewDynamicAdapter recyclerViewDynamicAdapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.estimatepage);
        cd = new ConnectionDetector(EstimateBuilderActivity.this);

        saveim = getApplicationContext().getSharedPreferences("changordersign", 0);
        SharedPreferences.Editor prefeditord = saveim.edit();
        prefeditord.clear();

        pref = getApplicationContext().getSharedPreferences("logindetails", 0);
        currency = pref.getString("myCurrencySymbol", "");
        imageView = findViewById(R.id.backclick);
        listView = findViewById(R.id.listview);
        setsignimage = findViewById(R.id.setsignimage);
        displaylist = findViewById(R.id.displaylist);
        downoptions = findViewById(R.id.downoptions);
        nodata = findViewById(R.id.nodata);
        edit = findViewById(R.id.editestimate);

        viewallestimate = findViewById(R.id.viewallestimate);

        writtenitemize = findViewById(R.id.writtenitemize);
        written = findViewById(R.id.written);

        toplay = findViewById(R.id.toplay);
        itemize = findViewById(R.id.itemize);
        hintshow = findViewById(R.id.hintshow);
        estimatehead = findViewById(R.id.estimatehead);
        imag = findViewById(R.id.imag);
        hideit = findViewById(R.id.hideit);

        details_orderid = findViewById(R.id.details_orderid);
        ongoing_detail_jobtype = findViewById(R.id.ongoing_detail_jobtype);
        ongoing_details_dateTv = findViewById(R.id.ongoing_details_dateTv);
        username = findViewById(R.id.username);
        ImageViewHistory = findViewById(R.id.ImageViewHistory);
        ImageViewEdit = findViewById(R.id.ImageViewEdit);


        ImageViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EstimateBuilderActivity.this, AddEstimateItemActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        ImageViewHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EstimateBuilderActivity.this, viewestimatebuilder.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        viewallestimate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EstimateBuilderActivity.this, viewestimatebuilder.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
        done = findViewById(R.id.done);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent intent = new Intent(EstimateBuilderActivity.this, BeforeAndAfterPhotoActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();*/

                if (arrayList.size() == 0) {
                    final PkDialog mDialog = new PkDialog(EstimateBuilderActivity.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.action_sorry));
                    mDialog.setDialogMessage(getResources().getString(R.string.no_estimate_send));
                    mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    });
                    mDialog.show();
                } else {
//                    loadingDialog();

                    final AlertDialog.Builder builder = new AlertDialog.Builder(EstimateBuilderActivity.this);
                    // Get the layout inflater
                    LayoutInflater inflater = EstimateBuilderActivity.this.getLayoutInflater();
                    // Inflate the layout for the dialog
                    // Pass null as the parent view because its going in the dialog layout
                    View EmailPopView = inflater.inflate(R.layout.dialog_custom_email_popup, null);
                    // Get your views by using view.findViewById() here and do your listeners.
                    Button ButtonSend = EmailPopView.findViewById(R.id.ButtonSend);
                    final EditText EditTextToEmail = EmailPopView.findViewById(R.id.EditTextToEmail);
                    final EditText EditTextCCEmail = EmailPopView.findViewById(R.id.EditTextCCEmail);
                    final EditText EditTextBCCEmail = EmailPopView.findViewById(R.id.EditTextBCCEmail);
                    final ImageView ImageViewEstimationPreview = EmailPopView.findViewById(R.id.ImageViewEstimationPreview);

                    if (userEmail.length() > 0) {
                        EditTextToEmail.setText(userEmail);
                    }

                    if (franchisee_email.length() > 0) {
                        EditTextBCCEmail.setText(franchisee_email);
                    }
                    // Set the dialog layout
                    builder.setView(EmailPopView);
                    builder.create();
                    final AlertDialog alertDialog = builder.show();

                    ImageViewEstimationPreview.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            SharedPreferences Job_idpref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
                            startActivity(new Intent(EstimateBuilderActivity.this,
                                    EstimationPreviewWebPageActivity.class).putExtra("Job_ID", Job_idpref.getString("jobid", "")));
                        }
                    });

                    ButtonSend.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (PopUpEditTextValidation(EditTextToEmail.getText().toString())
                                    && PopUpEditTextValidation(EditTextCCEmail.getText().toString())
                                    && PopUpEditTextValidation(EditTextBCCEmail.getText().toString())) {
                                /*if (!EditTextToEmail.getText().toString().trim().equals(userEmail))
                                    UserEmailUpdate(EstimateBuilderActivity.this,
                                            EditTextToEmail.getText().toString().trim().toLowerCase(),
                                            EditTextCCEmail.getText().toString().trim().toLowerCase(),
                                            EditTextBCCEmail.getText().toString().trim().toLowerCase(), alertDialog);
                                else
                                    sendrequest(EstimateBuilderActivity.this, ServiceConstant.SENDESTIMATE_URL,
                                            EditTextCCEmail.getText().toString().trim().toLowerCase(),
                                            EditTextBCCEmail.getText().toString().trim().toLowerCase(), alertDialog);*/
                                sendrequest(EstimateBuilderActivity.this, ServiceConstant.SENDESTIMATE_URL,
                                        EditTextCCEmail.getText().toString().trim().toLowerCase(),
                                        EditTextBCCEmail.getText().toString().trim().toLowerCase(), alertDialog);
                            } else {
                                HNDHelper.showErrorAlert(EstimateBuilderActivity.this, "Please enter valid Email");
                            }
                        }
                    });

//                    sendrequest(EstimateBuilderActivity.this, ServiceConstant.SENDESTIMATE_URL);
                }
            }
        });

        TextViewSubTotal = findViewById(R.id.TextViewSubTotal);
        TextViewHandyProSavings = findViewById(R.id.TextViewHandyProSavings);
        TextViewHandyProSavingsLabel = findViewById(R.id.TextViewHandyProSavingsLabel);
        TextViewTotal = findViewById(R.id.TextViewTotal);
        TextViewCouponName = findViewById(R.id.TextViewCouponName);
        TextViewHandyCouponNameLabel = findViewById(R.id.TextViewHandyCouponNameLabel);
        TextViewHandyCouponAmountLabel = findViewById(R.id.TextViewHandyCouponAmountLabel);
        TextViewCouponAmount = findViewById(R.id.TextViewCouponAmount);


        isInternetPresent = cd.isConnectingToInternet();
        /*if (isInternetPresent) {
            paymentPost(EstimateBuilderActivity.this, ServiceConstant.ESTIMATE_URL);
        } else {
            final PkDialog mDialog = new PkDialog(EstimateBuilderActivity.this);
            mDialog.setDialogTitle(getResources().getString(R.string.internet_label));
            mDialog.setDialogMessage(getResources().getString(R.string.no_inetnet_label));
            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                }
            });
            mDialog.show();
        }*/


        nodata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EstimateBuilderActivity.this, AddEstimateItemActivity.class);
                intent.putExtra("mylist1", arrayList);
                intent.putExtra("pos", "sa");
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });


        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("status", "0");
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
    }


    public static boolean isValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public boolean PopUpEditTextValidation(String value) {
        return value.length() <= 0 || isValidEmail(value);
    }

    private void paymentPost(Context mContext, String url) {

        loadingDialog();


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        String provider_ids = pref.getString("provider_id", "");
        jsonParams.put("provider_id", provider_ids);

        SharedPreferences Job_idpref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
        String Job_id = Job_idpref.getString("jobid", "");
        jsonParams.put("job_id", Job_id);


        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("EstimationBuilder", response);
                MainMultSubItemsList = new ArrayList<>();
                arrayList = new ArrayList<>();
                String provide_estimation = "", Str_response = "", Str_jobDescription = "", Str_NeedPayment = "", Str_Currency = "", Str_BtnGroup = "";


                try {
                    JSONObject jobject = new JSONObject(response);

                    grandtotal = jobject.getString("grandtotal");
                    if (jobject.has("handypro_plus_saving"))
                        HandyProSavings = jobject.getString("handypro_plus_saving");

                    if (jobject.has("provide_estimation")) {
                        provide_estimation = jobject.getString("provide_estimation");
                    } else {
                        provide_estimation = "1";
                    }

                    if (jobject.has("coupon") && jobject.getString("coupon").length() > 0)
                        TextViewCouponName.setText(jobject.getString("coupon"));
                    else {
                        TextViewHandyCouponNameLabel.setVisibility(View.GONE);
                        TextViewCouponName.setVisibility(View.GONE);
                    }

                    if (jobject.has("couponamt") && jobject.getString("couponamt").length() > 0)
                        TextViewCouponAmount.setText(currency + jobject.getString("couponamt"));
                    else {
                        TextViewHandyCouponAmountLabel.setVisibility(View.GONE);
                        TextViewCouponAmount.setVisibility(View.GONE);
                    }

                    storeuserid = jobject.getString("user_id");


                    if (jobject.has("job_id")) {
                        tjonbid = jobject.getString("job_id");
                    }

                    if (jobject.has("username")) {
                        tusernamed = jobject.getString("username");
                    }

                    if (jobject.has("avatar")) {
                        tuserphoto = /*ServiceConstant.Review_image+*/jobject.getString("avatar");
                    }

                    if (jobject.has("email")) {
                        userEmail = jobject.getString("email");
                    }

                    if (jobject.has("franchisee_email")) {
                        franchisee_email = jobject.getString("franchisee_email");
                    }


                    if (jobject.has("booking_date")) {
                        tdatetime = jobject.getString("booking_date");
                    }

                    if (jobject.has("job_address")) {
                        taddress = jobject.getString("job_address");
                    }


                    //    String taddress,tdatetime,tusernamed,tuserphoto;


                    if (provide_estimation.equalsIgnoreCase("1")) {
                        String currencyCode = CurrencySymbolConverter.getCurrencySymbol(Str_Currency);
                        String whatsincluded = "";


                        JSONArray jarry = jobject.getJSONArray("estimation_category");
                        if (jarry.length() > 0) {
                            JSONArray EstimationCategoryArry = new JSONArray();
                            for (int i = 0; i < jarry.length(); i++) {
                                JSONObject jobjects_amount = jarry.getJSONObject(i);

                                String title = jobjects_amount.getString("service_type");
                                if (jobjects_amount.has("whatsincluded")) {
                                    whatsincluded = jobjects_amount.getString("whatsincluded");
                                } else {

                                }

                                if (jobjects_amount.has("sub_items") && jobjects_amount.getJSONArray("sub_items").length() > 0) {
                                    JSONArray jsonArraySubItems = jobjects_amount.getJSONArray("sub_items");
                                    MainMultSubItemsList = new ArrayList<>();
                                    for (int j = 0; j < jsonArraySubItems.length(); j++) {
                                        JSONObject jsonObjectSubItems = jsonArraySubItems.getJSONObject(j);
                                        MultiSubItemPojo multiSubItemPojo = new MultiSubItemPojo();
                                        multiSubItemPojo.setName(jsonObjectSubItems.getString("name"));
                                        multiSubItemPojo.setHours(jsonObjectSubItems.getString("hours"));
                                        multiSubItemPojo.setAmount(jsonObjectSubItems.getString("amount"));
                                        multiSubItemPojo.setDescription(jsonObjectSubItems.getString("description"));
                                        multiSubItemPojo.setInnerPosition(j);
                                        multiSubItemPojo.setID(jsonObjectSubItems.getString("_id"));
                                        multiSubItemPojo.setStatus(jsonObjectSubItems.getString("status"));
                                        if (jobjects_amount.has("estimated_details")) {

                                            JSONObject fareobj = jobjects_amount.getJSONObject("estimated_details");
                                            if (fareobj.has("status")) {
                                                multiSubItemPojo.setParentEstimationStatus(Integer.parseInt(fareobj.getString("status")));
                                            }
                                        }
                                        MainMultSubItemsList.add(multiSubItemPojo);
                                    }
                                }


                                if (jobjects_amount.has("estimated_details")) {

                                    JSONObject fareobj = jobjects_amount.getJSONObject("estimated_details");

                                    if (fareobj.has("estimation_images")) {
                                        EstimationCategoryArry = fareobj.getJSONArray("estimation_images");
                                    }

                                    if (fareobj.has("hours")) {
                                        String hours = fareobj.getString("hours");
                                        String materialamount;
                                        String depositamount;


                                        if (fareobj.has("material")) {
                                            materialamount = fareobj.getString("material");

                                            if (materialamount.equalsIgnoreCase("") || materialamount.equalsIgnoreCase(" ")) {
                                                materialamount = "";
                                            } else {
                                                materialamount = currency + materialamount;
                                            }
                                        } else {
                                            materialamount = "";
                                        }


                                        if (fareobj.has("depositamount")) {
                                            depositamount = fareobj.getString("depositamount");

                                            if (depositamount.equalsIgnoreCase("") || depositamount.equalsIgnoreCase(" ")) {
                                                depositamount = "";
                                            } else {
                                                depositamount = currency + depositamount;
                                            }
                                        } else {
                                            depositamount = "";
                                        }


                                        String description = "";
                                        if (fareobj.has("description")) {
                                            description = fareobj.getString("description");
                                            if (description.equalsIgnoreCase("") || description.equalsIgnoreCase(" ")) {
                                                description = "";
                                            } else {
                                                description = description;
                                            }

                                        } else {
                                            description = "";
                                        }


                                        String material_desc;
                                        if (fareobj.has("material_desc")) {
                                            material_desc = fareobj.getString("material_desc");

                                        } else {
                                            material_desc = "";
                                        }


                                        String totalamount = fareobj.getString("totalamount");

                                        String status;
                                        if (fareobj.has("status")) {
                                            status = fareobj.getString("status");
                                        } else {

                                            status = "";
                                        }


                                        String singleamount;
                                        if (fareobj.has("amount")) {
                                            singleamount = fareobj.getString("amount");
                                            if (singleamount.equalsIgnoreCase("") || singleamount.equalsIgnoreCase(" ")) {
                                                singleamount = "";
                                            } else {
                                                singleamount = currency + singleamount;
                                            }

                                        } else {
                                            singleamount = "";
                                        }


                                        String helper_amount;
                                        if (fareobj.has("helper_amount")) {
                                            helper_amount = fareobj.getString("helper_amount");
                                            if (helper_amount.equalsIgnoreCase("") || helper_amount.equalsIgnoreCase(" ")) {
                                                helper_amount = "";
                                            } else {
                                                helper_amount = currency + helper_amount;
                                            }

                                        } else {
                                            helper_amount = "";
                                        }


                                        editbuilderpojo atomPayment = new editbuilderpojo();
                                        atomPayment.setVal2(Float.parseFloat(hours));
                                        atomPayment.setJobtype(title);
                                        atomPayment.setService_id(jobjects_amount.getString("service_id"));
                                        atomPayment.setJobdescription(description);
                                        atomPayment.setTotal(Double.parseDouble(totalamount));

                                        atomPayment.setWhats(whatsincluded);

                                        if (materialamount.endsWith(".0") || materialamount.endsWith(".5") || materialamount.endsWith(".1") || materialamount.endsWith(".2") || materialamount.endsWith(".3") || materialamount.endsWith(".4") || materialamount.endsWith(".6") || materialamount.endsWith(".7") || materialamount.endsWith(".8") || materialamount.endsWith(".9")) {
                                            atomPayment.setMaterial(materialamount + "0");
                                        } else {
                                            atomPayment.setMaterial(materialamount);
                                        }
                                        List<String> ImageArray = new ArrayList<>();
                                        for (int img = 0; img < EstimationCategoryArry.length(); img++) {
                                            ImageArray.add(EstimationCategoryArry.getString(img));
                                        }
                                        EstimatedDetails estimatedDetails = new EstimatedDetails();
                                        estimatedDetails.setEstimationImages(ImageArray);
                                        atomPayment.setEstimatedDetails(estimatedDetails);

                                        atomPayment.setDepositamount(depositamount);
                                        atomPayment.setMaterialdescription(material_desc);
                                        atomPayment.setApprovestatus(status);

                                        if (singleamount.endsWith(".0") || singleamount.endsWith(".5") || singleamount.endsWith(".1") || singleamount.endsWith(".2") || singleamount.endsWith(".3") || singleamount.endsWith(".4") || singleamount.endsWith(".6") || singleamount.endsWith(".7") || singleamount.endsWith(".8") || singleamount.endsWith(".9")) {
                                            atomPayment.setSingleamount(singleamount + "0");
                                        } else {
                                            atomPayment.setSingleamount(singleamount);
                                        }


                                        atomPayment.setHelperamount(helper_amount);
                                        atomPayment.setMultiSubItemPojo(MainMultSubItemsList);
                                        //After assigning value to pojo class, clear the MainMultSubItemsList. If not it'll keep on adding on all estimation.
                                        MainMultSubItemsList = new ArrayList<>();

                                        arrayList.add(atomPayment);
                                    }
                                }
                            }

                        }

                        if (arrayList.size() > 0) {

                            displaylist.setVisibility(View.VISIBLE);
                            nodata.setVisibility(View.GONE);
                            downoptions.setVisibility(View.VISIBLE);

//                            edit.setVisibility(View.VISIBLE);
                            ImageViewEdit.setVisibility(View.VISIBLE);

                            TextViewSubTotal.setText(currency + grandtotal);
                            if (HandyProSavings.isEmpty() || HandyProSavings.equals("0")) {
                                TextViewHandyProSavings.setVisibility(View.GONE);
                                TextViewHandyProSavingsLabel.setVisibility(View.GONE);
                            } else {
                                TextViewHandyProSavingsLabel.setVisibility(VISIBLE);
                                TextViewHandyProSavings.setVisibility(VISIBLE);
                                TextViewHandyProSavings.setText(currency + HandyProSavings);
                            }
                            int IntSubTotalNumber = 0, IntHandyProSavings = 0;
                            double DoubleSubTotalNumber = 0.0, DoubleHandyProSavings = 0.0;
                            if (grandtotal.length() >= 1) {
                                if (grandtotal.matches("[-+]?[0-9]*\\.?[0-9]+")) {
                                    DoubleSubTotalNumber = Double.parseDouble(grandtotal);
                                } else if (grandtotal.matches("[0-9]+")) {
                                    IntSubTotalNumber = Integer.parseInt(grandtotal);
                                }
                            }

                            if (HandyProSavings.length() >= 1) {
                                if (HandyProSavings.matches("[-+]?[0-9]*\\.?[0-9]+")) {
                                    DoubleHandyProSavings = Double.parseDouble(HandyProSavings);
                                    if (DoubleSubTotalNumber > 0) {
                                        String Total = String.valueOf(DoubleSubTotalNumber - DoubleHandyProSavings);
                                        if (Total.endsWith(".0") || Total.endsWith(".5") || Total.endsWith(".1") || Total.endsWith(".2") || Total.endsWith(".3") || Total.endsWith(".4") || Total.endsWith(".6") || Total.endsWith(".7") || Total.endsWith(".8") || Total.endsWith(".9")) {
                                            TextViewTotal.setText(currency + Total + "0");
                                        } else {
                                            TextViewTotal.setText(currency + Total);
                                        }
                                    } else if (IntSubTotalNumber > 0) {
                                        String Total = String.valueOf(IntSubTotalNumber - DoubleHandyProSavings);
                                        if (Total.endsWith(".0") || Total.endsWith(".5") || Total.endsWith(".1") || Total.endsWith(".2") || Total.endsWith(".3") || Total.endsWith(".4") || Total.endsWith(".6") || Total.endsWith(".7") || Total.endsWith(".8") || Total.endsWith(".9")) {
                                            TextViewTotal.setText(currency + Total + "0");
                                        } else {
                                            TextViewTotal.setText(currency + Total);
                                        }
                                    }
                                } else if (grandtotal.matches("[0-9]+")) {
                                    IntHandyProSavings = Integer.parseInt(HandyProSavings);
                                    if (DoubleSubTotalNumber > 0) {
                                        String Total = String.valueOf(DoubleSubTotalNumber - IntHandyProSavings);
                                        if (Total.endsWith(".0") || Total.endsWith(".5") || Total.endsWith(".1") || Total.endsWith(".2") || Total.endsWith(".3") || Total.endsWith(".4") || Total.endsWith(".6") || Total.endsWith(".7") || Total.endsWith(".8") || Total.endsWith(".9")) {
                                            TextViewTotal.setText(currency + Total + "0");
                                        } else {
                                            TextViewTotal.setText(currency + Total);
                                        }
                                    } else if (IntSubTotalNumber > 0) {
                                        String Total = String.valueOf(IntSubTotalNumber - DoubleHandyProSavings);
                                        if (Total.endsWith(".0") || Total.endsWith(".5") || Total.endsWith(".1") || Total.endsWith(".2") || Total.endsWith(".3") || Total.endsWith(".4") || Total.endsWith(".6") || Total.endsWith(".7") || Total.endsWith(".8") || Total.endsWith(".9")) {
                                            TextViewTotal.setText(currency + Total + "0");
                                        } else {
                                            TextViewTotal.setText(currency + Total);
                                        }
                                    }
                                }
                            }


                            listView.setEnabled(false);
                            adapter = new editestimationadapter(EstimateBuilderActivity.this, R.layout.reo_item2, arrayList);
                            listView.setAdapter(adapter);

                            if (jobject.has("estimation_status")) {
                                estimatehead.setText(getResources().getString(R.string.activity_rowitem_estimate));
                                String estimation_status = jobject.getString("estimation_status");
                                hintshow.setVisibility(View.VISIBLE);
                                if (estimation_status.equalsIgnoreCase("0")) {

                                    written.setVisibility(View.VISIBLE);

                                    writtenitemize.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {

                                            if (storemybase64.equals("") || storemybase64.equals(" ")) {

                                                SharedPreferences Job_idpref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
                                                String Job_id = Job_idpref.getString("jobid", "");

                                                SharedPreferences pref1 = getApplicationContext().getSharedPreferences("jjjooo", 0);
                                                SharedPreferences.Editor editor1 = pref1.edit();
                                                editor1.putString("jobbidd", "" + Job_id);
                                                editor1.apply();
                                                editor1.commit();


                                                Intent intent = new Intent(EstimateBuilderActivity.this, CaptureSignActivity.class);
                                                intent.putExtra("From", "EstimateBuilder");
                                                startActivityForResult(intent, ServiceConstant.GetSignatureRequestCode);
                                            } else {
                                                SharedPreferences pref1 = getApplicationContext().getSharedPreferences("senduserid", 0);
                                                SharedPreferences.Editor editor1 = pref1.edit();
                                                editor1.putString("storeuserid", "" + storeuserid);
                                                editor1.putString("storemybase64", "" + storemybase64);
                                                editor1.apply();
                                                editor1.commit();

                                                Intent intent = new Intent(EstimateBuilderActivity.this, ApproveEstimateBuilderActivity.class);
                                                startActivity(intent);
                                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                                finish();
                                            }


                                        }
                                    });

                                    hintshow.setText(getResources().getString(R.string.estimatebuilder_waiting_for_approval));
                                    done.setVisibility(View.GONE);
                                    hideit.setVisibility(View.GONE);
//                                    edit.setVisibility(View.VISIBLE);
                                    ImageViewEdit.setVisibility(View.VISIBLE);
                                    toplay.setVisibility(View.VISIBLE);
                                    details_orderid.setText(tjonbid);
                                    ongoing_detail_jobtype.setText(taddress);
                                    ongoing_details_dateTv.setText(getResources().getString(R.string.datenotavailable));
                                    username.setText(tusernamed);
                                    if (tuserphoto.isEmpty()) {
                                        imag.setImageResource(R.drawable.ic_no_user);
                                    } else {
                                        Picasso.with(EstimateBuilderActivity.this)
                                                .load(tuserphoto)

                                                .placeholder(R.drawable.ic_no_user)   // optional
                                                .error(R.drawable.ic_no_user)      // optional
                                                // optional
                                                .into(imag);
                                    }
                                } else if (estimation_status.equalsIgnoreCase("1")) {


                                    written.setVisibility(View.GONE);
//                                    edit.setVisibility(View.INVISIBLE);
                                    ImageViewEdit.setVisibility(View.GONE);


                                    toplay.setVisibility(View.VISIBLE);
                                    details_orderid.setText(tjonbid);
                                    ongoing_detail_jobtype.setText(taddress);
                                    ongoing_details_dateTv.setText(tdatetime);
                                    username.setText(tusernamed);
                                    if (tuserphoto.isEmpty()) {
                                        imag.setImageResource(R.drawable.ic_no_user);
                                    } else {
                                        Picasso.with(EstimateBuilderActivity.this)
                                                .load(tuserphoto)

                                                .placeholder(R.drawable.ic_no_user)   // optional
                                                .error(R.drawable.ic_no_user)      // optional
                                                // optional
                                                .into(imag);
                                    }

                                    hintshow.setText(getResources().getString(R.string.estimatebuilder_user_accepted));
                                    done.setVisibility(View.GONE);
                                    hideit.setVisibility(View.GONE);
//                                    edit.setVisibility(View.INVISIBLE);
                                    ImageViewEdit.setVisibility(View.GONE);

                                } else if (estimation_status.equalsIgnoreCase("2")) {

                                    written.setVisibility(View.GONE);
                                    hintshow.setText(getResources().getString(R.string.estimatebuilder_user_rejected));
                                    done.setVisibility(View.GONE);
                                    hideit.setVisibility(View.GONE);
//                                    edit.setVisibility(View.VISIBLE);
                                    ImageViewEdit.setVisibility(View.VISIBLE);
                                    toplay.setVisibility(View.VISIBLE);
                                    details_orderid.setText(tjonbid);
                                    ongoing_detail_jobtype.setText(taddress);
                                    ongoing_details_dateTv.setText(getResources().getString(R.string.datenotavailable));
                                    username.setText(tusernamed);
                                    if (tuserphoto.isEmpty()) {
                                        imag.setImageResource(R.drawable.ic_no_user);
                                    } else {
                                        Picasso.with(EstimateBuilderActivity.this)
                                                .load(tuserphoto)

                                                .placeholder(R.drawable.ic_no_user)   // optional
                                                .error(R.drawable.ic_no_user)      // optional
                                                // optional
                                                .into(imag);
                                    }
                                }

                            } else {
                                hintshow.setVisibility(View.VISIBLE);
                                hintshow.setText(getResources().getString(R.string.estimatebuilder_waiting_send));

                                done.setVisibility(View.VISIBLE);
                                hideit.setVisibility(View.VISIBLE);
//                                edit.setVisibility(View.VISIBLE);
                                ImageViewEdit.setVisibility(View.VISIBLE);
                            }

                        } else {
                            hintshow.setVisibility(View.GONE);
//                            edit.setVisibility(View.INVISIBLE);
                            ImageViewEdit.setVisibility(View.GONE);
                            downoptions.setVisibility(View.GONE);
                            displaylist.setVisibility(View.GONE);
                            /*nodata.setVisibility(View.VISIBLE);*/
                            //Since Craftsman not yet submitted the Estimation so based on response directly we are moving to AddEstimateItemActivity Activity
                            Intent intent = new Intent(EstimateBuilderActivity.this, AddEstimateItemActivity.class);
                            intent.putExtra("mylist1", arrayList);
                            intent.putExtra("pos", "sa");
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        }

                        JSONArray estimation_history = jobject.getJSONArray("estimation_history");
                        if (estimation_history.length() > 0) {
                            viewallestimate.setVisibility(View.GONE);
                            ImageViewHistory.setVisibility(View.VISIBLE);
                        }

                    } else {
                        final PkDialog mDialog = new PkDialog(EstimateBuilderActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.failed_label));
                        mDialog.setDialogMessage(getResources().getString(R.string.sorry_invalid_data));
                        mDialog.setPositiveButton(getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                            }
                        });
                        mDialog.show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }


                dismissDialog();

            }

            @Override
            public void onErrorListener() {
                dismissDialog();
            }
        });
    }

    private void loadingDialog() {

        dialog = new LoadingDialog(EstimateBuilderActivity.this);
        dialog.setLoadingTitle(getResources().getString(R.string.loading_in));
        dialog.show();
    }

    private void dismissDialog() {

        dialog.dismiss();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR


            Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("status", "0");
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);


            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    protected void onResume() {
        super.onResume();

        if (isInternetPresent) {
            paymentPost(EstimateBuilderActivity.this, ServiceConstant.ESTIMATE_URL);
        } else {
            final PkDialog mDialog = new PkDialog(EstimateBuilderActivity.this);
            mDialog.setDialogTitle(getResources().getString(R.string.internet_label));
            mDialog.setDialogMessage(getResources().getString(R.string.no_inetnet_label));
            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                }
            });
            mDialog.show();
        }
    }

    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }

    private void sendrequest(final Context aContext, String url, String CC, String BCC, final AlertDialog alertDialog) {

        loadingDialog();
        HashMap<String, String> params = new HashMap<>();


        SharedPreferences Job_idpref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
        String Job_id = Job_idpref.getString("jobid", "");
        params.put("job_id", Job_id);


        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        String provider_id = pref.getString("provider_id", "");
        params.put("provider_id", provider_id);

        params.put("status", "0");

        if (itemize.isChecked()) {
            params.put("estimation_itemize", "1");
        } else {
            params.put("estimation_itemize", "0");
        }

        params.put("cc", CC);
        params.put("bcc", BCC);

        alertDialog.dismiss();
        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {


                dismissDialog();

                System.out.println("-------------get Confirm Category list Response----------------" + response);


                try {
                    JSONObject object = new JSONObject(response);
                    String message = object.getString("response");
                    if (object.getString("status").equalsIgnoreCase("1")) {
                        Toast.makeText(EstimateBuilderActivity.this, "Estimation sent successfully", Toast.LENGTH_SHORT).show();

                        written.setVisibility(View.VISIBLE);
                        writtenitemize.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (storemybase64.equals("") || storemybase64.equals(" ")) {

                                    SharedPreferences Job_idpref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
                                    String Job_id = Job_idpref.getString("jobid", "");

                                    SharedPreferences pref1 = getApplicationContext().getSharedPreferences("jjjooo", 0);
                                    SharedPreferences.Editor editor1 = pref1.edit();
                                    editor1.putString("jobbidd", "" + Job_id);
                                    editor1.apply();


                                    Intent intent = new Intent(EstimateBuilderActivity.this, CaptureSignActivity.class);
                                    intent.putExtra("From", "EstimateBuilder");
                                    startActivityForResult(intent, ServiceConstant.GetSignatureRequestCode);
                                } else {
                                    SharedPreferences pref1 = getApplicationContext().getSharedPreferences("senduserid", 0);
                                    SharedPreferences.Editor editor1 = pref1.edit();
                                    editor1.putString("storeuserid", "" + storeuserid);
                                    editor1.putString("storemybase64", "" + storemybase64);
                                    editor1.apply();
                                    editor1.commit();

                                    Intent intent = new Intent(EstimateBuilderActivity.this, ApproveEstimateBuilderActivity.class);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                    finish();
                                }

                            }
                        });
                        done.setVisibility(View.GONE);
                        hideit.setVisibility(View.GONE);
                        hintshow.setText(getResources().getString(R.string.estimatebuilder_waiting_for_approval));


                    } else {
                        written.setVisibility(View.GONE);
                        final PkDialog mDialog = new PkDialog(EstimateBuilderActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.failed_label));
                        mDialog.setDialogMessage(message);
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        mDialog.dismiss();
                                    }
                                }
                        );


                        mDialog.show();

                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block


                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {
                dismissDialog();
            }
        });
    }

    private void UserEmailUpdate(final Context context, String UserEmail, final String CC, final String BCC, final AlertDialog alertDialog) {
        loadingDialog();
        HashMap<String, String> params = new HashMap<>();

        params.put("email_id", UserEmail);
        params.put("user_id", storeuserid);
        ServiceRequest mRequest = new ServiceRequest(context);
        mRequest.makeServiceRequest(ServiceConstant.UPDATE_USER_EMAIL_URL, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                dismissDialog();
                Log.e("EmailUpdate", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.get("status").toString().equalsIgnoreCase("1")) {
                        sendrequest(EstimateBuilderActivity.this, ServiceConstant.SENDESTIMATE_URL, CC, BCC, alertDialog);
                    } else {
                        HNDHelper.showErrorAlert(EstimateBuilderActivity.this, jsonObject.getJSONObject("response").toString());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                dismissDialog();
            }
        });


    }

    public class editestimationadapter extends ArrayAdapter<editbuilderpojo> {
        protected final String LOG_TAG = EstimateAdapter.class.getSimpleName();
        Typeface tf;
        SharedPreferences pref;
        String currency;
        private ArrayList<editbuilderpojo> items;
        private int layoutResourceId;
        private Context context;
        private GridViewAdapter gridAdapter;

        editestimationadapter(Context context, int layoutResourceId, ArrayList<editbuilderpojo> items) {
            super(context, layoutResourceId, items);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.items = items;
            tf = Typeface.createFromAsset(getContext().getAssets(), "Poppins-Regular.ttf");
            pref = context.getSharedPreferences("logindetails", 0);
            currency = pref.getString("myCurrencySymbol", "");
        }

        @NonNull
        @SuppressLint("ViewHolder")
        @Override
        public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
            View row = convertView;
            editestimationadapter.Holder holder = null;

            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new editestimationadapter.Holder();
            holder.bean = items.get(position);

            holder.materialdn = row.findViewById(R.id.materialdn);
            holder.jobdn = row.findViewById(R.id.jobdn);
            holder.matamt = row.findViewById(R.id.matamt);

            holder.whatsin = row.findViewById(R.id.whatsin);

            holder.etVal1 = row.findViewById(R.id.edVal1);
            holder.etVal2 = row.findViewById(R.id.edValue2);

            holder.deletecraftman = row.findViewById(R.id.deletecraftman);
            holder.ImageViewDeleteEstimation = row.findViewById(R.id.ImageViewDeleteEstimation);
            holder.gridview = row.findViewById(R.id.gridview);

            holder.etVal1.setTypeface(tf);
            holder.etVal2.setTypeface(tf);

            holder.materialdn.setTypeface(tf);
            holder.matamt.setTypeface(tf);
            holder.jobdn.setTypeface(tf);
            holder.whatsin.setTypeface(tf);


            holder.jobtytpeheading = row.findViewById(R.id.jobtytpeheading);
            holder.jobdescriptionheading = row.findViewById(R.id.jobdescriptionheading);
            holder.amountheading = row.findViewById(R.id.amountheading);
            holder.hourheadining = row.findViewById(R.id.hourheadining);
            holder.materialheading = row.findViewById(R.id.materialheading);
            holder.materialdescheading = row.findViewById(R.id.materialdescheading);
            holder.totalamount = row.findViewById(R.id.totalamount);


            holder.jobamount = row.findViewById(R.id.jobamount);
            holder.materialdhead = row.findViewById(R.id.materialdhead);
            holder.helperamount = row.findViewById(R.id.helperamount);
            holder.depositamount = row.findViewById(R.id.depositamount);


            holder.materialdhead.setTypeface(tf);
            holder.helperamount.setTypeface(tf);
            holder.jobtytpeheading.setTypeface(tf);
            holder.jobdescriptionheading.setTypeface(tf);
            holder.amountheading.setTypeface(tf);
            holder.hourheadining.setTypeface(tf);
            holder.materialheading.setTypeface(tf);
            holder.materialdescheading.setTypeface(tf);
            holder.totalamount.setTypeface(tf);
            holder.totalamount.setTypeface(tf);
            holder.jobamount.setTypeface(tf);

            holder.depositamount.setTypeface(tf);

            holder.materialdnlin = row.findViewById(R.id.materialdnlin);
            holder.matamlin = row.findViewById(R.id.matamlin);
            holder.jobdnlin = row.findViewById(R.id.jobdnlin);
            holder.jobamountlin = row.findViewById(R.id.jobamountlin);

            holder.helperamountlin = row.findViewById(R.id.helperamountlin);
            holder.depositamountlin = row.findViewById(R.id.depositamountlin);

            holder.materialdesc = row.findViewById(R.id.materialdesc);
            holder.materialamount = row.findViewById(R.id.materialamount);
            holder.jobdescription = row.findViewById(R.id.jobdescription);
            holder.RecyclerViewDynamic = row.findViewById(R.id.RecyclerViewDynamic);
            LinearLayoutManager llm = new LinearLayoutManager(EstimateBuilderActivity.this);
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            holder.RecyclerViewDynamic.setLayoutManager(llm);
            holder.RecyclerViewDynamic.setHasFixedSize(true);

            holder.materialdesc.setTypeface(tf);
            holder.jobdescription.setTypeface(tf);
            holder.materialdescheading.setTypeface(tf);
            holder.materialamount.setTypeface(tf);


            row.setTag(holder);

            setupItem(holder);

            SetUpSubItems(holder, position);

            if (items.get(position).getEstimatedDetails().getEstimationImages().size() > 0) {
                holder.gridview.setEnabled(false);
                gridAdapter = new GridViewAdapter(EstimateBuilderActivity.this, R.layout.grid_item, items.get(position).getEstimatedDetails().getEstimationImages());
                holder.gridview.setAdapter(gridAdapter);
                holder.gridview.setVisibility(VISIBLE);
            } else {
                holder.gridview.setVisibility(View.GONE);
            }


            return row;
        }

        private void SetUpSubItems(final editestimationadapter.Holder holder, final int position) {
            if (holder.bean.getMultiSubItemPojo().size() > 0) {
                List<MultiSubItemPojo> TempMainMultSubItemsList = new ArrayList<>();
                for (int i = 0; i < holder.bean.getMultiSubItemPojo().size(); i++) {
                    MultiSubItemPojo multiSubItemPojo = new MultiSubItemPojo();
                    multiSubItemPojo.setName(holder.bean.getMultiSubItemPojo().get(i).getName());
                    multiSubItemPojo.setHours(holder.bean.getMultiSubItemPojo().get(i).getHours());
                    multiSubItemPojo.setAmount(holder.bean.getMultiSubItemPojo().get(i).getAmount());
                    multiSubItemPojo.setDescription(holder.bean.getMultiSubItemPojo().get(i).getDescription());
                    multiSubItemPojo.setInnerPosition(holder.bean.getMultiSubItemPojo().get(i).getInnerPosition());
                    multiSubItemPojo.setID(holder.bean.getMultiSubItemPojo().get(i).getID());
                    multiSubItemPojo.setStatus(holder.bean.getMultiSubItemPojo().get(i).getStatus());
                    multiSubItemPojo.setParentEstimationStatus(holder.bean.getMultiSubItemPojo().get(i).getParentEstimationStatus());
                    TempMainMultSubItemsList.add(multiSubItemPojo);
                }

                recyclerViewDynamicAdapter = new RecyclerViewDynamicAdapter(
                        EstimateBuilderActivity.this, TempMainMultSubItemsList, position, "EstimationBuilderActivity", new RecyclerViewDynamicAdapter.RecyclerViewDynamicAdapterInterface() {
                    @Override
                    public void Delete(int InnerPosition) {
                        if (holder.bean.getMultiSubItemPojo().size() > 1) {
                            for (int i = 0; i < holder.bean.getMultiSubItemPojo().size(); i++) {
                                if (holder.bean.getMultiSubItemPojo().get(i).getInnerPosition() == InnerPosition) {

                                    final int finalI = i;
                                    new AlertDialog.Builder(EstimateBuilderActivity.this)
                                            .setTitle("Delete")
                                            .setMessage("Are you sure want to delete this sub item?")

                                            // Specifying a listener allows you to take an action before dismissing the dialog.
                                            // The dialog is automatically dismissed when a dialog button is clicked.
                                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // Continue with delete operation
                                                    dialog.dismiss();
                                                    loadingDialog();

                                                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                                                    SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
                                                    String provider_ids = pref.getString("provider_id", "");
                                                    jsonParams.put("provider_id", provider_ids);

                                                    SharedPreferences Job_idpref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
                                                    String Job_id = Job_idpref.getString("jobid", "");
                                                    jsonParams.put("job_id", Job_id);

                                                    jsonParams.put("service_id", holder.bean.getService_id());
                                                    jsonParams.put("subitem_id", holder.bean.getMultiSubItemPojo().get(finalI).getID());

                                                    ServiceRequest mservicerequest = new ServiceRequest(EstimateBuilderActivity.this);
                                                    mservicerequest.makeServiceRequest(ServiceConstant.SubItemsDelete, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
                                                        @Override
                                                        public void onCompleteListener(String response) {
                                                            dismissDialog();
                                                            try {
                                                                JSONObject jobject = new JSONObject(response);
                                                                if (jobject.getString("status").equalsIgnoreCase("1")) {
                                                                    if (isInternetPresent) {
                                                                        paymentPost(EstimateBuilderActivity.this, ServiceConstant.ESTIMATE_URL);
                                                                    } else {
                                                                        final PkDialog mDialog = new PkDialog(EstimateBuilderActivity.this);
                                                                        mDialog.setDialogTitle(getResources().getString(R.string.internet_label));
                                                                        mDialog.setDialogMessage(getResources().getString(R.string.no_inetnet_label));
                                                                        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                                                            @Override
                                                                            public void onClick(View v) {
                                                                                mDialog.dismiss();
                                                                            }
                                                                        });
                                                                        mDialog.show();
                                                                    }
                                                                } else if (jobject.getString("status").equalsIgnoreCase("0")) {
                                                                    final PkDialog mDialog = new PkDialog(EstimateBuilderActivity.this);
                                                                    mDialog.setDialogTitle(getResources().getString(R.string.sorry));
                                                                    mDialog.setDialogMessage(jobject.getString("response"));
                                                                    mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                                                        @Override
                                                                        public void onClick(View v) {
                                                                            if (isInternetPresent) {
                                                                                paymentPost(EstimateBuilderActivity.this, ServiceConstant.ESTIMATE_URL);
                                                                            } else {
                                                                                final PkDialog mDialog = new PkDialog(EstimateBuilderActivity.this);
                                                                                mDialog.setDialogTitle(getResources().getString(R.string.internet_label));
                                                                                mDialog.setDialogMessage(getResources().getString(R.string.no_inetnet_label));
                                                                                mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                                                                    @Override
                                                                                    public void onClick(View v) {
                                                                                        mDialog.dismiss();
                                                                                    }
                                                                                });
                                                                                mDialog.show();
                                                                            }
                                                                            mDialog.dismiss();
                                                                        }
                                                                    });
                                                                    mDialog.show();
                                                                }
                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }

                                                        @Override
                                                        public void onErrorListener() {
                                                            dismissDialog();
                                                        }
                                                    });
                                                }
                                            })

                                            // A null listener allows the button to dismiss the dialog and take no further action.
                                            .setNegativeButton(android.R.string.no, null)
                                            .setIcon(R.drawable.craftmanappmainlogo)
                                            .show();
                                }
                            }
                        }
                    }

                    @Override
                    public void Edit(int InnerPosition) {

                    }
                });

                holder.RecyclerViewDynamic.setAdapter(recyclerViewDynamicAdapter);
                recyclerViewDynamicAdapter.notifyDataSetChanged();
                adapter.notifyDataSetChanged();

            }
        }

        private void setupItem(final editestimationadapter.Holder holder) {

            if (items.size() > 1) {
                holder.ImageViewDeleteEstimation.setVisibility(VISIBLE);
            } else {
                holder.ImageViewDeleteEstimation.setVisibility(View.GONE);
            }

            if (holder.bean.getWhats().equals("")) {
                holder.whatsin.setVisibility(View.GONE);
            } else {
                holder.whatsin.setVisibility(View.VISIBLE);
            }

            holder.ImageViewDeleteEstimation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AlertDialog.Builder(EstimateBuilderActivity.this)
                            .setTitle("Delete")
                            .setMessage("Are you sure want to delete this estimation?")

                            // Specifying a listener allows you to take an action before dismissing the dialog.
                            // The dialog is automatically dismissed when a dialog button is clicked.
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Continue with delete operation
                                    dialog.dismiss();
                                    loadingDialog();

                                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                                    SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
                                    String provider_ids = pref.getString("provider_id", "");
                                    jsonParams.put("provider_id", provider_ids);

                                    SharedPreferences Job_idpref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
                                    String Job_id = Job_idpref.getString("jobid", "");
                                    jsonParams.put("booking_id", Job_id);

                                    jsonParams.put("service_id", holder.bean.getService_id());
                                    jsonParams.put("service_type", holder.bean.getJobtype());

                                    ServiceRequest mservicerequest = new ServiceRequest(EstimateBuilderActivity.this);
                                    mservicerequest.makeServiceRequest(ServiceConstant.deletestimate, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
                                        @Override
                                        public void onCompleteListener(String response) {
                                            dismissDialog();
                                            try {
                                                JSONObject jobject = new JSONObject(response);
                                                if (jobject.getString("status").equalsIgnoreCase("1")) {
                                                    if (isInternetPresent) {
                                                        arrayList.clear();
                                                        arrayList = new ArrayList<>();
                                                        paymentPost(EstimateBuilderActivity.this, ServiceConstant.ESTIMATE_URL);
                                                    } else {
                                                        final PkDialog mDialog = new PkDialog(EstimateBuilderActivity.this);
                                                        mDialog.setDialogTitle(getResources().getString(R.string.internet_label));
                                                        mDialog.setDialogMessage(getResources().getString(R.string.no_inetnet_label));
                                                        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View v) {
                                                                mDialog.dismiss();
                                                            }
                                                        });
                                                        mDialog.show();
                                                    }
                                                } else if (jobject.getString("status").equalsIgnoreCase("0")) {
                                                    final PkDialog mDialog = new PkDialog(EstimateBuilderActivity.this);
                                                    mDialog.setDialogTitle(getResources().getString(R.string.sorry));
                                                    mDialog.setDialogMessage(jobject.getString("response"));
                                                    mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            mDialog.dismiss();
                                                            if (isInternetPresent) {
                                                                arrayList.clear();
                                                                arrayList = new ArrayList<>();
                                                                paymentPost(EstimateBuilderActivity.this, ServiceConstant.ESTIMATE_URL);
                                                            } else {
                                                                final PkDialog mDialog = new PkDialog(EstimateBuilderActivity.this);
                                                                mDialog.setDialogTitle(getResources().getString(R.string.internet_label));
                                                                mDialog.setDialogMessage(getResources().getString(R.string.no_inetnet_label));
                                                                mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(View v) {
                                                                        mDialog.dismiss();
                                                                    }
                                                                });
                                                                mDialog.show();
                                                            }
                                                        }
                                                    });
                                                    mDialog.show();
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void onErrorListener() {
                                            dismissDialog();
                                        }
                                    });
                                }
                            })

                            // A null listener allows the button to dismiss the dialog and take no further action.
                            .setNegativeButton(android.R.string.no, null)
                            .setIcon(R.drawable.craftmanappmainlogo)
                            .show();
                }
            });


            holder.whatsin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    HNDHelper.showErrorAlert(EstimateBuilderActivity.this, holder.bean.getWhats());
                }
            });

            holder.jobdescription.setText(holder.bean.getJobdescription());
            holder.jobtytpeheading.setText(holder.bean.getJobtype());


            switch (holder.bean.getApprovestatus()) {
                case "2":
                    holder.deletecraftman.setBackgroundResource(R.drawable.cancelmark);
                    holder.deletecraftman.setVisibility(View.VISIBLE);
                    holder.ImageViewDeleteEstimation.setVisibility(View.GONE);
                    break;
                case "1":
                    holder.deletecraftman.setBackgroundResource(R.drawable.ticked);
                    holder.deletecraftman.setVisibility(View.VISIBLE);
                    holder.ImageViewDeleteEstimation.setVisibility(View.GONE);
                    break;
                default:
                    holder.deletecraftman.setVisibility(View.GONE);
                    break;
            }

            if (holder.bean.getMaterialdescription().equalsIgnoreCase("") || holder.bean.getMaterialdescription().equalsIgnoreCase(" ")) {
                holder.materialdnlin.setVisibility(View.GONE);
            } else {
                holder.materialdnlin.setVisibility(View.VISIBLE);
                holder.materialdn.setText(Html.fromHtml(holder.bean.getMaterialdescription()), TextView.BufferType.SPANNABLE);
            }


            if (holder.bean.getHelperamount().equalsIgnoreCase("") || holder.bean.getHelperamount().equalsIgnoreCase(" ")) {
                holder.helperamountlin.setVisibility(View.GONE);
            } else {
                holder.helperamountlin.setVisibility(View.VISIBLE);
                holder.helperamount.setText(Html.fromHtml(holder.bean.getHelperamount()), TextView.BufferType.SPANNABLE);
            }


            if (holder.bean.getSingleamount().equalsIgnoreCase("") || holder.bean.getSingleamount().equalsIgnoreCase(" ")) {
                holder.jobamountlin.setVisibility(View.GONE);
            } else {
                holder.jobamountlin.setVisibility(View.VISIBLE);
                holder.jobamount.setText(Html.fromHtml(holder.bean.getSingleamount()), TextView.BufferType.SPANNABLE);
            }


            if (holder.bean.getJobdescription().equalsIgnoreCase("") || holder.bean.getJobdescription().equalsIgnoreCase(" ")) {
                holder.jobdnlin.setVisibility(View.GONE);
            } else {
                holder.jobdnlin.setVisibility(View.VISIBLE);
                holder.jobdn.setText(Html.fromHtml(holder.bean.getJobdescription()), TextView.BufferType.SPANNABLE);
            }


            if (holder.bean.getDepositamount().equalsIgnoreCase("") || holder.bean.getDepositamount().equalsIgnoreCase("0") || holder.bean.getDepositamount().equalsIgnoreCase("$0") || holder.bean.getDepositamount().equalsIgnoreCase(" ")) {
                holder.depositamountlin.setVisibility(View.GONE);
            } else {
                holder.depositamountlin.setVisibility(View.VISIBLE);
                holder.depositamount.setText(Html.fromHtml(holder.bean.getDepositamount()), TextView.BufferType.SPANNABLE);
            }


            if (holder.bean.getMaterial().equalsIgnoreCase("") || holder.bean.getMaterial().equalsIgnoreCase(" ")) {
                holder.matamlin.setVisibility(View.GONE);
            } else {
                holder.matamlin.setVisibility(View.VISIBLE);
                holder.matamt.setText(Html.fromHtml(holder.bean.getMaterial()), TextView.BufferType.SPANNABLE);
            }


            holder.etVal1.setText(String.valueOf(holder.bean.getVal1()));
            holder.etVal2.setText(String.valueOf(holder.bean.getVal2()));
            String grandtotal = "" + holder.bean.getTotal();
            if (grandtotal.endsWith(".0") || grandtotal.endsWith(".5") || grandtotal.endsWith(".1") || grandtotal.endsWith(".2") || grandtotal.endsWith(".3") || grandtotal.endsWith(".4") || grandtotal.endsWith(".6") || grandtotal.endsWith(".7") || grandtotal.endsWith(".8") || grandtotal.endsWith(".9")) {
                holder.totalamount.setText(currency + holder.bean.getTotal() + "0");
            } else {
                holder.totalamount.setText(currency + holder.bean.getTotal());
            }


        }

        public class Holder {
            ImageView deletecraftman, ImageViewDeleteEstimation;
            editbuilderpojo bean;
            EditText etVal1;
            LinearLayout matamlin, jobdnlin, materialdnlin, jobamountlin, helperamountlin, depositamountlin;
            EditText etVal2, jobdescription, materialamount, materialdesc;
            TextView materialdhead, depositamount, helperamount, materialdn, matamt, whatsin, jobdn, totalamount, jobamount, jobtytpeheading, jobdescriptionheading, amountheading, hourheadining, materialheading, materialdescheading;
            MyGridView gridview;
            RecyclerView RecyclerViewDynamic;
        }


    }

    public class GridViewAdapter extends ArrayAdapter {
        private Context context;
        private List<String> ListImage = new ArrayList<String>();
        private int resourceId;
        private LayoutInflater inflater;

        private GridViewAdapter(Context context, int resourceId, List<String> ListImage) {
            super(context, resourceId, ListImage);
            this.resourceId = resourceId;
            this.context = context;
            this.ListImage = ListImage;
            inflater = LayoutInflater.from(context);

        }

        //I prefer to have Holder to keep all controls
        //So that I can recycle easily in getView
        class ViewHolder {
            ImageView image, close;
            LinearLayout LinearLayoutMainHolder;
        }


        @NonNull
        @Override
        public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
            View row = convertView;
            GridViewAdapter.ViewHolder holder = null;
            if (row == null) {
                row = inflater.inflate(resourceId, parent, false);
                holder = new GridViewAdapter.ViewHolder();
                holder.image = row.findViewById(R.id.image);
                holder.close = row.findViewById(R.id.close);
                holder.LinearLayoutMainHolder = row.findViewById(R.id.LinearLayoutMainHolder);
                row.setTag(holder);
            } else {
                holder = (GridViewAdapter.ViewHolder) row.getTag(); //Easy to recycle view
            }

            holder.close.setVisibility(View.GONE);


            Picasso.with(context).load(ListImage.get(position))
                    .resize(160, 160)
                    .placeholder(R.drawable.nouserimg)
                    .into(holder.image);
            holder.LinearLayoutMainHolder.setVisibility(View.VISIBLE);
            holder.close.setVisibility(View.GONE);


            return row;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ServiceConstant.GetSignatureRequestCode && resultCode == RESULT_OK) {

            String bitmaa = saveim.getString("sign", "");
            if (bitmaa.equals("") || bitmaa.equals(" ")) {
                storemybase64 = "";
                setsignimage.setVisibility(View.GONE);
            } else {
                Bitmap src = BitmapFactory.decodeFile(bitmaa);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                src.compress(Bitmap.CompressFormat.PNG, 100, baos);
                //  byte[] data = baos.toByteArray();
                storemybase64 = Base64.encodeToString(getBytesFromBitmap(src),
                        Base64.NO_WRAP);
                Log.v("log_tag", "Base64: " + storemybase64);


                setsignimage.setVisibility(VISIBLE);

                byte[] decodedString = Base64.decode(storemybase64, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                setsignimage.setImageBitmap(decodedByte);

                // setsignimage.setRotation(90);
                SharedPreferences pref1 = getApplicationContext().getSharedPreferences("senduserid", 0);
                SharedPreferences.Editor editor1 = pref1.edit();
                editor1.putString("storeuserid", "" + storeuserid);
                editor1.putString("storemybase64", "" + storemybase64);
                editor1.apply();
                editor1.commit();

                Intent intent = new Intent(EstimateBuilderActivity.this, ApproveEstimateBuilderActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        }
    }
}
