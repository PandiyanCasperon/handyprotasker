package noman.handyprotaskerapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import Dialog.LoadingDialog;
import Dialog.PkDialog;
import Utils.ConnectionDetector;
import Utils.CurrencySymbolConverter;
import de.hdodenhof.circleimageview.CircleImageView;
import noman.handyprotaskerapp.Adapter.EstimateAdapter;
import noman.handyprotaskerapp.Pojo.editbuilderpojo;
import noman.handyprotaskerapp.Service.HNDHelper;
import noman.handyprotaskerapp.WidgetSupport.listhei;
import service.ServiceConstant;
import volley.ServiceRequest;

/**
 * Created by user127 on 16-02-2018.
 */

public class viewestimatebuilder extends AppCompatActivity {

    TextView details_orderid, ongoing_detail_jobtype, ongoing_details_dateTv, username;
    LinearLayout imageView;
    LinearLayout displaylist;
    SharedPreferences pref;
    String dataisthere;
    listhei listView;
    String storemybase64 = "";

    private editestimationadapter adapter;
    ArrayList<editbuilderpojo> arrayList = new ArrayList<editbuilderpojo>();
    private ConnectionDetector cd;
    private Boolean isInternetPresent = false;
    SharedPreferences saveim;
    String tjonbid, taddress, tdatetime, tusernamed, tuserphoto;

    String currency;
    String grandtotal = "0";

    TextView hintshow, estimatehead;

    String storeuserid = "";
    private LoadingDialog dialog;
    CircleImageView imag;


    ImageView setsignimage;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewallestimtate);
        cd = new ConnectionDetector(viewestimatebuilder.this);

        saveim = getApplicationContext().getSharedPreferences("changordersign", 0);
        SharedPreferences.Editor prefeditord = saveim.edit();
        prefeditord.clear();
        prefeditord.apply();

        pref = getApplicationContext().getSharedPreferences("logindetails", 0);
        currency = pref.getString("myCurrencySymbol", "");
        imageView = (LinearLayout) findViewById(R.id.backclick);
        listView = (listhei) findViewById(R.id.listview);
        setsignimage = (ImageView) findViewById(R.id.setsignimage);
        displaylist = (LinearLayout) findViewById(R.id.displaylist);


        hintshow = (TextView) findViewById(R.id.hintshow);
        estimatehead = (TextView) findViewById(R.id.estimatehead);
        imag = (CircleImageView) findViewById(R.id.imag);


        details_orderid = (TextView) findViewById(R.id.details_orderid);
        ongoing_detail_jobtype = (TextView) findViewById(R.id.ongoing_detail_jobtype);
        ongoing_details_dateTv = (TextView) findViewById(R.id.ongoing_details_dateTv);
        username = (TextView) findViewById(R.id.username);


        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            paymentPost(viewestimatebuilder.this, ServiceConstant.ESTIMATE_URL);
        } else {
            final PkDialog mDialog = new PkDialog(viewestimatebuilder.this);
            mDialog.setDialogTitle(getResources().getString(R.string.internet_label));
            mDialog.setDialogMessage(getResources().getString(R.string.no_inetnet_label));
            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                }
            });
            mDialog.show();
        }


        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
    }

    public class editestimationadapter extends ArrayAdapter<editbuilderpojo> {
        protected final String LOG_TAG = EstimateAdapter.class.getSimpleName();

        private ArrayList<editbuilderpojo> items;
        private int layoutResourceId;
        private Context context;
        Typeface tf;
        SharedPreferences pref;
        String currency;

        public editestimationadapter(Context context, int layoutResourceId, ArrayList<editbuilderpojo> items) {
            super(context, layoutResourceId, items);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.items = items;
            tf = Typeface.createFromAsset(getContext().getAssets(), "Poppins-Regular.ttf");
            pref = context.getSharedPreferences("logindetails", 0);
            currency = pref.getString("myCurrencySymbol", "");
        }

        @SuppressLint("ViewHolder")
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View row = convertView;
            editestimationadapter.Holder holder = null;

            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new editestimationadapter.Holder();
            holder.bean = items.get(position);

            holder.materialdn = (TextView) row.findViewById(R.id.materialdn);
            holder.jobdn = (TextView) row.findViewById(R.id.jobdn);
            holder.matamt = (TextView) row.findViewById(R.id.matamt);

            holder.whatsin = (TextView) row.findViewById(R.id.whatsin);

            holder.etVal1 = (EditText) row.findViewById(R.id.edVal1);
            holder.etVal2 = (EditText) row.findViewById(R.id.edValue2);

            holder.deletecraftman = (ImageView) row.findViewById(R.id.deletecraftman);
            holder.ImageViewDeleteEstimation = (ImageView) row.findViewById(R.id.ImageViewDeleteEstimation);
            holder.ImageViewDeleteEstimation.setVisibility(View.GONE);


            holder.etVal1.setTypeface(tf);
            holder.etVal2.setTypeface(tf);

            holder.materialdn.setTypeface(tf);
            holder.matamt.setTypeface(tf);
            holder.jobdn.setTypeface(tf);
            holder.whatsin.setTypeface(tf);


            holder.jobtytpeheading = (TextView) row.findViewById(R.id.jobtytpeheading);
            holder.jobdescriptionheading = (TextView) row.findViewById(R.id.jobdescriptionheading);
            holder.amountheading = (TextView) row.findViewById(R.id.amountheading);
            holder.hourheadining = (TextView) row.findViewById(R.id.hourheadining);
            holder.materialheading = (TextView) row.findViewById(R.id.materialheading);
            holder.materialdescheading = (TextView) row.findViewById(R.id.materialdescheading);
            holder.totalamount = (TextView) row.findViewById(R.id.totalamount);


            holder.jobamount = (TextView) row.findViewById(R.id.jobamount);
            holder.materialdhead = (TextView) row.findViewById(R.id.materialdhead);
            holder.helperamount = (TextView) row.findViewById(R.id.helperamount);
            holder.depositamount = (TextView) row.findViewById(R.id.depositamount);


            holder.materialdhead.setTypeface(tf);
            holder.helperamount.setTypeface(tf);
            holder.jobtytpeheading.setTypeface(tf);
            holder.jobdescriptionheading.setTypeface(tf);
            holder.amountheading.setTypeface(tf);
            holder.hourheadining.setTypeface(tf);
            holder.materialheading.setTypeface(tf);
            holder.materialdescheading.setTypeface(tf);
            holder.totalamount.setTypeface(tf);
            holder.totalamount.setTypeface(tf);
            holder.jobamount.setTypeface(tf);

            holder.depositamount.setTypeface(tf);

            holder.materialdnlin = (LinearLayout) row.findViewById(R.id.materialdnlin);
            holder.matamlin = (LinearLayout) row.findViewById(R.id.matamlin);
            holder.jobdnlin = (LinearLayout) row.findViewById(R.id.jobdnlin);
            holder.jobamountlin = (LinearLayout) row.findViewById(R.id.jobamountlin);

            holder.helperamountlin = (LinearLayout) row.findViewById(R.id.helperamountlin);
            holder.depositamountlin = (LinearLayout) row.findViewById(R.id.depositamountlin);

            holder.materialdesc = (EditText) row.findViewById(R.id.materialdesc);
            holder.materialamount = (EditText) row.findViewById(R.id.materialamount);
            holder.jobdescription = (EditText) row.findViewById(R.id.jobdescription);

            holder.materialdesc.setTypeface(tf);
            holder.jobdescription.setTypeface(tf);
            holder.materialdescheading.setTypeface(tf);
            holder.materialamount.setTypeface(tf);


            row.setTag(holder);

            setupItem(holder);

            return row;
        }

        private void setupItem(final editestimationadapter.Holder holder) {


            if (holder.bean.getWhats().equals("")) {
                holder.whatsin.setVisibility(View.GONE);
            } else {
                holder.whatsin.setVisibility(View.VISIBLE);
            }


            holder.whatsin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    HNDHelper.showErrorAlert(viewestimatebuilder.this, holder.bean.getWhats());
                }
            });

            holder.jobdescription.setText(holder.bean.getJobdescription());
            holder.jobtytpeheading.setText(holder.bean.getJobtype());


            switch (holder.bean.getApprovestatus()) {
                case "2":
                    holder.deletecraftman.setBackgroundResource(R.drawable.cancelmark);
                    holder.deletecraftman.setVisibility(View.VISIBLE);
                    break;
                case "1":
                    holder.deletecraftman.setBackgroundResource(R.drawable.ticked);
                    holder.deletecraftman.setVisibility(View.VISIBLE);
                    break;
                default:
                    holder.deletecraftman.setVisibility(View.GONE);
                    break;
            }

            if (holder.bean.getMaterialdescription().equalsIgnoreCase("") || holder.bean.getMaterialdescription().equalsIgnoreCase(" ")) {
                holder.materialdnlin.setVisibility(View.GONE);
            } else {
                holder.materialdnlin.setVisibility(View.VISIBLE);
                holder.materialdn.setText(Html.fromHtml(holder.bean.getMaterialdescription()), TextView.BufferType.SPANNABLE);
            }


            if (holder.bean.getHelperamount().equalsIgnoreCase("") || holder.bean.getHelperamount().equalsIgnoreCase(" ")) {
                holder.helperamountlin.setVisibility(View.GONE);
            } else {
                holder.helperamountlin.setVisibility(View.VISIBLE);
                holder.helperamount.setText(Html.fromHtml(holder.bean.getHelperamount()), TextView.BufferType.SPANNABLE);
            }


            if (holder.bean.getSingleamount().equalsIgnoreCase("") || holder.bean.getSingleamount().equalsIgnoreCase(" ")) {
                holder.jobamountlin.setVisibility(View.GONE);
            } else {
                holder.jobamountlin.setVisibility(View.VISIBLE);
                holder.jobamount.setText(Html.fromHtml(holder.bean.getSingleamount()), TextView.BufferType.SPANNABLE);
            }


            if (holder.bean.getJobdescription().equalsIgnoreCase("") || holder.bean.getJobdescription().equalsIgnoreCase(" ")) {
                holder.jobdnlin.setVisibility(View.GONE);
            } else {
                holder.jobdnlin.setVisibility(View.VISIBLE);
                holder.jobdn.setText(Html.fromHtml(holder.bean.getJobdescription()), TextView.BufferType.SPANNABLE);
            }


            if (holder.bean.getDepositamount().equalsIgnoreCase("") || holder.bean.getDepositamount().equalsIgnoreCase("0") || holder.bean.getDepositamount().equalsIgnoreCase("$0") || holder.bean.getDepositamount().equalsIgnoreCase(" ")) {
                holder.depositamountlin.setVisibility(View.GONE);
            } else {
                holder.depositamountlin.setVisibility(View.VISIBLE);
                holder.depositamount.setText(Html.fromHtml(holder.bean.getDepositamount()), TextView.BufferType.SPANNABLE);
            }


            if (holder.bean.getMaterial().equalsIgnoreCase("") || holder.bean.getMaterial().equalsIgnoreCase(" ")) {
                holder.matamlin.setVisibility(View.GONE);
            } else {
                holder.matamlin.setVisibility(View.VISIBLE);
                holder.matamt.setText(Html.fromHtml(holder.bean.getMaterial()), TextView.BufferType.SPANNABLE);
            }


            holder.etVal1.setText(String.valueOf(holder.bean.getVal1()));
            holder.etVal2.setText(String.valueOf(holder.bean.getVal2()));
            String grandtotal = "" + String.valueOf(holder.bean.getTotal());
            if (grandtotal.endsWith(".0") || grandtotal.endsWith(".5") || grandtotal.endsWith(".1") || grandtotal.endsWith(".2") || grandtotal.endsWith(".3") || grandtotal.endsWith(".4") || grandtotal.endsWith(".6") || grandtotal.endsWith(".7") || grandtotal.endsWith(".8") || grandtotal.endsWith(".9")) {
                holder.totalamount.setText(currency + String.valueOf(holder.bean.getTotal()) + "0");
            } else {
                holder.totalamount.setText(currency + String.valueOf(holder.bean.getTotal()));
            }


        }

        public class Holder {
            ImageView deletecraftman, ImageViewDeleteEstimation;
            editbuilderpojo bean;
            EditText etVal1;
            LinearLayout matamlin, jobdnlin, materialdnlin, jobamountlin, helperamountlin, depositamountlin;
            EditText etVal2, jobdescription, materialamount, materialdesc;
            TextView materialdhead, depositamount, helperamount, materialdn, matamt, whatsin, jobdn, totalamount, jobamount, jobtytpeheading, jobdescriptionheading, amountheading, hourheadining, materialheading, materialdescheading;

        }


    }


    private void paymentPost(Context mContext, String url) {

        final ProgressDialog1 myDialog = new ProgressDialog1(viewestimatebuilder.this);
        myDialog.setCancelable(true);
        myDialog.setCanceledOnTouchOutside(true);

        myDialog.show();


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        String provider_ids = pref.getString("provider_id", "");
        jsonParams.put("provider_id", provider_ids);

        SharedPreferences Job_idpref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
        String Job_id = Job_idpref.getString("jobid", "");
        jsonParams.put("job_id", Job_id);


        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("get-job-estimation-category\t" + response);

                String provide_estimation = "", Str_Currency = "";
                try {
                    JSONObject jobject = new JSONObject(response);

                    grandtotal = jobject.getString("grandtotal");

                    if (jobject.has("provide_estimation")) {
                        provide_estimation = jobject.getString("provide_estimation");
                    } else {
                        provide_estimation = "1";
                    }
                    storeuserid = jobject.getString("user_id");
                    if (jobject.has("job_id")) {
                        tjonbid = jobject.getString("job_id");
                    }

                    if (jobject.has("username")) {
                        tusernamed = jobject.getString("username");
                    }

                    if (jobject.has("avatar")) {
                        tuserphoto = /*ServiceConstant.Review_image+*/jobject.getString("avatar");
                    }

                    if (jobject.has("booking_date")) {
                        tdatetime = jobject.getString("booking_date");
                    }

                    if (jobject.has("job_address")) {
                        taddress = jobject.getString("job_address");
                    }

                    if (provide_estimation.equalsIgnoreCase("1")) {
                        String currencyCode = CurrencySymbolConverter.getCurrencySymbol(Str_Currency);
                        String whatsincluded = "";

                        JSONArray jarry = jobject.getJSONArray("estimation_history");
                        if (jarry.length() > 0) {
                            for (int i = 0; i < jarry.length(); i++) {
                                JSONObject jobjects_amount = jarry.getJSONObject(i);

                                if (jobjects_amount.has("hours")) {
                                    String title = jobjects_amount.getString("service_type");

                                    String materialamount;
                                    String depositamount;

                                    String hours = jobjects_amount.getString("hours");

                                    if (jobjects_amount.has("material")) {
                                        materialamount = jobjects_amount.getString("material");

                                        if (materialamount.equalsIgnoreCase("") || materialamount.equalsIgnoreCase(" ")) {
                                            materialamount = "";
                                        } else {
                                            materialamount = currency + materialamount;
                                        }
                                    } else {
                                        materialamount = "";
                                    }

                                    if (jobjects_amount.has("depositamount")) {
                                        depositamount = jobjects_amount.getString("depositamount");

                                        if (depositamount.equalsIgnoreCase("") || depositamount.equalsIgnoreCase(" ")) {
                                            depositamount = "";
                                        } else {
                                            depositamount = currency + depositamount;
                                        }
                                    } else {
                                        depositamount = "";
                                    }

                                    String description = "";
                                    if (jobjects_amount.has("description")) {
                                        description = jobjects_amount.getString("description");
                                        if (description.equalsIgnoreCase("") || description.equalsIgnoreCase(" ")) {
                                            description = "";
                                        } else {
                                            description = description;
                                        }
                                    } else {
                                        description = "";
                                    }
                                    String material_desc;
                                    if (jobjects_amount.has("material_desc")) {
                                        material_desc = jobjects_amount.getString("material_desc");

                                    } else {
                                        material_desc = "";
                                    }
                                    String totalamount = jobjects_amount.getString("totalamount");

                                    String status;
                                    if (jobjects_amount.has("status")) {
                                        status = jobjects_amount.getString("status");
                                    } else {

                                        status = "";
                                    }
                                    String singleamount;
                                    if (jobjects_amount.has("amount")) {
                                        singleamount = jobjects_amount.getString("amount");
                                        if (singleamount.equalsIgnoreCase("") || singleamount.equalsIgnoreCase(" ")) {
                                            singleamount = "";
                                        } else {
                                            singleamount = currency + singleamount;
                                        }
                                    } else {
                                        singleamount = "";
                                    }
                                    String helper_amount;
                                    if (jobjects_amount.has("helper_amount")) {
                                        helper_amount = jobjects_amount.getString("helper_amount");
                                        if (helper_amount.equalsIgnoreCase("") || helper_amount.equalsIgnoreCase(" ")) {
                                            helper_amount = "";
                                        } else {
                                            helper_amount = currency + helper_amount;
                                        }
                                    } else {
                                        helper_amount = "";
                                    }

                                    editbuilderpojo atomPayment = new editbuilderpojo();
                                    atomPayment.setVal2(Integer.parseInt(hours));
                                    atomPayment.setJobtype(title);
                                    atomPayment.setJobdescription(description);
                                    atomPayment.setTotal(Double.parseDouble(totalamount));
                                    atomPayment.setWhats(whatsincluded);

                                    if (materialamount.endsWith(".0") || materialamount.endsWith(".5") || materialamount.endsWith(".1") || materialamount.endsWith(".2") || materialamount.endsWith(".3") || materialamount.endsWith(".4") || materialamount.endsWith(".6") || materialamount.endsWith(".7") || materialamount.endsWith(".8") || materialamount.endsWith(".9")) {
                                        atomPayment.setMaterial(materialamount + "0");
                                    } else {
                                        atomPayment.setMaterial(materialamount);
                                    }
                                    atomPayment.setDepositamount(depositamount);
                                    atomPayment.setMaterialdescription(material_desc);
                                    atomPayment.setApprovestatus(status);

                                    if (singleamount.endsWith(".0") || singleamount.endsWith(".5") || singleamount.endsWith(".1") || singleamount.endsWith(".2") || singleamount.endsWith(".3") || singleamount.endsWith(".4") || singleamount.endsWith(".6") || singleamount.endsWith(".7") || singleamount.endsWith(".8") || singleamount.endsWith(".9")) {
                                        atomPayment.setSingleamount(singleamount + "0");
                                    } else {
                                        atomPayment.setSingleamount(singleamount);
                                    }
                                    atomPayment.setHelperamount(helper_amount);
                                    arrayList.add(atomPayment);
                                }
                            }
                        }
                        if (arrayList.size() > 0) {
                            displaylist.setVisibility(View.VISIBLE);
                            listView.setEnabled(false);
                            adapter = new editestimationadapter(viewestimatebuilder.this, R.layout.reo_item2, arrayList);
                            listView.setAdapter(adapter);
                            if (jobject.has("estimation_status")) {
                                estimatehead.setText(getResources().getString(R.string.activity_rowitem_estimate));
                                String estimation_status = jobject.getString("estimation_status");
                                hintshow.setVisibility(View.VISIBLE);
                                if (estimation_status.equalsIgnoreCase("0")) {
                                    hintshow.setText(getResources().getString(R.string.estimatebuilder_waiting_for_approval));
                                    details_orderid.setText(tjonbid);
                                    ongoing_detail_jobtype.setText(taddress);
                                    ongoing_details_dateTv.setText(getResources().getString(R.string.datenotavailable));
                                    username.setText(tusernamed);
                                    if (tuserphoto.isEmpty()) {
                                        imag.setImageResource(R.drawable.ic_no_user);
                                    } else {
                                        Picasso.with(viewestimatebuilder.this)
                                                .load(tuserphoto)

                                                .placeholder(R.drawable.ic_no_user)   // optional
                                                .error(R.drawable.ic_no_user)      // optional
                                                // optional
                                                .into(imag);
                                    }
                                } else if (estimation_status.equalsIgnoreCase("1")) {
                                    details_orderid.setText(tjonbid);
                                    ongoing_detail_jobtype.setText(taddress);
                                    ongoing_details_dateTv.setText(tdatetime);
                                    username.setText(tusernamed);
                                    if (tuserphoto.isEmpty()) {
                                        imag.setImageResource(R.drawable.ic_no_user);
                                    } else {
                                        Picasso.with(viewestimatebuilder.this)
                                                .load(tuserphoto)

                                                .placeholder(R.drawable.ic_no_user)   // optional
                                                .error(R.drawable.ic_no_user)      // optional
                                                // optional
                                                .into(imag);
                                    }
                                    hintshow.setText(getResources().getString(R.string.estimatebuilder_user_accepted));
                                } else if (estimation_status.equalsIgnoreCase("2")) {
                                    hintshow.setText(getResources().getString(R.string.estimatebuilder_user_rejected));
                                    details_orderid.setText(tjonbid);
                                    ongoing_detail_jobtype.setText(taddress);
                                    ongoing_details_dateTv.setText(getResources().getString(R.string.datenotavailable));
                                    username.setText(tusernamed);
                                    if (tuserphoto.isEmpty()) {
                                        imag.setImageResource(R.drawable.ic_no_user);
                                    } else {
                                        Picasso.with(viewestimatebuilder.this)
                                                .load(tuserphoto)

                                                .placeholder(R.drawable.ic_no_user)   // optional
                                                .error(R.drawable.ic_no_user)      // optional
                                                // optional
                                                .into(imag);
                                    }
                                }
                            } else {
                                hintshow.setVisibility(View.VISIBLE);
                                hintshow.setText(getResources().getString(R.string.estimatebuilder_waiting_send));
                            }
                        } else {
                            hintshow.setVisibility(View.GONE);
                            displaylist.setVisibility(View.GONE);
                        }
                    } else {
                        final PkDialog mDialog = new PkDialog(viewestimatebuilder.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.failed_label));
                        mDialog.setDialogMessage(getResources().getString(R.string.sorry_invalid_data));
                        mDialog.setPositiveButton(getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                            }
                        });
                        mDialog.show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                myDialog.dismiss();

            }

            @Override
            public void onErrorListener() {
                myDialog.dismiss();
            }
        });
    }

    protected void onResume() {
        super.onResume();

    }

}
