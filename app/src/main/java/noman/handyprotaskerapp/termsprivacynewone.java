package noman.handyprotaskerapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;

import org.json.JSONObject;

import java.util.HashMap;

import Dialog.LoadingDialog;
import service.ServiceConstant;
import socket.SocketHandler;
import volley.ServiceRequest;


/**
 * Casperon Technology on 10/1-2/2015.
 */
public class termsprivacynewone extends AppCompatActivity {
    private RelativeLayout back;
    private TextView Tv_more_info;
    private SocketHandler socketHandler;
    private WebView myWebView;
    String web_url = "";
    int nex=0;
    LoadingDialog dialog;
    TextView clicktoaccept,aboutus_header_textview,backterms;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.termsofnewone);
        socketHandler = SocketHandler.getInstance(this);
        back = (RelativeLayout) findViewById(R.id.aboutus_header_back_layout);
        Tv_more_info = (TextView) findViewById(R.id.more_info_baseurl);
        aboutus_header_textview= (TextView) findViewById(R.id.aboutus_header_textview);
        progressBar = (ProgressBar) findViewById(R.id.webView_progressbar);
        myWebView = (WebView) findViewById(R.id.WebView);
        clicktoaccept= (TextView) findViewById(R.id.clicktoaccept);
        backterms= (TextView) findViewById(R.id.backterms);

        backterms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if(nex==0)
                {
                    nex=1;
                    clicktoaccept.setText(getResources().getString(R.string.clicktoaccepts));
                    backterms.setVisibility(View.VISIBLE);
                }
                else
                {
                    backterms.setVisibility(View.GONE);
                    nex=0;
                    clicktoaccept.setText(getResources().getString(R.string.next));
                }

                if(nex==0)
                {
                    postRegisterRequest(termsprivacynewone.this, ServiceConstant.Terms_Conditions_Url);
                    aboutus_header_textview.setText(getResources().getString(R.string.terms));
                }
                else
                {
                    postRegisterRequest(termsprivacynewone.this, ServiceConstant.Privacy_Polocy);
                    aboutus_header_textview.setText(getResources().getString(R.string.privacypolicy));
                }
            }
        });
        clicktoaccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



if(clicktoaccept.getText().toString().equals("ACCEPT"))
{

}
else
{
    backterms.setVisibility(View.VISIBLE);
    nex=1;
    clicktoaccept.setText(getResources().getString(R.string.clicktoaccepts));
    postRegisterRequest(termsprivacynewone.this, ServiceConstant.Privacy_Polocy);
    aboutus_header_textview.setText(getResources().getString(R.string.privacypolicy));

}





            }
        });


        postRegisterRequest(termsprivacynewone.this, ServiceConstant.Terms_Conditions_Url);
        aboutus_header_textview.setText(getResources().getString(R.string.terms));








        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        myWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int progress) {
                if (progress < 100 && progressBar.getVisibility() == ProgressBar.GONE) {
                    progressBar.setVisibility(ProgressBar.VISIBLE);
                }
                progressBar.setProgress(progress);

                if (progress == 100) {
                    progressBar.setVisibility(ProgressBar.GONE);
                }
            }
        });
    }

    private void postRegisterRequest(final Context mContext, String url) {
        dialog = new LoadingDialog(termsprivacynewone.this);
        dialog.setLoadingTitle(getResources().getString(R.string.loading_in));
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();

        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        jsonParams.put("franchisee", pref.getString("franchiseeid",""));
        jsonParams.put("app", "tasker");

        ServiceRequest mRequest = new ServiceRequest(mContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                dialog.dismiss();

                System.out.println("---------register response------------" + response);
                final WebSettings webSettings = myWebView.getSettings();
                Resources res = getResources();
                int fontSize = 7;
                webSettings.setDefaultFontSize((int) fontSize);

                myWebView.loadData(response, "text/html", "UTF-8");
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void postAccept(final Context mContext, String url) {
        dialog = new LoadingDialog(termsprivacynewone.this);
        dialog.setLoadingTitle(getResources().getString(R.string.loading_in));
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();

        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        jsonParams.put("provider_id", pref.getString("provider_id",""));


        ServiceRequest mRequest = new ServiceRequest(mContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {



                dialog.dismiss();


                try {
                    JSONObject jobject = new JSONObject(response);
                  String  Str_status = jobject.getString("status");

                  if(Str_status.equals("1"))
                  {


                      SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
                      SharedPreferences.Editor editor = pref.edit();
                      editor.putString("termsandconditions", "1");
                      editor.apply();
                      editor.commit();

                      Intent intent = new Intent(termsprivacynewone.this, Navigationdrawer.class);
                      startActivity(intent);
                      finish();
                  }



                }
                catch (Exception e) {
                    e.printStackTrace();
                }

               /* SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("termsandconditions", "1");
                editor.apply();
                editor.commit();
*/

            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            onBackPressed();
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return false;
    }


    @Override
    protected void onResume() {
        super.onResume();
    }
}
