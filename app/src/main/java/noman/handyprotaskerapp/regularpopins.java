package noman.handyprotaskerapp;



/**
 * Created by user127 on 13-02-2018.
 */

import android.content.Context;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.AttributeSet;

public class regularpopins extends AppCompatTextView {

    public regularpopins(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public regularpopins(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public regularpopins(Context context) {
        super(context);
        init();
    }


    public void init() {
//	    Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "Raleway-Regular.ttf");
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "Poppins-Regular.ttf");
        setTypeface(tf);
    }
}
