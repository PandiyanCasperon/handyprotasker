package noman.handyprotaskerapp.ApiInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import noman.handyprotaskerapp.Pojo.DocumentListsItem;
import noman.handyprotaskerapp.Pojo.DocumentPojo;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("provider/gettaskdocument")
    Call<DocumentPojo> geImgData(@FieldMap HashMap<String,String> meta);
}
