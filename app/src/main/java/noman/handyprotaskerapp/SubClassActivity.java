package noman.handyprotaskerapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;


/**
 * Created by user88 on 1/11/2016.
 */
public class SubClassActivity extends AppCompatActivity {
    BroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.finish.ReceiveCashPage");
        filter.addAction("com.finish.OtpPage");
        filter.addAction("com.finish.PaymentFareSummeryPage");
        filter.addAction("com.finish.DetailsPage");
        filter.addAction("com.finish.HomePage");
        filter.addAction("com.finish.NewLeadsPage");
        filter.addAction("com.finish.NewLeadsFragmet");
        filter.addAction("com.finish.MissedLeadsFragment");
        filter.addAction("com.finish.StatisticsPage");
        filter.addAction("com.finish.LoadingPage");


        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case "com.finish.ReceiveCashPage":
                        finish();
                        break;
                    case "com.finish.OtpPage":
                        finish();
                        break;
                    case "com.finish.PaymentFareSummeryPage":
                        finish();
                        break;
                    case "com.finish.DetailsPage":
                        finish();
                        break;
                    case "com.finish.HomePage":
                        finish();
                        break;
                    case "com.finish.NewLeadsPage":
                        finish();
                        break;
                    case "com.finish.NewLeadsFragmet":
                        finish();
                        break;
                    case "com.finish.MissedLeadsFragment":
                        finish();
                        break;
                    case "com.finish.StatisticsPage":
                        finish();
                        break;
                    case "com.finish.LoadingPage":
                        finish();
                        break;
                }
            }
        };
        registerReceiver(receiver, filter);
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(receiver);
        super.onDestroy();
    }

}
