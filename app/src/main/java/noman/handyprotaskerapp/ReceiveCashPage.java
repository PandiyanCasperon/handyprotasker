package noman.handyprotaskerapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Set;

import Dialog.LoadingDialog;
import Dialog.PkDialog;
import Utils.ConnectionDetector;
import Utils.MyCustomCompressClass;
import Utils.SessionManager;
import noman.handyprotaskerapp.activity.CardPayment;
import noman.handyprotaskerapp.activity.MyJobs_OnGoingDetailActivity;
import service.ServiceConstant;
import volley.ServiceRequest;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

/**
 * Created by user88 on 1/4/2016.
 */
public class ReceiveCashPage extends SubClassActivity {

    private static final int PICK_CAMERA_IMAGE = 1;
    private static final int PICK_GALLERY_IMAGE = 2;
    private static String IMAGE_DIRECTORY_NAME = "";
    private int OTP_CASH_REQUEST_CODE=300;
    String saveimageurltosend = "";
    File dir;
    ImageView imageviewstore;
    LinearLayout cashchecked, chequechecked, LinearLayoutInvoice;
    CheckBox chequecheckedli, cashcheckedli, CheckBoxInvoice;
    String storecheckedvalue = "";
    EditText etPaymentDescription;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    private String provider_id = "";
    private Button Bt_Done;
    private LoadingDialog dialog;
    private String Jobid = "", Amount_received = "", user_type = "";
    private TextView Tv_received_amount;
    private String otp = "", Flow = "",type="";
    private Uri fileUri; // file url to store image/video
    private RelativeLayout Rl_receivecash_back_layout;
    private File compressedImage;
    private TextView receivecash_text, receivecash_warning_text;
    ArrayList<String> PrepaymentList = new ArrayList<>();
    BroadcastReceiver receiver;
    private File actualImage;
    private Uri actualImageURI;
    private LinearLayout myCreditCardLAY, myOtherLAY;
    private CheckBox myCreditCardCHKBOX, myOtherCHKBOX;

    Button cardpayment;

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    public static String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.receive_cash);

        IMAGE_DIRECTORY_NAME = getResources().getString(R.string.directory_name);

        Intent intent = getIntent();
       type= intent.getStringExtra("Type");

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        if (getIntent().hasExtra("Type") && getIntent().hasExtra("PrepaymentList") && getIntent().getStringExtra("Type").equals("pre_payment_cash_initiated")) {
            Flow = getIntent().getStringExtra("Type");

            PrepaymentList = getIntent().getStringArrayListExtra("PrepaymentList");
        }

        if (getIntent().hasExtra("user_type")) {
            user_type = getIntent().getStringExtra("user_type");
        }


        dir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + ReceiveCashPage.this.getApplicationContext().getPackageName()
                + "/Files/Compressed");

        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (String child : children) {
                new File(dir, child).delete();
            }
        }

        initialize();

        Bt_Done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cd = new ConnectionDetector(ReceiveCashPage.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {

                    if (CheckBoxInvoice.isChecked()) {
                        dialog = new LoadingDialog(ReceiveCashPage.this);
                        dialog.setLoadingTitle(getResources().getString(R.string.loading_in));
                        dialog.show();

                        if (Flow.equals("pre_payment_cash_initiated")) {
                            ReceiveCashORCheck(ReceiveCashPage.this, ServiceConstant.ReceiveCommercialUserPrepayment, "CommercialUser");
                        } else {
                            //mobile/provider/invoice-received --> This API is used to Receive the payment from commercial user and complete the job.
                            //But for the commercial user type we won't collect any Cash or Check. Just we complete the payment by Invoice.
                            //At the time of registering the user, we set the type of user.
                            //Note: We no need to send any attachment(image) for this call.
                            ReceiveCashORCheck(ReceiveCashPage.this, ServiceConstant.ReceivePaymentFromCommercialUser, "CommercialUser");
                        }
                    } else {
                        if (saveimageurltosend.equals("") || storecheckedvalue.equals("")) {
                            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.fillall));
                        } else {
                            dialog = new LoadingDialog(ReceiveCashPage.this);
                            dialog.setLoadingTitle(getResources().getString(R.string.loading_in));
                            dialog.show();

                            if (storecheckedvalue.equals("cheque")) {
                                if (Flow.equals("pre_payment_cash_initiated")) {
                                    ReceiveCashORCheck(ReceiveCashPage.this, ServiceConstant.ReceiveChequeFinal, "NormalUser");
                                } else {
                                    ReceiveCashORCheck(ReceiveCashPage.this, ServiceConstant.JOB_CHEQUE_RECEIVED_URL, "NormalUser");
                                }
                            } else if (storecheckedvalue.equals("credit")) {
                                if (Flow.equals("pre_payment_cash_initiated")) {
                                    ReceiveCashORCheck(ReceiveCashPage.this, ServiceConstant.ReceiveCreditFinal, "NormalUser");
                                } else {
                                    ReceiveCashORCheck(ReceiveCashPage.this, ServiceConstant.JOB_CREDIT_RECEIVED_URL, "NormalUser");
                                }
                            } else if (storecheckedvalue.equals("other")) {
                                if (Flow.equals("pre_payment_cash_initiated")) {
                                    ReceiveCashORCheck(ReceiveCashPage.this, ServiceConstant.ReceiveOtherFinal, "NormalUser");
                                } else {
                                    ReceiveCashORCheck(ReceiveCashPage.this, ServiceConstant.JOB_OTHER_RECEIVED_URL, "NormalUser");
                                }
                            } else if (storecheckedvalue.equals("cash")) {

                                Intent intent = new Intent(ReceiveCashPage.this, OtpPage.class);
                                intent.putExtra("jobId", Jobid);
                                intent.putExtra("type", "postpayment");
                                intent.putExtra("saveimageurltosend", saveimageurltosend);
                                intent.putExtra("payment_description", etPaymentDescription.getText().toString());
                                startActivity(intent);
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                                if (Flow.equals("pre_payment_cash_initiated")) {
                                    ReceiveCashORCheck(ReceiveCashPage.this, ServiceConstant.ReceiveCashFinal, "NormalUser");
                                } else {
                                    ReceiveCashORCheck(ReceiveCashPage.this, ServiceConstant.JOB_CASH_RECEIVED_URL, "NormalUser");
                                }
                            }
                        }
                    }
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                }
            }
        });


        Rl_receivecash_back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    imageviewstore.setImageURI(actualImageURI);
                    saveimageurltosend = actualImage.getAbsoluteFile().toString();
                    /*Picasso.with(getApplicationContext()).load("file://" + actualImage.getAbsoluteFile())
                            .resize(500, 500)
                            .onlyScaleDown()
                            .placeholder(R.drawable.fileupload)
                            .into(imageviewstore);*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

    }


    //----------------Loading Method-----------

    private void initialize() {
        session = new SessionManager(ReceiveCashPage.this);
        HashMap<String, String> user = session.getUserDetails();
        provider_id = user.get(SessionManager.KEY_PROVIDERID);
        Bt_Done = findViewById(R.id.Bt_receivecash_done_btn);
        cardpayment = findViewById(R.id.cardpayment);
        Tv_received_amount = findViewById(R.id.Tv_receivecash_amount);
        Rl_receivecash_back_layout = findViewById(R.id.layout_receivecah_back);
        receivecash_text = findViewById(R.id.receivecash_Tv);
        receivecash_warning_text = findViewById(R.id.warning_text);
        etPaymentDescription = findViewById(R.id.etPaymentDescription);

        cashchecked = findViewById(R.id.cashchecked);
        chequechecked = findViewById(R.id.chequechecked);

        cashcheckedli = findViewById(R.id.cashcheckedli);
        chequecheckedli = findViewById(R.id.chequecheckedli);

        LinearLayoutInvoice = findViewById(R.id.LinearLayoutInvoice);
        CheckBoxInvoice = findViewById(R.id.CheckBoxInvoice);


        myCreditCardCHKBOX = findViewById(R.id.receive_cash_CHKBOX_creditcard);
        myCreditCardLAY = findViewById(R.id.receive_cash_LAY_creditcard);

        myOtherCHKBOX = findViewById(R.id.receive_cash_CHKBOX_other);
        myOtherLAY = findViewById(R.id.receive_cash_LAY_other);


        cashcheckedli.setChecked(true);
        chequecheckedli.setChecked(false);
        storecheckedvalue = "cash";

        myCreditCardLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                storecheckedvalue = "credit";
                cashcheckedli.setChecked(false);
                chequecheckedli.setChecked(false);
                CheckBoxInvoice.setChecked(false);
                myCreditCardCHKBOX.setChecked(true);
                myOtherCHKBOX.setChecked(false);
                Bt_Done.setText(getResources().getString(R.string.payment_received));

            }
        });

        myOtherLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                storecheckedvalue = "other";
                cashcheckedli.setChecked(false);
                chequecheckedli.setChecked(false);
                CheckBoxInvoice.setChecked(false);
                myCreditCardCHKBOX.setChecked(false);
                myOtherCHKBOX.setChecked(true);
                Bt_Done.setText(getResources().getString(R.string.payment_received));
            }
        });

        cashchecked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                storecheckedvalue = "cash";
                cashcheckedli.setChecked(true);
                chequecheckedli.setChecked(false);
                myCreditCardCHKBOX.setChecked(false);
                myOtherCHKBOX.setChecked(false);
                CheckBoxInvoice.setChecked(false);
                Bt_Done.setText(getResources().getString(R.string.payment_received));
            }
        });

        chequechecked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                storecheckedvalue = "cheque";
                cashcheckedli.setChecked(false);
                chequecheckedli.setChecked(true);
                CheckBoxInvoice.setChecked(false);
                myCreditCardCHKBOX.setChecked(false);
                myOtherCHKBOX.setChecked(false);
                Bt_Done.setText(getResources().getString(R.string.payment_received));
            }
        });

        CheckBoxInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBoxInvoice.setChecked(true);
                cashcheckedli.setChecked(false);
                chequecheckedli.setChecked(false);
                Bt_Done.setText(getResources().getString(R.string.complete_label_text));
            }
        });


        cardpayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ReceiveCashPage.this, CardPayment.class);
                intent.putStringArrayListExtra("PrepaymentList",PrepaymentList);
                intent.putExtra("jobId", Jobid);
                intent.putExtra("Type",type);
                startActivity(intent);
            }
        });


        imageviewstore = findViewById(R.id.imageviewstore);
        imageviewstore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final PkDialog mDialog = new PkDialog(ReceiveCashPage.this);
                mDialog.setDialogTitle(getResources().getString(R.string.select_label));
                mDialog.setDialogMessage(getResources().getString(R.string.activity_beforephoto_choose_to_add_photos));
                mDialog.setPositiveButton(
                        getResources().getString(R.string.cameraword), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();

                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                                fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

                                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

                                // start the image capture Intent
                                startActivityForResult(intent, PICK_CAMERA_IMAGE);


                            }
                        }
                );
                mDialog.setNegativeButton(
                        getResources().getString(R.string.galleryword), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();

                                Intent i = new Intent(
                                        Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(i, PICK_GALLERY_IMAGE);
                            }
                        }
                );

                mDialog.show();

            }
        });

        Intent i = getIntent();
        Jobid = i.getStringExtra("jobId");
        Amount_received = i.getStringExtra("Amount");
        otp = i.getExtras().getString("otp");
        Tv_received_amount.setText(Amount_received);

        if (Amount_received.equalsIgnoreCase("$0.00")) {

            Tv_received_amount.setEnabled(false);
            Tv_received_amount.setTextColor(Color.parseColor("#DCDCDC"));
            receivecash_text.setEnabled(false);
            receivecash_text.setTextColor(Color.parseColor("#DCDCDC"));
            receivecash_warning_text.setVisibility(View.VISIBLE);

        } else {

            receivecash_warning_text.setVisibility(View.GONE);
        }

        if (user_type.equals("1")) {
            LinearLayoutInvoice.setVisibility(View.VISIBLE);
        } else if (user_type.equals("0")) {
            LinearLayoutInvoice.setVisibility(View.GONE);
        }

    }

    //--------------Alert Method------------------
    private void Alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(ReceiveCashPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //----------------------Post method for Payment Fare------------
    private void ReceiveCashORCheck(Context mContext, String url, String UserType) {
        final HashMap<String, String> jsonParams = new HashMap<String, String>();
        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        String provider_ids = pref.getString("provider_id", "");

        jsonParams.put("provider_id", provider_ids);
        jsonParams.put("job_id", Jobid);
        jsonParams.put("otp", otp);
        if (Flow.equals("pre_payment_cash_initiated")) {
            Set<String> PrepaymentListWithoutDuplicates = new LinkedHashSet<String>(PrepaymentList);
            PrepaymentList.clear();
            PrepaymentList.addAll(PrepaymentListWithoutDuplicates);
            for (int i = 0; i < PrepaymentList.size(); i++) {
                jsonParams.put("prepayment_id[" + i + "]", PrepaymentList.get(i));
            }
        }

        if (!etPaymentDescription.getText().toString().isEmpty()) {
            jsonParams.put("payment_description", etPaymentDescription.getText().toString());
        }

        if (UserType.equals("NormalUser")) {
            Bitmap src = BitmapFactory.decodeFile(saveimageurltosend);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            src.compress(Bitmap.CompressFormat.PNG, 100, baos);
            String imgString = Base64.encodeToString(getBytesFromBitmap(src),
                    Base64.NO_WRAP);

            jsonParams.put("image", imgString);
        }

        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {
                System.out.println("received--------" + response);
                Log.e("received", response);

                String Str_Status = "", Str_Response = "";

                try {

                    JSONObject jobject = new JSONObject(response);
                    Str_Status = jobject.getString("status");
                    Str_Response = jobject.getString("response");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (Str_Status.equalsIgnoreCase("1")) {
                    final PkDialog mdialog = new PkDialog(ReceiveCashPage.this);
                    mdialog.setDialogTitle(getResources().getString(R.string.action_loading_sucess));
                    mdialog.setDialogMessage(Str_Response);
                    mdialog.setPositiveButton(getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mdialog.dismiss();

                                   /* Intent intent = new Intent(ReceiveCashPage.this, MyJobsActivity.class);
                                    intent.putExtra("status", "complete");
                                    startActivity(intent);
                                    finish();*/

                                    Intent intent = new Intent(ReceiveCashPage.this, ReviwesPage.class);
                                    intent.putExtra("jobId", Jobid);
                                    startActivity(intent);
                                }
                            }
                    );
                    mdialog.show();

                } else if (Str_Status.equalsIgnoreCase("11")) {
                    Toast.makeText(ReceiveCashPage.this, Str_Response, Toast.LENGTH_SHORT).show();
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("page", "1");
                    editor.apply();

                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", Jobid);
                    prefeditor.apply();
                    prefeditor.commit();


                    Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), Str_Response);
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });

    }


    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == 0) {

            } else {
                String picturePath = fileUri.getPath();
                actualImageURI = fileUri;
                actualImage = new File(picturePath);
                MyCustomCompressClass myCustomCompressClass = new MyCustomCompressClass();
                myCustomCompressClass.constructor(this, actualImage);
            }
        } else if (requestCode == 2) {

            if (resultCode == 0) {

            } else {
                String picturePath = "";
                Uri selectedImageUri = data.getData();
                actualImageURI = selectedImageUri;
                picturePath = getPath(getApplicationContext().getApplicationContext(), selectedImageUri);
                actualImage = new File(picturePath);
                MyCustomCompressClass myCustomCompressClass = new MyCustomCompressClass();
                myCustomCompressClass.constructor(this, actualImage);
            }

        }
        else if(requestCode==OTP_CASH_REQUEST_CODE){


            if (resultCode == 0) {

            } else {

                 if(data!=null){

                     otp=data.getStringExtra("otp");

                     if (Flow.equals("pre_payment_cash_initiated")) {
                         ReceiveCashORCheck(ReceiveCashPage.this, ServiceConstant.ReceiveCashFinal, "NormalUser");
                     } else {
                         ReceiveCashORCheck(ReceiveCashPage.this, ServiceConstant.JOB_CASH_RECEIVED_URL, "NormalUser");
                     }
                 }

            }
        }
    }

    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }


    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on scren orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        fileUri = savedInstanceState.getParcelable("file_uri");
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.handypro.files.selected");
        registerReceiver(receiver, filter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
