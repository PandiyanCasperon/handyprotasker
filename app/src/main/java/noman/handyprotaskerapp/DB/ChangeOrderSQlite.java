package noman.handyprotaskerapp.DB;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ChangeOrderSQlite extends SQLiteOpenHelper {


    public static String DATABASE_NAME = "chghnghjeh";
    public static final String TABLE_NAME = "hghjkgh";


    public static final String KEY_AMOUNT = "fname";
    public static final String KEY_TYPE = "typev";
    public static final String KEY_DESCRIPTION = "descr";
    public static final String KEY_ID = "id";


    public ChangeOrderSQlite(Context context) {
        super(context, DATABASE_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" + KEY_ID + " INTEGER PRIMARY KEY, " + KEY_AMOUNT + " TEXT, " + KEY_TYPE + " TEXT, " + KEY_DESCRIPTION + " TEXT)";
        db.execSQL(CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);

    }

    public int getProfilesCount() {
        String countQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }
}

