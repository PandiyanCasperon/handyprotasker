package noman.handyprotaskerapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;



import java.util.ArrayList;

import noman.handyprotaskerapp.Pojo.Unavailabiltypojo;

/**
 * Created by user145 on 11/7/2016.
 */
public class UnAvailabilityadapter extends BaseAdapter {

    private ArrayList<Unavailabiltypojo> data;
    private LayoutInflater mInflater;
    private Context context;

    public UnAvailabilityadapter(Context c, ArrayList<Unavailabiltypojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    public class ViewHolder {
        public ImageView messageStatus;
        private TextView day;
        private TextView tick1;

        private TextView tick3;
        private RelativeLayout Rl_left, Rl_right;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.availabilitysingle, parent, false);
            holder = new ViewHolder();
            holder.day = (TextView) view.findViewById(R.id.days);
            holder.tick1 = (TextView) view.findViewById(R.id.tick1);

            holder.tick3 = (TextView) view.findViewById(R.id.tick3);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();

        }




        holder.day.setText(data.get(position).getDays());

        String mor=data.get(position).getMorning();
        holder.tick1.setText(""+mor);


        String after= data.get(position).getAfternoon();
        holder.tick3.setText(""+after);


        return view;
    }
}