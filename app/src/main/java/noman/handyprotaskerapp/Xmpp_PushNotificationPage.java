package noman.handyprotaskerapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import noman.handyprotaskerapp.activity.EstimateBuilderActivity;
import noman.handyprotaskerapp.activity.GetMessageChatActivity;
import noman.handyprotaskerapp.activity.HelperOngoingPageActivity;
import noman.handyprotaskerapp.activity.MyJobs_OnGoingDetailActivity;
import noman.handyprotaskerapp.activity.WeeklyCalendarActivity;
import socket.SocketHandler;


/**
 * Created by user88 on 1/11/2016.
 */
public class Xmpp_PushNotificationPage extends AppCompatActivity {
    private TextView Message_Tv, Textview_Ok, Textview_alert_header, jobid, thanks, thankfor, handdone;
    final Handler handler = new Handler();
    private RelativeLayout Rl_layout_alert_ok;
    String Job_status_message = "", actionnew = "";

    String Job_id = "", usernamee = "", lastvalue, value;
    ArrayList<String> PrepaymentList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pushnotification);

        try {
            SocketHandler.getInstance(Xmpp_PushNotificationPage.this).getSocketManager().connect();

        } catch (Exception e) {
            e.printStackTrace();
        }


        initialize();


        MediaPlayer mPlayer = MediaPlayer.create(Xmpp_PushNotificationPage.this, R.raw.notifysound);
        if (mPlayer.isPlaying()) {
            mPlayer.pause();
        } else {
            mPlayer.start();
        }

    }

    private void NotificationMethod() {

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("TITLE_INTENT")) {
            value = getIntent().getExtras().getString("TITLE_INTENT");
            if (getIntent().hasExtra("PrepaymentList")) {
                PrepaymentList = new ArrayList<>();
                PrepaymentList = getIntent().getStringArrayListExtra("PrepaymentList");
            }
        }

        SQLiteDatabase dataBase;
        notificationdb mHelper = new notificationdb(this);
        dataBase = mHelper.getWritableDatabase();
        Cursor mCursor = dataBase.rawQuery("SELECT * FROM " + notificationdb.TABLE_NAME, null);

        if (mCursor.moveToLast()) {
            do {
                lastvalue = mCursor.getString(mCursor.getColumnIndex(notificationdb.KEY_FNAME));
                System.out.println("last inserted record--->fcm-okl" + lastvalue);
            } while (mCursor.moveToNext());
        }


        if (lastvalue.equals("")) {

            if (lastvalue.equalsIgnoreCase(value)) {
                value = lastvalue;
            } else {
                value = lastvalue;

            }
            CommonUIChange();

        } else {
            CommonUIChange();
        }
    }

    private void CommonUIChange() {

        try {

            JSONObject json = new JSONObject(value);
            JSONObject object1 = json.getJSONObject("data");
            if (object1.has("key0")) {
                Job_id = object1.getString("key0");
                Job_status_message = object1.getString("message");
                actionnew = object1.getString("action");

                if (actionnew.equalsIgnoreCase("job_request") || actionnew.equalsIgnoreCase("change_order_notaccepted") || actionnew.equalsIgnoreCase("change_order_accepted") || actionnew.equalsIgnoreCase("estimation_accepted") || actionnew.equalsIgnoreCase("estimation_notaccepted")) {
                    if (object1.has("key3")) {
                        usernamee = object1.getString("key3");
                    }

                } else if (actionnew.equalsIgnoreCase("job_accepted")) {
                    if (object1.has("key2")) {
                        usernamee = object1.getString("key2");
                    }
                } else if (actionnew.equalsIgnoreCase("job_rescheduled")) {
                    if (object1.has("key3")) {
                        usernamee = object1.getString("key3");
                    }
                } else if (actionnew.equalsIgnoreCase("job_photo_status")) {
                    if (object1.has("key2")) {
                        usernamee = object1.getString("key2");
                    }
                } else if (actionnew.equalsIgnoreCase("job_request_now")) {
                    if (object1.has("key6")) {
                        usernamee = object1.getString("key6");
                    }
                } else if (actionnew.equalsIgnoreCase("cash_initiated")) {
                    if (object1.has("key2")) {
                        usernamee = object1.getString("key2");
                    }
                } else if (actionnew.equalsIgnoreCase("pre_payment_cash_initiated")) {
                    if (object1.has("key2")) {
                        usernamee = object1.getString("key2");
                    }

                    if (object1.has("action") && object1.has("key3")) {
                        if (object1.getString("action").equals("pre_payment_cash_initiated")) {
                            PrepaymentList = new ArrayList<>();
                            JSONArray jsonArray = object1.getJSONArray("key3");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                PrepaymentList.add(jsonArray.get(i).toString());
                            }
                        }
                    }
                } else if (actionnew.equalsIgnoreCase("payment_paid")) {
                    if (object1.has("key2")) {
                        usernamee = object1.getString("key2");
                    }
                } else if (actionnew.equalsIgnoreCase("prepayment_rejected")) {
                    if (object1.has("key2")) {
                        usernamee = object1.getString("key2");
                    }
                } else {
                    usernamee = "";
                }


                if (actionnew.equalsIgnoreCase("admin_notification")) {
                    jobid.setText("ADMIN NOTIFICATION");
                    Textview_Ok.setText("CLOSE");
                } else {
                    if (actionnew.equalsIgnoreCase("job_request") && object1.has("key4")) {
                        if (object1.getString("key4").equalsIgnoreCase("Job")) {
                            jobid.setText("JOB ID:" + Job_id);
                        } else if (object1.getString("key4").equalsIgnoreCase("Estimate")) {
                            jobid.setText("ESTIMATION ID:" + Job_id);
                        }

                    } else if (actionnew.equalsIgnoreCase("job_accepted") && object1.has("key3")) {
                        if (object1.getString("key3").equalsIgnoreCase("Job")) {
                            jobid.setText("JOB ID:" + Job_id);
                        } else if (object1.getString("key3").equalsIgnoreCase("Estimate")) {
                            jobid.setText("ESTIMATION ID:" + Job_id);
                        }

                    } else {
                        jobid.setText("JOB ID:" + Job_id);
                    }

                }


                SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
                String username = pref.getString("provider_name", "");

                if (actionnew.equalsIgnoreCase("job_request")) {
                    if (Job_status_message.contains("You have been assigned for a job")) {
                        Job_status_message = getResources().getString(R.string.jobre);
                    }
                } else if (actionnew.equalsIgnoreCase("job_accepted")) {
                    if (Job_status_message.contains("You are assigned to the Job")) {
                        Job_status_message = getResources().getString(R.string.jobre);
                    }
                } else if (actionnew.equalsIgnoreCase("estimation_accepted")) {

                    Job_status_message = getResources().getString(R.string.estaccepted);

                } else if (actionnew.equalsIgnoreCase("job_rescheduled")) {
                    Job_status_message = getResources().getString(R.string.ree);
                } else if (actionnew.equalsIgnoreCase("estimation_notaccepted")) {

                    Job_status_message = getResources().getString(R.string.estrejected);

                } else if (actionnew.equalsIgnoreCase("job_photo_status")) {
                    if (Job_status_message.contains("User approved the job")) {
                        Job_status_message = getResources().getString(R.string.jobapprove);
                    }


                } else if (actionnew.equalsIgnoreCase("cash_initiated")) {

                    if (Job_status_message.contains("User")) {
                        Job_status_message = Job_status_message.replace("User", "client");
                    }

                } else if (actionnew.equalsIgnoreCase("pre_payment_cash_initiated")) {
                    if (Job_status_message.contains("User")) {
                        Job_status_message = Job_status_message.replace("User", "client");
                    }
                } else if (actionnew.equalsIgnoreCase("prepayment_rejected")) {
                    if (Job_status_message.contains("User")) {
                        Job_status_message = Job_status_message.replace("User", "client");
                    }
                }


                Message_Tv.setText("Hi " + username + "!. " + Job_status_message);
                Textview_alert_header.setText(getResources().getString(R.string.notif_from_jobid) + "\n" + usernamee);


                if (actionnew.equalsIgnoreCase("payment_paid")) {

                    thanks.setVisibility(View.VISIBLE);
                    thankfor.setVisibility(View.VISIBLE);
                    handdone.setVisibility(View.VISIBLE);


                    thanks.setText("Well Done " + username + "!");

                    SpannableStringBuilder builders = new SpannableStringBuilder();
                    String red = "Thank You for continuing to build trust in the";
                    SpannableString redSpannable = new SpannableString(red);
                    redSpannable.setSpan(new ForegroundColorSpan(Color.BLACK), 0, red.length(), 0);
                    builders.append(redSpannable);

                    String white = " HANDY";
                    SpannableString whiteSpannable = new SpannableString(white);
                    whiteSpannable.setSpan(new ForegroundColorSpan(Color.BLUE), 0, white.length(), 0);
                    builders.append(whiteSpannable);

                    String blue = "PRO ";
                    SpannableString blueSpannable = new SpannableString(blue);
                    blueSpannable.setSpan(new ForegroundColorSpan(Color.RED), 0, blue.length(), 0);
                    builders.append(blueSpannable);

                    String Craftsman = "Brand.";
                    SpannableString blueSpannableCraftsman = new SpannableString(Craftsman);
                    blueSpannableCraftsman.setSpan(new ForegroundColorSpan(Color.BLACK), 0, blue.length(), 0);
                    builders.append(blueSpannableCraftsman);

                    thankfor.setText(builders, TextView.BufferType.SPANNABLE);
                } else {
                    thanks.setVisibility(View.GONE);
                    handdone.setVisibility(View.GONE);
                    thankfor.setVisibility(View.GONE);
                }


            } else {

                thanks.setVisibility(View.GONE);
                thankfor.setVisibility(View.GONE);
                handdone.setVisibility(View.GONE);


                actionnew = object1.getString("action");
                jobid.setText("");
                Message_Tv.setText(getResources().getString(R.string.received_one_new_msg));
                Textview_alert_header.setText(getResources().getString(R.string.chat_notif));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void initialize() {

        thankfor = findViewById(R.id.thankfor);
        thanks = findViewById(R.id.thanks);
        Textview_Ok = findViewById(R.id.pushnotification_alert_ok_textview);
        Message_Tv = findViewById(R.id.pushnotification_alert_messge_textview);
        Textview_alert_header = findViewById(R.id.pushnotification_alert_messge_label);
        Rl_layout_alert_ok = findViewById(R.id.pushnotification_alert_ok);
        jobid = findViewById(R.id.jobid);
        handdone = findViewById(R.id.handdone);

        NotificationMethod();

        Rl_layout_alert_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (actionnew.equalsIgnoreCase("job_request")) {

                    SharedPreferences presf1 = getApplicationContext().getSharedPreferences("needrefresh", 0);
                    SharedPreferences.Editor editosr1 = presf1.edit();
                    editosr1.putString("yesno", "2");
                    editosr1.apply();


                    SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("page", "1");
                    editor.apply();


                    System.out.println("dispaly jobid-------->" + jobid);
                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", Job_id);
                    prefeditor.apply();

                    Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                    startActivity(intent);
                    finish();
                } else if (actionnew.equalsIgnoreCase("job_request_now")) {

                    Intent intent = new Intent(getApplicationContext(), newjobrequesttimer.class);
                    intent.putExtra("valuefortimer", value);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                } else if (actionnew.equalsIgnoreCase("admin_notification")) {
                    finish();
                } else if (actionnew.equalsIgnoreCase("helper_job_actions")) {

                    SharedPreferences presf1 = getApplicationContext().getSharedPreferences("needrefresh", 0);
                    SharedPreferences.Editor editosr1 = presf1.edit();
                    editosr1.putString("yesno", "2");
                    editosr1.apply();


                    SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("page", "1");
                    editor.apply();


                    System.out.println("dispaly jobid-------->" + jobid);
                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", Job_id);
                    prefeditor.apply();

                    Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                    startActivity(intent);
                    finish();
                } else if (actionnew.equalsIgnoreCase("assign_helper")) {

                    SharedPreferences presf1 = getApplicationContext().getSharedPreferences("needrefresh", 0);
                    SharedPreferences.Editor editosr1 = presf1.edit();
                    editosr1.putString("yesno", "2");
                    editosr1.apply();


                    SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("page", "1");
                    editor.apply();


                    System.out.println("dispaly jobid-------->" + jobid);
                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", Job_id);
                    prefeditor.apply();

                    Intent intent = new Intent(getApplicationContext(), HelperOngoingPageActivity.class);
                    startActivity(intent);
                    finish();
                } else if (actionnew.equalsIgnoreCase("estimation_pending")) {


                    SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("page", "1");
                    editor.apply();


                    System.out.println("dispaly jobid-------->" + jobid);
                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", Job_id);
                    prefeditor.apply();


                    Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                    startActivity(intent);
                    finish();


                } else if (actionnew.equalsIgnoreCase("estimation_notaccepted")) {
                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", Job_id);
                    prefeditor.apply();

                    SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("page", "1");
                    editor.apply();


//                    Intent intent = new Intent(getApplicationContext(), EstimateBuilderActivity.class);
                    Intent intent = new Intent(getApplicationContext(), WeeklyCalendarActivity.class);
                    startActivity(intent);
                    finish();


                } else if (actionnew.equalsIgnoreCase("change_order_notaccepted")) {

                    SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("page", "1");
                    editor.apply();


                    System.out.println("dispaly jobid-------->" + jobid);
                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", Job_id);
                    prefeditor.apply();


                    Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                    startActivity(intent);
                    finish();

                } else if (actionnew.equalsIgnoreCase("cash_initiated") || actionnew.equalsIgnoreCase("pre_payment_cash_initiated")) {
                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", Job_id);
                    prefeditor.apply();


                    Intent intent = new Intent(getApplicationContext(), OtpPage.class);
                    intent.putExtra("jobId", Job_id);
                    intent.putExtra("type", "prepayment");
                    intent.putExtra("Type", actionnew);
                    if (actionnew.equalsIgnoreCase("pre_payment_cash_initiated")) {
                        intent.putStringArrayListExtra("PrepaymentList", PrepaymentList);
                    }
                    startActivity(intent);
                    finish();


                } else if (actionnew.equalsIgnoreCase("helper_assigned")) {
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("page", "1");
                    editor.apply();


                    System.out.println("dispaly jobid-------->" + jobid);
                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", Job_id);
                    prefeditor.apply();


                    Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                    startActivity(intent);
                    finish();


                } else if (actionnew.equalsIgnoreCase("change_order_accepted")) {
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("page", "1");
                    editor.apply();


                    System.out.println("dispaly jobid-------->" + jobid);
                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", Job_id);
                    prefeditor.apply();

                    Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                    startActivity(intent);
                    finish();


                } else if (actionnew.equalsIgnoreCase("job_cancelled")) {


                    SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("page", "1");
                    editor.apply();


                    System.out.println("dispaly jobid-------->" + jobid);
                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", Job_id);
                    prefeditor.apply();

                    Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                    startActivity(intent);
                    finish();


                } else if (actionnew.equalsIgnoreCase("payment_paid")) {
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("page", "1");
                    editor.apply();


                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", Job_id);
                    prefeditor.apply();


                    Intent intent = new Intent(Xmpp_PushNotificationPage.this, ReviwesPage.class);
                    intent.putExtra("jobId", Job_id);
                    startActivity(intent);
                    finish();

                } else if (actionnew.equalsIgnoreCase("pre_payment_paid")) {
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("page", "1");
                    editor.apply();

                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", Job_id);
                    prefeditor.apply();

                    Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                    startActivity(intent);
                    finish();
                } else if (actionnew.equalsIgnoreCase("estimation_accepted")) {
                    System.out.println("dispaly jobid-------->" + jobid);
                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", Job_id);
                    prefeditor.apply();

                    SharedPreferences pref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("jobid", Job_id);
                    editor.apply();

                    Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("EXIT", true);
                    startActivity(intent);
                    finish();

                } else if (actionnew.equalsIgnoreCase("job_photo_status")) {

                    SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("page", "1");
                    editor.apply();

                    System.out.println("dispaly jobid-------->" + jobid);
                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", Job_id);
                    prefeditor.apply();

                    Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                    startActivity(intent);
                    finish();


                } else if (actionnew.equalsIgnoreCase("job_accepted")) {


                    SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("page", "1");
                    editor.apply();


                    System.out.println("dispaly jobid-------->" + jobid);
                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", Job_id);
                    prefeditor.apply();


                    Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                    startActivity(intent);
                    finish();
                } else if (actionnew.equalsIgnoreCase("start_off")) {

                    SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("page", "1");
                    editor.apply();

                    System.out.println("dispaly jobid-------->" + jobid);
                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", Job_id);
                    prefeditor.apply();


                    Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                    startActivity(intent);
                    finish();
                } else if (actionnew.equalsIgnoreCase("provider_reached")) {

                    SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("page", "1");
                    editor.apply();

                    System.out.println("dispaly jobid-------->" + jobid);
                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", Job_id);
                    prefeditor.apply();

                    Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                    startActivity(intent);
                    finish();
                } else if (actionnew.equalsIgnoreCase("job_started")) {


                    System.out.println("dispaly jobid-------->" + jobid);
                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", Job_id);
                    prefeditor.apply();

                    SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("page", "1");
                    editor.apply();

                    Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                    startActivity(intent);
                    finish();
                } else if (actionnew.equalsIgnoreCase("job_rescheduled")) {


                    SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("page", "1");
                    editor.apply();


                    System.out.println("dispaly jobid-------->" + jobid);
                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", Job_id);
                    prefeditor.apply();

                    Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                    startActivity(intent);
                    finish();
                } else if (actionnew.equalsIgnoreCase("job_completed")) {


                    SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("page", "1");
                    editor.apply();


                    System.out.println("dispaly jobid-------->" + jobid);
                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", Job_id);
                    prefeditor.apply();

                    Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                    startActivity(intent);
                    finish();
                } else if (actionnew.equalsIgnoreCase("change_order_request")) {

                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", Job_id);
                    prefeditor.apply();

                    Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                    startActivity(intent);
                    finish();
                } else if (actionnew.equalsIgnoreCase("estimation_received")) {

                    SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("page", "1");
                    editor.apply();

                    System.out.println("dispaly jobid-------->" + jobid);
                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", Job_id);
                    prefeditor.apply();

                    Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                    startActivity(intent);
                    finish();
                } else if (actionnew.equalsIgnoreCase("chat")) {
                    Intent intent = new Intent(getApplicationContext(), GetMessageChatActivity.class);
                    startActivity(intent);
                    finish();
                } else if (actionnew.equalsIgnoreCase("prepayment_rejected")) {
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("page", "1");
                    editor.apply();

                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("jobid", Job_id);
                    prefeditor.apply();

                    Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
                    startActivity(intent);
                    finish();
                } else if (actionnew.equalsIgnoreCase("subitem_rejected")) {
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("jobid", Job_id);
                    editor.apply();

                    Intent intent = new Intent(getApplicationContext(), EstimateBuilderActivity.class);
                    startActivity(intent);
                }
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }

    @Override
    protected void onStop() {
        super.onStop();
        handler.removeCallbacksAndMessages(null);
    }
}
