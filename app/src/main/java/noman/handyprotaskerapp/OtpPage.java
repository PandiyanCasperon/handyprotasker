package noman.handyprotaskerapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Set;

import Dialog.LoadingDialog;
import Dialog.PkDialog;
import Utils.ConnectionDetector;
import Utils.CurrencySymbolConverter;
import Utils.SessionManager;
import noman.handyprotaskerapp.activity.MyJobs_OnGoingDetailActivity;
import service.ServiceConstant;
import volley.ServiceRequest;


/**
 * Created by user88 on 12/31/2015.
 */
public class OtpPage extends SubClassActivity {

    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;

    private SessionManager session;
    EditText Et_otp;
    Button BT_otp_confirm;
    private LinearLayout checkbox_layout;
    private String Str_receive_amount = "";
    String Str_otp = "", Str_amount = "";
    private String StrjobId = "", provider_id = "",saveimageurltosend="",payment_description="";
    String overallcode, Flow = "", user_type = "";

    private LoadingDialog dialog;
    CheckBox checkedhelp;
    private RelativeLayout layout_back;
    ArrayList<String> PrepaymentList = new ArrayList<>();
    private String is_coming_page="",type = "",postpayment="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_page);
        initialize();

        BT_otp_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String otp = Et_otp.getText().toString();

                if(is_coming_page.equals("change_order")){

                    Intent i=new Intent();
                    i.putExtra("otp",otp);
                    setResult(Activity.RESULT_OK,i);
                    finish();

                } else {


                    if (checkedhelp.isChecked()) {

                        if (type.equalsIgnoreCase("prepayment")){


                            Intent s=new Intent();
                            s.putExtra("otp",overallcode);
                            setResult(Activity.RESULT_OK,s);
                            finish();

                            Intent intent = new Intent(OtpPage.this, ReceiveCashPage.class);
                            intent.putExtra("jobId", StrjobId);
                            intent.putExtra("Amount", Str_receive_amount);
                            intent.putExtra("otp", overallcode);
                            //Newly added for Prepayment flow

                            if (Flow.equals("pre_payment_cash_initiated")) {
                                Set<String> PrepaymentListWithoutDuplicates = new LinkedHashSet<String>(PrepaymentList);
                                PrepaymentList.clear();
                                PrepaymentList.addAll(PrepaymentListWithoutDuplicates);
                                for (int i = 0; i < PrepaymentList.size(); i++) {
                                    intent.putStringArrayListExtra("PrepaymentList", PrepaymentList);
                                    intent.putExtra("Type", Flow);
                                }
                            }
                            intent.putExtra("user_type", user_type);
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        }
                        else {
                            ReceiveCashORCheck(OtpPage.this, ServiceConstant.JOB_CASH_RECEIVED_URL, "NormalUser");


                        }


                    } else {
                        if (Et_otp.getText().toString().length() == 0) {
                            Alert("", getResources().getString(R.string.lbel_otp_code_enter_alert));

                        } else if (!Et_otp.getText().toString().trim().equals(overallcode)) {
                            Alert("", getResources().getString(R.string.lbel_otp_not_valid));

                        } else {

                            Intent s=new Intent();
                            s.putExtra("otp",Et_otp.getText().toString().trim());
                            setResult(Activity.RESULT_OK,s);
                            finish();

                            Intent intent = new Intent(OtpPage.this, ReceiveCashPage.class);
                            intent.putExtra("jobId", StrjobId);
                            intent.putExtra("Amount", Str_receive_amount);
                            intent.putExtra("otp", Et_otp.getText().toString().trim());
                            //Newly added for Prepayment flow
                            if (Flow.equals("pre_payment_cash_initiated")) {
                                Set<String> PrepaymentListWithoutDuplicates = new LinkedHashSet<String>(PrepaymentList);
                                PrepaymentList.clear();
                                PrepaymentList.addAll(PrepaymentListWithoutDuplicates);
                                for (int i = 0; i < PrepaymentList.size(); i++) {
                                    intent.putStringArrayListExtra("PrepaymentList", PrepaymentList);
                                    intent.putExtra("Type", Flow);
                                }
                            }
                            intent.putExtra("user_type", user_type);
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);


                        }
                    }
                }

            }
        });

        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

    }
    private void ReceiveCashORCheck(Context mContext, String url, String UserType) {
        final HashMap<String, String> jsonParams = new HashMap<String, String>();
        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        String provider_ids = pref.getString("provider_id", "");

        jsonParams.put("provider_id", provider_ids);
        jsonParams.put("job_id", StrjobId);
        jsonParams.put("otp",  Et_otp.getText().toString().trim());
        if (Flow.equals("pre_payment_cash_initiated")) {
            Set<String> PrepaymentListWithoutDuplicates = new LinkedHashSet<String>(PrepaymentList);
            PrepaymentList.clear();
            PrepaymentList.addAll(PrepaymentListWithoutDuplicates);
            for (int i = 0; i < PrepaymentList.size(); i++) {
                jsonParams.put("prepayment_id[" + i + "]", PrepaymentList.get(i));
            }
        }

        if (!payment_description.isEmpty()) {
            jsonParams.put("payment_description", payment_description);
        }

        if (UserType.equals("NormalUser")) {
            Bitmap src = BitmapFactory.decodeFile(saveimageurltosend);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            src.compress(Bitmap.CompressFormat.PNG, 100, baos);
            String imgString = Base64.encodeToString(getBytesFromBitmap(src),
                    Base64.NO_WRAP);

            jsonParams.put("image", imgString);
        }

        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {
                System.out.println("received--------" + response);
                Log.e("received", response);

                String Str_Status = "", Str_Response = "";

                try {

                    JSONObject jobject = new JSONObject(response);
                    Str_Status = jobject.getString("status");
                    Str_Response = jobject.getString("response");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (Str_Status.equalsIgnoreCase("1")) {
                    final PkDialog mdialog = new PkDialog(OtpPage.this);
                    mdialog.setDialogTitle(getResources().getString(R.string.action_loading_sucess));
                    mdialog.setDialogMessage(Str_Response);
                    mdialog.setPositiveButton(getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mdialog.dismiss();

                                   /* Intent intent = new Intent(ReceiveCashPage.this, MyJobsActivity.class);
                                    intent.putExtra("status", "complete");
                                    startActivity(intent);
                                    finish();*/

                                    Intent intent = new Intent(OtpPage.this, ReviwesPage.class);
                                    intent.putExtra("jobId", StrjobId);
                                    startActivity(intent);
                                }
                            }
                    );
                    mdialog.show();

                } else if (Str_Status.equalsIgnoreCase("11")) {
                    Intent intent = new Intent(OtpPage.this, ReviwesPage.class);
                    intent.putExtra("jobId", StrjobId);
                    startActivity(intent);
//                    Toast.makeText(OtpPage.this, Str_Response, Toast.LENGTH_SHORT).show();
//                    SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
//                    SharedPreferences.Editor editor = pref.edit();
//                    editor.putString("page", "1");
//                    editor.apply();
//
//                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
//                    SharedPreferences.Editor prefeditor = prefjobid.edit();
//                    prefeditor.putString("jobid", StrjobId);
//                    prefeditor.apply();
//                    prefeditor.commit();
//
//
//                    Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
//                    startActivity(intent);
//                    finish();
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), Str_Response);
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });

    }
    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }
    private void initialize() {
        session = new SessionManager(OtpPage.this);

        HashMap<String, String> user = session.getUserDetails();
        provider_id = user.get(SessionManager.KEY_PROVIDERID);
        Et_otp = (EditText) findViewById(R.id.otp_enter_code);
        BT_otp_confirm = (Button) findViewById(R.id.otp_confirm_btn);
        layout_back = (RelativeLayout) findViewById(R.id.layout_otp_back);
        checkbox_layout=(LinearLayout)findViewById(R.id.checkbox_layout);


        checkedhelp = (CheckBox) findViewById(R.id.checkedhelp);

        Intent i = getIntent();
        StrjobId = i.getStringExtra("jobId");
        saveimageurltosend=i.getStringExtra("saveimageurltosend");
        payment_description=i.getStringExtra("payment_description");
        type = i.getStringExtra("type");

        if(postpayment.equalsIgnoreCase("")){
            postpayment = i.getStringExtra("postpayment");
        }

        if (i.hasExtra("Type") && i.hasExtra("PrepaymentList") && i.getStringExtra("Type").equals("pre_payment_cash_initiated")) {
            Flow = getIntent().getStringExtra("Type");
            PrepaymentList = getIntent().getStringArrayListExtra("PrepaymentList");
        }


         if(i.hasExtra("page")){

             is_coming_page=i.getStringExtra("page");

             if(is_coming_page.equals("change_order")){

                 checkbox_layout.setVisibility(View.GONE);

                 cd = new ConnectionDetector(OtpPage.this);
                 isInternetPresent = cd.isConnectingToInternet();
                 if (isInternetPresent) {
                     dialog = new LoadingDialog(OtpPage.this);
                     dialog.setLoadingTitle(getResources().getString(R.string.loading_in));
                     dialog.show();

                     ChangeOrderPageViaOTPRequest(OtpPage.this, ServiceConstant.Change_order_Otp_Request);

                 } else {

                     Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                 }

             }

         } else {

             cd = new ConnectionDetector(OtpPage.this);
             isInternetPresent = cd.isConnectingToInternet();
             if (isInternetPresent) {
                 dialog = new LoadingDialog(OtpPage.this);
                 dialog.setLoadingTitle(getResources().getString(R.string.loading_in));
                 dialog.show();
                 if (Flow.equals("pre_payment_cash_initiated")) {
                     otpPost(OtpPage.this, ServiceConstant.ReceiveCashInOTP);
                 } else {
                     otpPost(OtpPage.this, ServiceConstant.JOB_RECEIVECSH_OTP_URL);
                     System.out.println("provider/receive-cash" + ServiceConstant.JOB_RECEIVECSH_OTP_URL);
                 }
             } else {

                 Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
             }
         }
        //  Et_otp.setFocusable(false);

    }

    //--------------Alert Method------------------
    private void Alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(OtpPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    //----------------------Post method for Otp------------
    private void otpPost(Context mContext, String url) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        String provider_ids = pref.getString("provider_id", "");
        jsonParams.put("provider_id", provider_ids);
        jsonParams.put("job_id", StrjobId);
        if (Flow.equals("pre_payment_cash_initiated")) {
            Set<String> PrepaymentListWithoutDuplicates = new LinkedHashSet<String>(PrepaymentList);
            PrepaymentList.clear();
            PrepaymentList.addAll(PrepaymentListWithoutDuplicates);
            for (int i = 0; i < PrepaymentList.size(); i++) {
                jsonParams.put("prepayment_id[" + i + "]", PrepaymentList.get(i));
            }
        }

        System.out.println("provider_id------------" + provider_id);
        System.out.println("job_id------------" + StrjobId);


        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                System.out.println("provider/receive-cash--------" + response);

                Log.e("otp", response);

                String Str_status = "", Str_response = "", Str_otp_code = "", Str_currency = "", Str_otp_status = "", Str_jobId = "", Str_message = "";
                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");

                    if (Str_status.equalsIgnoreCase("1")) {

                        JSONObject object = jobject.getJSONObject("response");

                        Str_otp_code = object.getString("otp_string");
                        Str_currency = object.getString("currency");
                        //Currency currencycode = Currency.getInstance(getLocale(Str_currency));
                        String currencyCode = CurrencySymbolConverter.getCurrencySymbol(Str_currency);

                        Str_otp_status = object.getString("otp_status");
                        Str_jobId = object.getString("job_id");
                        Str_receive_amount = currencyCode + object.getString("receive_amount");

                        if (object.has("user_type")){
                            user_type = object.getString("user_type");
                        }

                        System.out.println("otpcode-------" + Str_otp_code);
                        System.out.println("otpstatus-------" + Str_otp_status);
                        System.out.println("jobid-------" + Str_jobId);

                        overallcode = Str_otp_code;


                    } else {
                        Str_response = jobject.getString("response");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                dialog.dismiss();

                if (Str_status.equalsIgnoreCase("1")) {

                    if (Str_otp_status.equalsIgnoreCase("development")) {

                        System.out.println("otpstatus---inside----" + Str_otp_status);
                        System.out.println("otp---inside----" + Str_otp_code);


                        Et_otp.setText(Str_otp_code);
                    }
                } else {
                    //Alert(getResources().getString(R.string.server_lable_header), Str_response);
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    private void ChangeOrderPageViaOTPRequest(Context mContext, String url) {

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("job_id", StrjobId);

        System.out.println("job_id------------" + StrjobId);

        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                System.out.println("provider/receive-cash--------" + response);

                Log.e("otp", response);

                String Str_status = "", Str_response = "", Str_otp_code = "", Str_currency = "", Str_otp_status = "", Str_jobId = "", Str_message = "";
                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");

                    if (Str_status.equalsIgnoreCase("1")) {

                        JSONObject object = jobject.getJSONObject("response");

                        Str_otp_code = object.getString("otp");
                        Str_otp_status = object.getString("otp_status");

                        if(Str_otp_status.equals("development")){

                            Et_otp.setText(Str_otp_code);
                        }

                    } else {

                        Str_response = jobject.getString("response");
                        Alert(getResources().getString(R.string.server_lable_header), Str_response);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    //method to convert currency code to currency symbol
    private static Locale getLocale(String strCode) {

        for (Locale locale : NumberFormat.getAvailableLocales()) {
            String code = NumberFormat.getCurrencyInstance(locale).getCurrency().getCurrencyCode();
            if (strCode.equals(code)) {
                return locale;
            }
        }
        return null;
    }




    @Override
    public void onResume() {
        super.onResume();

       /* if (!socketHandler.getSocketManager().isConnected){
            socketHandler.getSocketManager().connect();
        }*/
    }


}
