package noman.handyprotaskerapp;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.widget.TextView;

import net.bohush.geometricprogressview.GeometricProgressView;


/**
 * Created by CAS63 on 2/16/2018.
 */

public class ProgressDialog1 extends Dialog {
    private GeometricProgressView myProgressView;
    private TextView myMessageTXT;

    public ProgressDialog1(Context aContext) {
        super(aContext);

        try {
            this.requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.setContentView(R.layout.dialog_progress);
            this.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
            myProgressView = (GeometricProgressView) this
                    .findViewById(R.id.dialog_progressView);
            this.setCanceledOnTouchOutside(true); // need to change false in future


        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public void show() {
        try {
            super.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
