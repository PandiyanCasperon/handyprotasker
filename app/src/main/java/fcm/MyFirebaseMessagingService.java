package fcm;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Notification;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Utils.SessionManager;
import noman.handyprotaskerapp.BuildConfig;
import noman.handyprotaskerapp.OtpPage;
import noman.handyprotaskerapp.R;
import noman.handyprotaskerapp.ReviwesPage;
import noman.handyprotaskerapp.Xmpp_PushNotificationPage;
import noman.handyprotaskerapp.activity.EstimateBuilderActivity;
import noman.handyprotaskerapp.activity.GetMessageChatActivity;
import noman.handyprotaskerapp.activity.HelperOngoingPageActivity;
import noman.handyprotaskerapp.activity.MyJobs_OnGoingDetailActivity;
import noman.handyprotaskerapp.activity.WeeklyCalendarActivity;
import noman.handyprotaskerapp.newjobrequesttimer;
import noman.handyprotaskerapp.notificationdb;

/**
 * Created by user145 on 8/4/2017.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {



    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    Notification n;
    notificationdb mHelper;
    SQLiteDatabase dataBase;
    private NotificationUtils notificationUtils;
    private String actionnew;
    private String value;
    ArrayList<String> PrepaymentList;
    private SessionManager mySession;
    private String Job_id = "", Title = BuildConfig.App_Name, Message = "You received new notification", usernamee = "";

    @Override
    public void onNewToken(@NonNull String refreshedToken) {
        super.onNewToken(refreshedToken);

        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);

        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);
    }

    private void storeRegIdInPref(String token) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", token);
        editor.apply();
    }

    public static String getRegistrationId(Context context) {
        final SharedPreferences pref = context.getSharedPreferences(Config.SHARED_PREF, 0);
        String registrationId = pref.getString("regId", "");
        if (registrationId.isEmpty()) {
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("regId", FirebaseInstanceId.getInstance().getToken());
            editor.apply();
            return FirebaseInstanceId.getInstance().getToken();
        }
        return registrationId;
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        notificationUtils = new NotificationUtils(MyFirebaseMessagingService.this);
        Log.e(TAG, "From: " + remoteMessage.getFrom());
        System.out.println("--------FCM Received-------" + remoteMessage);

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());

                SharedPreferences pref = getSharedPreferences("logindetails", 0);
                String provider_id = pref.getString("provider_id", "0");
                Log.d(TAG, "onRunJob: " + provider_id);

                if (provider_id.equals("0")) {
            /*Intent myService = new Intent(this.getContext(), ChatMessageService.class);
            this.getContext().stopService(myService);*/
                } else {
                    handleDataMessage(json);

                }

            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }


    @SuppressLint("WrongConstant")
    private void handleDataMessage(JSONObject json) {
        System.out.println("Sara response--->fcm-" + json.toString());
        value = json.toString();

        SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
        String provider_id = pref.getString("provider_id", "");

        if (provider_id.equalsIgnoreCase("") || provider_id.equalsIgnoreCase(" ") || provider_id.equalsIgnoreCase("0")) {

        } else {
            System.out.println("I am here sara-->" + json.toString());

            System.out.println("Sara response--->fcm-" + value);

            try {
                JSONObject json1 = new JSONObject(json.toString());
                JSONObject object1 = json1.getJSONObject("data");


                if (object1.has("key0")) {
                    actionnew = object1.getString("action");
                    if (object1.has("uniqueId")) {
                        int okl = 0;
                        String uniqueId = object1.getString("uniqueId");
                        System.out.println("Sara response--->fcm-unique id" + uniqueId);
                        mHelper = new notificationdb(this);
                        dataBase = mHelper.getWritableDatabase();
                        Cursor mCursor = dataBase.rawQuery("SELECT * FROM " + mHelper.TABLE_NAME + " WHERE uniqueid ='" + uniqueId + "'", null);

                        if (mCursor.moveToFirst()) {
                            do {
                                okl++;
                                System.out.println("Sara response--->fcm-okl" + okl);
                            } while (mCursor.moveToNext());
                        }

                        if (okl == 0) {
                            System.out.println("Sara response--->fcm-record not exists" + mHelper.getProfilesCount());
                            mHelper = new notificationdb(this);
                            dataBase = mHelper.getWritableDatabase();
                            ContentValues values = new ContentValues();
                            values.put(notificationdb.KEY_FNAME, json.toString());
                            values.put(notificationdb.KEY_LNAME, uniqueId);
                            dataBase.insert(notificationdb.TABLE_NAME, null, values);
                            if (isAppOnForeground(MyFirebaseMessagingService.this)) {
                                CommonNotificationCall();
                            } else {
                                if (object1.has("key0")) {
                                    commonTrayMessageNotification(object1);
                                }
                            }
                        } else {
                            System.out.println("Sara response--->fcm-record exists" + mHelper.getProfilesCount());
                        }
                    } else {
                        if (isAppOnForeground(MyFirebaseMessagingService.this)) {
                            CommonNotificationCall();
                        } else {
                            if (object1.has("key0")) {
                                commonTrayMessageNotification(object1);
                            }
                        }
                    }
                } else {
                    System.out.println("Sara response--->fcm-" + value);
                    String checkdataa = object1.getString("action");
                    if (checkdataa.equalsIgnoreCase("chat")) {
                        Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
                        dialogIntent.putExtra("TITLE_INTENT", value);
                        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(dialogIntent);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void CommonNotificationCall() {
        if (actionnew.equalsIgnoreCase("job_request")) {
            Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("job_request_now")) {
            Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("helper_job_actions")) {
            Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("helper_assigned")) {
            Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("assign_helper")) {
            Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("estimation_notaccepted")) {
            Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("change_order_notaccepted")) {
            Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("admin_notification")) {
            Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("cash_initiated")) {
            Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("pre_payment_cash_initiated")) {
            Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("change_order_accepted")) {
            Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("job_cancelled")) {
            Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("payment_paid")) {
            Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("pre_payment_paid")) {
            Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("estimation_accepted")) {
            Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("job_photo_status")) {
            Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("job_accepted")) {
            Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("start_off")) {
            Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("provider_reached")) {
            Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("job_started")) {
            Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("job_rescheduled")) {
            Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("job_completed")) {
            Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("prepayment_rejected")) {
            Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("subitem_rejected")) {
            Intent dialogIntent = new Intent(getApplicationContext(), Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        }

    }

    private void commonTrayMessageNotification(JSONObject object1) {
        try {
            if (object1.has("key0")) {
                Job_id = object1.getString("key0");
                Message = object1.getString("message");
                actionnew = object1.getString("action");

                if (actionnew.equalsIgnoreCase("job_request") || actionnew.equalsIgnoreCase("change_order_notaccepted") || actionnew.equalsIgnoreCase("change_order_accepted") || actionnew.equalsIgnoreCase("estimation_accepted") || actionnew.equalsIgnoreCase("estimation_notaccepted")) {
                    if (object1.has("key3")) {
                        usernamee = object1.getString("key3");
                    }

                } else if (actionnew.equalsIgnoreCase("job_accepted")) {
                    if (object1.has("key2")) {
                        usernamee = object1.getString("key2");
                    }
                } else if (actionnew.equalsIgnoreCase("job_rescheduled")) {
                    if (object1.has("key3")) {
                        usernamee = object1.getString("key3");
                    }
                } else if (actionnew.equalsIgnoreCase("job_photo_status")) {
                    if (object1.has("key2")) {
                        usernamee = object1.getString("key2");
                    }
                } else if (actionnew.equalsIgnoreCase("job_request_now")) {
                    if (object1.has("key6")) {
                        usernamee = object1.getString("key6");
                    }
                } else if (actionnew.equalsIgnoreCase("cash_initiated")) {
                    if (object1.has("key2")) {
                        usernamee = object1.getString("key2");
                    }
                } else if (actionnew.equalsIgnoreCase("pre_payment_cash_initiated")) {
                    if (object1.has("key2")) {
                        usernamee = object1.getString("key2");
                    }

                    if (object1.has("action") && object1.has("key3")) {
                        if (object1.getString("action").equals("pre_payment_cash_initiated")) {
                            PrepaymentList = new ArrayList<>();
                            JSONArray jsonArray = object1.getJSONArray("key3");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                PrepaymentList.add(jsonArray.get(i).toString());
                            }
                        }
                    }
                } else if (actionnew.equalsIgnoreCase("payment_paid")) {
                    if (object1.has("key2")) {
                        usernamee = object1.getString("key2");
                    }
                } else if (actionnew.equalsIgnoreCase("prepayment_rejected")) {
                    if (object1.has("key2")) {
                        usernamee = object1.getString("key2");
                    }
                } else {
                    usernamee = "";
                }


                if (actionnew.equalsIgnoreCase("admin_notification")) {
                    Title = "ADMIN NOTIFICATION";
                } else {
                    if (actionnew.equalsIgnoreCase("job_request") && object1.has("key4")) {
                        if (object1.getString("key4").equalsIgnoreCase("Job")) {
                            Title = "JOB ID:" + Job_id;
                        } else if (object1.getString("key4").equalsIgnoreCase("Estimate")) {
                            Title = "ESTIMATION ID:" + Job_id;
                        }

                    } else if (actionnew.equalsIgnoreCase("job_accepted") && object1.has("key3")) {
                        if (object1.getString("key3").equalsIgnoreCase("Job")) {
                            Title = "JOB ID:" + Job_id;
                        } else if (object1.getString("key3").equalsIgnoreCase("Estimate")) {
                            Title = "ESTIMATION ID:" + Job_id;
                        }

                    } else {
                        Title = "JOB ID:" + Job_id;
                    }

                }


                SharedPreferences pref = getApplicationContext().getSharedPreferences("logindetails", MODE_PRIVATE);
                String username = pref.getString("provider_name", "");

                if (actionnew.equalsIgnoreCase("job_request")) {
                    if (Message.contains("You have been assigned for a job")) {
                        Message = getResources().getString(R.string.jobre);
                    }
                } else if (actionnew.equalsIgnoreCase("job_accepted")) {
                    if (Message.contains("You are assigned to the Job")) {
                        Message = getResources().getString(R.string.jobre);
                    }
                } else if (actionnew.equalsIgnoreCase("estimation_accepted")) {

                    Message = getResources().getString(R.string.estaccepted);

                } else if (actionnew.equalsIgnoreCase("job_rescheduled")) {
                    Message = getResources().getString(R.string.ree);
                } else if (actionnew.equalsIgnoreCase("estimation_notaccepted")) {

                    Message = getResources().getString(R.string.estrejected);

                } else if (actionnew.equalsIgnoreCase("job_photo_status")) {
                    if (Message.contains("User approved the job")) {
                        Message = getResources().getString(R.string.jobapprove);
                    }


                } else if (actionnew.equalsIgnoreCase("cash_initiated")) {

                    if (Message.contains("User")) {
                        Message = Message.replace("User", "client");
                    }

                } else if (actionnew.equalsIgnoreCase("pre_payment_cash_initiated")) {
                    if (Message.contains("User")) {
                        Message = Message.replace("User", "client");
                    }
                } else if (actionnew.equalsIgnoreCase("prepayment_rejected")) {
                    if (Message.contains("User")) {
                        Message = Message.replace("User", "client");
                    }
                }

                Message = "Hi " + username + "!. " + Message;
//                Textview_alert_header.setText(getResources().getString(R.string.notif_from_jobid) + "\n" + usernamee);


                if (actionnew.equalsIgnoreCase("payment_paid")) {


                    Title = "Well Done " + username + "!";

                    SpannableStringBuilder builders = new SpannableStringBuilder();
                    String red = "Thank You for continuing to build trust in the";
                    SpannableString redSpannable = new SpannableString(red);
                    redSpannable.setSpan(new ForegroundColorSpan(Color.BLACK), 0, red.length(), 0);
                    builders.append(redSpannable);

                    String white = " HANDY";
                    SpannableString whiteSpannable = new SpannableString(white);
                    whiteSpannable.setSpan(new ForegroundColorSpan(Color.BLUE), 0, white.length(), 0);
                    builders.append(whiteSpannable);

                    String blue = "PRO ";
                    SpannableString blueSpannable = new SpannableString(blue);
                    blueSpannable.setSpan(new ForegroundColorSpan(Color.RED), 0, blue.length(), 0);
                    builders.append(blueSpannable);

                    String Craftsman = "Brand.";
                    SpannableString blueSpannableCraftsman = new SpannableString(Craftsman);
                    blueSpannableCraftsman.setSpan(new ForegroundColorSpan(Color.BLACK), 0, blue.length(), 0);
                    builders.append(blueSpannableCraftsman);
                    Message = builders.toString();
                }
                commonTrayNotification(Job_id, Title, Message);


            } else {
                actionnew = object1.getString("action");
                commonTrayNotification(Job_id, getResources().getString(R.string.chat_notif), getResources().getString(R.string.received_one_new_msg));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void commonTrayNotification(String Job_id, String Title, String Message) {
        if (actionnew.equalsIgnoreCase("job_request")) {

            SharedPreferences presf1 = getApplicationContext().getSharedPreferences("needrefresh", 0);
            SharedPreferences.Editor editosr1 = presf1.edit();
            editosr1.putString("yesno", "2");
            editosr1.apply();


            SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("page", "1");
            editor.apply();


            System.out.println("dispaly jobid-------->" + Job_id);
            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("jobid", Job_id);
            prefeditor.apply();

            Intent resultIntent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
            notificationUtils.showNotificationMessage(Title, Message, "10.00", resultIntent);
        } else if (actionnew.equalsIgnoreCase("job_request_now")) {

            Intent resultIntent = new Intent(getApplicationContext(), newjobrequesttimer.class);
            resultIntent.putExtra("valuefortimer", value);
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            notificationUtils.showNotificationMessage(Title, Message, "10.00", resultIntent);
        } else if (actionnew.equalsIgnoreCase("admin_notification")) {
            notificationUtils.showNotificationMessage(Title, Message, "10.00", new Intent());
        } else if (actionnew.equalsIgnoreCase("helper_job_actions")) {

            SharedPreferences presf1 = getApplicationContext().getSharedPreferences("needrefresh", 0);
            SharedPreferences.Editor editosr1 = presf1.edit();
            editosr1.putString("yesno", "2");
            editosr1.apply();


            SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("page", "1");
            editor.apply();


            System.out.println("dispaly jobid-------->" + Job_id);
            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("jobid", Job_id);
            prefeditor.apply();

            notificationUtils.showNotificationMessage(Title, Message, "10.00", new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class));
        } else if (actionnew.equalsIgnoreCase("assign_helper")) {

            SharedPreferences presf1 = getApplicationContext().getSharedPreferences("needrefresh", 0);
            SharedPreferences.Editor editosr1 = presf1.edit();
            editosr1.putString("yesno", "2");
            editosr1.apply();


            SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("page", "1");
            editor.apply();


            System.out.println("dispaly jobid-------->" + Job_id);
            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("jobid", Job_id);
            prefeditor.apply();

            notificationUtils.showNotificationMessage(Title, Message, "10.00", new Intent(getApplicationContext(), HelperOngoingPageActivity.class));
        } else if (actionnew.equalsIgnoreCase("estimation_pending")) {


            SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("page", "1");
            editor.apply();


            System.out.println("dispaly jobid-------->" + Job_id);
            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("jobid", Job_id);
            prefeditor.apply();

            notificationUtils.showNotificationMessage(Title, Message, "10.00", new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class));
        } else if (actionnew.equalsIgnoreCase("estimation_notaccepted")) {
            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("jobid", Job_id);
            prefeditor.apply();

            SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("page", "1");
            editor.apply();

            notificationUtils.showNotificationMessage(Title, Message, "10.00", new Intent(getApplicationContext(), WeeklyCalendarActivity.class));

        } else if (actionnew.equalsIgnoreCase("change_order_notaccepted")) {

            SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("page", "1");
            editor.apply();


            System.out.println("dispaly jobid-------->" + Job_id);
            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("jobid", Job_id);
            prefeditor.apply();

            notificationUtils.showNotificationMessage(Title, Message, "10.00", new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class));

        } else if (actionnew.equalsIgnoreCase("cash_initiated") || actionnew.equalsIgnoreCase("pre_payment_cash_initiated")) {
            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("jobid", Job_id);
            prefeditor.apply();


            Intent intent = new Intent(getApplicationContext(), OtpPage.class);
            intent.putExtra("jobId", Job_id);
            intent.putExtra("Type", actionnew);
            intent.putExtra("type", "prepayment");
            if (actionnew.equalsIgnoreCase("pre_payment_cash_initiated")) {
                intent.putStringArrayListExtra("PrepaymentList", PrepaymentList);
            }

            notificationUtils.showNotificationMessage(Title, Message, "10.00", intent);

        } else if (actionnew.equalsIgnoreCase("helper_assigned")) {
            SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("page", "1");
            editor.apply();


            System.out.println("dispaly jobid-------->" + Job_id);
            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("jobid", Job_id);
            prefeditor.apply();
            notificationUtils.showNotificationMessage(Title, Message, "10.00", new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class));

        } else if (actionnew.equalsIgnoreCase("change_order_accepted")) {
            SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("page", "1");
            editor.apply();


            System.out.println("dispaly jobid-------->" + Job_id);
            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("jobid", Job_id);
            prefeditor.apply();

            notificationUtils.showNotificationMessage(Title, Message, "10.00", new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class));

        } else if (actionnew.equalsIgnoreCase("job_cancelled")) {


            SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("page", "1");
            editor.apply();


            System.out.println("dispaly jobid-------->" + Job_id);
            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("jobid", Job_id);
            prefeditor.apply();

            notificationUtils.showNotificationMessage(Title, Message, "10.00", new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class));

        } else if (actionnew.equalsIgnoreCase("payment_paid")) {
            SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("page", "1");
            editor.apply();


            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("jobid", Job_id);
            prefeditor.apply();


            Intent intent = new Intent(getApplicationContext(), ReviwesPage.class);
            intent.putExtra("jobId", Job_id);
            notificationUtils.showNotificationMessage(Title, Message, "10.00", intent);

        } else if (actionnew.equalsIgnoreCase("pre_payment_paid")) {
            SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("page", "1");
            editor.apply();

            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("jobid", Job_id);
            prefeditor.apply();

            notificationUtils.showNotificationMessage(Title, Message, "10.00", new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class));
        } else if (actionnew.equalsIgnoreCase("estimation_accepted")) {
            System.out.println("dispaly jobid-------->" + Job_id);
            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("jobid", Job_id);
            prefeditor.apply();

            SharedPreferences pref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("jobid", Job_id);
            editor.apply();

            Intent intent = new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("EXIT", true);
            notificationUtils.showNotificationMessage(Title, Message, "10.00", intent);

        } else if (actionnew.equalsIgnoreCase("job_photo_status")) {

            SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("page", "1");
            editor.apply();

            System.out.println("dispaly jobid-------->" + Job_id);
            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("jobid", Job_id);
            prefeditor.apply();

            notificationUtils.showNotificationMessage(Title, Message, "10.00", new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class));


        } else if (actionnew.equalsIgnoreCase("job_accepted")) {


            SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("page", "1");
            editor.apply();


            System.out.println("dispaly jobid-------->" + Job_id);
            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("jobid", Job_id);
            prefeditor.apply();

            notificationUtils.showNotificationMessage(Title, Message, "10.00", new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class));
        } else if (actionnew.equalsIgnoreCase("start_off")) {

            SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("page", "1");
            editor.apply();

            System.out.println("dispaly jobid-------->" + Job_id);
            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("jobid", Job_id);
            prefeditor.apply();

            notificationUtils.showNotificationMessage(Title, Message, "10.00", new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class));
        } else if (actionnew.equalsIgnoreCase("provider_reached")) {

            SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("page", "1");
            editor.apply();

            System.out.println("dispaly jobid-------->" + Job_id);
            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("jobid", Job_id);
            prefeditor.apply();

            notificationUtils.showNotificationMessage(Title, Message, "10.00", new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class));
        } else if (actionnew.equalsIgnoreCase("job_started")) {


            System.out.println("dispaly jobid-------->" + Job_id);
            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("jobid", Job_id);
            prefeditor.apply();

            SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("page", "1");
            editor.apply();

            notificationUtils.showNotificationMessage(Title, Message, "10.00", new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class));
        } else if (actionnew.equalsIgnoreCase("job_rescheduled")) {


            SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("page", "1");
            editor.apply();


            System.out.println("dispaly jobid-------->" + Job_id);
            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("jobid", Job_id);
            prefeditor.apply();

            notificationUtils.showNotificationMessage(Title, Message, "10.00", new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class));
        } else if (actionnew.equalsIgnoreCase("job_completed")) {


            SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("page", "1");
            editor.apply();


            System.out.println("dispaly jobid-------->" + Job_id);
            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("jobid", Job_id);
            prefeditor.apply();

            notificationUtils.showNotificationMessage(Title, Message, "10.00", new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class));
        } else if (actionnew.equalsIgnoreCase("change_order_request")) {

            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("arrivedjobid", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("jobid", Job_id);
            prefeditor.apply();

            notificationUtils.showNotificationMessage(Title, Message, "10.00", new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class));
        } else if (actionnew.equalsIgnoreCase("estimation_received")) {

            SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("page", "1");
            editor.apply();

            System.out.println("dispaly jobid-------->" + Job_id);
            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("jobid", Job_id);
            prefeditor.apply();

            notificationUtils.showNotificationMessage(Title, Message, "10.00", new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class));
        } else if (actionnew.equalsIgnoreCase("chat")) {
            notificationUtils.showNotificationMessage(Title, Message, "10.00", new Intent(getApplicationContext(), GetMessageChatActivity.class));
        } else if (actionnew.equalsIgnoreCase("prepayment_rejected")) {
            SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("page", "1");
            editor.apply();

            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("ongoingpage", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("jobid", Job_id);
            prefeditor.apply();

            notificationUtils.showNotificationMessage(Title, Message, "10.00", new Intent(getApplicationContext(), MyJobs_OnGoingDetailActivity.class));
        } else if (actionnew.equalsIgnoreCase("subitem_rejected")) {
            SharedPreferences pref = getApplicationContext().getSharedPreferences("ChangeOrderActivity", MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("jobid", Job_id);
            editor.apply();
            notificationUtils.showNotificationMessage(Title, Message, "10.00", new Intent(getApplicationContext(), EstimateBuilderActivity.class));
        }
    }

    private boolean isAppOnForeground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        assert activityManager != null;
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }

}