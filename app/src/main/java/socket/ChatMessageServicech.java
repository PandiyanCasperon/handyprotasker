package socket;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import androidx.annotation.Nullable;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import noman.handyprotaskerapp.Pojo.ReceiveMessageEvent;
import noman.handyprotaskerapp.Pojo.SendMessageEvent;

/**
 * Created by user145 on 4/7/2017.
 */
public class ChatMessageServicech extends Service {

    static ChatMessageSocketManager manager;
    static public ChatMessageServicech service;
    private ActiveSocketDispatcher dispatcher;
    public static Context context;
    public static String task_id = "";
    public static String tasker_id = "";
    public static String user_id = "";
    private String mCurrentUserId;
    //  private MessageDBHelper messageDB;
    public static final long INTERVAL=3000;//variable to execute services every 10 second
    private Handler mHandler=new Handler(); // run on another Thread to avoid crash
    private Timer mTimer=null; // timer handling

    public static final long MIN_GET_OFFLINE_MESSAGES_TIME = 60 * 1000; // 60 seconds

    public static boolean isStarted() {
        if (service == null)
            return false;
        return true;
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(SendMessageEvent event) {
        manager.send(event.getMessageObject(), event.getEventName());
    }

    @Override
    public void onCreate() {
        super.onCreate();

        EventBus.getDefault().register(this);

        manager = new ChatMessageSocketManager(this, callBack);
        service = this;
        context = this;
        dispatcher = new ActiveSocketDispatcher();




        if(mTimer!=null)
            mTimer.cancel();
        else
            mTimer=new Timer(); // recreate new timer
        mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(),0,INTERVAL);//

    }



    ChatMessageSocketManager.SocketCallBack callBack = new ChatMessageSocketManager.SocketCallBack() {
        @Override
        public void onSuccessListener(String eventName, Object... response) {
            ReceiveMessageEvent me = new ReceiveMessageEvent();
            me.setEventName(eventName);
            me.setObjectsArray(response);

            switch (eventName) {

                case ChatMessageSocketManager.EVENT_CONNECT:
                    if (!manager.isConnected()) {
                        manager.connect();
                    }
                    Log.e("CHAT MANAGER", "CHAT MANAGER RECONNECTED");
                    break;
                case ChatMessageSocketManager.EVENT_DISCONNECT:
                    manager.connect();
                    break;

                case ChatMessageSocketManager.EVENT_UPDATE_CHAT:
                    Log.e("CHAT MANAGER", response[0].toString());
                   // PushNotification(response[0].toString());
                    break;

                case ChatMessageSocketManager.EVENT_TYPING:
                    break;

                case ChatMessageSocketManager.EVENT_STOP_TYPING:
                    break;

                case ChatMessageSocketManager.EVENT_SINGLE_MESSAGE_STATUS:
                    break;


            }

            dispatcher.addwork(me);
        }
    };

    public class ActiveSocketDispatcher {
        private BlockingQueue<Runnable> dispatchQueue
                = new LinkedBlockingQueue<Runnable>();

        public ActiveSocketDispatcher() {
            Thread mThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        try {
                            dispatchQueue.take().run();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            mThread.start();
        }

        private void addwork(final Object packet) {
            try {
                dispatchQueue.put(new Runnable() {
                    @Override
                    public void run() {
                        EventBus.getDefault().post(packet);
                    }
                });
            } catch (Exception e) {
            }
        }
    }




    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Toast.makeText(this, "SERVICE IS START_STICKY " + intent, Toast.LENGTH_LONG).show();
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        service = null;
        manager.disconnect();
        //Toast.makeText(this, "SERVICE IS DESROYERD ", Toast.LENGTH_LONG).show();
        super.onDestroy();
    }


    private class TimeDisplayTimerTask extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
                @Override
                public void run() {

                    System.out.println("run run");
                    SharedPreferences pref = context.getSharedPreferences("logindetails", 0);
                    mCurrentUserId=pref.getString("provider_id","");
                    manager.connect();


                }
            });
        }
    }

}
