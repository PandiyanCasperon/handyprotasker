package socket;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Messenger;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import noman.handyprotaskerapp.Xmpp_PushNotificationPage;
import noman.handyprotaskerapp.notificationdb;

public class SocketHandler {
    private Context context;
    private SocketManager manager;
    String value;
    private static SocketHandler instance;
    private ArrayList<Messenger> listenerMessenger;
    private String Job_status_message = "",actionnew="";
     public static String user;
    ArrayList<String> PrepaymentList = new ArrayList<>();

     private SocketHandler(Context context) {
        this.context = context;
        this.manager = new SocketManager(callBack,context);
        this.listenerMessenger = new ArrayList<Messenger>();

    }

    public static SocketHandler getInstance(Context context) {
        if (instance == null) {
            instance = new SocketHandler(context);
        }
        return instance;
    }

    public void addChatListener(Messenger messenger) {
        listenerMessenger.add(messenger);
    }

    public SocketManager getSocketManager() {
        SharedPreferences pref = context.getSharedPreferences("logindetails", 0);
        String provider_id=pref.getString("provider_id","");
        manager.setTaskId(provider_id);
        return manager;
    }

    public SocketManager.SocketCallBack callBack = new SocketManager.SocketCallBack() {
        boolean isChat = false;

        @Override
        public void onSuccessListener(Object response) {
            if (response instanceof JSONObject) {

                JSONObject jsonObject = (JSONObject) response;
                value=jsonObject.toString();
                System.out.println("Sara response--->socket-"+jsonObject.toString());

                SharedPreferences pref = context.getSharedPreferences("logindetails", 0);
                String provider_id = pref.getString("provider_id", "");

                if (provider_id.equalsIgnoreCase("") || provider_id.equalsIgnoreCase(" ") || provider_id.equalsIgnoreCase("0")) {

                }
                else
                    {
                        try {
                            JSONObject json1 = new JSONObject(jsonObject.toString());
                            JSONObject object1 = json1.getJSONObject("data");

                            if (object1.has("key0")) {

                                actionnew = object1.getString("action");

                                if (json1.has("uniqueId"))
                                {

                                    notificationdb mHelper;
                                    SQLiteDatabase dataBase;
                                    int okl=0;
                                    String  uniqueId= json1.getString("uniqueId");
                                    System.out.println("Sara response--->socket-unique id"+uniqueId);
                                    mHelper=new notificationdb(context);
                                    dataBase = mHelper.getWritableDatabase();
                                    Cursor mCursor = dataBase.rawQuery("SELECT * FROM "+mHelper.TABLE_NAME+" WHERE uniqueid ='" + uniqueId + "'", null);

                                    if (mCursor.moveToFirst()) {
                                        do {
                                            okl++;
                                            System.out.println("Sara response--->socket-okl"+okl);
                                        } while (mCursor.moveToNext());
                                    }

                                    if (okl==0)
                                    {
                                        System.out.println("Sara response--->socket-record not exists"+mHelper.getProfilesCount());
                                        mHelper=new notificationdb(context);
                                        dataBase=mHelper.getWritableDatabase();
                                        ContentValues values=new ContentValues();
                                        values.put(notificationdb.KEY_FNAME,value);
                                        values.put(notificationdb.KEY_LNAME,uniqueId );
                                        dataBase.insert(notificationdb.TABLE_NAME, null, values);
                                        commonnotification();
                                    }
                                    else
                                    {
                                        System.out.println("Sara response--->socket-record exists"+mHelper.getProfilesCount());
                                    }
                                }


                                else  if (object1.has("uniqueId"))
                                {

                                    notificationdb mHelper;
                                    SQLiteDatabase dataBase;
                                    int okl=0;
                                    String  uniqueId= object1.getString("uniqueId");
                                    System.out.println("Sara response--->socket-unique id"+uniqueId);
                                    mHelper=new notificationdb(context);
                                    dataBase = mHelper.getWritableDatabase();
                                    Cursor mCursor = dataBase.rawQuery("SELECT * FROM "+mHelper.TABLE_NAME+" WHERE uniqueid ='" + uniqueId + "'", null);

                                    if (mCursor.moveToFirst()) {
                                        do {
                                            okl++;
                                            System.out.println("Sara response--->socket-okl"+okl);
                                        } while (mCursor.moveToNext());
                                    }

                                    if (okl==0)
                                    {
                                        System.out.println("Sara response--->socket-record not exists"+mHelper.getProfilesCount());
                                        mHelper=new notificationdb(context);
                                        dataBase=mHelper.getWritableDatabase();
                                        ContentValues values=new ContentValues();
                                        values.put(notificationdb.KEY_FNAME,value);
                                        values.put(notificationdb.KEY_LNAME,uniqueId );
                                        dataBase.insert(notificationdb.TABLE_NAME, null, values);
                                        commonnotification();
                                    }
                                    else
                                    {
                                        System.out.println("Sara response--->socket-record exists"+mHelper.getProfilesCount());
                                    }
                                }
                                else
                                {
                                    commonnotification();
                                }
                            }

                            else
                            {
                                System.out.println("Sara response--->socket-"+value);
                                String  checkdataa = object1.getString("action");
                                if(checkdataa.equalsIgnoreCase("chat"))
                                {
                                    Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
                                    dialogIntent.putExtra("TITLE_INTENT", value);
                                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.startActivity(dialogIntent);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                        }




                }


            }
        }
    };

    private void commonnotification()
    {

        System.out.println("Sara response--->socket-"+value);

        if(actionnew.equalsIgnoreCase("job_request"))
        {
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        }
        else if(actionnew.equalsIgnoreCase("job_request_now"))
        {
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        }
       /* else if(actionnew.equalsIgnoreCase("estimation_pending"))
        {
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        }*/

        else if(actionnew.equalsIgnoreCase("helper_assigned"))
        {
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        }
        else if(actionnew.equalsIgnoreCase("admin_notification"))
        {
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        }
        else if(actionnew.equalsIgnoreCase("helper_job_actions"))
        {
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        }
        else if(actionnew.equalsIgnoreCase("assign_helper"))
        {
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        }
        else if(actionnew.equalsIgnoreCase("estimation_notaccepted"))
        {
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        }
        else if(actionnew.equalsIgnoreCase("change_order_notaccepted"))
        {
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        }
        else if(actionnew.equalsIgnoreCase("cash_initiated"))
        {
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        } else if(actionnew.equalsIgnoreCase("pre_payment_cash_initiated")) {
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        }
        else if(actionnew.equalsIgnoreCase("change_order_accepted"))
        {
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        }
        else if(actionnew.equalsIgnoreCase("job_cancelled"))
        {
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        }
        else if(actionnew.equalsIgnoreCase("payment_paid"))
        {
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        }else if(actionnew.equalsIgnoreCase("pre_payment_paid")){
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        }
        else if(actionnew.equalsIgnoreCase("estimation_accepted"))
        {
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        }
        else if(actionnew.equalsIgnoreCase("job_photo_status"))
        {
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        }
        else if(actionnew.equalsIgnoreCase("job_accepted"))
        {
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        }

        else if(actionnew.equalsIgnoreCase("start_off"))
        {
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        }


        else if(actionnew.equalsIgnoreCase("provider_reached"))
        {
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        }
        else if(actionnew.equalsIgnoreCase("job_started"))
        {
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        }
        else if(actionnew.equalsIgnoreCase("job_rescheduled"))
        {
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        }
        else if(actionnew.equalsIgnoreCase("job_completed"))
        {
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        }else if(actionnew.equalsIgnoreCase("prepayment_rejected")){
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        }else if (actionnew.equalsIgnoreCase("subitem_rejected")){
            Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(dialogIntent);
        }
    }


}
