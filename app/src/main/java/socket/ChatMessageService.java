package socket;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import androidx.annotation.Nullable;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by user145 on 4/7/2017.
 */
public class ChatMessageService extends Service {


    public static Context context;
    public static String task_id = "";
    public static String user_id = "";
    public static final long INTERVAL=4000;//variable to execute services every 10 second
    private Handler mHandler=new Handler(); // run on another Thread to avoid crash
    private Timer mTimer=null; // timer handling
    @Override
    public void onCreate() {
        super.onCreate();

        if(mTimer!=null)
            mTimer.cancel();
        else
            mTimer=new Timer(); // recreate new timer
        mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(),0,INTERVAL);//
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Toast.makeText(this, "SERVICE IS START_STICKY " + intent, Toast.LENGTH_LONG).show();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        try {
            try {
                mTimer.cancel();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } finally {

        }

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    private class TimeDisplayTimerTask extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
                @Override
                public void run() {

                    SharedPreferences pref = ChatMessageService.this.getSharedPreferences("logindetails", MODE_PRIVATE);
                    String provider_id=pref.getString("provider_id","0");

                    System.out.println("provider_id----->"+provider_id);


                    SocketHandler.getInstance(ChatMessageService.this).getSocketManager().connect();


                }
            });
        }
    }

}
