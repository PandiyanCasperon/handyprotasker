package Utils

import Dialog.LoadingDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import id.zelory.compressor.Compressor
import id.zelory.compressor.constraint.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import noman.handyprotaskerapp.R
import java.io.File


class MyCustomCompressClass {
    var dialog: LoadingDialog? = null
    var compressedFile = File("")
    fun constructor(context: Context,
                    imageFile: File) {
        GlobalScope.launch(Dispatchers.Main) {
            dialog = LoadingDialog(context)
            dialog!!.setLoadingTitle(context.resources.getString(R.string.loading_in))
            dialog!!.show()
            val ephemeralKey = async(Dispatchers.IO) { myCompress(context, imageFile) }
            if (ephemeralKey.await().isFile) {
                dialog!!.dismiss()
                compressedFile = ephemeralKey.await()
                val intent = Intent()
                intent.putExtra("Status", "Created")
                intent.action = "com.handypro.files.selected"
                context.sendBroadcast(intent)
            } else {
                val intent = Intent()
                intent.putExtra("Status", "Fail")
                intent.action = "com.handypro.files.selected"
                context.sendBroadcast(intent)
                dialog!!.dismiss()
            }
        }
    }

    fun constructorWithFixedFileName(context: Context,
                                     imageFile: File) {
        GlobalScope.launch(Dispatchers.Main) {
            dialog = LoadingDialog(context)
            dialog!!.setLoadingTitle(context.resources.getString(R.string.loading_in))
            dialog!!.show()
            val ephemeralKey = async(Dispatchers.IO) { myCompressWithFixedFileName(context, imageFile) }
            if (ephemeralKey.await().isFile) {
                dialog!!.dismiss()
                compressedFile = ephemeralKey.await()
                val intent = Intent()
                intent.putExtra("Status", "Created")
                intent.action = "com.handypro.files.selected"
                context.sendBroadcast(intent)
            } else {
                val intent = Intent()
                intent.putExtra("Status", "Fail")
                intent.action = "com.handypro.files.selected"
                context.sendBroadcast(intent)
                dialog!!.dismiss()
            }
        }
    }

    private suspend fun myCompress(context: Context,
                                   imageFile: File): File {
        return GlobalScope.async(Dispatchers.IO) {
            return@async Compressor.compress(context, imageFile) {
                val extension: String = imageFile.absolutePath.substring(imageFile.absolutePath.lastIndexOf("."))
                resolution(640, 480)
                quality(75)
                size(2_097_152) // 2 MB
                destination(File(context.getExternalFilesDir("").toString() + "/Compressed/" + System.currentTimeMillis() + extension))
            }
        }.await()
    }

    private suspend fun myCompressWithFixedFileName(context: Context,
                                   imageFile: File): File {
        return GlobalScope.async(Dispatchers.IO) {
            return@async Compressor.compress(context, imageFile) {
                val extension: String = imageFile.absolutePath.substring(imageFile.absolutePath.lastIndexOf("."))
                resolution(640, 480)
                quality(75)
                format(Bitmap.CompressFormat.WEBP)
                size(2_097_152) // 2 MB
                destination(File(context.getExternalFilesDir("").toString() + "/Compressed/" + imageFile.absolutePath.substring(imageFile.absolutePath.lastIndexOf("/") + 1)))
            }
        }.await()
    }
}