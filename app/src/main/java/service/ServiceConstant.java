package service;



/**
 * Created by Administrator on 10/1/2015.
 */
public interface ServiceConstant {

    /*Since I have implemented the Flavors in app.build.gradle, we can fetch the base URL from there.
    It Mean based on the current Build Type and Build Variant App Base URL will fetch from app.build.gradle*/
    //String URL = "http://192.168.1.35:3009/";// local
    String URL = "https://book.handypro.com/";//live
    //String URL = "https://beta.handypro.com/";
   // String URL = "https://handypro.casperon.co/";//Staging

    String BASE_URL = URL + "mobile/";
    String BASE_URLIMAGE = URL;
    String SOCKET_CHAT_URL = URL + "chat";
    String SOCKET_NOTIFY = URL + "notify";
    String Review_image = URL;
    String EmptyBaseURL = URL;

    int GetSignatureRequestCode = 10102;
    int Change_order_OTP_RequestCode = 11102;

    String Estimateavailability = BASE_URL + "app/show-tasker-schedule";
    String Estimateapproval = BASE_URL + "app/estimation-client-approval";
    String CLIENTAPPROVE = BASE_URL + "app/client-approval";

    String LOGIN_URL = BASE_URL + "provider/login";
    String MYJOBON_LIST_URL = BASE_URL + "provider/jobs-list";

    String OPENJOB_LIST_URL = BASE_URL + "provider/openJobsList";


    String MYJOB_DETAIL_INFORMATION_URL = BASE_URL + "provider/view-job";
    String NEWLEADS_URL = BASE_URL + "provider/new-job";
    String ACCEPT_JOB_URL = BASE_URL + "provider/accept-job";
    String ARRIVED_JOB_URL = BASE_URL + "provider/arrived";

    String STARTSTATUSS = BASE_URL + "provider/updateJob";

    String helperstatusupdate = BASE_URL + "provider/updatehelperJob";


    String STARTOFFJOB_JOB_URL = BASE_URL + "provider/start-off";
    String STARTJOB_URL = BASE_URL + "provider/start-job";
    String JOBCOMPLETE_URL = BASE_URL + "provider/job-completed";
    String BILSEND = BASE_URL + "provider/add-bill";

    String CARDPAYMENT = BASE_URL + "provider/makepaymentbystripe";

    String PRECARDPAYMENT = BASE_URL + "provider/pre_amount/stripe-payment-process";

    String helperupdatestatusurl = BASE_URL + "provider/update_helper_taskstatus";

    String PAYMENT_URL = BASE_URL + "provider/job-more-info";
    String JOB_CANCELL_REASON_URL = BASE_URL + "provider/cancellation-reason";
    String JOB_CANCELLED_URL = BASE_URL + "provider/cancel-job";
    String JOB_RECEIVECSH_OTP_URL = BASE_URL + "provider/receive-cash";
    String REQUEST_PAYMENT_URL = BASE_URL + "provider/request-payment";
    String BALANCECLOSE = BASE_URL + "app/complete_zero_balance";
    String JOB_CASH_RECEIVED_URL = BASE_URL + "provider/cash-received";
    String JOB_CHEQUE_RECEIVED_URL = BASE_URL + "provider/cheque-received";
    String STATISTICS_EARNINGS_STATE_URL = BASE_URL + "provider/earnings-stats";
    String STATISTICS_JOB_STATES_URL = BASE_URL + "provider/jobs-stats";
    String PROFILEINFO_URL = BASE_URL + "provider/provider-info";

    String addestimate = BASE_URL + "provider/addestitems";
    String SENDCLIENTAPPROVAL = BASE_URL + "provider/send-for-client-approval";
    String ESTIMATE_URL = BASE_URL + "provider/get-job-estimation-category";
    String SENDESTIMATE_URL = BASE_URL + "provider/send-estimation-for-client";
    String UPDATE_USER_EMAIL_URL = BASE_URL + "provider/update_user_email";
    String beforephoto = BASE_URL + "provider/job-photo";

    String USERESTIMATE_URL = BASE_URL + "user/get-job-estimation-category";

    String billtypee = BASE_URL + "app/billsource";

    String MYPROFILE_REVIWES_URL = BASE_URL + "provider/provider-rating";
    String GET_BANK_INFO_URL = BASE_URL + "provider/get-banking-info";
    String SAVE_BANK_INFO_URL = BASE_URL + "provider/save-banking-info";
    String REVIWES_GET_URL = BASE_URL + "get-rattings-options";
    String REVIWES_SUBMIT_URL = BASE_URL + "submit-rattings";
    String FORGOT_PASSWORD_URL = BASE_URL + "provider/forgot-password";
    String CHANGE_PASSWORD_URL = BASE_URL + "provider/change-password";

    String Privacy_Polocy = BASE_URL + "app/mobile/privacypolicy";

    String acceptclickurl = BASE_URL + "provider/acceptterms";

    String JOB_OTHER_RECEIVED_URL = BASE_URL + "provider/others-received";


    String Terms_Conditions_Url = BASE_URL + "app/mobile/termsandconditions";

    String myjobs_sortingurl = BASE_URL + "provider/jobs-list";
    String chat_detail_url = BASE_URL + "chat/chathistory";

    String App_Info = BASE_URL + "app/mobile/appinfo";

    String GETMESSAGECHAT_URL = BASE_URL + "app/getmessage";
    //Reviews
    String REVIEW_URL = BASE_URL + "app/get-reviews";

    //Notification URl
    String NOTIFICATION_URL = BASE_URL + "app/notification";

    //Transaction
    String TRANSACTION_URL = BASE_URL + "app/provider-transaction";
    String TRANSACTION_DETAIL_URL = BASE_URL + "app/providerjob-transaction";

    //------------------Xmpp key for Notification
    String Plumbal_partnerAgent = "tasker";
    String Plumbal_partner_Apptype = "android";

    String Distance = "https://maps.googleapis.com/maps/api/distancematrix/json?key=";
    String place_search_url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?types=geocode&key=";
    String GetAddressFrom_LatLong_url = "https://maps.googleapis.com/maps/api/place/details/json?key=";

    //-------------filter--
    String Filter_booking_url = BASE_URL + "provider/recent-list";
    //-----------------------------Notification Process-----------------

    String MODEUPDATE_URL = BASE_URL + "user/notification_mode";

    //----------------Log out-------------------

    String logout_url = BASE_URL + "app/provider/logout";

    //-----------------------Category Edit Add Delet--------------------------------

    String ADD_CATEGORY_DATA = BASE_URL + "tasker/add-category";
    String GET_MAIN_CATEGORY = BASE_URL + "provider/get-maincategory";
    String GET_SUB_CATEGORY = BASE_URL + "provider/get-subcategory";
    String GET_SUB_CATEGORY_DETAILS = BASE_URL + "provider/get-subcategorydetails";
    String UPDATE_CATEGORY = BASE_URL + "tasker/update-category";

    String Aboutus_Url = BASE_URL + "app/mobile/aboutus";

    //API used to get pre-amount details
    String PreRequestDisplay = BASE_URL + "provider/pre_amount";
    //API used to request pre-amount
    String PreRequestRequest = BASE_URL + "provider/pre_amount/request";
    //mobile/user/pre_amount/receiveCash --> API used to get details of Prepayment details in OTP activity
    String ReceiveCashInOTP = BASE_URL + "provider/pre_amount/receiveCash";
    //provider/pre_amount/cashReceived --> API used to send cash received to server.
    String ReceiveCashFinal = BASE_URL + "provider/pre_amount/cashReceived";
    //provider/pre_amount/chequeReceived --> API used to send cheque received to server.
    String ReceiveChequeFinal = BASE_URL + "provider/pre_amount/chequeReceived";
    //mobile/provider/pre_amount/delete-payment --> API used to delete the request which are made from craftsman
    String PreRequestDelete = BASE_URL + "provider/pre_amount/delete-payment";
    //mobile/provider/remove_subitems --> This API is used to delete the craftman created sub items. But we must have at least single sub items.
    String SubItemsDelete = BASE_URL + "provider/remove_subitems";
    //provider/deleteestitems --> This API is ised to delete the Estimation for a single category
    String deletestimate = BASE_URL + "provider/deleteestitems";

    String Change_order_Otp_Request = BASE_URL + "provider/otp-client-approval";

    String ReceiveCreditFinal = BASE_URL + "provider/pre_amount/creditReceived";
    String ReceiveOtherFinal = BASE_URL + "provider/pre_amount/othersReceived";

    String JOB_CREDIT_RECEIVED_URL = BASE_URL + "provider/credit-received";


    //mobile/provider/invoice-received --> This API is used to Receive the payment from commercial user and complete the job.
    //But for the commercial user type we won't collect any Cash or Check. Just we complete the payment by Invoice.
    //At the time of registering the user, we set the type of user.
    //Note: We no need to send any attachment(image) for this call.
    String ReceivePaymentFromCommercialUser = BASE_URL + "provider/invoice-received";

    //mobile/provider/pre_amount/invoiceReceived --> This API call is used to receive the prepayment for commercial user.
    String ReceiveCommercialUserPrepayment = BASE_URL + "provider/pre_amount/invoiceReceived";
    //delete_multi_estitems --> This call is used to delete multiple Estimation
    String DeleteMultiEstimation = BASE_URL + "provider/delete_multi_estitems";
    //mobile/app/estimation_preview_pdf
    String ESTIMATION_PREVIEW = BASE_URL + "app/estimation_preview_pdf";
}
